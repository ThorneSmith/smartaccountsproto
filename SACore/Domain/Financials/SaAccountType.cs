﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SACore.Domain.Financials
{
    [Table("SaAccountType")]
    public partial class SaAccountType : BaseEntity
    {
        public SaAccountType()
        {
        }
        [Key]
        public long Id { get; set; }
        [Required]
        public Guid LegalEntityId { get; set; }
        public string Description { get; set; }
        
    }
}
