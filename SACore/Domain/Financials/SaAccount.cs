﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace SACore.Domain.Financials
{
    [Table("SaAccount")]
    public partial class SaAccount : BaseEntity
    {
        public SaAccount()
        {
            ChildSaAccounts = new HashSet<SaAccount>();
            GeneralLedgerLines = new HashSet<GeneralLedgerLine>();
        }
        [Key]
        public long Id { get; set; }
        [Required]
        public Guid LegalEntityId { get; set; }
        public long SaAccountClassId { get; set; }
        public long? SaAccountSubClassId { get; set; }
        public long? ParentSaAccountId { get; set; }
        public SaAccountTypes SaAccountType { get; set; }
        [Required]
        [StringLength(50)]
        public string SaAccountCode { get; set; }
        [Required]
        [StringLength(200)]
        public string SaAccountName { get; set; }
        [StringLength(200)]
        public string Description { get; set; }
        public bool IsCash { get; set; }
        
        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] RowVersion { get; set; }

   
        public virtual SaAccount ParentSaAccount { get; set; }
        public virtual SaAccountClass SaAccountClass { get; set; }
        public virtual SaAccountSubClass SaAccountSubClass { get; set; }

        public virtual ICollection<SaAccount> ChildSaAccounts { get; set; }
        public virtual ICollection<GeneralLedgerLine> GeneralLedgerLines { get; set; }


        [NotMapped]
        public decimal Balance { get { return GetBalance(); } }
        [NotMapped]
        public string BalanceSide { get; set; }

        public decimal GetBalance()
        {
            decimal balance = 0;

            var dr = from d in GeneralLedgerLines
                     where d.DrCr == TransactionTypes.Dr
                     select d;
            var cr = from c in GeneralLedgerLines
                     where c.DrCr == TransactionTypes.Cr
                     select c;

            decimal drAmount = dr.Sum(d => d.Amount);
            decimal crAmount = cr.Sum(c => c.Amount);

            if (drAmount > crAmount)
            {
                balance = drAmount - crAmount;
                BalanceSide = "Dr";
            }
            else if (crAmount > drAmount)
            {
                balance = crAmount - drAmount;
                BalanceSide = "Cr";
            }
            else
            {
                // Both sides are equal, returns balance = 0
                BalanceSide = string.Empty;
            }

            if (!SaAccountClass.NormalBalance.Equals(BalanceSide) && !string.IsNullOrEmpty(BalanceSide))
                balance = balance * -1;

            return balance;
        }

        public bool HasChildSaAccounts()
        {
            if (ChildSaAccounts != null && ChildSaAccounts.Count > 0)
                return false;
            return true;
        }
    }

}
