﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SACore.Domain.Financials
{
    [Table("SaAccountSubClass")]
    public class SaAccountSubClass : BaseEntity
    {
        public SaAccountSubClass()
        {
            SaAccounts = new HashSet<SaAccount>();
        }

        [Key]
        public long Id { get; set; }
        public Guid LegalEntityId { get; set; }
        public string Name { get; set; }
        public string BeginningRangeForType { get; set; }
        public string EndRangeForType { get; set; }
        
        public string NormalBalance { get; set; }

        public virtual ICollection<SaAccount> SaAccounts { get; set; }
    }
}
