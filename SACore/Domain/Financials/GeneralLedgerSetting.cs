﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using SACore.Domain.MasterTables;

namespace SACore.Domain.Financials
{
    [Table("GeneralLedgerSetting")]
    public partial class GeneralLedgerSettings : BaseEntity
    {
        public GeneralLedgerSettings()
        {
        }
        public Guid LegalEntityId { get; set; }
        public long? CompanyId { get; set; }
        public virtual Company Company { get; set; }

        public long? PayableSaAccountId { get; set; }
        public long? PurchaseDiscountSaAccountId { get; set; }
        public long? GoodsReceiptNoteClearingSaAccountId { get; set; }
        public long? InvoicingDiscountSaAccountId { get; set; }
        public long? ShippingChargeSaAccountId { get; set; }

        public virtual SaAccount PayableSaAccount { get; set; }
        public virtual SaAccount PurchaseDiscountSaAccount { get; set; }
        public virtual SaAccount GoodsReceiptNoteClearingSaAccount { get; set; }
        public virtual SaAccount InvoicingDiscountSaAccount { get; set; }
        public virtual SaAccount ShippingChargeSaAccount { get; set; } 

    }
}
