﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SACore.Domain
{
    [Table("SequenceNumber")]
    public partial class SequenceNumber : BaseEntity
    {
        public SequenceNumberTypes SequenceNumberType { get; set; }
        public string Description { get; set; }
        public string Prefix { get; set; }
        public int NextNumber { get; set; }
        public bool UsePrefix { get; set; }

        //public string CreatedBy { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public string ModifiedBy { get; set; }
        //public DateTime ModifiedOn { get; set; }
    }


}
