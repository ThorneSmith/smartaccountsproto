﻿namespace SACore.Domain
{
    public enum DocumentTypes
    {
        InvoicingQuote = 1,
        InvoicingOrder,
        InvoicingDelivery,
        InvoicingInvoice,
        InvoicingReceipt,
        InvoicingDebitMemo,
        InvoicingCreditMemo,
        PurchaseOrder,
        PurchaseReceipt,
        PurchaseInvoice,
        PurchaseDebitMemo,
        PurchaseCreditMemo,
        PurchaseInvoicePayment,
        JournalEntry,
        CustomerAllocation
    }

    public enum SaAccountTypes
    {
        Posting = 1,
        Heading,
        Total,
        BeginTotal,
        EndTotal
    }

    public enum TransactionTypes
    {
        Dr = 1,
        Cr = 2
    }

    public enum PartyTypes
    {
        Customer = 1,
        Vendor = 2,
        Contact = 3
    }

    public enum JournalVoucherTypes
    {
        CashDisbursement = 1
    }

    public enum PurchaseStatuses
    {
        Open,
        PartiallyReceived,
        FullReceived,
        Invoiced,
        Closed
    }

    public enum PurchaseInvoiceStatuses
    {
        Open,
        Paid
    }

    public enum SequenceNumberTypes
    {
        InvoicingQuote = 1,
        InvoicingOrder,
        InvoicingDelivery,
        InvoicingInvoice,
        InvoicingReceipt,
        PurchaseOrder,
        PurchaseReceipt,
        PurchaseInvoice,
        VendorPayment,
        JournalEntry,
        Item,
        Customer,
        Vendor,
        Contact
    }

    public enum AddressTypes
    {
        Office,
        Home
    }

    public enum ContactTypes
    {
        Customer = 1,
        Vendor = 2,
        Company = 3
    }

    public enum ItemTypes
    {
        Manufactured = 1,
        Purchased,
        Service,
        Charge
    }

    public enum PaymentTypes
    {
        Prepaymnet = 1,
        Cash,
        AfterNoOfDays,
        DayInTheFollowingMonth
    }

    public enum BankTypes
    {
        CheckingSaAccount = 1,
        SavingsSaAccount,
        CashSaAccount
    }

    public enum InvoicingInvoiceStatus
    {
        //Draft,
        Open,       
        Overdue,
        Closed,
        Void
    }
    public enum SaAccountingType
    {
        Cash = 1,
        ModifiedCash = 2,
        Accrual = 3
    }

    public enum ProfitModel
    {
        ForProfit = 1,
        NonProfit = 2
    }
}
