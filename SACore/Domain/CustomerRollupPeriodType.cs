﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;



namespace SACore.Domain
{
    [Table("CustomerRollupPeriodType")]
    public partial class CustomerRollupPeriodType : BaseEntity
    {
        public CustomerRollupPeriodType()
        {
        }


        public string CustomerRollupPeriodTypeDescription { get; set; }
        public long CustomerRollupPeriodDivisor { get; set; }


        public long CustomerId { get; private set; }

    }
}