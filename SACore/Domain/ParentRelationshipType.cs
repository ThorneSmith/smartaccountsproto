﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;


namespace SACore.Domain
{
    [Table("ParentRelationshipType")]
    public partial class ParentRelationshipType 
    {
        public ParentRelationshipType()
        {
        }
        public long ParentRelationshipTypeId { get; set; }
        public string ParentRelationshipTypeName { get; set; }

    }
}