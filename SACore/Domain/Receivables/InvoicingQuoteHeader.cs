﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using SACore.Domain.MasterTables;

namespace SACore.Domain.Receivables
{
    [Table("InvoicingQuoteHeader")]
    public partial class InvoicingQuoteHeader : BaseEntity
    {
        public long CustomerId { get; set; }
        public DateTime Date { get; set; }

        //public string CreatedBy { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public string ModifiedBy { get; set; }
        //public DateTime ModifiedOn { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
