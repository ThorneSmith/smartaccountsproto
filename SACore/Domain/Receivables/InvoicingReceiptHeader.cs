﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using SACore.Domain.Financials;
using SACore.Domain.MasterTables;

namespace SACore.Domain.Receivables
{
    [Table("InvoicingReceiptHeader")]
    public partial class InvoicingReceiptHeader : BaseEntity
    {
        public InvoicingReceiptHeader()
        {
            InvoicingReceiptLines = new HashSet<InvoicingReceiptLine>();
            CustomerAllocations = new HashSet<CustomerAllocation>();
        }

        public long CustomerId { get; set; }
        public long? GeneralLedgerHeaderId { get; set; }
        public long? SaAccountToDebitId { get; set; }
        //public long? InvoicingInvoiceHeaderId { get; set; }
        public string No { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual GeneralLedgerHeader GeneralLedgerHeader { get; set; }
        public virtual SaAccount SaAccountToDebit { get; set; }
        //public virtual InvoicingInvoiceHeader InvoicingInvoiceHeader { get; set; }

        public virtual ICollection<InvoicingReceiptLine> InvoicingReceiptLines { get; set; }
        public virtual ICollection<CustomerAllocation> CustomerAllocations { get; set; }

        [NotMapped]
        public decimal AvailableAmountToAllocate { get { return GetAvailableAmountToAllocate(); } }
        private decimal GetAvailableAmountToAllocate()
        {
            return InvoicingReceiptLines.Sum(a => a.AmountPaid) - CustomerAllocations.Sum(a => a.Amount);
        }
    }
}
