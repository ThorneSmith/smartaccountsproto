﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SACore.Domain.Receivables
{
    [Table("InvoicingQuoteLine")]
    public partial class InvoicingQuoteLine : BaseEntity
    {
        public long InvoicingQuoteHeaderId { get; set; }

        //public string CreatedBy { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public string ModifiedBy { get; set; }
        //public DateTime ModifiedOn { get; set; }

        public InvoicingQuoteHeader InvoicingQuoteHeader { get; set; }
    }
}
