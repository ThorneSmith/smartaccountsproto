﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using SACore.Domain.Financials;
using SACore.Domain.MasterTables;
using SACore.Domain.Receivables;

namespace SACore.Domain.Receivables
{
    [Table("InvoicingInvoiceHeader")]
    public partial class InvoicingInvoiceHeader : BaseEntity
    {
        public InvoicingInvoiceHeader()
        {
            InvoicingInvoiceLines = new HashSet<InvoicingInvoiceLine>();
            InvoicingReceipts = new HashSet<InvoicingReceiptHeader>();
            CustomerAllocations = new HashSet<CustomerAllocation>();
        }

        public long CustomerId { get; set; }
        public long? GeneralLedgerHeaderId { get; set; }
        public long? InvoicingDeliveryHeaderId { get; set; }
        public string No { get; set; }
        public DateTime Date { get; set; }
        public decimal ShippingHandlingCharge{ get; set; }
        public InvoicingInvoiceStatus Status { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual GeneralLedgerHeader GeneralLedgerHeader { get; set; }
        public virtual InvoicingDeliveryHeader InvoicingDeliveryHeader { get; set; }

        public virtual ICollection<InvoicingInvoiceLine> InvoicingInvoiceLines { get; set; }
        [NotMapped]
        public virtual ICollection<InvoicingReceiptHeader> InvoicingReceipts { get; set; }
        public virtual ICollection<CustomerAllocation> CustomerAllocations { get; set; }

        public decimal ComputeTotalTax()
        {
            decimal totalTax = 0;
            return totalTax;
        }

        public decimal ComputeTotalDiscount()
        {
            decimal totalDiscount = 0;
            return totalDiscount;
        }

        public bool IsFullPaid()
        {
            decimal totalInvoiceAmount = InvoicingInvoiceLines.Sum(a => a.Amount);
            decimal totalPaidAmount = 0;
            decimal totalAllocation = CustomerAllocations.Sum(a => a.Amount);
            foreach (var line in InvoicingInvoiceLines)
            {
                totalPaidAmount += line.GetAmountPaid();
            }
            return (totalPaidAmount + totalAllocation) >= totalInvoiceAmount;
        }

        public decimal ComputeTotalAmount()
        {
            decimal totalInvoiceAmount = 0;
            foreach (var line in InvoicingInvoiceLines)
            {
                totalInvoiceAmount += line.Quantity * line.Amount;
            }
            return totalInvoiceAmount;
        }
    }
}
