﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using SACore.Domain.Financials;
using SACore.Domain.MasterTables;

namespace SACore.Domain.Receivables
{
    [Table("InvoicingDeliveryHeader")]
    public partial class InvoicingDeliveryHeader : BaseEntity
    {
        public InvoicingDeliveryHeader()
        {
            InvoicingDeliveryLines = new HashSet<InvoicingDeliveryLine>();
        }

        public long? PaymentTermId { get; set; }
        public long? CustomerId { get; set; }
        public long? GeneralLedgerHeaderId { get; set; }
        public long? InvoicingOrderHeaderId { get; set; }
        public string No { get; set; }
        public DateTime Date { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual PaymentTerm PaymentTerm { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual GeneralLedgerHeader GeneralLedgerHeader { get; set; }
        public virtual InvoicingOrderHeader InvoicingOrderHeader { get; set; }

        public virtual ICollection<InvoicingDeliveryLine> InvoicingDeliveryLines { get; set; }
    }
}
