﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using SACore.Domain.Items;

namespace SACore.Domain.Receivables
{
    [Table("InvoicingDeliveryLine")]
    public partial class InvoicingDeliveryLine : BaseEntity
    {
        public long InvoicingDeliveryHeaderId { get; set; }
        public long? ItemId { get; set; }
        public long? MeasurementId { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Discount { get; set; }

        public virtual InvoicingDeliveryHeader InvoicingDeliveryHeader { get; set; }
        public virtual Item Item { get; set; }
        public virtual Measurement Measurement { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        public decimal GetPriceAfterTax()
        {
            decimal priceAfterTax = 0;
            return priceAfterTax;
        }
    }
}
