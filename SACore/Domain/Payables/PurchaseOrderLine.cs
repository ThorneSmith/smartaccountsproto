﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using SACore.Domain.Items;

namespace SACore.Domain.Payables
{
    [Table("PurchaseOrderLine")]
    public partial class PurchaseOrderLine : BaseEntity
    {
        public PurchaseOrderLine()
        {
            PurchaseReceiptLines = new HashSet<PurchaseReceiptLine>();
        }

        public long PurchaseOrderHeaderId { get; set; }
        public long ItemId { get; set; }
        public long MeasurementId { get; set; }
        public decimal Quantity { get; set; }
        public decimal Cost { get; set; }
        public decimal Discount { get; set; }
        public decimal Amount { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual PurchaseOrderHeader PurhcaseOrderHeader { get; set; }
        public virtual Item Item { get; set; }
        public virtual Measurement Measurement { get; set; }

        public virtual ICollection<PurchaseReceiptLine> PurchaseReceiptLines { get; set; }

        public decimal? GetReceivedQuantity()
        {
            decimal? qty = 0;
            foreach (var stock in PurchaseReceiptLines)
            {
                qty += stock.InventoryControlJournal.INQty;
            }
            return qty;
        }

        public bool IsCompleted()
        {
            bool completed = false;
            decimal totalReceiptAmount = PurchaseReceiptLines.Sum(d => d.Amount);
            if (totalReceiptAmount == Amount)
                completed = true;
            return completed;
        }
    }
}
