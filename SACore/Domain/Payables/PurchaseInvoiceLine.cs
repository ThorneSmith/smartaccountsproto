﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using SACore.Domain.Items;
using SACore.Domain.MasterTables;

namespace SACore.Domain.Payables
{
    [Table("PurchaseInvoiceLine")]
    public partial class PurchaseInvoiceLine : BaseEntity
    {
        public long PurchaseInvoiceHeaderId { get; set; }
        public long ItemId { get; set; }
        public long? TaxId { get; set; }
        public long MeasurementId { get; set; }
        public long? InventoryControlJournalId { get; set; }
        public decimal Quantity { get; set; }
        public decimal? ReceivedQuantity { get; set; }
        public decimal? Cost { get; set; }
        public decimal? Discount { get; set; }
        public decimal Amount { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual PurchaseInvoiceHeader PurchaseInvoiceHeader { get; set; }
        public virtual Item Item { get; set; }
        public virtual Measurement Measurement { get; set; }
        public virtual Tax Tax { get; set; }
        public virtual InventoryControlJournal InventoryControlJournal { get; set; }

        [NotMapped]
        public decimal LineTaxAmount { get { return ComputeLineTaxAmount(); } }

        private decimal ComputeLineTaxAmount()
        {
            decimal? lineTaxAmount = 0;
            foreach(var tax in Item.ItemTaxGroup.ItemTaxGroupTax)
            {
                lineTaxAmount += (tax.Tax.Rate / (Quantity * Cost)) * 100;
            }
            return lineTaxAmount.Value;
        }
    }
}
