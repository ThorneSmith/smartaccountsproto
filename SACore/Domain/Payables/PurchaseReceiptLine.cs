﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using SACore.Domain.Items;
using SACore.Domain.MasterTables;

namespace SACore.Domain.Payables
{
    [Table("PurchaseReceiptLine")]
    public partial class PurchaseReceiptLine : BaseEntity
    {
        public long PurchaseReceiptHeaderId { get; set; }
        public long ItemId { get; set; }
        public long? TaxId { get; set; }
        public long? InventoryControlJournalId { get; set; }
        public long? PurchaseOrderLineId { get; set; }
        public long MeasurementId { get; set; }
        public decimal Quantity { get; set; }
        public decimal ReceivedQuantity { get; set; }
        public decimal Cost { get; set; }
        public decimal Discount { get; set; }
        public decimal Amount { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual PurchaseReceiptHeader PurchaseReceiptHeader { get; set; }
        public virtual Item Item { get; set; }
        public virtual Measurement Measurement { get; set; }
        public virtual Tax Tax { get; set; }
        public virtual InventoryControlJournal InventoryControlJournal { get; set; }
        public virtual PurchaseOrderLine PurchaseOrderLine { get; set; }

        [NotMapped]
        public decimal LineTaxAmount { get { return Tax != null ? (Tax.Rate * Amount) : 0; } }
    }
}
