﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using SACore.Domain.Financials;
using SACore.Domain.MasterTables;

namespace SACore.Domain.Payables
{

    [Table("VendorObligation")]
    public partial class VendorObligation : BaseEntity
    {        
        public VendorObligation()
        { }

        public long VendorId { get; set; }
        public DateTime DueDate { get; set; }
        public decimal Amount { get; set; }
        public long VendorObligationStatus  { get; set; }


        public virtual Vendor Vendor { get; set; }

    }
}
