﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using SACore.Domain.Financials;

namespace SACore.Domain.Items
{
    [Table("ItemCategory")]
    public partial class ItemCategory : BaseEntity
    {
        public ItemCategory()
        {
            Items = new HashSet<Item>();
        }        
        
        public ItemTypes ItemType { get; set; }
        public int? MeasurementId { get; set; }        
        public int? InvoicingSaAccountId { get; set; }
        public int? InventorySaAccountId { get; set; }
        public int? CostOfGoodsSoldSaAccountId { get; set; }
        public int? AdjustmentSaAccountId { get; set; }
        public int? AssemblySaAccountId { get; set; }
        public string Name { get; set; }
        public Guid LegalEntityId { get; set; }

        //public string CreatedBy { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public string ModifiedBy { get; set; }
        //public DateTime ModifiedOn { get; set; }

        public virtual Measurement Measurement { get; set; }
        public virtual SaAccount InvoicingSaAccount { get; set; }
        public virtual SaAccount InventorySaAccount { get; set; }
        public virtual SaAccount CostOfGoodsSoldSaAccount { get; set; }
        public virtual SaAccount AdjustmentSaAccount { get; set; }
        public virtual SaAccount AssemblySaAccount { get; set; }

        public virtual ICollection<Item> Items { get; set; }
    }
}
