﻿
using System.ComponentModel.DataAnnotations;

namespace SACore.Domain
{
    public abstract partial class BaseEntity
    {
        /// <summary>
        /// Gets or sets the entity identifier
        /// </summary>
      [Key]
      public long Id { get; set; }
    }
}
