﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using SACore.Domain.Financials;
using SACore.Domain.MasterTables;
using SACore.Domain.Payables;

namespace SACore.Domain.MasterTables
{
    [Table("Vendor")]
    public partial class Vendor : Party
    {
        public Vendor()
        {
            LegalEntitys = new HashSet<LegalEntity>();
            PurchaseOrders = new HashSet<PurchaseOrderHeader>();
            PurchaseReceipts = new HashSet<PurchaseReceiptHeader>();
            PurchaseInvoices = new HashSet<PurchaseInvoiceHeader>();
            VendorPayments = new HashSet<VendorPayment>();
            VendorObligations = new HashSet<VendorObligation>();
            //Contacts = new HashSet<Contact>();
        }

        public string No { get; set; }
        public Guid LegalEntityId { get; set; }
        public long? AccountsPayableSaAccountId { get; set; }
        public long? PurchaseSaAccountId { get; set; }
        public long? PurchaseDiscountSaAccountId { get; set; }
        public long? PrimaryContactId { get; set; }
        public long? PaymentTermId { get; set; }


        public virtual Party Party { get; set; }
        public virtual LegalEntity LegalEntity { get; set; }
        public virtual SaAccount AccountsPayableSaAccount { get; set; }
        public virtual SaAccount PurchaseSaAccount { get; set; }
        public virtual SaAccount PurchaseDiscountSaAccount { get; set; }
        public virtual Contact PrimaryContact { get; set; }
        public virtual PaymentTerm PaymentTerm { get; set; }

        public virtual ICollection<LegalEntity> LegalEntitys { get; set; }
        public virtual ICollection<PurchaseOrderHeader> PurchaseOrders { get; set; }
        public virtual ICollection<PurchaseReceiptHeader> PurchaseReceipts { get; set; }
        public virtual ICollection<PurchaseInvoiceHeader> PurchaseInvoices { get; set; }
        public virtual ICollection<VendorPayment> VendorPayments { get; set; }
        public virtual ICollection<VendorObligation> VendorObligations { get; set; }
        //public virtual ICollection<Contact> Contacts { get; set; }

        public decimal GetBalance()
        {
            decimal balance = 0;
            decimal totalInvoiceAmount = 0;
            decimal totalInvoicePayment = 0;

            foreach (var invoice in PurchaseInvoices)
                totalInvoiceAmount += invoice.PurchaseInvoiceLines.Sum(a => a.Amount);

            foreach (var payment in VendorPayments)
                totalInvoicePayment += payment.Amount;

            balance = totalInvoiceAmount - totalInvoicePayment;
            return balance;
        }
    }
}
