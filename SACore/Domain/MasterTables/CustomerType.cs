﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SACore.Domain.MasterTables
{
    [Table("CustomerType")]
    public class CustomerType : BaseEntity
    {
        public static readonly CustomerType EmptyObject = new CustomerType();

        public CustomerType()
        { }
        public CustomerType(string name)
        {
            Name = name;
        }

        [Required]
        public string Name { get; set; }



    }
}
