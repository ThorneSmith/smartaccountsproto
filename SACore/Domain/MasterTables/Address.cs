﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SACore.Domain.MasterTables
{
    [Table("Address")]
    public partial class Address : BaseEntity
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }



    }
}
