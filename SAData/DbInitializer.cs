﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Runtime.InteropServices;
using System.Transactions;
using Microsoft.VisualBasic.FileIO;
using SACore.Domain;
using SACore.Domain.Financials;
using SACore.Domain.Items;
using SACore.Domain.MasterTables;
using SACore.Domain.Payables;
using SACore.Domain.Receivables;

namespace SAData
{
    public class DbInitializer<TContext> : IDatabaseInitializer<TContext> where TContext : ApplicationContext, new()
    {
        protected ApplicationContext _context;
        protected const string Sql =
            "if (select DB_ID('{0}')) is not null "
            + "begin "
            + "alter database [{0}] set offline with rollback immediate; "
            + "alter database [{0}] set online; "
            + "drop database [{0}]; "
            + "end";

        public virtual void InitializeDatabase(TContext context)
        {
            if (DbExists(context))
            {
                //if (context.Database.CompatibleWithModel(true))
                    return;
                //DropDatabase(context);
            }
            context.Database.Create();
            Seed(context);
            context.SaveChanges();
        }

        protected virtual bool DbExists(TContext context)
        {
            using (new TransactionScope(TransactionScopeOption.Suppress))
            {
                return context.Database.Exists();
            }
        }

        protected virtual void DropDatabase(TContext context)
        {
            context.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, string.Format(Sql, context.Database.Connection.Database));
        }

        protected virtual void Seed(TContext context)
        {
            _context = context;
            InitData();
        }

        private void InitData()
        {

            InitMenuDetail();
            InitMenuMaster();

            InitSaAccounts();
            InitParty();
            InitDataTypeType();
            InitDataType();
            //InitTax();
            //InitItems();

            InitLegalEntityType();
            InitParentRelationshipType();
            InitLegalEntities();
            InitCustomerRollupPeriodType();
            InitCustomerRollupPeriodHeader();
            InitCustomerEntitySaAccountRollUp();
            InitGeneralLedgerSetting();
            InitPaymentTerms();
            InitBanks();
            InitFiscalYear();

            InitContact();
            InitCompany();


            InitGeneralLedgerHeader();
            InitGeneralLedgerLine();

            InitJournalEntryHeader();
            InitJournalEntryLine();
            InitVendor();
            InitCustomer();
        }

        private void InitMenuDetail()
        {
            // var InitValue = new MenuDetail() { Menu_ChildID = "Root", MenuName = "Home", MenuDisplayTxt = "Shanu Home", MenuFileName = "Index", MenuURL = "Home", UserID = "Shanu" }; _context.MenuDetails.Add(InitValue);

            //InitValue = new MenuDetail() { Menu_ChildID = "Home", MenuName = "Home", MenuDisplayTxt = "Shanu Home", MenuFileName = "Index", MenuURL = "Home", UserID = "Shanu"                         }; _context.MenuDetails.Add(InitValue);
            //InitValue = new MenuDetail() { Menu_ChildID = "About", MenuName = "About", MenuDisplayTxt = "About Shanu", MenuFileName = "About", MenuURL = "Home", UserID = "Shanu"                      }; _context.MenuDetails.Add(InitValue);
            //InitValue = new MenuDetail() { Menu_ChildID = "Contact", MenuName = "Contact", MenuDisplayTxt = "Contact Shanu", MenuFileName = "Contact", MenuURL = "Home", UserID = "Shanu"              }; _context.MenuDetails.Add(InitValue);
            //InitValue = new MenuDetail() { Menu_ChildID = "Masters", MenuName = "Masters", MenuDisplayTxt = "Masters", MenuFileName = "ItemDetails", MenuURL = "Masters", UserID = "Shanu"             }; _context.MenuDetails.Add(InitValue);
            //InitValue = new MenuDetail() { Menu_ChildID = "ITM001", MenuName = "ItemMaster", MenuDisplayTxt = "Item Master", MenuFileName = "ItemDetails", MenuURL = "Masters", UserID = "Shanu"       }; _context.MenuDetails.Add(InitValue);
            //InitValue = new MenuDetail() { Menu_ChildID = "ITM002", MenuName = "ItemDetail", MenuDisplayTxt = "Item Details", MenuFileName = "ItemDetails", MenuURL = "Masters", UserID = "Shanu"      }; _context.MenuDetails.Add(InitValue);
            //InitValue = new MenuDetail() { Menu_ChildID = "ITM003", MenuName = "ItemManage", MenuDisplayTxt = "Item Manage", MenuFileName = "ItemManage", MenuURL = "Masters", UserID = "Shanu"        }; _context.MenuDetails.Add(InitValue);
            //InitValue = new MenuDetail() { Menu_ChildID = "CAT001", MenuName = "CatMaster", MenuDisplayTxt = "Category Masters", MenuFileName = "CATDetails", MenuURL = "Masters", UserID = "Shanu"    }; _context.MenuDetails.Add(InitValue);
            //InitValue = new MenuDetail() { Menu_ChildID = "CAT002", MenuName = "CATDetail", MenuDisplayTxt = "Category Details", MenuFileName = "CATDetails", MenuURL = "Masters", UserID = "Shanu"    }; _context.MenuDetails.Add(InitValue);
            //InitValue = new MenuDetail() { Menu_ChildID = "CAT003", MenuName = "CATManage", MenuDisplayTxt = "Category Manage", MenuFileName = "CATManage", MenuURL = "Masters", UserID = "Shanu"      }; _context.MenuDetails.Add(InitValue);
            //InitValue = new MenuDetail() { Menu_ChildID = "Inventory", MenuName = "Inventory", MenuDisplayTxt = "Inventory", MenuFileName = "Index", MenuURL = "Inventory", UserID = "Shanu"           }; _context.MenuDetails.Add(InitValue);
            //InitValue = new MenuDetail() { Menu_ChildID = "INV001", MenuName = "Inventory", MenuDisplayTxt = "Inventory Details", MenuFileName = "Index", MenuURL = "Inventory", UserID = "Shanu"      }; _context.MenuDetails.Add(InitValue);
            //InitValue = new MenuDetail() { Menu_ChildID = "FG001", MenuName = "FGIN", MenuDisplayTxt = "FG Receipt", MenuFileName = "FGIN", MenuURL = "Inventory", UserID = "Shanu"                    }; _context.MenuDetails.Add(InitValue);
            //InitValue = new MenuDetail() { Menu_ChildID = "FG002", MenuName = "FGOUT", MenuDisplayTxt = "FG Issue", MenuFileName = "FGOUT", MenuURL = "Inventory", UserID = "Shanu"                    }; _context.MenuDetails.Add(InitValue);

            //_context.SaveChanges();

        }

        private void InitMenuMaster()
        {
            //    var InitValue = new MenuMaster() { Menu_ID = 1, Menu_RootID = "Root", Menu_ChildID = "Home", UserID = "Shanu" }; _context.MenuMasters.Add(InitValue);

            //        InitValue = new MenuMaster(){ Menu_ID = 1,Menu_RootID = "Home"       ,Menu_ChildID = "About",    UserID = "Shanu"}; _context.MenuMasters.Add(InitValue);
            //        InitValue = new MenuMaster(){ Menu_ID = 1,Menu_RootID = "Home"       ,Menu_ChildID = "Contact",  UserID = "Shanu"}; _context.MenuMasters.Add(InitValue);
            //        InitValue = new MenuMaster(){ Menu_ID = 1,Menu_RootID = "Root"       ,Menu_ChildID = "Masters",  UserID = "Shanu"}; _context.MenuMasters.Add(InitValue);
            //        InitValue = new MenuMaster(){ Menu_ID = 1,Menu_RootID = "Masters"    ,Menu_ChildID = "ITM001",   UserID = "Shanu"}; _context.MenuMasters.Add(InitValue);
            //        InitValue = new MenuMaster(){ Menu_ID = 1,Menu_RootID = "ITM001"     ,Menu_ChildID = "ITM002",   UserID = "Shanu"}; _context.MenuMasters.Add(InitValue);
            //        InitValue = new MenuMaster(){ Menu_ID = 1,Menu_RootID = "ITM001"     ,Menu_ChildID = "ITM003",   UserID = "Shanu"}; _context.MenuMasters.Add(InitValue);
            //        InitValue = new MenuMaster(){ Menu_ID = 1,Menu_RootID = "Masters"    ,Menu_ChildID = "CAT001",   UserID = "Shanu"}; _context.MenuMasters.Add(InitValue);
            //        InitValue = new MenuMaster(){ Menu_ID = 1,Menu_RootID = "CAT001"     ,Menu_ChildID = "CAT001",   UserID = "Shanu"}; _context.MenuMasters.Add(InitValue);
            //        InitValue = new MenuMaster(){ Menu_ID = 1,Menu_RootID = "CAT001"     ,Menu_ChildID = "CAT002",   UserID = "Shanu"}; _context.MenuMasters.Add(InitValue);
            //        InitValue = new MenuMaster(){ Menu_ID = 1,Menu_RootID = "CAT001"     ,Menu_ChildID = "CAT003",   UserID = "Shanu"}; _context.MenuMasters.Add(InitValue);
            //        InitValue = new MenuMaster(){ Menu_ID = 1,Menu_RootID = "Root"       ,Menu_ChildID = "Inventory",UserID = "Shanu"}; _context.MenuMasters.Add(InitValue);
            //        InitValue = new MenuMaster(){ Menu_ID = 1,Menu_RootID = "Inventory"  ,Menu_ChildID = "INV001",   UserID = "Shanu"}; _context.MenuMasters.Add(InitValue);
            //        InitValue = new MenuMaster(){ Menu_ID = 1,Menu_RootID = "INV001"     ,Menu_ChildID = "FG001",    UserID = "Shanu"}; _context.MenuMasters.Add(InitValue);
            //        InitValue = new MenuMaster(){ Menu_ID = 1,Menu_RootID = "INV001"     ,Menu_ChildID = "FG002",    UserID = "Shanu"}; _context.MenuMasters.Add(InitValue);
            //        _context.SaveChanges();

        }

        private void InitJournalEntryLine()
        {
            var InitValue = new JournalEntryLine()
            {
                Id = 1,
                JournalEntryHeaderId = 1,
                SaAccountId = 47,
                DrCr = SACore.Domain.TransactionTypes.Cr,
                Amount = Convert.ToDecimal(100.00),
                Memo = "Test Data 1",



            };
            _context.JournalEntryLines.Add(InitValue);


            InitValue = new JournalEntryLine()
            {
                Id = 2,
                JournalEntryHeaderId = 1,
                SaAccountId = 47,
                DrCr = SACore.Domain.TransactionTypes.Dr,
                Amount = Convert.ToDecimal(100.00),
                Memo = "Test Data 1",



            };
            _context.JournalEntryLines.Add(InitValue);


            InitValue = new JournalEntryLine()
            {
                Id = 3,
                JournalEntryHeaderId = 2,
                SaAccountId = 6,
                DrCr = SACore.Domain.TransactionTypes.Cr,
                Amount = Convert.ToDecimal(500.00),
                Memo = "Test Data 1",



            };
            _context.JournalEntryLines.Add(InitValue);


            InitValue = new JournalEntryLine()
            {
                Id = 4,
                JournalEntryHeaderId = 2,
                SaAccountId = 58,
                DrCr = SACore.Domain.TransactionTypes.Dr,
                Amount = Convert.ToDecimal(500.00),
                Memo = "Test Data 1",



            };
            _context.JournalEntryLines.Add(InitValue);

            _context.SaveChanges();

        }

        private void InitJournalEntryHeader()
        {
            var InitValue = new JournalEntryHeader()
            {
                Id = 1,
                LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"),

                GeneralLedgerHeaderId = 1,
                PartyId = null,
                VoucherType = null,
                Date = DateTime.Now,
                Memo = "Test Data",
                ReferenceNo = "1",

            };
            _context.JournalEntryHeaders.Add(InitValue);

            InitValue = new JournalEntryHeader()
            {
                Id = 2,
                LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"),

                GeneralLedgerHeaderId = 2,
                PartyId = null,
                VoucherType = null,
                Date = DateTime.Now,
                Memo = "Test Data",
                ReferenceNo = "2",

            };
            _context.JournalEntryHeaders.Add(InitValue);
            _context.SaveChanges();
        }

        private void InitGeneralLedgerLine()
        {
            var InitValue = new GeneralLedgerLine()
            {
                Id = 1,
                GeneralLedgerHeaderId = 1,
                SaAccountId = 47,
                DrCr = SACore.Domain.TransactionTypes.Cr,
                Amount = Convert.ToDecimal(100.00),



            };
            _context.GeneralLedgerLines.Add(InitValue);


            InitValue = new GeneralLedgerLine()
            {
                Id = 2,
                GeneralLedgerHeaderId = 1,
                SaAccountId = 47,
                DrCr = SACore.Domain.TransactionTypes.Dr,
                Amount = Convert.ToDecimal(100.00),



            };
            _context.GeneralLedgerLines.Add(InitValue);


            InitValue = new GeneralLedgerLine()
            {
                Id = 3,
                GeneralLedgerHeaderId = 2,
                SaAccountId = 6,
                DrCr = SACore.Domain.TransactionTypes.Cr,
                Amount = Convert.ToDecimal(500.00),



            };
            _context.GeneralLedgerLines.Add(InitValue);


            InitValue = new GeneralLedgerLine()
            {
                Id = 4,
                GeneralLedgerHeaderId = 2,
                SaAccountId = 58,
                DrCr = SACore.Domain.TransactionTypes.Dr,
                Amount = Convert.ToDecimal(500.00),



            };
            _context.GeneralLedgerLines.Add(InitValue);

            _context.SaveChanges();
        }

        private void InitGeneralLedgerHeader()
        {
            var InitValue = new GeneralLedgerHeader()
            {
                Id = 1,
                LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"),
                Date = DateTime.Now,
                DocumentType = SACore.Domain.DocumentTypes.JournalEntry,
                Description = "2",


            };
            _context.GeneralLedgerHeaders.Add(InitValue);

            InitValue = new GeneralLedgerHeader()
            {
                Id = 2,
                LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"),
                Date = DateTime.Now,
                DocumentType = SACore.Domain.DocumentTypes.JournalEntry,
                Description = "3",


            };
            _context.GeneralLedgerHeaders.Add(InitValue);
            _context.SaveChanges();

        }

        private void InitContact()
        {
            var InitValue = new Contact()
            {
                Id = 1,
                ContactType = ContactTypes.Customer,
                //PartyId = 1,
                LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"),
                FirstName = "Bob",
                LastName = "Boberman",
                MiddleName = "Bob",

            };
            _context.Contacts.Add(InitValue);

            InitValue = new Contact()
            {
                Id = 2,
                ContactType = ContactTypes.Customer,
                //PartyId = 2,
                LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"),
                FirstName = "Susan",
                LastName = "Boberman",
                MiddleName = "Susan",

            };
            _context.Contacts.Add(InitValue);


            InitValue = new Contact()
            {
                Id = 3,
                ContactType = ContactTypes.Customer,
                //PartyId = 3,
                LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"),
                FirstName = "Don",
                LastName = "Boberman",
                MiddleName = "Don",





            };
            _context.Contacts.Add(InitValue);

            InitValue = new Contact()
            {
                Id = 4,
                ContactType = ContactTypes.Customer,
                //PartyId = 4,
                LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"),
                FirstName = "Bill",
                LastName = "Boberman",
                MiddleName = "Don",
            };
            _context.Contacts.Add(InitValue);

            _context.SaveChanges();
        }

        private void InitParty()
        {
            var InitValue = new Party()
            {
                Id = 1,
                PartyType = PartyTypes.Contact,
                Email = "bob@boberman.com",
                Name = "Bob Boberman",
                IsActive = true,



            };
            _context.Parties.Add(InitValue);

            InitValue = new Party()
            {
                Id = 2,
                PartyType = PartyTypes.Contact,
                Email = "Susan@boberman.com",
                Name = "Susan Boberman",
                IsActive = true,




            };
            _context.Parties.Add(InitValue);

            InitValue = new Party()
            {
                Id = 3,
                PartyType = PartyTypes.Contact,
                Email = "Ben@boberman.com",
                Name = "Ben Boberman",
                IsActive = true,




            };
            _context.Parties.Add(InitValue);
            _context.SaveChanges();
        }


        private void InitEntity2Entity()
        {

            var E2E = new Entity2Entity()
            {
                ParentGuid = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34c1"),
                ChildGuid = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"),
                ParentRelationshipType = 1,

            };

            _context.Entity2Entities.Add(E2E);
            _context.SaveChanges();
        }



        private void InitLegalEntityType()
        {
            //var LET = new LegalEntityType()
            //{
            //    LegalEntityTypeId = 1,      // LegalEntityId]
            //    LegalEntityTypeName = "Management Company",

            //};
            //_context.LegalEntityTypes.Add(LET);

            //LET = new LegalEntityType()
            //{
            //    LegalEntityTypeId = 2,      // LegalEntityId]
            //    LegalEntityTypeName = "Home Owner Association",

            //};
            //_context.LegalEntityTypes.Add(LET);
            //_context.SaveChanges();
        }

        private void InitParentRelationshipType()
        {
            var PRT = new ParentRelationshipType()
            {
                ParentRelationshipTypeId = 1,      // LegalEntityId]
                ParentRelationshipTypeName = "Owned Parent",


            };
            _context.ParentRelationshipTypes.Add(PRT);

            PRT = new ParentRelationshipType()
            {
                ParentRelationshipTypeId = 2,      // LegalEntityId]
                ParentRelationshipTypeName = "Managed by Contract",


            };
            _context.ParentRelationshipTypes.Add(PRT);
            _context.SaveChanges();
        }

        private void InitLegalEntities()
        {
            var LE = new LegalEntity()
            {
                LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"),
                LegalEntityName = "HOA Management Co.",
                LegalEntityParentId = null,
                ParentRelationshipType = 0,
                LegalEntityType = 1,

                InActive = false,


            };
            _context.LegalEntities.Add(LE);

            LE = new LegalEntity()
            {
                LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34c1"),
                LegalEntityName = "Small HOA",
                LegalEntityParentId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"),
                ParentRelationshipType = 1,
                InActive = false,


            };
            _context.LegalEntities.Add(LE);
            _context.SaveChanges();
        }

        private void InitCustomerRollupPeriodType()
        {
            var LERollupPeriodType = new CustomerRollupPeriodType()
            {

                CustomerRollupPeriodTypeDescription = "Annual",
                CustomerRollupPeriodDivisor = 1,


            };
            _context.CustomerRollupPeriodTypes.Add(LERollupPeriodType);

            LERollupPeriodType = new CustomerRollupPeriodType()
            {

                CustomerRollupPeriodTypeDescription = "Semi-Annual",
                CustomerRollupPeriodDivisor = 2,

            };
            _context.CustomerRollupPeriodTypes.Add(LERollupPeriodType);
            LERollupPeriodType = new CustomerRollupPeriodType()
            {

                CustomerRollupPeriodTypeDescription = "Quarterly",
                CustomerRollupPeriodDivisor = 4,


            };
            _context.CustomerRollupPeriodTypes.Add(LERollupPeriodType);
            LERollupPeriodType = new CustomerRollupPeriodType()
            {

                CustomerRollupPeriodTypeDescription = "BI-Monthly",
                CustomerRollupPeriodDivisor = 6,


            };
            _context.CustomerRollupPeriodTypes.Add(LERollupPeriodType);

            LERollupPeriodType = new CustomerRollupPeriodType()
            {

                CustomerRollupPeriodTypeDescription = "Monthly",
                CustomerRollupPeriodDivisor = 12,

            };
            _context.CustomerRollupPeriodTypes.Add(LERollupPeriodType);
            _context.SaveChanges();
        }

        private void InitCustomerRollupPeriodHeader()
        {
            var CustomerRollupPeriodHeader = new CustomerRollupPeriodHeader()
            {

                CustomerRollupPeriodHeaderCustomerId = 1,
                CustomerRollupPeriodHeaderTypeId = 1,
                ParentRelationshipType = 0,


            };
            _context.CustomerRollupPeriodHeaders.Add(CustomerRollupPeriodHeader);

            CustomerRollupPeriodHeader = new CustomerRollupPeriodHeader()
            {
                CustomerRollupPeriodHeaderCustomerId = 2,
                CustomerRollupPeriodHeaderTypeId = 1,
                ParentRelationshipType = 5,


            };
            _context.CustomerRollupPeriodHeaders.Add(CustomerRollupPeriodHeader);

            _context.SaveChanges();
        }


        private void InitCustomerEntitySaAccountRollUp()
        {
            var CustomerSaAccountRollUp = new CustomerSaAccountRollUp()
            {

                SourceCustomerId = new Guid("43a476f7-21fe-45f5-8521-ab27580fa1a2"),
                TargetCustomerParentId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"),
                SourceSaAccountId = 1001,
                TargetSaAccountId = 1,


            };
            _context.CustomerSaAccountRollUps.Add(CustomerSaAccountRollUp);

            CustomerSaAccountRollUp = new CustomerSaAccountRollUp()
            {

                SourceCustomerId = new Guid("43a476f7-21fe-45f5-8521-ab27580fa1a2"),
                TargetCustomerParentId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"),
                SourceSaAccountId = 1030,
                TargetSaAccountId = 30,

            };
            _context.CustomerSaAccountRollUps.Add(CustomerSaAccountRollUp);

            _context.SaveChanges();
        }



        private void InitUser()
        {
            //var result = await UserManager.CreateAsync("admin", "P@ssw0rd!");
        }

        private void InitBanks()
        {
            var bank = new Bank()
            {
                SaAccountId = _context.SaAccounts.Where(a => a.SaAccountCode == "10111").FirstOrDefault().Id,
                Name = "General Fund",
                Type = BankTypes.CheckingSaAccount,
                BankName = "GFB",
                Number = "1234567890",
                Address = "123 Main St.",
                IsDefault = true,
                IsActive = true,

            };
            _context.Banks.Add(bank);

            bank = new Bank()
            {
                SaAccountId = _context.SaAccounts.Where(a => a.SaAccountCode == "10113").FirstOrDefault().Id,
                Name = "Petty Cash SaAccount",
                Type = BankTypes.CashSaAccount,
                IsDefault = false,
                IsActive = true,

            };
            _context.Banks.Add(bank);
            _context.SaveChanges();
        }

        private void InitPaymentTerms()
        {
            _context.PaymentTerms.Add(new PaymentTerm()
            {
                Description = "Payment due within 10 days",
                PaymentType = PaymentTypes.AfterNoOfDays,
                DueAfterDays = 10,
                IsActive = true,


            });
            _context.PaymentTerms.Add(new PaymentTerm()
            {
                Description = "Due 15th Of the Following Month 	",
                PaymentType = PaymentTypes.DayInTheFollowingMonth,
                DueAfterDays = 15,
                IsActive = true,


            });
            _context.PaymentTerms.Add(new PaymentTerm()
            {
                Description = "Cash Only",
                PaymentType = PaymentTypes.Cash,
                IsActive = true,


            });
            _context.SaveChanges();
        }

        private void InitDataTypeType()
        {
            var dataTypeType = new DataTypeType() { DataType = "DocumentTypes", DataTypeDescription = "Document Types" }; _context.DataTypeType.Add(dataTypeType);
            dataTypeType = new DataTypeType() { DataType = "SaAccountTypes", DataTypeDescription = "SaAccount Types" }; _context.DataTypeType.Add(dataTypeType);
            dataTypeType = new DataTypeType() { DataType = "TransactionTypes", DataTypeDescription = "Transaction Types" }; _context.DataTypeType.Add(dataTypeType);
            dataTypeType = new DataTypeType() { DataType = "PartyTypes", DataTypeDescription = "Party Types" }; _context.DataTypeType.Add(dataTypeType);
            dataTypeType = new DataTypeType() { DataType = "JournalVoucherTypes", DataTypeDescription = "Journal Voucher Types" }; _context.DataTypeType.Add(dataTypeType);
            dataTypeType = new DataTypeType() { DataType = "PurchaseStatuses", DataTypeDescription = "Purchase Statuses" }; _context.DataTypeType.Add(dataTypeType);
            dataTypeType = new DataTypeType() { DataType = "PurchaseInvoiceStatuses", DataTypeDescription = "Purchase Invoice Statuses" }; _context.DataTypeType.Add(dataTypeType);
            dataTypeType = new DataTypeType() { DataType = "SequenceNumberTypes", DataTypeDescription = "SequenceNumberTypes" }; _context.DataTypeType.Add(dataTypeType);
            dataTypeType = new DataTypeType() { DataType = "AddressTypes", DataTypeDescription = "Address Types" }; _context.DataTypeType.Add(dataTypeType);
            dataTypeType = new DataTypeType() { DataType = "ContactTypes", DataTypeDescription = "Contact Types" }; _context.DataTypeType.Add(dataTypeType);
            dataTypeType = new DataTypeType() { DataType = "ItemTypes", DataTypeDescription = "Item Types" }; _context.DataTypeType.Add(dataTypeType);
            dataTypeType = new DataTypeType() { DataType = "PaymentTypes", DataTypeDescription = "Payment Types" }; _context.DataTypeType.Add(dataTypeType);
            dataTypeType = new DataTypeType() { DataType = "BankTypes", DataTypeDescription = "Bank Types" }; _context.DataTypeType.Add(dataTypeType);
            dataTypeType = new DataTypeType() { DataType = "InvoicingInvoiceStatus", DataTypeDescription = "Invoicing Invoice Status" }; _context.DataTypeType.Add(dataTypeType);
            dataTypeType = new DataTypeType() { DataType = "SaAccountingType", DataTypeDescription = "SaAccounting Type" }; _context.DataTypeType.Add(dataTypeType);
            dataTypeType = new DataTypeType() { DataType = "ProfitModel", DataTypeDescription = "Profit Model" }; _context.DataTypeType.Add(dataTypeType);
            dataTypeType = new DataTypeType() { DataType = "LegalEntity", DataTypeDescription = "Type of Legal Entity" }; _context.DataTypeType.Add(dataTypeType);
            dataTypeType = new DataTypeType() { DataType = "OrganizationalUnitType", DataTypeDescription = "Type of Organizational Unit" }; _context.DataTypeType.Add(dataTypeType);

            _context.SaveChanges();
        }

        private void InitDataType()
        {

            //DocumentTypes
            var dataType = new DataType() { Type = "InvoicingQuote", TypeValue = 1, Description = "Invoicing Quote", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 1 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "InvoicingOrder", TypeValue = 2, Description = "Invoicing Order", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 1 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "InvoicingDelivery", TypeValue = 3, Description = "Invoicing Delivery", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 1 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "InvoicingInvoice", TypeValue = 4, Description = "Invoicing Invoice", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 1 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "InvoicingReceipt", TypeValue = 5, Description = "Invoicing Receipt", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 1 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "InvoicingDebitMemo", TypeValue = 6, Description = "Invoicing Debit Memo", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 1 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "InvoicingCreditMemo", TypeValue = 7, Description = "Invoicing Credit Memo", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 1 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "PurchaseOrder", TypeValue = 8, Description = "Purchase Order", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 1 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "PurchaseReceipt", TypeValue = 9, Description = "Purchase Receipt", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 1 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "PurchaseInvoice", TypeValue = 10, Description = "Purchase Invoice", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 1 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "PurchaseDebitMemo", TypeValue = 11, Description = "Purchase Debi tMemo", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 1 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "PurchaseCreditMemo", TypeValue = 12, Description = "Purchase Credi tMemo", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 1 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "PurchaseInvoicePayment", TypeValue = 13, Description = "Purchase Invoice Payment", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 1 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "JournalEntry", TypeValue = 14, Description = "Journal Entry", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 1 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "CustomerAllocation", TypeValue = 15, Description = "Customer Allocation", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 1 }; _context.DataType.Add(dataType);

            //SaAccountTypes
            dataType = new DataType() { Type = "Posting", TypeValue = 1, Description = "Posting", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 2 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Heading", TypeValue = 2, Description = "Heading", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 2 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Total", TypeValue = 3, Description = "Total", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 2 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "BeginTotal", TypeValue = 4, Description = "BeginTotal", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 2 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "EndTotal", TypeValue = 5, Description = "EndTotal", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 2 }; _context.DataType.Add(dataType);

            //TransactionTypes
            dataType = new DataType() { Type = "Dr", TypeValue = 1, Description = "Dr", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 3 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Cr", TypeValue = 2, Description = "Cr", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 3 }; _context.DataType.Add(dataType);

            //PartyTypes
            dataType = new DataType() { Type = "Customer", TypeValue = 1, Description = "Customer", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 4 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Vendor", TypeValue = 2, Description = "Vendor", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 4 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Contact", TypeValue = 3, Description = "Contact", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 4 }; _context.DataType.Add(dataType);

            //JournalVoucherTypes
            dataType = new DataType() { Type = "CashDisbursement", TypeValue = 1, Description = "Cash Disbursement", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 5 }; _context.DataType.Add(dataType);


            //PurchaseStatuses
            dataType = new DataType() { Type = "Open", TypeValue = 1, Description = "Open", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 6 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "PartiallyReceived", TypeValue = 2, Description = "Partially Received", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 6 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "FullReceived", TypeValue = 3, Description = "Full Received", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 6 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Invoiced", TypeValue = 4, Description = "Invoiced", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 6 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Closed", TypeValue = 5, Description = "Closed", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 6 }; _context.DataType.Add(dataType);


            //PurchaseInvoiceStatuses
            dataType = new DataType() { Type = "Open", TypeValue = 1, Description = "Open", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 7 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Paid", TypeValue = 2, Description = "Paid", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 7 }; _context.DataType.Add(dataType);

            //SequenceNumberTypes
            dataType = new DataType() { Type = "InvoicingQuote", TypeValue = 1, Description = "Invoicing Quote", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 8 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "InvoicingOrder", TypeValue = 2, Description = "Invoicing Order", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 8 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "InvoicingDelivery", TypeValue = 3, Description = "Invoicing Delivery", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 8 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "InvoicingInvoice", TypeValue = 4, Description = "Invoicing Invoice", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 8 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "InvoicingReceipt", TypeValue = 5, Description = "Invoicing Receipt", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 8 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "PurchaseOrder", TypeValue = 6, Description = "Purchase Order", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 8 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "PurchaseReceipt", TypeValue = 7, Description = "Purchase Receipt", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 8 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "PurchaseInvoice", TypeValue = 8, Description = "Purchase Invoice", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 8 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "VendorPayment", TypeValue = 9, Description = "Vendor Payment", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 8 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "JournalEntry", TypeValue = 10, Description = "Journa lEntry", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 8 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Item", TypeValue = 11, Description = "Item", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 8 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Customer", TypeValue = 12, Description = "Customer", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 8 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Vendor", TypeValue = 13, Description = "Vendor", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 8 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Contact", TypeValue = 14, Description = "Contact", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 8 }; _context.DataType.Add(dataType);

            //AddressTypes
            dataType = new DataType() { Type = "Office", TypeValue = 1, Description = "Office", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 9 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Home", TypeValue = 2, Description = "Home", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 9 }; _context.DataType.Add(dataType);


            //ContactTypes
            dataType = new DataType() { Type = "Customer", TypeValue = 1, Description = "Customer", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 10 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Vendor", TypeValue = 2, Description = "Vendor", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 10 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Company", TypeValue = 3, Description = "Company", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 10 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Tenant", TypeValue = 4, Description = "Tenant", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 10 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "PropertyOwner", TypeValue = 5, Description = "PropertyOwner", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 10 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "PropertyManager", TypeValue = 6, Description = "PropertyManager", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 10 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "LegalAgent", TypeValue = 7, Description = "LegalAgent", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 10 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Agent", TypeValue = 8, Description = "Agent", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 10 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Individual", TypeValue = 9, Description = "Individual", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 10 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "LandDeveloper", TypeValue = 10, Description = "LandDeveloper", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 10 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Trustee", TypeValue = 11, Description = "Trustee", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 10 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "PowerOfAttorney", TypeValue = 12, Description = "PowerOfAttorney", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 10 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Builder", TypeValue = 13, Description = "Builder", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 10 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Owner-Voting", TypeValue = 14, Description = "Owner-Voting", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 10 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Owner-Non-Voting", TypeValue = 15, Description = "Owner-Non-Voting", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 10 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "UnitManagingEntity", TypeValue = 16, Description = "UnitManagingEntity", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 10 }; _context.DataType.Add(dataType);

            //ItemTypes
            dataType = new DataType() { Type = "Manufactured", TypeValue = 1, Description = "Manufactured", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 11 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Purchased", TypeValue = 2, Description = "Purchased", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 11 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Service", TypeValue = 3, Description = "Service", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 11 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Charge", TypeValue = 4, Description = "Charge", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 11 }; _context.DataType.Add(dataType);

            //PaymentTypes
            dataType = new DataType() { Type = "Prepaymnet", TypeValue = 1, Description = "Prepaymnet", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 12 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Cash", TypeValue = 2, Description = "Cash", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 12 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "AfterNoOfDays", TypeValue = 3, Description = "After No Of Days", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 12 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "DayInTheFollowingMonth", TypeValue = 4, Description = "Day In The Followin gMonth", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 12 }; _context.DataType.Add(dataType);

            //BankTypes
            dataType = new DataType() { Type = "CheckingSaAccount", TypeValue = 1, Description = "Checking SaAccount", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 13 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "SavingsSaAccount", TypeValue = 2, Description = "Savings SaAccount", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 13 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "CashSaAccount", TypeValue = 3, Description = "Cash SaAccount", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 13 }; _context.DataType.Add(dataType);


            //InvoicingInvoiceStatus
            dataType = new DataType() { Type = "Open", TypeValue = 1, Description = "Open", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 14 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Overdue", TypeValue = 2, Description = "Overdue", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 14 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Closed", TypeValue = 3, Description = "Closed", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 14 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Void", TypeValue = 4, Description = "Void", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 14 }; _context.DataType.Add(dataType);


            //SaAccountingType
            dataType = new DataType() { Type = "Cash", TypeValue = 1, Description = "Cash", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 15 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "ModifiedCash", TypeValue = 2, Description = "Modified Cash", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 15 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Accrual", TypeValue = 3, Description = "Accrual", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 15 }; _context.DataType.Add(dataType);

            //ProfitModel
            dataType = new DataType() { Type = "ForProfit", TypeValue = 1, Description = "For Profit", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 16 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "NonProfit", TypeValue = 2, Description = "Non Profit", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 16 }; _context.DataType.Add(dataType);

            // Legal Entity Types
            dataType = new DataType() { Type = "ManagementCompany", TypeValue = 1, Description = "Management Company", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 17 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "HomeOwnerAssociation", TypeValue = 2, Description = "HomeOwnerAssociation", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 17 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "BuildingAssociation", TypeValue = 3, Description = "Building Association", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 17 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "CondoAssociation", TypeValue = 4, Description = "Condo Association", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 17 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "WaterAssociation", TypeValue = 5, Description = "Water Association", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 17 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "DockAssociation", TypeValue = 6, Description = "Dock Association", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 17 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Vendor", TypeValue = 7, Description = "Vendor", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 17 }; _context.DataType.Add(dataType);

            //OrganizationalUnit Types
            dataType = new DataType() { Type = "Dwelling", TypeValue = 1, Description = "Dwelling", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 18 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Condo", TypeValue = 2, Description = "Dwelling", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 18 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Boat Dock", TypeValue = 3, Description = "Dwelling", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 18 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Dwelling", TypeValue = 4, Description = "Dwelling", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 18 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "Amennity", TypeValue = 5, Description = "Amennity", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 18 }; _context.DataType.Add(dataType);
            dataType = new DataType() { Type = "CommonArea", TypeValue = 6, Description = "CommonArea", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), TypeId = 18 }; _context.DataType.Add(dataType);


            _context.SaveChanges();
        }


        private void InitCompany()
        {
            var company = new Company()
            {
                Name = "HomeAssociation Managment Inc.",
                ShortName = "HAMI",
                ParentId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"),
                CompanyType = 1,



            };

            _context.Companies.Add(company);

            company = new Company()
            {
                Name = "Smaller Home Owner Association Management Inc.",
                ShortName = "SHOA",
                ParentId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"),
                CompanyType = 1,


            };

            _context.Companies.Add(company);

            company = new Company()
            {
                Name = "Home Owner Association Inc.",
                ShortName = "SHOA",
                ParentId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"),
                CompanyType = 2,

            };

            _context.Companies.Add(company);



            _context.SaveChanges();
        }

        private void InitFiscalYear()
        {
            _context.FiscalYears.Add(new FiscalYear()
            {
                FiscalYearCode = "FY1516",
                FiscalYearName = "FY 2015/2016",
                LegalEntityId = new Guid("43a476f7-21fe-45f5-8521-ab27580fa1a2"),
                StartDate = new DateTime(2015, 01, 01),
                EndDate = new DateTime(2015, 12, 31),
                IsActive = true,


            });
            _context.FiscalYears.Add(new FiscalYear()
            {
                FiscalYearCode = "FY1516",
                FiscalYearName = "FY 2015/2016",
                LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"),
                StartDate = new DateTime(2015, 01, 01),
                EndDate = new DateTime(2015, 12, 31),
                IsActive = true,

            });
            _context.SaveChanges();
        }

        private void InitGeneralLedgerSetting()
        {
            var glSetting = new GeneralLedgerSettings()
            {
                LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"),
                Company = _context.Companies.FirstOrDefault(),
                GoodsReceiptNoteClearingSaAccount = _context.SaAccounts.Where(a => a.SaAccountCode == "10810").FirstOrDefault(),
                ShippingChargeSaAccount = _context.SaAccounts.Where(a => a.SaAccountCode == "40500").FirstOrDefault(),
                InvoicingDiscountSaAccount = _context.SaAccounts.Where(a => a.SaAccountCode == "40400").FirstOrDefault()
            };
            _context.GeneralLedgerSettings.Add(glSetting);
            _context.SaveChanges();
        }


        private void InitSaAccounts()
        {
            _context.SaAccountSubClass.Add(new SaAccountSubClass() { Name = "None",                         BeginningRangeForType=  "100",EndRangeForType =  "199",  LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), NormalBalance = "Dr" });
            _context.SaAccountSubClass.Add(new SaAccountSubClass() { Name = "Accounts Receivable",          BeginningRangeForType=  "200",EndRangeForType =  "299",  LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), NormalBalance = "Dr" });
            _context.SaAccountSubClass.Add(new SaAccountSubClass() { Name = "Invoicing Account",            BeginningRangeForType=  "300",EndRangeForType =  "399",  LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), NormalBalance = "Dr" });
            _context.SaAccountSubClass.Add(new SaAccountSubClass() { Name = "Penalty Account",              BeginningRangeForType = "400", EndRangeForType = "499", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), NormalBalance = "Cr" });
            _context.SaAccountSubClass.Add(new SaAccountSubClass() { Name = "Invoicing Discount Account",   BeginningRangeForType = "500", EndRangeForType = "599", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), NormalBalance = "Cr" });
            _context.SaAccountSubClass.Add(new SaAccountSubClass() { Name = "Accounts Payable Account",     BeginningRangeForType = "600", EndRangeForType = "699", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), NormalBalance = "Dr" });
            _context.SaAccountSubClass.Add(new SaAccountSubClass() { Name = "Purchase Account",             BeginningRangeForType = "700", EndRangeForType = "799", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), NormalBalance = "Dr" });
            _context.SaAccountSubClass.Add(new SaAccountSubClass() { Name = "Purchase Discount Account",    BeginningRangeForType = "800", EndRangeForType = "899", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), NormalBalance = "Dr" });
            //_context.SaAccountSubClass.Add(new SaAccountSubClass() { Name = "HOA Deposits ",                BeginningRangeForType = "900", EndRangeForType = "999", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), NormalBalance = "Dr" });
            ////_context.SaAccountSubClass.Add(new SaAccountSubClass() { Name = "Loans ",                       BeginningRangeForType = "1000", EndRangeForType = "1099", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), NormalBalance = "Dr" });
            //_context.SaAccountSubClass.Add(new SaAccountSubClass() { Name = "Operating Capital ",           BeginningRangeForType = "1100", EndRangeForType = "1199", LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), NormalBalance = "Dr" });

            _context.SaAccountClasses.Add(new SaAccountClass() { Name = "Assets",       BeginningRangeForType= "10000",EndRangeForType = "19999",  LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), NormalBalance = "Dr" });
            _context.SaAccountClasses.Add(new SaAccountClass() { Name = "Liabilities",  BeginningRangeForType= "20000",EndRangeForType = "29999",  LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), NormalBalance = "Cr" });
            _context.SaAccountClasses.Add(new SaAccountClass() { Name = "Equity",       BeginningRangeForType= "30000",EndRangeForType = "39999",  LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), NormalBalance = "Cr" });
            _context.SaAccountClasses.Add(new SaAccountClass() { Name = "Revenue",      BeginningRangeForType= "40000",EndRangeForType = "49999",  LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), NormalBalance = "Cr" });
            _context.SaAccountClasses.Add(new SaAccountClass() { Name = "Expense",      BeginningRangeForType= "50000",EndRangeForType = "59999",  LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), NormalBalance = "Dr" });
            _context.SaveChanges();

            string path = AppDomain.CurrentDomain.BaseDirectory + "/App_Data/coaNew.csv";
            DataTable csvData = new DataTable();
            using (TextFieldParser csvReader = new TextFieldParser(path))
            {
                csvReader.SetDelimiters(new string[] { "," });
                csvReader.HasFieldsEnclosedInQuotes = true;
                string[] colFields = csvReader.ReadFields();
                foreach (string column in colFields)
                {
                    DataColumn datecolumn = new DataColumn(column);
                    datecolumn.AllowDBNull = true;
                    csvData.Columns.Add(datecolumn);
                }
                while (!csvReader.EndOfData)
                {
                    string[] fieldData = csvReader.ReadFields();
                    for (int i = 0; i < fieldData.Length; i++)
                    {
                        if (fieldData[i] == "")
                        {
                            fieldData[i] = null;
                        }
                    }
                    csvData.Rows.Add(fieldData);
                }

                foreach (DataRow row in csvData.Rows)
                {
                    SaAccount saSaAccount = new SaAccount();
                    saSaAccount.LegalEntityId = new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc");
                    saSaAccount.SaAccountCode = row[0].ToString();
                    //saSaAccount.SaAccountSubClassId = 2;

                    saSaAccount.SaAccountSubClassId = int.Parse(row[1].ToString());
                    saSaAccount.SaAccountName = row[2].ToString();
                    saSaAccount.SaAccountClassId = int.Parse(row[4].ToString());

                    if (saSaAccount.SaAccountCode == "10113")
                        saSaAccount.IsCash = true;
                    _context.SaAccounts.Add(saSaAccount);
                }
                _context.SaveChanges();
                UpdateSaAccounts();
                // InitCustomers();
            }
        }

        private void UpdateSaAccounts()
        {
            DataTable csvData = new DataTable("Test");
            string path = AppDomain.CurrentDomain.BaseDirectory + "/App_Data/coaNew.csv";
            //using (TextFieldParser csvReader = new TextFieldParser(@"C:\SW\SmartSaAccountsProto\SmartAccountsUI\App_Data\coa.csv"))
            using (TextFieldParser csvReader = new TextFieldParser(path))
            {
                csvReader.SetDelimiters(new string[] { "," });
                csvReader.HasFieldsEnclosedInQuotes = true;
                string[] colFields = csvReader.ReadFields();
                foreach (string column in colFields)
                {
                    DataColumn datecolumn = new DataColumn(column);
                    datecolumn.AllowDBNull = true;
                    csvData.Columns.Add(datecolumn);
                }
                while (!csvReader.EndOfData)
                {
                    string[] fieldData = csvReader.ReadFields();
                    for (int i = 0; i < fieldData.Length; i++)
                    {
                        if (fieldData[i] == "")
                        {
                            fieldData[i] = null;
                        }
                    }
                    csvData.Rows.Add(fieldData);
                }

                foreach (DataRow row in csvData.Rows)
                {
                    string saSaAccountCode = row[0].ToString();
                    string parentSaAccountCode = row[3].ToString();

                    var saSaAccount = _context.SaAccounts.FirstOrDefault(a => a.SaAccountCode == saSaAccountCode);

                    _context.Set<SaAccount>().Attach(saSaAccount);

                    var parentSaAccount = _context.SaAccounts.FirstOrDefault(a => a.SaAccountCode == parentSaAccountCode);

                    if (parentSaAccount != null)
                        saSaAccount.ParentSaAccountId = parentSaAccount.Id;

                    _context.SaveChanges();
                }
                var saSaAccounts = _context.SaAccounts.ToList();
                foreach (var saSaAccount in saSaAccounts)
                {
                    var acc = _context.SaAccounts.Where(a => a.SaAccountCode == saSaAccount.SaAccountCode).FirstOrDefault();
                    _context.Set<SaAccount>().Attach(acc);
                    
                    if (acc.ChildSaAccounts != null && acc.ChildSaAccounts.Count > 0)
                        acc.SaAccountType = SACore.Domain.SaAccountTypes.Heading;
                    else
                        acc.SaAccountType = SACore.Domain.SaAccountTypes.Posting;

                    _context.SaveChanges();
                }
            }
        }

        private void InitTax()
        {
            var FedTax = new Tax()
            {
                TaxCode = "FederalTax",
                TaxName = "FederalTax",
                Rate = Convert.ToDecimal(.2),
                IsActive = true,
                InvoicingSaAccountId = 37,
                PurchasingSaAccountId = 37,


            };

            var StateTax = new Tax()
            {
                TaxCode = "StateTax",
                TaxName = "State Tax",
                Rate = Convert.ToDecimal(6),
                IsActive = true,
                InvoicingSaAccountId = 37,
                PurchasingSaAccountId = 37,



            };

            var CountyTax = new Tax()
            {
                TaxCode = "CountyTax",
                TaxName = "County Tax",
                Rate = Convert.ToDecimal(.2),
                IsActive = true,
                InvoicingSaAccountId = 37,
                PurchasingSaAccountId = 37,


            };

            var ServiceTax = new Tax()
            {
                TaxCode = "ServiceTax",
                TaxName = "Service Tax",
                Rate = 1,
                IsActive = true,
                InvoicingSaAccountId = 37,
                PurchasingSaAccountId = 37,


            };

            var RoundRockCityTax = new Tax()
            {
                TaxCode = "RoundRockCityTax",
                TaxName = "Round Rock City Tax",
                Rate = Convert.ToDecimal(1.2),
                IsActive = true,
                InvoicingSaAccountId = 37,
                PurchasingSaAccountId = 37,


            };

            _context.Taxes.Add(FedTax);
            _context.Taxes.Add(StateTax);
            _context.Taxes.Add(CountyTax);
            _context.Taxes.Add(ServiceTax);
            _context.Taxes.Add(RoundRockCityTax);

            var taxGroupFed = new TaxGroup()
            {
                Description = "Fed",
                TaxAppliedToShipping = false,
                IsActive = true,


            };

            var taxGroupState = new TaxGroup()
            {
                Description = "State",
                TaxAppliedToShipping = false,
                IsActive = true,


            };

            var taxGroupService = new TaxGroup()
            {
                Description = "Service",
                TaxAppliedToShipping = false,
                IsActive = true,


            };

            _context.TaxGroups.Add(taxGroupFed);
            _context.TaxGroups.Add(taxGroupService);

            var itemTaxGroupRegular = new ItemTaxGroup()
            {
                Name = "Regular",
                IsFullyExempt = false,


            };

            var itemTaxGroupRegularPreferenced = new ItemTaxGroup()
            {
                Name = "Preferenced",
                IsFullyExempt = false,



            };

            _context.ItemTaxGroups.Add(itemTaxGroupRegular);
            _context.ItemTaxGroups.Add(itemTaxGroupRegularPreferenced);

            FedTax.TaxGroupTaxes.Add(new TaxGroupTax()
            {
                TaxGroup = taxGroupFed,


            });

            StateTax.TaxGroupTaxes.Add(new TaxGroupTax()
            {
                TaxGroup = taxGroupState,


            });

            _context.SaveChanges();
        }

        private void InitItems()
        {
            _context.Measurements.Add(new Measurement() { Code = "EA", Description = "Each", });
            _context.Measurements.Add(new Measurement() { Code = "PK", Description = "Pack", });
            _context.Measurements.Add(new Measurement() { Code = "MO", Description = "Monthly", });
            _context.Measurements.Add(new Measurement() { Code = "HR", Description = "Hour", });
            _context.SaveChanges();

            // SaAccounts = Invoicing A/C (40100), Inventory (10800), COGS (50300), Inv Adjustment (50500), Item Assm Cost (10900)
            var invoicing = _context.SaAccounts.Where(a => a.SaAccountCode == "40100").FirstOrDefault();
            var inventory = _context.SaAccounts.Where(a => a.SaAccountCode == "10800").FirstOrDefault();
            var invAdjusment = _context.SaAccounts.Where(a => a.SaAccountCode == "50500").FirstOrDefault();
            var cogs = _context.SaAccounts.Where(a => a.SaAccountCode == "50300").FirstOrDefault();
            var assemblyCost = _context.SaAccounts.Where(a => a.SaAccountCode == "10900").FirstOrDefault();

            _context.ItemCategories.Add(new ItemCategory()
            {
                Name = "Charges",
                Measurement = _context.Measurements.Where(m => m.Code == "EA").FirstOrDefault(),
                ItemType = ItemTypes.Charge,
                InvoicingSaAccount = invoicing,
                InventorySaAccount = inventory,
                AdjustmentSaAccount = invAdjusment,
                CostOfGoodsSoldSaAccount = cogs,
                AssemblySaAccount = assemblyCost,


            }).Items.Add(new Item()
            {

                Description = "HOA Dues",
                SellDescription = "HOA Dues",
                PurchaseDescription = "HOA Dues",
                Price = 350,
                SmallestMeasurement = _context.Measurements.Where(m => m.Code == "MO").FirstOrDefault(),
                SellMeasurement = _context.Measurements.Where(m => m.Code == "MO").FirstOrDefault(),
                InvoicingSaAccount = _context.SaAccounts.Where(a => a.SaAccountCode == "40200").FirstOrDefault(),
                No = "1",

            });

            var componentCategory = new ItemCategory()
            {
                Name = "Components",
                Measurement = _context.Measurements.Where(m => m.Code == "EA").FirstOrDefault(),
                ItemType = ItemTypes.Purchased,
                InvoicingSaAccount = invoicing,
                InventorySaAccount = inventory,
                AdjustmentSaAccount = invAdjusment,
                CostOfGoodsSoldSaAccount = cogs,
                AssemblySaAccount = assemblyCost,





            };

            var carStickerItem = new Item()
            {

                Description = "Car Sticker",
                SellDescription = "Car Sticker",
                PurchaseDescription = "Car Sticker",
                Price = 100,
                Cost = 40,
                SmallestMeasurement = _context.Measurements.Where(m => m.Code == "EA").FirstOrDefault(),
                SellMeasurement = _context.Measurements.Where(m => m.Code == "EA").FirstOrDefault(),
                PurchaseMeasurement = _context.Measurements.Where(m => m.Code == "EA").FirstOrDefault(),
                InvoicingSaAccount = invoicing,
                InventorySaAccount = inventory,
                CostOfGoodsSoldSaAccount = cogs,
                InventoryAdjustmentSaAccount = invAdjusment,
                ItemTaxGroup = _context.ItemTaxGroups.Where(m => m.Name == "Regular").FirstOrDefault(),
                No = "2",




            };

            var otherItem = new Item()
            {

                Description = "Optical Mouse",
                SellDescription = "Optical Mouse",
                PurchaseDescription = "Optical Mouse",
                Price = 80,
                Cost = 30,
                SmallestMeasurement = _context.Measurements.Where(m => m.Code == "EA").FirstOrDefault(),
                SellMeasurement = _context.Measurements.Where(m => m.Code == "EA").FirstOrDefault(),
                PurchaseMeasurement = _context.Measurements.Where(m => m.Code == "EA").FirstOrDefault(),
                InvoicingSaAccount = invoicing,
                InventorySaAccount = inventory,
                CostOfGoodsSoldSaAccount = cogs,
                InventoryAdjustmentSaAccount = invAdjusment,
                ItemTaxGroup = _context.ItemTaxGroups.Where(m => m.Name == "Regular").FirstOrDefault(),
                No = "3",




            };

            componentCategory.Items.Add(carStickerItem);
            componentCategory.Items.Add(otherItem);
            _context.ItemCategories.Add(componentCategory);

            _context.ItemCategories.Add(new ItemCategory()
            {
                Name = "Services",
                Measurement = _context.Measurements.Where(m => m.Code == "HR").FirstOrDefault(),
                ItemType = ItemTypes.Service,
                InvoicingSaAccount = invoicing,
                InventorySaAccount = inventory,
                AdjustmentSaAccount = invAdjusment,
                CostOfGoodsSoldSaAccount = cogs,
                AssemblySaAccount = assemblyCost,





            });

            _context.ItemCategories.Add(new ItemCategory()
            {
                Name = "Systems",
                Measurement = _context.Measurements.Where(m => m.Code == "EA").FirstOrDefault(),
                ItemType = ItemTypes.Manufactured,
                InvoicingSaAccount = invoicing,
                InventorySaAccount = inventory,
                AdjustmentSaAccount = invAdjusment,
                CostOfGoodsSoldSaAccount = cogs,
                AssemblySaAccount = assemblyCost,





            });

            _context.SaveChanges();
        }

        private void InitVendor()
        {
            Vendor vendor = new Vendor();
            vendor.No = "1";
            vendor.Name = "ABC Sample Supplier";
            vendor.AccountsPayableSaAccountId = _context.SaAccounts.Where(a => a.SaAccountName == "Accounts Payable").FirstOrDefault().Id;
            vendor.PurchaseSaAccountId = _context.SaAccounts.Where(a => a.SaAccountName == "Purchase A/C").FirstOrDefault().Id;
            vendor.PurchaseDiscountSaAccountId = _context.SaAccounts.Where(a => a.SaAccountName == "Purchase Discounts").FirstOrDefault().Id;
            vendor.PartyType = SACore.Domain.PartyTypes.Vendor;
            vendor.IsActive = true;



            Contact primaryContact = new Contact();
            primaryContact.ContactType = ContactTypes.Vendor;
            primaryContact.PartyType = PartyTypes.Contact;
            primaryContact.FirstName = "Donald";
            primaryContact.LastName = "Trump";

            primaryContact.Party = vendor;

            vendor.PrimaryContact = primaryContact;

            _context.Vendors.Add(vendor);


            vendor = new Vendor();
            vendor.No = "2";
            vendor.Name = "AAA StreetRepair";
            vendor.AccountsPayableSaAccountId = _context.SaAccounts.Where(a => a.SaAccountName == "Accounts Payable").FirstOrDefault().Id;
            vendor.PurchaseSaAccountId = _context.SaAccounts.Where(a => a.SaAccountName == "Purchase A/C").FirstOrDefault().Id;
            vendor.PurchaseDiscountSaAccountId = _context.SaAccounts.Where(a => a.SaAccountName == "Purchase Discounts").FirstOrDefault().Id;
            vendor.PartyType = SACore.Domain.PartyTypes.Vendor;
            vendor.IsActive = true;



            primaryContact = new Contact();
            primaryContact.ContactType = ContactTypes.Vendor;
            primaryContact.PartyType = PartyTypes.Contact;
            primaryContact.FirstName = "Sam";
            primaryContact.LastName = "Repair";

            primaryContact.Party = vendor;

            vendor.PrimaryContact = primaryContact;

            _context.Vendors.Add(vendor);





        }

        private void InitCustomer()
        {


            var saSaAccountAR = _context.SaAccounts.Where(e => e.SaAccountCode == "10120").FirstOrDefault();
            var legalEntity = _context.LegalEntities.FirstOrDefault();

            Customer customer = new Customer();
            customer.LegalEntityId = legalEntity.LegalEntityId;
            customer.No = "1";
            customer.IsActive = true;
            customer.PartyType = SACore.Domain.PartyTypes.Customer;
            customer.Name = "ABC Customer";
            customer.AccountsReceivableSaAccountId = saSaAccountAR != null ? (int?)saSaAccountAR.Id : null;
            //customer.CreatedBy = "System";
            //customer.CreatedOn = DateTime.Now;
            //customer.ModifiedBy = "System";
            //customer.ModifiedOn = DateTime.Now;

            Contact primaryContact = new Contact();
            primaryContact.ContactType = ContactTypes.Customer;
            primaryContact.PartyType = PartyTypes.Contact;
            primaryContact.FirstName = "John";
            primaryContact.LastName = "Doe";
            primaryContact.Party = customer;

            customer.PrimaryContact = primaryContact;

            _context.Customers.Add(customer);
            _context.SaveChanges();
        }


    }


        //var saSaAccountAR = _context.SaAccounts.Where(e => e.SaAccountCode == "10120").FirstOrDefault();

        ////customer.PrimaryContact = primaryContact;

        //var InitValue = new Customer("Cust1" ,1,  new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), 4, null, saSaAccountAR.Id, null, null, null, null, null); _context.Customers.Add(InitValue);
        //    InitValue = new Customer("Cust2" ,2,  new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"),5, null, saSaAccountAR.Id, null, null, null, null, null); _context.Customers.Add(InitValue);
        //    InitValue = new Customer("Cust3", 3, new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc"), 6, null, saSaAccountAR.Id, null, null, null, null, null); _context.Customers.Add(InitValue);
        //    InitValue = new Customer("Cust4", 4, new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cd"), 7, null, saSaAccountAR.Id, null, null, null, null, null); _context.Customers.Add(InitValue);

        //_context.SaveChanges();
    

        //private void InitCustomers()
        //{

        //    DataTable csvData = new DataTable();
        //    using (TextFieldParser csvReader = new TextFieldParser(@"C:\SW\OurApproach\SmartSaAccounts\SmartAccountsUI\App_Data\HomeOwner.csv"))
        //    {
        //        csvReader.SetDelimiters(new string[] { "," });
        //        csvReader.HasFieldsEnclosedInQuotes = true;
        //        string[] colFields = csvReader.ReadFields();
        //        foreach (string column in colFields)
        //        {
        //            DataColumn datecolumn = new DataColumn(column);
        //            datecolumn.AllowDBNull = true;
        //            csvData.Columns.Add(datecolumn);
        //        }
        //        while (!csvReader.EndOfData)
        //        {
        //            string[] fieldData = csvReader.ReadFields();
        //            for (int i = 0; i < fieldData.Length; i++)
        //            {
        //                if (fieldData[i] == "")
        //                {
        //                    fieldData[i] = null;
        //                }
        //            }
        //            csvData.Rows.Add(fieldData);
        //        }
        //    }

        //    var saSaAccountAR = _context.SaAccounts.Where(e => e.SaAccountCode == "10120").FirstOrDefault();
        //    foreach (DataRow row in csvData.Rows)
        //    {
        //        Customer customer = new Customer();
        //        customer.IsActive = true;

        //        //customer.FirstName = row[1].ToString();
        //        //customer.LastName = row[2].ToString();
        //        //customer.Name = row[0].ToString() + " " + customer.FirstName + " " + customer.LastName;
        //        customer.AccountsReceivableSaAccountId = saSaAccountAR != null ? (int?)saSaAccountAR.Id : null;

        //        _context.Customers.Add(customer);
        //    }
        //    _context.SaveChanges();
        //}

    
}
