﻿using System.Collections.Generic;
using System.Web.Mvc;
using SAServices.Financial;
using SAServices.MasterTable;
using SmartAccountsUI.Models.ViewModels.MasterTable;

namespace SmartAccountsUI.Controllers
{
    public class MasterTableController : Controller
    {
        private readonly IMasterTableService _masterTableService;

        public MasterTableController(IMasterTableService masterTableService)
        {
            _masterTableService = masterTableService;
        }

        // GET: /Customer/
        public ActionResult Index()
        {
            var cv = _masterTableService.GetCustomers();
            
            List<CustomerViewModel> lcvm = new List<CustomerViewModel>();
            foreach (var item in cv)
            {
                //CustomerViewModel newCV = new CustomerViewModel();
                //newCV.Id = item.Id;
                //newCV.Party = item.Party;
                //newCV.Contacts = item.Contacts;
                //newCV.LegalEntity = item.LegalEntity;
                //newCV.Name = item.Name;
                //newCV.Balance = item.Balance;
                //newCV.Id = item.Id;
                //newCV.Id = item.Id;

                //lcvm.Add(newCV);


            }
            return View(lcvm);
        }

        // GET: /Customer/Details/5
        //public ActionResult Details(long? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Customer customer = db.Customers.Find(id);
        //    if (customer == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(customer);
        //}

        //// GET: /Customer/Create
        //public ActionResult Create()
        //{
        //    ViewBag.PrimaryContactId = new SelectList(db.Contacts, "Id", "FirstName");
        //    ViewBag.Id = new SelectList(db.Parties, "Id", "Name");
        //    ViewBag.PaymentTermId = new SelectList(db.PaymentTerms, "Id", "Description");
        //    ViewBag.InvoicingDiscountSaAccountId = new SelectList(db.SaAccounts, "Id", "SaAccountCode");
        //    ViewBag.InvoicingSaAccountId = new SelectList(db.SaAccounts, "Id", "SaAccountCode");
        //    ViewBag.PromptPaymentDiscountSaAccountId = new SelectList(db.SaAccounts, "Id", "SaAccountCode");
        //    ViewBag.SaAccountsReceivableSaAccountId = new SelectList(db.SaAccounts, "Id", "SaAccountCode");
        //    ViewBag.TaxGroupId = new SelectList(db.TaxGroups, "Id", "Description");
        //    return View();
        //}

        //// POST: /Customer/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include="Id,No,LegalEntityId,PrimaryContactId,TaxGroupId,SaAccountsReceivableSaAccountId,InvoicingSaAccountId,InvoicingDiscountSaAccountId,PromptPaymentDiscountSaAccountId,PaymentTermId,BackendTypeId")] Customer customer)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Customers.Add(customer);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.PrimaryContactId = new SelectList(db.Contacts, "Id", "FirstName", customer.PrimaryContactId);
        //    ViewBag.Id = new SelectList(db.Parties, "Id", "Name", customer.Id);
        //    ViewBag.PaymentTermId = new SelectList(db.PaymentTerms, "Id", "Description", customer.PaymentTermId);
        //    ViewBag.InvoicingDiscountSaAccountId = new SelectList(db.SaAccounts, "Id", "SaAccountCode", customer.InvoicingDiscountSaAccountId);
        //    ViewBag.InvoicingSaAccountId = new SelectList(db.SaAccounts, "Id", "SaAccountCode", customer.InvoicingSaAccountId);
        //    ViewBag.PromptPaymentDiscountSaAccountId = new SelectList(db.SaAccounts, "Id", "SaAccountCode", customer.PromptPaymentDiscountSaAccountId);
        //    ViewBag.SaAccountsReceivableSaAccountId = new SelectList(db.SaAccounts, "Id", "SaAccountCode", customer.SaAccountsReceivableSaAccountId);
        //    ViewBag.TaxGroupId = new SelectList(db.TaxGroups, "Id", "Description", customer.TaxGroupId);
        //    return View(customer);
        //}

        //// GET: /Customer/Edit/5
        //public ActionResult Edit(long? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Customer customer = db.Customers.Find(id);
        //    if (customer == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.PrimaryContactId = new SelectList(db.Contacts, "Id", "FirstName", customer.PrimaryContactId);
        //    ViewBag.Id = new SelectList(db.Parties, "Id", "Name", customer.Id);
        //    ViewBag.PaymentTermId = new SelectList(db.PaymentTerms, "Id", "Description", customer.PaymentTermId);
        //    ViewBag.InvoicingDiscountSaAccountId = new SelectList(db.SaAccounts, "Id", "SaAccountCode", customer.InvoicingDiscountSaAccountId);
        //    ViewBag.InvoicingSaAccountId = new SelectList(db.SaAccounts, "Id", "SaAccountCode", customer.InvoicingSaAccountId);
        //    ViewBag.PromptPaymentDiscountSaAccountId = new SelectList(db.SaAccounts, "Id", "SaAccountCode", customer.PromptPaymentDiscountSaAccountId);
        //    ViewBag.SaAccountsReceivableSaAccountId = new SelectList(db.SaAccounts, "Id", "SaAccountCode", customer.SaAccountsReceivableSaAccountId);
        //    ViewBag.TaxGroupId = new SelectList(db.TaxGroups, "Id", "Description", customer.TaxGroupId);
        //    return View(customer);
        //}

        //// POST: /Customer/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include="Id,No,LegalEntityId,PrimaryContactId,TaxGroupId,SaAccountsReceivableSaAccountId,InvoicingSaAccountId,InvoicingDiscountSaAccountId,PromptPaymentDiscountSaAccountId,PaymentTermId,BackendTypeId")] Customer customer)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(customer).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.PrimaryContactId = new SelectList(db.Contacts, "Id", "FirstName", customer.PrimaryContactId);
        //    ViewBag.Id = new SelectList(db.Parties, "Id", "Name", customer.Id);
        //    ViewBag.PaymentTermId = new SelectList(db.PaymentTerms, "Id", "Description", customer.PaymentTermId);
        //    ViewBag.InvoicingDiscountSaAccountId = new SelectList(db.SaAccounts, "Id", "SaAccountCode", customer.InvoicingDiscountSaAccountId);
        //    ViewBag.InvoicingSaAccountId = new SelectList(db.SaAccounts, "Id", "SaAccountCode", customer.InvoicingSaAccountId);
        //    ViewBag.PromptPaymentDiscountSaAccountId = new SelectList(db.SaAccounts, "Id", "SaAccountCode", customer.PromptPaymentDiscountSaAccountId);
        //    ViewBag.SaAccountsReceivableSaAccountId = new SelectList(db.SaAccounts, "Id", "SaAccountCode", customer.SaAccountsReceivableSaAccountId);
        //    ViewBag.TaxGroupId = new SelectList(db.TaxGroups, "Id", "Description", customer.TaxGroupId);
        //    return View(customer);
        //}

        //// GET: /Customer/Delete/5
        //public ActionResult Delete(long? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Customer customer = db.Customers.Find(id);
        //    if (customer == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(customer);
        //}

        //// POST: /Customer/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(long id)
        //{
        //    Customer customer = db.Customers.Find(id);
        //    db.Customers.Remove(customer);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
