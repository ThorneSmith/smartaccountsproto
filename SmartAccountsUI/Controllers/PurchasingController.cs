﻿using System;
using System.Linq;
using System.Web.Mvc;
using SACore.Domain.Payables;
using SAServices.Financial;
using SAServices.Inventory;
using SAServices.Purchasing;
using SmartAccountsUI.Models;
using SmartAccountsUI.Models.ViewModels.Purchases;
using Vendor = SmartAccountsUI.Models.ViewModels.MasterTable.Vendors;

namespace SmartAccountsUI.Controllers
{
    [Authorize]
    public class PurchasingController : Controller
    {
        private readonly IInventoryService _inventoryService;
        private readonly IFinancialService _financialService;
        private readonly IPurchasingService _purchasingService;

        public PurchasingController(IInventoryService inventoryService,
            IFinancialService financialService,
            IPurchasingService purchasingService)
        {
            _inventoryService = inventoryService;
            _financialService = financialService;
            _purchasingService = purchasingService;
        }

        public ActionResult PurchaseOrders()
        {
            var purchaseOrders = _purchasingService.GetPurchaseOrders();
            var model = new PurchaseOrders();
            foreach (var po in purchaseOrders)
            {
                model.PurchaseOrderListLines.Add(new PurchaseOrderListLine()
                {
                    Id = po.Id,
                    No = po.No,
                    Date = po.Date,
                    Vendor = po.Vendor.Name,
                    Amount = po.PurchaseOrderLines.Sum(e => e.Amount),
                    Completed = po.IsCompleted(),
                    Paid = po.IsPaid(),
                    HasInvoiced = po.PurchaseInvoiceHeaderId.HasValue
                });
            }
            return View(model);
        }

        public ActionResult AddPurchaseOrder()
        {
            var model = new AddPurchaseOrder();
            var items = _inventoryService.GetAllItems();
            var saSaAccounts = _financialService.GetSaAccounts();
            var measurements = _inventoryService.GetMeasurements();
            var taxes = _financialService.GetTaxes();
            var itemCategories = _inventoryService.GetItemCategories();
            var vendors = _purchasingService.GetVendors();
            model.Items = ModelViewHelper.Items(items);
            model.Vendors = ModelViewHelper.Vendors(vendors);
            model.UnitOfMeasurements = ModelViewHelper.Measurements(measurements);
            return View(model);
        }

        [HttpPost, ActionName("AddPurchaseOrder")]
        [FormValueRequired("AddPurchaseOrderLine")]
        public ActionResult AddPurchaseOrderLine(AddPurchaseOrder model)
        {
            var items = _inventoryService.GetAllItems();
            var saSaAccounts = _financialService.GetSaAccounts();
            var measurements = _inventoryService.GetMeasurements();
            var taxes = _financialService.GetTaxes();
            var itemCategories = _inventoryService.GetItemCategories();
            var vendors = _purchasingService.GetVendors();
            model.Items = ModelViewHelper.Items(items);
            model.Vendors = ModelViewHelper.Vendors(vendors);
            model.UnitOfMeasurements = ModelViewHelper.Measurements(measurements);
            try
            {
                if (model.Quantity > 0)
                {
                    var item = _inventoryService.GetItemById(model.ItemId);
                    model.PurchaseOrderLines.Add(new AddPurchaseOrderLine()
                    {
                        ItemId = model.ItemId,
                        Description = item.PurchaseDescription,
                        Cost = item.Cost,
                        UnitOfMeasurementId = model.UnitOfMeasurementId,
                        Quantity = model.Quantity,
                        TotalLineCost = item.Cost.Value * model.Quantity
                    });
                }

                return View(model);
            }
            catch
            {
                return View(model);
            }
        }

        [HttpPost, ActionName("AddPurchaseOrder")]
        [FormValueRequired("SavePurchaseOrder")]
        public ActionResult SavePurchaseOrder(AddPurchaseOrder model)
        {
            var items = _inventoryService.GetAllItems();
            var saSaAccounts = _financialService.GetSaAccounts();
            var measurements = _inventoryService.GetMeasurements();
            var taxes = _financialService.GetTaxes();
            var itemCategories = _inventoryService.GetItemCategories();
            var vendors = _purchasingService.GetVendors();
            model.Items = ModelViewHelper.Items(items);
            model.Vendors = ModelViewHelper.Vendors(vendors);
            model.UnitOfMeasurements = ModelViewHelper.Measurements(measurements);
            try
            {
                var po = new PurchaseOrderHeader()
                {
                    CreatedBy = User.Identity.Name,
                    CreatedOn = DateTime.Now,
                    ModifiedBy = User.Identity.Name,
                    ModifiedOn = DateTime.Now,
                    VendorId = model.VendorId,
                    Date = model.Date,
                    //No = _settingService.GetNextNumber(Core.Module.Common.Data.SequenceNumberTypes.PurchaseOrder).ToString(),
                    //DocumentTypeId = (int)DocumentTypes.PurchaseOrder
                };
                foreach (var item in model.PurchaseOrderLines)
                {
                    var persistedItem = _inventoryService.GetItemById(item.ItemId);
                    po.PurchaseOrderLines.Add(new PurchaseOrderLine()
                    {
                        Amount = item.TotalLineCost,
                        ItemId = item.ItemId,
                        Quantity = item.Quantity,
                        MeasurementId = item.UnitOfMeasurementId,
                        Cost = persistedItem.Cost.Value,
                        CreatedBy = User.Identity.Name,
                        CreatedOn = DateTime.Now,
                        ModifiedBy = User.Identity.Name,
                        ModifiedOn = DateTime.Now,
                    });
                }
                _purchasingService.AddPurchaseOrder(po, true);
                return RedirectToAction("PurchaseOrders");
            }
            catch
            {
                return View(model);
            }
        }

        [HttpPost, ActionName("AddPurchaseOrder")]
        [FormValueRequired("DeletePurchaseOrderLine")]
        public ActionResult DeletePurchaseOrderLine(AddPurchaseOrder model)
        {
            var items = _inventoryService.GetAllItems();
            var saSaAccounts = _financialService.GetSaAccounts();
            var measurements = _inventoryService.GetMeasurements();
            var taxes = _financialService.GetTaxes();
            var itemCategories = _inventoryService.GetItemCategories();
            var vendors = _purchasingService.GetVendors();
            model.Items = ModelViewHelper.Items(items);
            model.Vendors = ModelViewHelper.Vendors(vendors);
            model.UnitOfMeasurements = ModelViewHelper.Measurements(measurements);

            var request = HttpContext.Request;
            var deletedItem = request.Form["DeletedLineItem"];
            model.PurchaseOrderLines.Remove(model.PurchaseOrderLines.Where(i => i.ItemId == int.Parse(deletedItem.ToString())).FirstOrDefault());
            return View(model);
        }

        public ActionResult AddPurchaseReceipt(long id)
        {
            var po = _purchasingService.GetPurchaseOrderById(id);
            var model = new AddPurchaseReceipt();
            model.PreparePurchaseReceiptViewModel(po);
            return View(model);
        }

        [HttpPost, ActionName("AddPurchaseReceipt")]
        [FormValueRequired("SavePurchaseReceipt")]
        public ActionResult AddPurchaseReceipt(AddPurchaseReceipt model)
        {
            bool hasChanged = false;
            foreach (var line in model.PurchaseReceiptLines)
            {
                if (line.InQty.HasValue && line.InQty.Value != 0)
                {
                    hasChanged = true;
                    break;
                }
            }

            if (!hasChanged)
                return RedirectToAction("PurchaseOrders");

            var po = _purchasingService.GetPurchaseOrderById(model.Id);

            var poReceipt = new PurchaseReceiptHeader()
            {
                Date = DateTime.Now,
                Vendor = po.Vendor,
                VendorId = po.VendorId,
                PurchaseOrderHeaderId = po.Id,
                CreatedBy = User.Identity.Name,
                CreatedOn = DateTime.Now,
                ModifiedBy = User.Identity.Name,
                ModifiedOn = DateTime.Now
            };

            foreach (var receipt in model.PurchaseReceiptLines)
            {
                if((receipt.InQty + receipt.ReceiptQuantity) > receipt.Quantity)
                    return RedirectToAction("PurchaseOrders");

                poReceipt.PurchaseReceiptLines.Add(new PurchaseReceiptLine()
                {
                    PurchaseOrderLineId = receipt.PurchaseOrderLineId,
                    ItemId = receipt.ItemId,
                    MeasurementId = receipt.UnitOfMeasurementId,
                    Quantity = receipt.Quantity,
                    ReceivedQuantity = (receipt.InQty.HasValue ? receipt.InQty.Value : 0),
                    Cost = receipt.Cost.Value,
                    Amount = receipt.Cost.Value * (receipt.InQty.HasValue ? receipt.InQty.Value : 0),
                    CreatedBy = User.Identity.Name,
                    CreatedOn = DateTime.Now,
                    ModifiedBy = User.Identity.Name,
                    ModifiedOn = DateTime.Now
                });
            }

            _purchasingService.AddPurchaseOrderReceipt(poReceipt);
            return RedirectToAction("PurchaseOrders");
        }

        public ActionResult AddPurchaseInvoice(int? id = null)
        {
            AddPurchaseInvoice model = new AddPurchaseInvoice();
            if (id != null)
            {
                var existingPO = _purchasingService.GetPurchaseOrderById(id.Value);
                model.Date = existingPO.Date;
                model.Vendor = existingPO.Vendor.Name;
                model.No = existingPO.No;
                model.Amount = existingPO.PurchaseOrderLines.Sum(a => a.Amount);

                foreach (var line in existingPO.PurchaseOrderLines)
                {
                    model.PurchaseInvoiceLines.Add(new AddPurchaseInvoiceLine()
                    {
                        Id = line.Id,
                        ItemId = line.ItemId,
                        UnitOfMeasurementId = line.MeasurementId,
                        Description = line.Item.Description,
                        Quantity = line.Quantity,
                        Cost = line.Cost,
                        TotalLineCost = line.Cost * line.Quantity,
                        ReceivedQuantity = line.GetReceivedQuantity().Value
                    });
                }
            }
            return View(model);
        }

        [HttpPost, ActionName("AddPurchaseInvoice")]
        [FormValueRequired("SavePurchaseInvoice")]
        public ActionResult AddPurchaseInvoice(AddPurchaseInvoice model)
        {
            if(string.IsNullOrEmpty(model.VendorInvoiceNo))
                return RedirectToAction("PurchaseOrders");

            var existingPO = _purchasingService.GetPurchaseOrderById(model.Id);
            var vendor = _purchasingService.GetVendorById(existingPO.VendorId);

            var purchInvoice = new PurchaseInvoiceHeader()
            {
                Date = model.Date,
                VendorInvoiceNo = model.VendorInvoiceNo,
                Vendor = vendor,
                VendorId = vendor.Id,
                CreatedBy = User.Identity.Name,
                CreatedOn = DateTime.Now,
                ModifiedBy = User.Identity.Name,
                ModifiedOn = DateTime.Now
            };

            foreach (var line in model.PurchaseInvoiceLines)
            {
                var item = _inventoryService.GetItemById(line.ItemId);
                var measurement = _inventoryService.GetMeasurementById(line.UnitOfMeasurementId);
                purchInvoice.PurchaseInvoiceLines.Add(new PurchaseInvoiceLine()
                {
                    ItemId = item.Id,
                    MeasurementId = measurement.Id,
                    Quantity = line.Quantity,
                    ReceivedQuantity = line.ReceivedQuantity,
                    Cost = item.Cost.Value,
                    Discount = 0,
                    Amount = item.Cost.Value * line.ReceivedQuantity,
                    CreatedBy = User.Identity.Name,
                    CreatedOn = DateTime.Now,
                    ModifiedBy = User.Identity.Name,
                    ModifiedOn = DateTime.Now
                });
            }
            _purchasingService.AddPurchaseInvoice(purchInvoice, existingPO.Id);
            return RedirectToAction("PurchaseOrders");
        }

        

        public ActionResult PurchaseInvoices()
        {
            var model = new PurchaseInvoices();
            var invoices = _purchasingService.GetPurchaseInvoices();
            foreach(var invoice in invoices)
            {
                model.PurchaseInvoiceListLines.Add(new PurchaseInvoiceListLine()
                {
                    Id = invoice.Id,
                    No = invoice.No,
                    Date = invoice.Date,
                    Vendor = invoice.Vendor.Name,
                    Amount = invoice.PurchaseInvoiceLines.Sum(a => a.Amount),
                    IsPaid = invoice.IsPaid()
                });
            }
            return View(model);
        }

        public ActionResult MakePayment(long id)
        {
            var model = new MakePayment();
            var invoice = _purchasingService.GetPurchaseInvoiceById(id);
            model.InvoiceId = invoice.Id;
            model.InvoiceNo = invoice.No;
            model.Vendor = invoice.Vendor.Name;
            model.Amount = invoice.PurchaseInvoiceLines.Sum(a => a.Amount);
            return View(model);
        }

        [HttpPost, ActionName("MakePayment")]
        [FormValueRequired("SavePaymentToVendor")]
        public ActionResult SavePaymentToVendor(MakePayment model)
        {
            var invoice = _purchasingService.GetPurchaseInvoiceById(model.InvoiceId);
            if(model.AmountToPay < 1)
                return RedirectToAction("MakePayment", new { id = model.InvoiceId });
            _purchasingService.SavePayment(invoice.Id, invoice.VendorId.Value, model.SaAccountId, model.AmountToPay, DateTime.Now);
            return RedirectToAction("PurchaseInvoices");
        }

        public ActionResult PurchaseOrder(long id = 0)
        {
            var model = new PurchaseHeaderViewModel();
            model.DocumentType = SACore.Domain.DocumentTypes.PurchaseOrder;
            if (id == 0)
            {
                model.Id = id;
                return View(model);
            }
            else
            {
                var order = _purchasingService.GetPurchaseOrderById(id);
                model.Id = order.Id;
                model.VendorId = order.VendorId;
                model.Date = order.Date;
                model.ReferenceNo = string.Empty;
                foreach (var line in order.PurchaseOrderLines)
                {
                    var lineItem = new PurchaseLineItemViewModel();
                    lineItem.Id = line.Id;
                    lineItem.ItemId = line.ItemId;
                    lineItem.ItemNo = line.Item.No;
                    lineItem.ItemDescription = line.Item.Description;
                    lineItem.Measurement = line.Measurement.Description;
                    lineItem.Quantity = line.Quantity;
                    lineItem.Price = line.Amount;
                    model.PurchaseLine.PurchaseLineItems.Add(lineItem);
                }
                return View(model);
            }
        }

        public ActionResult PurchaseDelivery(long id = 0, int orderid = 0)
        {
            var model = new PurchaseHeaderViewModel();
            model.DocumentType = SACore.Domain.DocumentTypes.PurchaseReceipt;
            if (id == 0)
            {
                model.Id = id;
                if (orderid != 0)
                {
                    var order = _purchasingService.GetPurchaseOrderById(orderid);
                    model.Id = order.Id;
                    model.VendorId = order.VendorId;
                    model.Date = order.Date;
                    model.ReferenceNo = string.Empty;
                    foreach (var line in order.PurchaseOrderLines)
                    {
                        var lineItem = new PurchaseLineItemViewModel();
                        lineItem.Id = line.Id;
                        lineItem.ItemId = line.ItemId;
                        lineItem.ItemNo = line.Item.No;
                        lineItem.ItemDescription = line.Item.Description;
                        lineItem.Measurement = line.Measurement.Description;
                        lineItem.Quantity = line.Quantity;
                        lineItem.Price = line.Amount;
                        model.PurchaseLine.PurchaseLineItems.Add(lineItem);
                    }
                }
                return View(model);
            }
            else
            {
                var delivery = _purchasingService.GetPurchaseReceiptById(id);
                model.Id = delivery.Id;
                model.VendorId = delivery.VendorId;
                model.Date = delivery.Date;
                model.ReferenceNo = string.Empty;
                foreach (var line in delivery.PurchaseReceiptLines)
                {
                    var lineItem = new PurchaseLineItemViewModel();
                    lineItem.Id = line.Id;
                    lineItem.ItemId = line.ItemId;
                    lineItem.ItemNo = line.Item.No;
                    lineItem.ItemDescription = line.Item.Description;
                    lineItem.Measurement = line.Measurement.Description;
                    lineItem.Quantity = line.Quantity;
                    lineItem.Price = line.Amount;
                    model.PurchaseLine.PurchaseLineItems.Add(lineItem);
                }
                return View(model);
            }
        }

        public ActionResult PurchaseInvoice(long id = 0, int deliveryId = 0)
        {
            var model = new PurchaseHeaderViewModel();
            model.DocumentType = SACore.Domain.DocumentTypes.PurchaseInvoice;
            model.IsDirect = deliveryId == 0;
            model.Id = id;
            if (id == 0)
            {
                return View(model);
            }
            else
            {
                var invoice = _purchasingService.GetPurchaseInvoiceById(id);
                model.Id = invoice.Id;
                model.VendorId = invoice.VendorId;
                model.Date = invoice.Date;
                model.ReferenceNo = string.Empty;
                foreach (var line in invoice.PurchaseInvoiceLines)
                {
                    var lineItem = new PurchaseLineItemViewModel();
                    lineItem.Id = line.Id;
                    lineItem.ItemId = line.ItemId;
                    lineItem.ItemNo = line.Item.No;
                    lineItem.ItemDescription = line.Item.Description;
                    lineItem.Measurement = line.Measurement.Description;
                    lineItem.Quantity = line.Quantity;
                    lineItem.Price = line.Amount;
                    model.PurchaseLine.PurchaseLineItems.Add(lineItem);
                }
                return View(model);
            }
        }

        [NonAction]
        protected void AddLineItem(PurchaseHeaderViewModel model)
        {
            var item = _inventoryService.GetItemByNo(model.PurchaseLine.ItemNo);
            var newLine = new PurchaseLineItemViewModel()
            {
                ItemId = item.Id,
                ItemNo = item.No,
                ItemDescription = item.Description,
                Measurement = item.SellMeasurement.Description,
                Quantity = model.PurchaseLine.Quantity,
                Price = model.PurchaseLine.Price,
            };
            model.PurchaseLine.PurchaseLineItems.Add(newLine);

            foreach (var line in model.PurchaseLine.PurchaseLineItems)
            {
                var taxes = _financialService.ComputeInputTax(line.ItemId, line.Quantity, line.Price);
                var taxVM = new PurchaseLineItemTaxViewModel();
                foreach (var tax in taxes)
                {
                    var t = _financialService.GetTaxes().Where(tx => tx.Id == int.Parse(tax.Key.ToString())).FirstOrDefault();
                    taxVM.TaxId = int.Parse(tax.Key.ToString());
                    taxVM.Amount = tax.Value;
                    taxVM.TaxRate = t.Rate;
                    taxVM.TaxName = t.TaxName;
                    model.PurchaseLine.PurchaseLineItemsTaxes.Add(taxVM);
                }
            }
        }

        public ActionResult ReturnView(PurchaseHeaderViewModel model)
        {
            string actionName = string.Empty;
            switch (model.DocumentType)
            {
                case SACore.Domain.DocumentTypes.InvoicingOrder:
                    actionName = "PurchaseOrder";
                    break;
                case SACore.Domain.DocumentTypes.InvoicingInvoice:
                    actionName = "Purchasevoice";
                    break;
                case SACore.Domain.DocumentTypes.InvoicingDelivery:
                    actionName = "PurchaseDelivery";
                    break;
            }

            return View(actionName, model);
        }

        [HttpPost, ActionName("PurchaseOrder")]
        [FormValueRequired("AddLineItem")]
        public ActionResult AddLineItemOrder(PurchaseHeaderViewModel model)
        {
            AddLineItem(model);
            return ReturnView(model);
        }

        [HttpPost, ActionName("PurchaseInvoice")]
        [FormValueRequired("AddLineItem")]
        public ActionResult AddLineItemInvoice(PurchaseHeaderViewModel model)
        {
            AddLineItem(model);
            return ReturnView(model);
        }

        [HttpPost, ActionName("PurchaseDelivery")]
        [FormValueRequired("AddLineItem")]
        public ActionResult AddLineItemDelivery(PurchaseHeaderViewModel model)
        {
            AddLineItem(model);
            return ReturnView(model);
        }

        [HttpPost, ActionName("PurchaseOrder")]
        [FormValueRequired("DeleteLineItem")]
        public ActionResult DeleteLineItemOrder(PurchaseHeaderViewModel model)
        {
            DeleteLineItem(model);
            return ReturnView(model);
        }

        [HttpPost, ActionName("PurchaseInvoice")]
        [FormValueRequired("DeleteLineItem")]
        public ActionResult DeleteLineItemInvoice(PurchaseHeaderViewModel model)
        {
            DeleteLineItem(model);
            return ReturnView(model);
        }

        [HttpPost, ActionName("PurchaseDelivery")]
        [FormValueRequired("DeleteLineItem")]
        public ActionResult DeleteLineItemDelivery(PurchaseHeaderViewModel model)
        {
            DeleteLineItem(model);
            return ReturnView(model);
        }

        [NonAction]
        protected void DeleteLineItem(PurchaseHeaderViewModel model)
        {
            var request = HttpContext.Request;
            var deletedItem = request.Form["DeletedLineItem"];
            if (!string.IsNullOrEmpty(deletedItem))
            {
                model.PurchaseLine.PurchaseLineItems.RemoveAt(int.Parse(deletedItem));
            }
        }

        [HttpPost, ActionName("PurchaseOrder")]
        [FormValueRequired("Save")]
        public ActionResult SaveOrder(PurchaseHeaderViewModel model)
        {
            PurchaseOrderHeader order = null;
            if (model.Id.HasValue == false || model.Id == 0)
            {
                order = new PurchaseOrderHeader();
                order.CreatedBy = User.Identity.Name;
                order.CreatedOn = DateTime.Now;
            }
            else
            {
                order = _purchasingService.GetPurchaseOrderById(model.Id.Value);
            }
            order.VendorId = model.VendorId.Value;
            order.Date = model.Date;
            order.Description = string.Empty;
            order.ModifiedBy = User.Identity.Name;
            order.ModifiedOn = DateTime.Now;

            foreach (var line in model.PurchaseLine.PurchaseLineItems)
            {
                var item = _inventoryService.GetItemById(line.ItemId);
                order.PurchaseOrderLines.Add(new PurchaseOrderLine()
                {
                    Amount = line.Price,
                    ItemId = item.Id,
                    Quantity = line.Quantity,
                    MeasurementId = item.PurchaseMeasurementId.Value,
                    Cost = item.Cost.Value,
                    CreatedBy = User.Identity.Name,
                    CreatedOn = DateTime.Now,
                    ModifiedBy = User.Identity.Name,
                    ModifiedOn = DateTime.Now,
                });
            }

            _purchasingService.AddPurchaseOrder(order, true);
            return RedirectToAction("PurchaseOrders");
        }

        [HttpPost, ActionName("PurchaseDelivery")]
        [FormValueRequired("Save")]
        public ActionResult SaveDelivery(PurchaseHeaderViewModel model)
        {
            if (model.Id == 0)
            {
            }
            else
            {
            }
            return RedirectToAction("PurchaseDeliveries");
        }

        [HttpPost, ActionName("PurchaseInvoice")]
        [FormValueRequired("Save")]
        public ActionResult SaveInvoice(PurchaseHeaderViewModel model)
        {
            if (model.Id == 0)
            {
                var invoice = new PurchaseInvoiceHeader();
                invoice.Date = model.Date;
                invoice.Description = string.Empty;
                invoice.VendorId = model.VendorId;
                invoice.VendorInvoiceNo = model.ReferenceNo;
            }
            else
            {

            }
            return RedirectToAction("PurchaseInvoices");
        }
    }
}
