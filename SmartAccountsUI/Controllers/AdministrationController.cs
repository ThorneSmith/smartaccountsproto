﻿using System.Web.Mvc;
using System.Linq;
using System;

using System.Collections.Generic;
using SACore.Domain;
using SACore.Domain.MasterTables;
using SACore.Domain.Receivables;
using SAServices.Administration;
using SAServices.Inventory;
using SAServices.Receivables;

using SmartAccountsUI.Models.ViewModels.Administration;

namespace SmartAccountsUI.Controllers
{
    [Authorize]
    public class AdministrationController : Controller
    {
        private readonly IInventoryService _inventoryService = null;
        private readonly IInvoicingService _invoicingService = null;
        private readonly IAdministrationService _administrationService = null;

        public AdministrationController(IInventoryService inventoryService,
            IInvoicingService invoicingService,
            IAdministrationService administrationService)
        {
            _inventoryService = inventoryService;
            _invoicingService = invoicingService;
            _administrationService = administrationService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult BulkInvoice()
        {
            var model = new Models.ViewModels.Administration.CreateBulkInvoice();
            var itemAssociationDues = (from i in _inventoryService.GetAllItems().ToList()
                                       where (i != null
                                       && i.SellDescription.ToLower().Contains("dues"))
                                       select i).FirstOrDefault();
            var day = 15;
            var month = DateTime.Now.Month == 12 ? 1 : DateTime.Now.Month + 1;
            var year = DateTime.Now.Month == 12 ? DateTime.Now.Year + 1 : DateTime.Now.Year;
            DateTime invoiceDate = new DateTime(year, month, day);
            model.InvoiceDate = invoiceDate;
            model.DueDate = new DateTime(year, month, day + 5);
            model.TotalAmountPerInvoice = (decimal)itemAssociationDues.Price;
            return View(model);
        }

        [HttpPost, ActionName("BulkInvoice")]
        [FormValueRequiredAttribute("GenerateBulkInvoice")]
        public ActionResult GenerateBulkInvoice(Models.ViewModels.Administration.CreateBulkInvoice model)
        {
            var day = 15;
            var month = DateTime.Now.Month == 12 ? 1 : DateTime.Now.Month + 1;
            var year = DateTime.Now.Month == 12 ? DateTime.Now.Year + 1 : DateTime.Now.Year;
            DateTime invoiceDate = new DateTime(year, month, day);

            var query = from c in _invoicingService.GetCustomers()
                        select c;
            var customers = query.ToList();

            var itemAssociationDues = (from i in _inventoryService.GetAllItems().ToList()
                                       where (i != null
                                       && i.SellDescription.ToLower().Contains("dues"))
                                       select i).FirstOrDefault();

            var invoices = new List<InvoicingInvoiceHeader>();

            foreach (var customer in customers)
            {
                var current = (from si in _invoicingService.GetInvoicingInvoices()
                               where si.CustomerId == customer.Id
                               && si.Date.Year == invoiceDate.Year
                               && si.Date.Month == invoiceDate.Month
                               && si.Date.Day == invoiceDate.Day
                               select si).FirstOrDefault();
                if (current != null)
                    return RedirectToAction("BulkInvoice");

                var invoiceLine = new InvoicingInvoiceLine();
                invoiceLine.ItemId = itemAssociationDues.Id;
                invoiceLine.Quantity = 1;
                invoiceLine.Amount = Convert.ToDecimal(itemAssociationDues.Price * invoiceLine.Quantity);
                invoiceLine.MeasurementId = itemAssociationDues.SmallestMeasurementId.Value;
                invoiceLine.CreatedBy = User.Identity.Name;
                invoiceLine.CreatedOn = DateTime.Now;
                invoiceLine.ModifiedBy = User.Identity.Name;
                invoiceLine.ModifiedOn = DateTime.Now;

                var invoice = new InvoicingInvoiceHeader();
                invoice.Date = invoiceDate;                
                invoice.CustomerId = customer.Id;
                invoice.CreatedBy = User.Identity.Name;
                invoice.CreatedOn = DateTime.Now;
                invoice.ModifiedBy = User.Identity.Name;
                invoice.ModifiedOn = DateTime.Now;
                invoice.InvoicingInvoiceLines.Add(invoiceLine);

                invoices.Add(invoice);
            }

            foreach (var invoice in invoices)
                _invoicingService.AddInvoicingInvoice(invoice, null);

            return RedirectToAction("InvoicingInvoices", "Invoicing");
        }

        public ActionResult Taxes()
        {
            var taxes = _administrationService.GetAllTaxes(false);
            var model = new Models.ViewModels.Administration.TaxesViewModel();
            foreach (var tax in taxes)
            {
                var taxVM = new Models.ViewModels.Administration.TaxViewModel();
                taxVM.IsActive = tax.IsActive;
                taxVM.PurchasingSaAccountId = tax.PurchasingSaAccountId;
                taxVM.InvoicingSaAccountId = tax.InvoicingSaAccountId;
                taxVM.TaxCode = tax.TaxCode;
                taxVM.TaxName = tax.TaxName;
                taxVM.Rate = tax.Rate;
                model.Taxes.Add(taxVM);
            }
            return View(model);
        }

        public ActionResult ItemTaxGroups()
        {
            var itemTaxGroups = _administrationService.GetItemTaxGroups(true);
            var model = new List<ItemTaxGroupViewModel>();
            foreach (var itemTax in itemTaxGroups)
            {
                var m = new ItemTaxGroupViewModel();
                m.Id = itemTax.Id;
                m.IsFullyExempt = itemTax.IsFullyExempt;
                m.Name = itemTax.Name;
                foreach(var itemTaxGroup in itemTax.ItemTaxGroupTax)
                {
                    var m_ = new ItemTaxGroupTaxViewModel();
                    m_.IsExempt = itemTaxGroup.IsExempt;
                    m_.TaxName = itemTaxGroup.Tax.TaxName;
                    m_.TaxId = itemTaxGroup.TaxId;
                    m_.Rate = itemTaxGroup.Tax.Rate;
                    m.ItemTaxGroupTaxes.Add(m_);
                }
                model.Add(m);
            }
            return View(model);
        }
        public ActionResult AddItemTaxGroup(ItemTaxGroup itemTaxGroup)
        {
            var itemTaxGroups = _administrationService.AddNewItemTaxGroup(itemTaxGroup);
            var model = new List<ItemTaxGroupViewModel>();
            return View(model);
        }
        public ActionResult ItemTaxGroup(long id)
        {
            var itemTaxGroup = _administrationService.GetItemTaxGroups(true).FirstOrDefault(t => t.Id == id);
            var model = new ItemTaxGroupViewModel();
            foreach (var m in itemTaxGroup.ItemTaxGroupTax)
            {
                var m_ = new ItemTaxGroupTaxViewModel();
                m_.IsExempt = m.IsExempt;
                m_.TaxName = m.Tax.TaxName;
                m_.TaxId = m.TaxId;
                m_.Rate = m.Tax.Rate;
                model.ItemTaxGroupTaxes.Add(m_);
            }
            return View(model);
        }

        public ActionResult TaxGroups()
        {
            var taxGroups = _administrationService.GetTaxGroups(false);
            return View();
        }

        public ActionResult LegalEntities()
        {
            var legalEntities = _administrationService.GetAllLegalEntities(false);
            var model = new SmartAccountsUI.Models.ViewModels.Administration.LegalEntities();
            if (legalEntities != null)
            {
                foreach (var legalEntity in legalEntities)
                {
                    model.LegalEntitiesList.Add(new Models.ViewModels.Administration.LegalEntitiesListLine()
                    {
                        LegalEntityId = legalEntity.LegalEntityId,
                        LegalEntityName = legalEntity.LegalEntityName
                    });
                }
            }
            return View(model);
        }

        public ActionResult AddOrEditLegalEntities(Guid id = new Guid())
        {
            LegalEntity legalentity = null;
            var model = new LegalEntity();
            model.LegalEntityId = id;
            if (id != Guid.Empty)
            {
                //legalentity = _administrationService.GetLegalEntityById(id, false);
                //model.LegalEntityName = legalentity.LegalEntityName;
                //model.LegalEntityParentId = legalentity.LegalEntityParentId;
                //model.ParentRelationshipType = legalentity.ParentRelationshipType;
            }

            return View(model);
        }

        [HttpPost, ActionName("AddOrEditLegalEntity")]
        [FormValueRequiredAttribute("SaveLegalEntity")]
        public ActionResult AddOrEditLegalEntity(LegalEntity model)
        {
            LegalEntity legalEntity = null;
            if (model.LegalEntityId != Guid.Empty)
            {
                legalEntity = _administrationService.GetLegalEntityById(model.LegalEntityId, false);
            }
            else
            {
                legalEntity = new LegalEntity();

            }

            legalEntity.LegalEntityName = model.LegalEntityName;


            if (model.LegalEntityId != Guid.Empty)
                _administrationService.UpdateLegalEntity(legalEntity);
            else
                _administrationService.AddNewLegalEntity(legalEntity);

            return RedirectToAction("LegalEntity");
        }

        [HttpPost, ActionName("AddLegalEntity")]
        [FormValueRequiredAttribute("SaveLegalEntity")]
        public ActionResult AddLegalEntity(Models.ViewModels.Administration.AddLegalEntity model)
        {
            var legalEntity = new LegalEntity()
            {
                //Name = model.LegalEntityName,
                //AccountsPayableSaAccountId = model.AccountsPayableSaAccountId.Value == -1 ? null : model.AccountsPayableSaAccountId,
                //PurchaseSaAccountId = model.PurchaseSaAccountId.Value == -1 ? null : model.PurchaseSaAccountId,
                //PurchaseDiscountSaAccountId = model.PurchaseDiscountSaAccountId.Value == -1 ? null : model.PurchaseDiscountSaAccountId,
                //CreatedBy = User.Identity.Name,
                //CreatedOn = DateTime.Now,
                //ModifiedBy = User.Identity.Name,
                //ModifiedOn = DateTime.Now
            };
            //_administrationService.AddLegalEntity(legalEntity);
            return RedirectToAction("LegalEntities");
        }





    }
}
