﻿using System;
using System.Linq;
using System.Web.Mvc;
using SACore.Domain.Financials;
using SAServices.Financial;
using SmartAccountsUI.Extensions;
using SmartAccountsUI.Models;
using SmartAccountsUI.Models.ViewModels.Financials;
using System.Collections.Generic;
using System.Net;
using SACore.Domain;

namespace SmartAccountsUI.Controllers
{
    [Authorize]
    public class FinancialController : BaseController
    {
        private readonly IFinancialService _financialService;

        public FinancialController(IFinancialService financialService)
        {
            _financialService = financialService;
        }

        public ActionResult SaAccountIndex()
        {
            var saSaAccounts = _financialService.GetSaAccounts();
            var model = new SaAccounts();
            foreach(var saSaAccount in saSaAccounts)
            {
                model.SaAccountsListLines.Add(new SaAccountsListLine()
                {
                    Id = saSaAccount.Id,
                    SaAccountCode = saSaAccount.SaAccountCode,
                    SaAccountName = saSaAccount.SaAccountName,
                    Balance = saSaAccount.Balance
                });
            }
            return View(model);
        }


        // GET: /SaAccount/Create
        public ActionResult SaAccountEdit(long id = 0)
        {
            SaAccountEdit model = new SaAccountEdit();
            if (id != 0)
            {
                SaAccount saAccount = _financialService.GetSaAccountById(id);
                model.Id = saAccount.Id;
                model.SaAccountName = saAccount.SaAccountName;
                model.ParentSaAccountId = saAccount.ParentSaAccountId ?? -1;
                model.SaAccountClassId = saAccount.SaAccountClassId;
                model.SaAccountSubClassId = saAccount.SaAccountSubClassId ?? -1;
                model.SaAccountType = saAccount.SaAccountType;
                model.SaAccountCode = saAccount.SaAccountCode;
            }
            return View(model);

        }

        [HttpPost, ActionName("SaAccountEdit")]
        [FormValueRequired("SaveSaAccount")]
        public ActionResult Edit(SaAccountEdit model)
        {

            SACore.Domain.Financials.SaAccount saAccount = null;
            if (model.Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (model.Id == 0)
            {
                saAccount = new SACore.Domain.Financials.SaAccount();
                model.SaAccountCode = _financialService.CreateSaAccountCode(model.ParentSaAccountId, model.SaAccountType, model.SaAccountSubClassId, model.SaAccountClassId);
            }
            else
            {
               saAccount = _financialService.GetSaAccountById(model.Id);
            }

            saAccount.SaAccountCode = model.SaAccountCode;
            saAccount.SaAccountName = model.SaAccountName;
            saAccount.ParentSaAccountId = model.ParentSaAccountId;
            saAccount.SaAccountSubClassId = saAccount.SaAccountSubClassId ?? -1;
            saAccount.SaAccountType = saAccount.SaAccountType;
            saAccount.SaAccountSubClassId = saAccount.SaAccountSubClassId ?? -1;
            saAccount.Description = model.Description;
            
            if (model.Id != 0)
                _financialService.UpdateSaAccount(saAccount);
            else
                _financialService.AddSaAccount(saAccount);

            return RedirectToAction("SaAccountIndex");

        }

        public ActionResult ViewSaAccountsPDF()
        {
            var saSaAccounts = _financialService.GetSaAccounts();
            var model = new SaAccounts();
            foreach (var saSaAccount in saSaAccounts)
            {
                model.SaAccountsListLines.Add(new SaAccountsListLine()
                {
                    Id = saSaAccount.Id,
                    SaAccountCode = saSaAccount.SaAccountCode,
                    SaAccountName = saSaAccount.SaAccountName,
                    Balance = saSaAccount.Balance
                });
            }

            var html = base.RenderPartialViewToString("SaAccounts", model);
            HttpContext.Response.Clear();
            HttpContext.Response.AddHeader("Content-Type", "application/pdf");
            HttpContext.Response.Filter = new PdfFilter(HttpContext.Response.Filter, html);
            
            var rtn = Json(html, JsonRequestBehavior.AllowGet);
            PdfPrint pPdf = new PdfPrint();
            pPdf.print(html);

            return null;

        }


        public ActionResult ViewGeneralLedgerMasterPDF()
        {
            var masterGeneralLedger = _financialService.MasterGeneralLedger();
            var model = new SmartAccountsUI.Models.ViewModels.Financials.MasterGeneralLedger();
            foreach (var mLedger in masterGeneralLedger)
            {
                model.MasterGeneralLedgerListLines.Add(new MasterGeneralLedgerListLine()
                {
                   TransactionNo = mLedger.TransactionNo, SaAccountCode = mLedger.SaAccountCode, Credit = mLedger.Credit, Debit = mLedger.Debit
                });
            }

            var html = base.RenderPartialViewToString("MasterGeneralLedgerListLine", model);
            HttpContext.Response.Clear();
            HttpContext.Response.AddHeader("Content-Type", "application/pdf");
            HttpContext.Response.Filter = new PdfFilter(HttpContext.Response.Filter, html);

            var rtn = Json(html, JsonRequestBehavior.AllowGet);
            PdfPrint pPdf = new PdfPrint();
            pPdf.print(html);

            return null;

        }

        public ActionResult JournalEntries()
        {
            var journalEntries = _financialService.GetJournalEntries();
            var model = new JournalEntries();
            foreach(var je in journalEntries)
            {
                foreach (var line in je.JournalEntryLines)
                {
                    model.JournalEntriesListLines.Add(new JournalEntriesListLine()
                    {
                        SaAccountId = line.SaAccountId,
                        SaAccountCode = line.SaAccount.SaAccountCode,
                        SaAccountName = line.SaAccount.SaAccountName,
                        DrCr = (int)line.DrCr == 1 ? "Dr" : "Cr",
                        Amount = line.Amount,
                        Date = je.Date
                    });
                }
            }
            return View(model);
        }

        public ActionResult AddJournalEntry()
        {
            var model = new AddJournalEntry();
            return View(model);
        }

        [HttpPost, ActionName("AddJournalEntry")]
        [FormValueRequired("AddJournalEntryLine")]
        public ActionResult AddJournalEntryLine(AddJournalEntry model)
        {
            if(model.SaAccountId != -1 && model.Amount > 0)
            {
                var rowId = Guid.NewGuid().ToString();
                model.AddJournalEntryLines.Add(new AddJournalEntryLine()
                {
                    RowId = rowId,
                    SaAccountId = model.SaAccountId,
                    SaAccountName = _financialService.GetSaAccounts().Where(a => a.Id == model.SaAccountId).FirstOrDefault().SaAccountName,
                    DrCr = model.DrCr,
                    Amount = model.Amount,
                    Memo = model.MemoLine,

                });

            }
            return View(model);
        }

        [HttpPost, ActionName("AddJournalEntry")]
        [FormValueRequired("DeleteJournalEntryLine")]
        public ActionResult DeleteJournalEntryLine(AddJournalEntry model)
        {
            var request = HttpContext.Request;
            var deletedItem = request.Form["DeletedLineItem"];
            model.AddJournalEntryLines.Remove(model.AddJournalEntryLines.Where(i => i.RowId.ToString() == deletedItem.ToString()).FirstOrDefault());
            return View(model);
        }

        [HttpPost, ActionName("AddJournalEntry")]
        [FormValueRequired("SaveJournalEntry")]
        public ActionResult SaveJournalEntry(AddJournalEntry model)
        {
            if(model.AddJournalEntryLines.Count < 2)
                return View(model);
            var journalEntry = new JournalEntryHeader()
            {
                Date = DateTime.Now,
                Memo = model.Memo,
                ReferenceNo = model.ReferenceNo,
            };
            foreach(var line in model.AddJournalEntryLines)
            {
                journalEntry.JournalEntryLines.Add(new JournalEntryLine()
                {
                    SaAccountId = line.SaAccountId,
                    DrCr = line.DrCr,
                    Amount = line.Amount,
                    Memo = line.Memo,
                    
                });
            }

            SAServices.ErrorHandler.Errors eh = new SAServices.ErrorHandler.Errors();
            eh = _financialService.AddJournalEntry(journalEntry);


            if (eh.NoErrors)
            { 
                model.ErrorMsgLine = "";
                AddJournalEntryLine ajel = new AddJournalEntryLine();
                List<AddJournalEntryLine> lAJ = new List<AddJournalEntryLine>();

                model.AddJournalEntryLines = lAJ;
            }
            else
            {
                model.ErrorMsgLine = "Unable to save: " + eh.ErrorMsg;
            }
            

            eh.NoErrors = true;
            return View(model);
        }

        public ActionResult MasterGeneralLedger()
        {
            var model = _financialService.MasterGeneralLedger();
            return View(model);
        }

        public ActionResult TrialBalance()
        {
            var model = _financialService.TrialBalance();
            return View(model);
        }

        public ActionResult BalanceSheet()
        {
            var model = _financialService.BalanceSheet();
            return View(model);
        }

        public ActionResult IncomeStatement()
        {
            var model = _financialService.IncomeStatement();
            return View(model);
        }

       

        public ActionResult GeneralLedgerSettings()
        {
            var glSetting = _financialService.GetGeneralLedgerSettings();
            var model = new GeneralLedgerSettingsViewModel();
            model.Id = glSetting.Id;
            model.CompanyId = glSetting.CompanyId;
            model.PayableSaAccountId = glSetting.PayableSaAccountId;
            model.PurchaseDiscountSaAccountId = glSetting.PurchaseDiscountSaAccountId;
            model.InvoicingDiscountSaAccountId = glSetting.InvoicingDiscountSaAccountId;
            model.ShippingChargeSaAccountId = glSetting.ShippingChargeSaAccountId;
            model.GoodsReceiptNoteClearingSaAccountId = glSetting.GoodsReceiptNoteClearingSaAccountId;
            return View(model);
        }

        [HttpPost, ActionName("GeneralLedgerSettings")]
        [FormValueRequired("Save")]
        public ActionResult GeneralLedgerSettings(GeneralLedgerSettingsViewModel model)
        {
            var glSetting = _financialService.GetGeneralLedgerSettings();
            glSetting.CompanyId = model.CompanyId;
            glSetting.PayableSaAccountId = model.PayableSaAccountId;
            glSetting.PurchaseDiscountSaAccountId = model.PurchaseDiscountSaAccountId;
            glSetting.InvoicingDiscountSaAccountId = model.InvoicingDiscountSaAccountId;
            glSetting.ShippingChargeSaAccountId = model.ShippingChargeSaAccountId;
            glSetting.GoodsReceiptNoteClearingSaAccountId = model.GoodsReceiptNoteClearingSaAccountId;
            _financialService.UpdateGeneralLedgerSettings(glSetting);
            return View(model);
        }
    }
}
