﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SAServices.Financial;

namespace WebApplication3.Controllers
{
    public class SaAccountsController : Controller
    {
        private readonly IFinancialService _financialService;

        public SaAccountsController(IFinancialService financialService)
        {
            _financialService = financialService;
        }

        // GET: /SaAccounts/
        public ActionResult Index()
        {
            var saaccounts = _financialService.GetSaAccounts();
            return View(saaccounts.ToList());
        }

        // GET: /SaAccounts/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SaAccount saaccount = db.SaAccounts.Find(id);
            if (saaccount == null)
            {
                return HttpNotFound();
            }
            return View(saaccount);
        }

        // GET: /SaAccounts/Create
        public ActionResult Create()
        {
            ViewBag.ParentSaAccountId = new SelectList(db.SaAccounts, "Id", "SaAccountCode");
            ViewBag.SaAccountClassId = new SelectList(db.SaAccountClasses, "Id", "Name");
            return View();
        }

        // POST: /SaAccounts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,LegalEntityId,SaAccountClassId,ParentSaAccountId,SaAccountType,SaAccountCode,SaAccountName,Description,IsCash,RowVersion")] SaAccount saaccount)
        {
            if (ModelState.IsValid)
            {
                db.SaAccounts.Add(saaccount);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ParentSaAccountId = new SelectList(db.SaAccounts, "Id", "SaAccountCode", saaccount.ParentSaAccountId);
            ViewBag.SaAccountClassId = new SelectList(db.SaAccountClasses, "Id", "Name", saaccount.SaAccountClassId);
            return View(saaccount);
        }

        // GET: /SaAccounts/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SaAccount saaccount = db.SaAccounts.Find(id);
            if (saaccount == null)
            {
                return HttpNotFound();
            }
            ViewBag.ParentSaAccountId = new SelectList(db.SaAccounts, "Id", "SaAccountCode", saaccount.ParentSaAccountId);
            ViewBag.SaAccountClassId = new SelectList(db.SaAccountClasses, "Id", "Name", saaccount.SaAccountClassId);
            return View(saaccount);
        }

        // POST: /SaAccounts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,LegalEntityId,SaAccountClassId,ParentSaAccountId,SaAccountType,SaAccountCode,SaAccountName,Description,IsCash,RowVersion")] SaAccount saaccount)
        {
            if (ModelState.IsValid)
            {
                db.Entry(saaccount).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ParentSaAccountId = new SelectList(db.SaAccounts, "Id", "SaAccountCode", saaccount.ParentSaAccountId);
            ViewBag.SaAccountClassId = new SelectList(db.SaAccountClasses, "Id", "Name", saaccount.SaAccountClassId);
            return View(saaccount);
        }

        // GET: /SaAccounts/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SaAccount saaccount = db.SaAccounts.Find(id);
            if (saaccount == null)
            {
                return HttpNotFound();
            }
            return View(saaccount);
        }

        // POST: /SaAccounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            SaAccount saaccount = db.SaAccounts.Find(id);
            db.SaAccounts.Remove(saaccount);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
