﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication3;

namespace WebApplication3.Controllers
{
    public class CustomerAllocationController : Controller
    {
        private SAEntities db = new SAEntities();

        // GET: /CustomerAllocation/
        public ActionResult Index()
        {
            var customerallocations = db.CustomerAllocations.Include(c => c.Customer).Include(c => c.InvoicingInvoiceHeader).Include(c => c.InvoicingReceiptHeader);
            return View(customerallocations.ToList());
        }

        // GET: /CustomerAllocation/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CustomerAllocation customerallocation = db.CustomerAllocations.Find(id);
            if (customerallocation == null)
            {
                return HttpNotFound();
            }
            return View(customerallocation);
        }

        // GET: /CustomerAllocation/Create
        public ActionResult Create()
        {
            ViewBag.CustomerId = new SelectList(db.Customers, "Id", "No");
            ViewBag.InvoicingInvoiceHeaderId = new SelectList(db.InvoicingInvoiceHeaders, "Id", "No");
            ViewBag.InvoicingReceiptHeaderId = new SelectList(db.InvoicingReceiptHeaders, "Id", "No");
            return View();
        }

        // POST: /CustomerAllocation/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,CustomerId,InvoicingInvoiceHeaderId,InvoicingReceiptHeaderId,Date,Amount")] CustomerAllocation customerallocation)
        {
            if (ModelState.IsValid)
            {
                db.CustomerAllocations.Add(customerallocation);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CustomerId = new SelectList(db.Customers, "Id", "No", customerallocation.CustomerId);
            ViewBag.InvoicingInvoiceHeaderId = new SelectList(db.InvoicingInvoiceHeaders, "Id", "No", customerallocation.InvoicingInvoiceHeaderId);
            ViewBag.InvoicingReceiptHeaderId = new SelectList(db.InvoicingReceiptHeaders, "Id", "No", customerallocation.InvoicingReceiptHeaderId);
            return View(customerallocation);
        }

        // GET: /CustomerAllocation/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CustomerAllocation customerallocation = db.CustomerAllocations.Find(id);
            if (customerallocation == null)
            {
                return HttpNotFound();
            }
            ViewBag.CustomerId = new SelectList(db.Customers, "Id", "No", customerallocation.CustomerId);
            ViewBag.InvoicingInvoiceHeaderId = new SelectList(db.InvoicingInvoiceHeaders, "Id", "No", customerallocation.InvoicingInvoiceHeaderId);
            ViewBag.InvoicingReceiptHeaderId = new SelectList(db.InvoicingReceiptHeaders, "Id", "No", customerallocation.InvoicingReceiptHeaderId);
            return View(customerallocation);
        }

        // POST: /CustomerAllocation/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,CustomerId,InvoicingInvoiceHeaderId,InvoicingReceiptHeaderId,Date,Amount")] CustomerAllocation customerallocation)
        {
            if (ModelState.IsValid)
            {
                db.Entry(customerallocation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CustomerId = new SelectList(db.Customers, "Id", "No", customerallocation.CustomerId);
            ViewBag.InvoicingInvoiceHeaderId = new SelectList(db.InvoicingInvoiceHeaders, "Id", "No", customerallocation.InvoicingInvoiceHeaderId);
            ViewBag.InvoicingReceiptHeaderId = new SelectList(db.InvoicingReceiptHeaders, "Id", "No", customerallocation.InvoicingReceiptHeaderId);
            return View(customerallocation);
        }

        // GET: /CustomerAllocation/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CustomerAllocation customerallocation = db.CustomerAllocations.Find(id);
            if (customerallocation == null)
            {
                return HttpNotFound();
            }
            return View(customerallocation);
        }

        // POST: /CustomerAllocation/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            CustomerAllocation customerallocation = db.CustomerAllocations.Find(id);
            db.CustomerAllocations.Remove(customerallocation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
