﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SACore.Domain.Receivables;
using SAServices.Financial;
using SAServices.Inventory;
using SAServices.Receivables;
using SmartAccountsUI.Models;
using SmartAccountsUI.Models.ViewModels;
using SmartAccountsUI.Models.ViewModels.Invoicing;
using SACore.Domain;
using SACore.Domain.MasterTables;
using CustomerAllocation = SmartAccountsUI.Models.ViewModels.Invoicing.CustomerAllocation;

namespace SmartAccountsUI.Controllers
{
    [Authorize]
    public class InvoicingController : BaseController
    {
        private InvoicingViewModelBuilder _viewModelBuilder;

        private readonly IInventoryService _inventoryService;
        private readonly IFinancialService _financialService;
        private readonly IInvoicingService _invoicingService;

        public InvoicingController(IInventoryService inventoryService, IFinancialService financialService, IInvoicingService invoicingService)
        {
            _inventoryService = inventoryService;
            _financialService = financialService;
            _invoicingService = invoicingService;

            _viewModelBuilder = new InvoicingViewModelBuilder(inventoryService, financialService, invoicingService);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult InvoicingOrders()
        {
            var invoicingOrders = _invoicingService.GetInvoicingOrders();
            var model = _viewModelBuilder.CreateInvoicingOrdersViewModel(invoicingOrders.ToList());
            return View(model);
        }

        public ActionResult InvoicingDeliveries()
        {
            var invoicingDeliveries = _invoicingService.GetInvoicingDeliveries();
            var model = _viewModelBuilder.CreateInvoicingDeliveriesViewModel(invoicingDeliveries.ToList());
            return View(model);
        }

        public ActionResult AddInvoicingDelivery(bool direct = false)
        {
            if (direct)
            {
                var model = _viewModelBuilder.CreateInvoicingDeliveryViewModel();
                return View(model);
            }
            return RedirectToAction("InvoicingDeliveries");
        }

        [HttpPost, ActionName("AddInvoicingDelivery")]
        [FormValueRequired("SaveInvoicingDelivery")]
        public ActionResult SaveInvoicingDelivery(InvoicingDeliveryViewModel model)
        {
            var invoicingDelivery = new InvoicingDeliveryHeader()
            {
                CustomerId = model.CustomerId,
                PaymentTermId = model.PaymentTermId,
                Date = model.Date,
                CreatedBy = User.Identity.Name,
                CreatedOn = DateTime.Now,
                ModifiedBy = User.Identity.Name,
                ModifiedOn = DateTime.Now,
            };
            foreach(var line in model.InvoicingDeliveryLines)
            {
                invoicingDelivery.InvoicingDeliveryLines.Add(new InvoicingDeliveryLine()
                {
                    ItemId = line.ItemId,
                    MeasurementId = line.MeasurementId,
                    Quantity = line.Quantity,
                    Discount = line.Discount,
                    Price = line.Quantity * line.Price,
                    CreatedBy = User.Identity.Name,
                    CreatedOn = DateTime.Now,
                    ModifiedBy = User.Identity.Name,
                    ModifiedOn = DateTime.Now,
                });
            }
            _invoicingService.AddInvoicingDelivery(invoicingDelivery, true);
            return RedirectToAction("InvoicingDeliveries");
        }

        [HttpPost, ActionName("AddInvoicingDelivery")]
        [FormValueRequired("AddInvoicingDeliveryLineItem")]
        public ActionResult AddInvoicingDeliveryLineItem(InvoicingDeliveryViewModel model)
        {
            var item = _inventoryService.GetItemById(model.ItemId.Value);
            var line = new InvoicingDeliveryLineViewModel()
            {
                ItemId = model.ItemId,
                MeasurementId = model.MeasurementId,
                Quantity = model.Quantity,
                Price = item.Price.Value * model.Quantity,
                Discount  = model.Discount,
                LineTotalTaxAmount = item.ItemTaxAmountOutput * model.Quantity
            };

            model.InvoicingDeliveryLines.Add(line);
            return View(model);
        }

        [HttpPost, ActionName("AddInvoicingDelivery")]
        [FormValueRequired("DeleteSaleDeliveryLineItem")]
        public ActionResult DeleteSaleDeliveryLineItem(InvoicingDeliveryViewModel model)
        {
            var request = HttpContext.Request;
            var deletedItem = request.Form["DeletedLineItem"];
            model.InvoicingDeliveryLines.Remove(model.InvoicingDeliveryLines.Where(i => i.ItemId == int.Parse(deletedItem.ToString())).FirstOrDefault());
            return View(model);
        }

        public ActionResult InvoicingInvoices()
        {
            var invoices = _invoicingService.GetInvoicingInvoices();
            var model = new InvoicingInvoices();
            foreach(var invoice in invoices)
            {
                model.InvoicingInvoiceListLines.Add(new InvoicingInvoiceListLine()
                {
                    Id = invoice.Id,
                    No = invoice.No,
                    Customer = invoice.Customer.Name,
                    Date = invoice.Date,
                    Amount = invoice.ComputeTotalAmount(),
                    IsFullPaid = invoice.IsFullPaid()
                });
            }
            return View(model);
        }

        public ActionResult AddInvoicingInvoice(bool direct = false)
        {
            var model = new AddInvoicingInvoice();
            model.Customers = ModelViewHelper.Customers(_invoicingService.GetCustomers());
            model.Items = ModelViewHelper.Items(_inventoryService.GetAllItems());
            model.Measurements = ModelViewHelper.Measurements(_inventoryService.GetMeasurements());

            return View(model);
        }

        [HttpPost, ActionName("AddInvoicingInvoice")]
        [FormValueRequired("SaveInvoicingInvoice")]
        public ActionResult SaveInvoicingInvoice(AddInvoicingInvoice model)
        {
            if (model.AddInvoicingInvoiceLines.Sum(i => i.Amount) == 0 || model.AddInvoicingInvoiceLines.Count < 1)
            {
                model.Customers = ModelViewHelper.Customers(_invoicingService.GetCustomers());
                model.Items = ModelViewHelper.Items(_inventoryService.GetAllItems());
                model.Measurements = ModelViewHelper.Measurements(_inventoryService.GetMeasurements());
                ModelState.AddModelError("Amount", "No invoice line");
                return View(model);
            }
            var invoiceHeader = new InvoicingInvoiceHeader();
            var invoiceLines = new List<InvoicingInvoiceLine>();
            foreach (var item in model.AddInvoicingInvoiceLines)
            {
                var Item = _inventoryService.GetItemById(item.ItemId);
                var invoiceDetail = new InvoicingInvoiceLine();
                invoiceDetail.TaxId = Item.ItemTaxGroupId;
                invoiceDetail.ItemId = item.ItemId;
                invoiceDetail.MeasurementId = item.MeasurementId;
                invoiceDetail.Quantity = item.Quantity;
                invoiceDetail.Discount = item.Discount;
                invoiceDetail.Amount = Convert.ToDecimal(item.Quantity * Item.Price);
                invoiceDetail.CreatedBy = User.Identity.Name;
                invoiceDetail.CreatedOn = DateTime.Now;
                invoiceDetail.ModifiedBy = User.Identity.Name;
                invoiceDetail.ModifiedOn = DateTime.Now;
                invoiceLines.Add(invoiceDetail);
            }
            invoiceHeader.InvoicingInvoiceLines = invoiceLines;
            invoiceHeader.CustomerId = model.CustomerId;
            invoiceHeader.Date = model.Date;
            invoiceHeader.ShippingHandlingCharge = 4;// model.ShippingHandlingCharge;
            invoiceHeader.CreatedBy = User.Identity.Name;
            invoiceHeader.CreatedOn = DateTime.Now;
            invoiceHeader.ModifiedBy = User.Identity.Name;
            invoiceHeader.ModifiedOn = DateTime.Now;

            _invoicingService.AddInvoicingInvoice(invoiceHeader, model.InvoicingOrderId);
            return RedirectToAction("InvoicingInvoices");
        }

        [HttpPost, ActionName("AddInvoicingInvoice")]
        [FormValueRequired("AddInvoicingInvoiceLine")]
        public ActionResult AddInvoicingInvoiceLine(AddInvoicingInvoice model)
        {
            model.Customers = ModelViewHelper.Customers(_invoicingService.GetCustomers());
            model.Items = ModelViewHelper.Items(_inventoryService.GetAllItems());
            model.Measurements = ModelViewHelper.Measurements(_inventoryService.GetMeasurements());
            if (model.Quantity > 0)
            {
                var item = _inventoryService.GetItemById(model.ItemId);
                if (!item.Price.HasValue)
                {
                    ModelState.AddModelError("Amount", "Selling price is not set.");
                    return View(model);
                }
                AddInvoicingInvoiceLine itemModel = new AddInvoicingInvoiceLine()
                {
                    ItemId = model.ItemId,
                    MeasurementId = model.MeasurementId,
                    Quantity = model.Quantity,
                    Discount = model.Discount,
                    Amount = item.Price.Value * model.Quantity,
                    Price = item.Price.Value,
                };
                if (model.AddInvoicingInvoiceLines.FirstOrDefault(i => i.ItemId == model.ItemId) == null)
                    model.AddInvoicingInvoiceLines.Add(itemModel);
            }
            return View(model);
        }

        [HttpPost, ActionName("AddInvoicingInvoice")]
        [FormValueRequired("DeleteInvoiceLineItem")]
        public ActionResult DeleteInvoiceLineItem(AddInvoicingInvoice model)
        {
            model.Customers = ModelViewHelper.Customers(_invoicingService.GetCustomers());
            model.Items = ModelViewHelper.Items(_inventoryService.GetAllItems());
            model.Measurements = ModelViewHelper.Measurements(_inventoryService.GetMeasurements());

            var request = HttpContext.Request;
            var deletedItem = request.Form["DeletedLineItem"];
            model.AddInvoicingInvoiceLines.Remove(model.AddInvoicingInvoiceLines.Where(i => i.ItemId == int.Parse(deletedItem.ToString())).FirstOrDefault());

            return View(model);
        }

        public ActionResult AddInvoicingReceipt(long? invoicingInvoiceId = null)
        {
            var model = new AddInvoicingReceipt();
            if (invoicingInvoiceId.HasValue)
            {
                var invoicingInvoice = _invoicingService.GetInvoicingInvoiceById(invoicingInvoiceId.Value);
                model.InvoicingInvoiceId = invoicingInvoice.Id;
                model.InvoicingInvoiceNo = invoicingInvoice.No;
                model.InvoiceDate = invoicingInvoice.Date;
                model.Date = DateTime.Now;
                model.CustomerId = invoicingInvoice.CustomerId;

                foreach (var line in invoicingInvoice.InvoicingInvoiceLines)
                {
                    model.AddInvoicingReceiptLines.Add(new AddInvoicingReceiptLine()
                    {
                        InvoicingInvoiceLineId = line.Id,
                        ItemId = line.ItemId,
                        MeasurementId = line.MeasurementId,
                        Quantity = line.Quantity,
                        Discount = line.Discount,
                        Amount = line.Amount
                    });
                }
            }
            return View(model);
        }

        [HttpPost, ActionName("AddInvoicingReceipt")]
        [FormValueRequired("SaveInvoicingReceipt")]
        public ActionResult SaveInvoicingReceipt(AddInvoicingReceipt model)
        {
            var receipt = new InvoicingReceiptHeader()
            {
                SaAccountToDebitId = model.SaAccountToDebitId,
                //InvoicingInvoiceHeaderId = model.InvoicingInvoiceId,
                CustomerId = model.CustomerId.Value,
                Date = model.Date,
                CreatedBy = User.Identity.Name,
                CreatedOn = DateTime.Now,
                ModifiedBy = User.Identity.Name,
                ModifiedOn = DateTime.Now,
            };
            foreach(var line in model.AddInvoicingReceiptLines)
            {
                if(line.AmountToPay > line.Amount)
                    return RedirectToAction("InvoicingInvoices");

                receipt.InvoicingReceiptLines.Add(new InvoicingReceiptLine()
                {
                    InvoicingInvoiceLineId = line.InvoicingInvoiceLineId,
                    ItemId = line.ItemId,
                    MeasurementId = line.MeasurementId,
                    Quantity = line.Quantity,
                    Discount = line.Discount,
                    Amount = line.Amount,
                    AmountPaid = line.AmountToPay,
                    CreatedBy = User.Identity.Name,
                    CreatedOn = DateTime.Now,
                    ModifiedBy = User.Identity.Name,
                    ModifiedOn = DateTime.Now,
                });
            }
            _invoicingService.AddInvoicingReceipt(receipt);
            return RedirectToAction("InvoicingInvoices");
        }

        public ActionResult Customers()
        {
            var customers = _invoicingService.GetCustomers();
            var model = new Customers();
            foreach(var customer in customers)
            {
                model.CustomerListLines.Add(new CustomerListLine()
                {
                    Id = customer.Id,
                    Name = customer.Name,
                    //FirstName = customer.FirstName,
                    //LastName = customer.LastName,
                    //Balance = customer.Balance
                });
            }
            return View(model);
        }

        public ActionResult Customer(long id = 0)
        {
            var model = new CustomerViewModel();
            if (id == 0)
            {
                model.Id = id;
            }
            else
            {
                var customer = _invoicingService.GetCustomerById(id);
                var allocations = _invoicingService.GetCustomerReceiptsForAllocation(id);
                model.Id = customer.Id;
                model.Name = customer.Name;
                //model.Balance = customer.Balance;
                foreach (var receipt in allocations)
                {
                    model.CustomerAllocations.Add(new CustomerAllocation()
                    {
                        Id = receipt.Id,
                        AmountAllocated = receipt.InvoicingReceiptLines.Sum(a => a.AmountPaid),
                        AvailableAmountToAllocate = receipt.AvailableAmountToAllocate
                    });
                }
                //foreach (var invoice in customer.InvoicingInvoices)
                //{
                //    model.CustomerInvoices.Add(new CustomerInvoicingInvoice()
                //    {
                //        Id = invoice.Id,
                //        InvoiceNo = invoice.No,
                //        Date = invoice.Date,
                //        Amount = invoice.InvoicingInvoiceLines.Sum(a => a.Amount),
                //        Status = invoice.IsFullPaid() ? "Paid" : "Open"
                //    });
                //}            
            }
            return View(model);
        }

        public ActionResult AddCustomer()
        {
            var model = new AddCustomer();
            return View(model);
        }

        [HttpPost, ActionName("AddCustomer")]
        [FormValueRequired("SaveCustomer")]
        public ActionResult AddCustomer(AddCustomer model)
        {
            var customer = new Customer()
            {
                Name = model.Name,
            };
            _invoicingService.AddCustomer(customer);
            return RedirectToAction("EditCustomer", new { id = customer.Id });
        }

        public ActionResult AddOrEditCustomer(long id = 0)
        {
            EditCustomer model = new EditCustomer();
            var saSaAccounts = _financialService.GetSaAccounts();
            if (id != 0)
            {
                var customer = _invoicingService.GetCustomerById(id);
                model.Id = customer.Id;
                model.Name = customer.Name;
                model.PrimaryContactId = customer.PrimaryContactId.HasValue ? customer.PrimaryContactId.Value : -1;
                model.AccountsReceivableSaAccountId = customer.AccountsReceivableSaAccountId.HasValue ? customer.AccountsReceivableSaAccountId : -1;
                model.InvoicingSaAccountId = customer.InvoicingSaAccountId.HasValue ? customer.InvoicingSaAccountId : -1;
                model.InvoicingDiscountSaAccountId = customer.InvoicingDiscountSaAccountId.HasValue ? customer.InvoicingDiscountSaAccountId : -1;
                model.InvoicingPenaltySaAccountId = customer.InvoicingPenaltySaAccountId.HasValue ? customer.InvoicingPenaltySaAccountId : -1;
            }
            return View(model);
        }

        [HttpPost, ActionName("AddOrEditCustomer")]
        [FormValueRequired("SaveCustomer")]
        public ActionResult AddOrEditCustomer(EditCustomer model)
        {
            Customer customer = null;
            if (model.Id != 0)
            {
                customer = _invoicingService.GetCustomerById(model.Id);                
            }
            else
            {
                customer = new Customer();

            }


            customer.Name = model.Name;
            if (model.PrimaryContactId != -1) customer.PrimaryContactId = model.PrimaryContactId;
            customer.AccountsReceivableSaAccountId = model.AccountsReceivableSaAccountId.Value == -1 ? null : model.AccountsReceivableSaAccountId;
            customer.InvoicingSaAccountId = model.InvoicingSaAccountId.Value == -1 ? null : model.InvoicingSaAccountId;
            customer.InvoicingDiscountSaAccountId = model.InvoicingDiscountSaAccountId.Value == -1 ? null : model.InvoicingDiscountSaAccountId;
            customer.InvoicingPenaltySaAccountId = model.InvoicingPenaltySaAccountId.Value == -1 ? null : model.InvoicingPenaltySaAccountId;

            if (model.Id != 0)
                _invoicingService.UpdateCustomer(customer);
            else
                _invoicingService.AddCustomer(customer);

            return RedirectToAction("Customers");
        }

        public ActionResult CustomerDetail(long id)
        {
            var customer = _invoicingService.GetCustomerById(id);
            var allocations = _invoicingService.GetCustomerReceiptsForAllocation(id);
            var model = new CustomerDetail()
            {
                Id = customer.Id,
                Name = customer.Name,
                //Balance = customer.Balance
            };
            foreach(var receipt in allocations)
            {
                model.CustomerAllocations.Add(new CustomerAllocation()
                {
                    Id = receipt.Id,
                    AmountAllocated = receipt.InvoicingReceiptLines.Sum(a => a.AmountPaid),
                    AvailableAmountToAllocate = receipt.AvailableAmountToAllocate
                });
            }
            //foreach (var allocation in customer.CustomerAllocations)
            //{
            //    model.ActualAllocations.Add(new Allocations()
            //    {
            //        InvoiceNo = allocation.InvoicingInvoiceHeader.No,
            //        ReceiptNo = allocation.InvoicingReceiptHeader.No,
            //        Date = allocation.Date,
            //        Amount = allocation.Amount
            //    });
            //}
            //foreach(var invoice in customer.InvoicingInvoices)
            //{
            //    model.CustomerInvoices.Add(new CustomerInvoicingInvoice()
            //    {
            //        Id = invoice.Id,
            //        InvoiceNo = invoice.No,
            //        Date = invoice.Date,
            //        Amount = invoice.ComputeTotalAmount(),
            //        Status = invoice.Status.ToString()
            //     });
            //}
            return View(model);
        }

        public ActionResult Allocate(long id)
        {
            var receipt = _invoicingService.GetInvoicingReceiptById(id);
            var customer = _invoicingService.GetCustomerById(receipt.CustomerId);
            var allocations = _invoicingService.GetCustomerReceiptsForAllocation(customer.Id);
            var model = new Allocate();
            model.ReceiptId = id;
            model.TotalAmountAvailableToAllocate = allocations.Sum(a => a.AvailableAmountToAllocate);
            model.LeftToAllocateFromReceipt = receipt.AvailableAmountToAllocate;
            var openInvoices = _invoicingService.GetInvoicingInvoices().Where(inv => inv.IsFullPaid() == false);
            foreach(var invoice in openInvoices)
            {
                model.OpenInvoices.Add(new SelectListItem()
                {
                    Value = invoice.Id.ToString(),
                    Text = invoice.No + " - " + (invoice.ComputeTotalAmount() - invoice.InvoicingInvoiceLines.Sum(a => a.GetAmountPaid()))
                });
            }
            return PartialView(model);
        }

        [HttpPost, ActionName("Allocate")]
        [FormValueRequired("SaveAllocation")]
        public ActionResult SaveAllocation(Allocate model)
        {
            var receipt = _invoicingService.GetInvoicingReceiptById(model.ReceiptId);
            var customer = _invoicingService.GetCustomerById(receipt.CustomerId);
            var invoice = _invoicingService.GetInvoicingInvoiceById(model.InvoiceId);
            var allocations = _invoicingService.GetCustomerReceiptsForAllocation(customer.Id);
            model.InvoiceId = model.InvoiceId;
            model.TotalAmountAvailableToAllocate = allocations.Sum(a => a.AvailableAmountToAllocate);
            model.LeftToAllocateFromReceipt = receipt.AvailableAmountToAllocate;
            if (invoice == null)
            {
                return View(model);
            }
            else
            {
                var invoiceTotalAmount = invoice.ComputeTotalAmount();
                if (model.AmountToAllocate > invoiceTotalAmount
                    || model.AmountToAllocate > receipt.AvailableAmountToAllocate
                    || invoice.Status == SACore.Domain.InvoicingInvoiceStatus.Closed)
                {
                    return View(model);
                }

                var allocation = new CustomerAllocation()
                {
                    //CustomerId = customer.Id,
                    //InvoicingReceiptHeaderId = receipt.Id,
                    //InvoicingInvoiceHeaderId = invoice.Id,
                    //Amount = model.AmountToAllocate,
                    //Date = DateTime.Now
                };
                //_invoicingService.SaveCustomerAllocation(allocation);
            }
            return RedirectToAction("CustomerDetail", new { id = customer.Id });
        }

        /// <summary>
        /// Add new receipt with no invoice
        /// </summary>
        /// <returns></returns>
        public ActionResult AddReceipt()
        {
            var model = new AddInvoicingReceipt();
            model.SaAccountToCreditId = _financialService.GetSaAccounts().Where(a => a.SaAccountName == "SaAccounts Receivable").FirstOrDefault().Id;
            return View(model);
        }

        [HttpPost, ActionName("AddReceipt")]
        [FormValueRequired("SaveReceipt")]
        public ActionResult SaveReceipt(AddInvoicingReceipt model)
        {
            if (model.SaAccountToDebitId.Value == -1 || model.CustomerId.Value == -1)
                return View(model);

            var receipt = new InvoicingReceiptHeader()
            {
                SaAccountToDebitId = model.SaAccountToDebitId,
                CustomerId = model.CustomerId.Value,
                Date = model.Date,
                CreatedBy = User.Identity.Name,
                CreatedOn = DateTime.Now,
                ModifiedBy = User.Identity.Name,
                ModifiedOn = DateTime.Now,
                Amount = model.PaymentAmount
            };

            receipt.InvoicingReceiptLines.Add(new InvoicingReceiptLine()
            {
                SaAccountToCreditId = model.SaAccountToCreditId,
                Amount = model.Amount,
                AmountPaid = model.PaymentAmount,
                CreatedBy = User.Identity.Name,
                CreatedOn = DateTime.Now,
                ModifiedBy = User.Identity.Name,
                ModifiedOn = DateTime.Now,
            });

            _invoicingService.AddInvoicingReceiptNoInvoice(receipt);
            return RedirectToAction("Receipts");
        }

        [HttpPost, ActionName("AddReceipt")]
        [FormValueRequired("AddReceiptItem")]
        public ActionResult AddReceiptItem(AddInvoicingReceipt model)
        {
            var rowId = Guid.NewGuid();
            if (model.ItemId.Value != -1 && model.Quantity > 0)
            {
                var item = _inventoryService.GetItemById(model.ItemId.Value);
                if (!item.Price.HasValue)
                {
                    ModelState.AddModelError("Amount", "Selling price is not set.");
                    return View(model);
                }
                AddInvoicingReceiptLine itemModel = new AddInvoicingReceiptLine()
                {
                    RowId = rowId.ToString(),
                    ItemId = model.ItemId.Value,
                    MeasurementId = model.MeasurementId.Value,
                    Quantity = model.Quantity,
                    Discount = model.Discount,
                    Amount = item.Price.Value * model.Quantity,
                    AmountToPay = model.AmountToPay
                };
                if (model.AddInvoicingReceiptLines.FirstOrDefault(i => i.ItemId == model.ItemId) == null)
                    model.AddInvoicingReceiptLines.Add(itemModel);
            }
            else if(!string.IsNullOrEmpty(model.SaAccountCode) && model.Amount != 0)
            {
                var saSaAccount = _financialService.GetSaAccounts().Where(a => a.SaAccountCode == model.SaAccountCode).FirstOrDefault();
                if(saSaAccount != null)
                {
                    AddInvoicingReceiptLine saSaAccountItemModel = new AddInvoicingReceiptLine()
                    {
                        RowId = rowId.ToString(),
                        SaAccountToCreditId = saSaAccount.Id,
                        Amount = model.AmountToPay,
                        AmountToPay = model.AmountToPay,
                    };
                    model.AddInvoicingReceiptLines.Add(saSaAccountItemModel);
                }                
            }
            return View(model);
        }

        [HttpPost, ActionName("AddReceipt")]
        [FormValueRequired("DeleteReceiptLineItem")]
        public ActionResult DeleteReceiptLineItem(AddInvoicingReceipt model)
        {
            var request = HttpContext.Request;
            var deletedItem = request.Form["DeletedLineItem"];
            model.AddInvoicingReceiptLines.Remove(model.AddInvoicingReceiptLines.Where(i => i.ItemId.ToString() == deletedItem.ToString()).FirstOrDefault());
            return View(model);
        }

        public ActionResult Receipts()
        {
            var receipts = _invoicingService.GetInvoicingReceipts();
            var model = new InvoicingReceipts();
            foreach(var receipt in receipts)
            {
                model.InvoicingReceiptListLines.Add(new InvoicingReceiptListLine()
                {
                    No = receipt.No,
                    //InvoiceNo = receipt.InvoicingInvoiceHeader != null ? receipt.InvoicingInvoiceHeader.No : string.Empty,
                    CustomerId = receipt.CustomerId,
                    CustomerName = receipt.Customer.Name,
                    Date = receipt.Date,
                    Amount = receipt.InvoicingReceiptLines.Sum(r => r.Amount),
                    AmountPaid = receipt.InvoicingReceiptLines.Sum(r => r.AmountPaid)
                });
            }
            return View(model);
        }

        public ActionResult InvoicingDelivery(long id = 0)
        {
            var model = new InvoicingHeaderViewModel(_inventoryService, _financialService);
            model.DocumentType = SACore.Domain.DocumentTypes.InvoicingDelivery;

            if (id == 0)
            {
                return View(model);
            }
            else
            {
                var invoice = _invoicingService.GetInvoicingInvoiceById(id);
                model.Id = invoice.Id;
                model.CustomerId = invoice.CustomerId;
                model.Date = invoice.Date;
                model.No = invoice.No;
                foreach (var line in invoice.InvoicingInvoiceLines)
                {
                    var lineItem = new InvoicingLineItemViewModel(_financialService);
                    lineItem.Id = line.Id;
                    lineItem.ItemId = line.ItemId;
                    lineItem.ItemNo = line.Item.No;
                    lineItem.ItemDescription = line.Item.Description;
                    lineItem.Measurement = line.Measurement.Description;
                    lineItem.Quantity = line.Quantity;
                    lineItem.Discount = line.Discount;
                    lineItem.Price = line.Amount;
                    model.InvoicingLine.InvoicingLineItems.Add(lineItem);
                }
                return View(model);
            }
        }

        public ActionResult InvoicingInvoice(long id = 0)
        {
            var model = new InvoicingHeaderViewModel(_inventoryService, _financialService);
            model.DocumentType = SACore.Domain.DocumentTypes.InvoicingInvoice;

            if (id == 0)
            {   
                return View(model);
            }
            else
            {
                var invoice = _invoicingService.GetInvoicingInvoiceById(id);
                model.Id = invoice.Id;
                model.CustomerId = invoice.CustomerId;
                model.Date = invoice.Date;
                model.No = invoice.No;
                foreach (var line in invoice.InvoicingInvoiceLines)
                {
                    var lineItem = new InvoicingLineItemViewModel(_financialService);
                    lineItem.SetServiceHelpers(_financialService);
                    lineItem.Id = line.Id;
                    lineItem.ItemId = line.ItemId;
                    lineItem.ItemNo = line.Item.No;
                    lineItem.ItemDescription = line.Item.Description;
                    lineItem.Measurement = line.Measurement.Description;
                    lineItem.Quantity = line.Quantity;
                    lineItem.Discount = line.Discount;
                    lineItem.Price = line.Amount;
                    model.InvoicingLine.InvoicingLineItems.Add(lineItem);
                }
                return View(model);
            }
        }

        public ActionResult InvoicingOrder(long id = 0)
        {
            var model = new InvoicingHeaderViewModel(_inventoryService, _financialService);
            model.DocumentType = SACore.Domain.DocumentTypes.InvoicingOrder;

            if (id == 0)
            {
                return View(model);
            }
            else
            {
                var order = _invoicingService.GetInvoicingOrderById(id);
                model.Id = order.Id;
                model.CustomerId = order.CustomerId;
                model.PaymentTermId = order.PaymentTermId;
                model.Date = order.Date;
                model.Reference = order.ReferenceNo;
                model.No = order.No;

                foreach (var line in order.InvoicingOrderLines)
                {
                    var lineItem = new InvoicingLineItemViewModel(_financialService);
                    lineItem.Id = line.Id;
                    lineItem.ItemId = line.ItemId;
                    lineItem.ItemNo = line.Item.No;
                    lineItem.ItemDescription = line.Item.Description;
                    lineItem.Measurement = line.Measurement.Description;
                    lineItem.Quantity = line.Quantity;
                    lineItem.Discount = line.Discount;
                    lineItem.Price = line.Amount;
                    model.InvoicingLine.InvoicingLineItems.Add(lineItem);
                }
                return View(model);
            }
        }

        [HttpPost, ActionName("InvoicingOrder")]
        [FormValueRequired("Save")]
        public ActionResult SaveOrder(InvoicingHeaderViewModel model)
        {
            InvoicingOrderHeader order = null;
            if (model.Id == 0)
            {
                order = new InvoicingOrderHeader();
                order.CreatedBy = User.Identity.Name;
                order.CreatedOn = DateTime.Now;
            }
            else
            {
                order = _invoicingService.GetInvoicingOrderById(model.Id);
            }

            order.ModifiedBy = User.Identity.Name;
            order.ModifiedOn = DateTime.Now;
            order.CustomerId = model.CustomerId.Value;
            order.PaymentTermId = model.PaymentTermId;
            order.Date = model.Date;

            foreach (var line in model.InvoicingLine.InvoicingLineItems)
            {
                InvoicingOrderLine lineItem = null;
                var item = _inventoryService.GetItemByNo(line.ItemNo);
                if (!line.Id.HasValue)
                {
                    lineItem = new InvoicingOrderLine();
                    lineItem.CreatedBy = User.Identity.Name;
                    lineItem.CreatedOn = DateTime.Now;
                    order.InvoicingOrderLines.Add(lineItem);
                }
                else
                {
                    lineItem = order.InvoicingOrderLines.Where(i => i.Id == line.Id).FirstOrDefault();
                }

                lineItem.ModifiedBy = User.Identity.Name;
                lineItem.ModifiedOn = DateTime.Now;
                lineItem.ItemId = line.ItemId;
                lineItem.MeasurementId = item.SellMeasurementId.Value;
                lineItem.Quantity = line.Quantity;
                lineItem.Discount = line.Discount;
                lineItem.Amount = line.Price;
            }

            if (model.Id == 0)
                _invoicingService.AddInvoicingOrder(order, true);
            else
                _invoicingService.UpdateInvoicingOrder(order);

            return RedirectToAction("InvoicingOrders");
        }

        [HttpPost, ActionName("InvoicingDelivery")]
        [FormValueRequired("Save")]
        public ActionResult SaveDelivery(InvoicingHeaderViewModel model)
        {
            InvoicingDeliveryHeader delivery = null;
            if (model.Id == 0)
            {
                delivery = new InvoicingDeliveryHeader();
                delivery.CreatedBy = User.Identity.Name;
                delivery.CreatedOn = DateTime.Now;
            }
            else
            {
                delivery = _invoicingService.GetInvoicingDeliveryById(model.Id);
            }

            delivery.ModifiedBy = User.Identity.Name;
            delivery.ModifiedOn = DateTime.Now;
            delivery.CustomerId = model.CustomerId.Value;
            delivery.PaymentTermId = model.PaymentTermId;
            delivery.Date = model.Date;

            foreach (var line in model.InvoicingLine.InvoicingLineItems)
            {
                InvoicingDeliveryLine lineItem = null;
                var item = _inventoryService.GetItemByNo(line.ItemNo);
                if (!line.Id.HasValue)
                {
                    lineItem = new InvoicingDeliveryLine();
                    lineItem.CreatedBy = User.Identity.Name;
                    lineItem.CreatedOn = DateTime.Now;
                    delivery.InvoicingDeliveryLines.Add(lineItem);
                }
                else
                {
                    lineItem = delivery.InvoicingDeliveryLines.Where(i => i.Id == line.Id).FirstOrDefault();
                }

                lineItem.ModifiedBy = User.Identity.Name;
                lineItem.ModifiedOn = DateTime.Now;
                lineItem.ItemId = line.ItemId;
                lineItem.MeasurementId = item.SellMeasurementId.Value;
                lineItem.Quantity = line.Quantity;
                lineItem.Discount = line.Discount;
                lineItem.Price = line.Price;
            }

            if (model.Id == 0)
            {
                _invoicingService.AddInvoicingDelivery(delivery, true);
            }
            else
            {
                
            }

            return RedirectToAction("InvoicingDeliveries");
        }


        [HttpPost, ActionName("InvoicingInvoice")]
        [FormValueRequired("Save")]
        public ActionResult SaveInvoice(InvoicingHeaderViewModel model)
        {
            InvoicingInvoiceHeader invoice = null;
            if (model.Id == 0)
            {
                invoice = new InvoicingInvoiceHeader();
                invoice.CreatedBy = User.Identity.Name;
                invoice.CreatedOn = DateTime.Now;
            }
            else
            {
                invoice = _invoicingService.GetInvoicingInvoiceById(model.Id);
            }

            invoice.ModifiedBy = User.Identity.Name;
            invoice.ModifiedOn = DateTime.Now;
            invoice.CustomerId = model.CustomerId.Value;
            invoice.Date = model.Date;
            invoice.ShippingHandlingCharge = model.ShippingHandlingCharges;
            
            foreach (var line in model.InvoicingLine.InvoicingLineItems)
            {
                InvoicingInvoiceLine lineItem = null;
                var item = _inventoryService.GetItemByNo(line.ItemNo);
                if (!line.Id.HasValue)
                {
                    lineItem = new InvoicingInvoiceLine();
                    lineItem.CreatedBy = User.Identity.Name;
                    lineItem.CreatedOn = DateTime.Now;
                    invoice.InvoicingInvoiceLines.Add(lineItem);
                }
                else
                {
                    lineItem = invoice.InvoicingInvoiceLines.Where(i => i.Id == line.Id).FirstOrDefault();
                }

                lineItem.ModifiedBy = User.Identity.Name;
                lineItem.ModifiedOn = DateTime.Now;
                lineItem.ItemId = line.ItemId;
                lineItem.MeasurementId = item.SellMeasurementId.Value;
                lineItem.Quantity = line.Quantity;
                lineItem.Discount = line.Discount;
                lineItem.Amount = line.Price;
            }

            if (model.Id == 0)
            {
                _invoicingService.AddInvoicingInvoice(invoice, null);
            }
            else
            {
                _invoicingService.UpdateInvoicingInvoice(invoice);
            }

            return RedirectToAction("InvoicingInvoices");
        }

        [HttpPost, ActionName("InvoicingOrder")]
        [FormValueRequired("AddLineItem")]
        public ActionResult AddLineItemOrder(InvoicingHeaderViewModel model)
        {
            AddLineItem(model);
            return ReturnView(model);
        }

        [HttpPost, ActionName("InvoicingInvoice")]
        [FormValueRequired("AddLineItem")]
        public ActionResult AddLineItemInvoice(InvoicingHeaderViewModel model)
        {
            AddLineItem(model);
            return ReturnView(model);
        }

        [HttpPost, ActionName("InvoicingDelivery")]
        [FormValueRequired("AddLineItem")]
        public ActionResult AddLineItemDelivery(InvoicingHeaderViewModel model)
        {
            AddLineItem(model);
            return ReturnView(model);
        }

        public ActionResult ReturnView(InvoicingHeaderViewModel model)
        {
            string actionName = string.Empty;
            switch (model.DocumentType)
            {
                case SACore.Domain.DocumentTypes.InvoicingOrder:
                    actionName = "InvoicingOrder";
                    break;
                case SACore.Domain.DocumentTypes.InvoicingInvoice:
                    actionName = "InvoicingInvoice";
                    break;
                case SACore.Domain.DocumentTypes.InvoicingDelivery:
                    actionName = "InvoicingDelivery";
                    break;
            }

            return View(actionName, model);
        }

        private void AddLineItem(InvoicingHeaderViewModel model)
        {
            var item = _inventoryService.GetItemByNo(model.InvoicingLine.ItemNo);
            var newLine = new InvoicingLineItemViewModel(_financialService);
            newLine.ItemId = item.Id;
            newLine.ItemNo = item.No;
            newLine.ItemDescription = item.Description;
            newLine.Measurement = item.SellMeasurement.Description;
            newLine.Quantity = model.InvoicingLine.Quantity;
            newLine.Price = model.InvoicingLine.Price;
            newLine.Discount = model.InvoicingLine.Discount;            
            model.InvoicingLine.InvoicingLineItems.Add(newLine);

            foreach (var line in model.InvoicingLine.InvoicingLineItems)
                line.SetServiceHelpers(_financialService);
        }

        [HttpPost, ActionName("InvoicingOrder")]
        [FormValueRequired("DeleteLineItem")]
        public ActionResult DeleteLineItemOrder(InvoicingHeaderViewModel model)
        {
            DeleteLineItem(model);
            return ReturnView(model);
        }

        [HttpPost, ActionName("InvoicingInvoice")]
        [FormValueRequired("DeleteLineItem")]
        public ActionResult DeleteLineItemInvoice(InvoicingHeaderViewModel model)
        {
            DeleteLineItem(model);
            return ReturnView(model);
        }

        [HttpPost, ActionName("InvoicingDelivery")]
        [FormValueRequired("DeleteLineItem")]
        public ActionResult DeleteLineItemDelivery(InvoicingHeaderViewModel model)
        {
            DeleteLineItem(model);
            return ReturnView(model);
        }

        private void DeleteLineItem(InvoicingHeaderViewModel model)
        {
            model.SetServiceHelpers(_inventoryService, _financialService);
            var request = HttpContext.Request;
            var deletedItem = request.Form["DeletedLineItem"];
            if (!string.IsNullOrEmpty(deletedItem))
            {
                model.InvoicingLine.InvoicingLineItems.RemoveAt(int.Parse(deletedItem));
            }
        }

        public ActionResult AddCustomerContact()
        {
            var model = new ContactViewModel();
            return View("Contact", model);
        }

        [HttpPost]
        public ActionResult AddCustomerContact(ContactViewModel model)
        {
            var contact = new Contact();
            contact.ContactType = SACore.Domain.ContactTypes.Customer;
            contact.FirstName = model.FirstName;
            contact.MiddleName = model.MiddleName;
            contact.LastName = model.LastName;
            contact.IsActive = true;

            contact.Name = model.FirstName + " " + model.LastName;


            _invoicingService.SaveContact(contact);
            if(contact.Id > 0)
                return Json(new { Status = "success" });
            else
                return Json(new { Status = "falied" });
        }
    }
}
