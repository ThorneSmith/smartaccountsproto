﻿using System.Web.Mvc;
using SAServices.Financial;
using SAServices.Inventory;
using SAServices.Receivables;
using SmartAccountsUI.Extensions;
using SmartAccountsUI.Models.ViewModels.Invoicing;

namespace SmartAccountsUI.Controllers
{
    public class ReportsController : BaseController, IController
    {
        private readonly IInvoicingService _invoicingService;
        private readonly IInventoryService _inventoryService;
        private readonly IFinancialService _financialService;

        public ReportsController(IInvoicingService invoicingService,
            IInventoryService inventoryService,
            IFinancialService financialService)
        {
            _invoicingService = invoicingService;
            _inventoryService = inventoryService;
            _financialService = financialService;
        }
        //
        // GET: /Reports/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult InvoicingOrder(long id)
        {
            var order = _invoicingService.GetInvoicingOrderById(id);
            var model = new InvoicingHeaderViewModel(_inventoryService, null);
            model.Id = order.Id;
            model.CustomerId = order.CustomerId;
            model.PaymentTermId = order.PaymentTermId;
            model.Date = order.Date;
            model.Reference = order.ReferenceNo;
            model.No = order.No;

            foreach (var line in order.InvoicingOrderLines)
            {
                var lineItem = new InvoicingLineItemViewModel(null);
                lineItem.Id = line.Id;
                lineItem.ItemId = line.ItemId;
                lineItem.ItemNo = line.Item.No;
                lineItem.ItemDescription = line.Item.Description;
                lineItem.Measurement = line.Measurement.Description;
                lineItem.Quantity = line.Quantity;
                lineItem.Discount = line.Discount;
                lineItem.Price = line.Amount;
                model.InvoicingLine.InvoicingLineItems.Add(lineItem);
            }
            
            HttpContext.Response.Clear();
            HttpContext.Response.AddHeader("Content-Type", "application/pdf");
            var html = base.RenderPartialViewToString("~/Views/Invoicing/InvoicingOrder.cshtml", model);
            HttpContext.Response.Filter = new PdfFilter(HttpContext.Response.Filter, html);
            return View(model);
        }

        public ActionResult InvoicingInvoice(long id)
        {
            var order = _invoicingService.GetInvoicingInvoiceById(id);
            var model = new InvoicingHeaderViewModel(_inventoryService, _financialService);
            model.Id = order.Id;
            model.CustomerId = order.CustomerId;
            model.Date = order.Date;
            model.No = order.No;

            foreach (var line in order.InvoicingInvoiceLines)
            {
                var lineItem = new InvoicingLineItemViewModel(_financialService);
                lineItem.Id = line.Id;
                lineItem.ItemId = line.ItemId;
                lineItem.ItemNo = line.Item.No;
                lineItem.ItemDescription = line.Item.Description;
                lineItem.Measurement = line.Measurement.Description;
                lineItem.Quantity = line.Quantity;
                lineItem.Discount = line.Discount;
                lineItem.Price = line.Amount;
                model.InvoicingLine.InvoicingLineItems.Add(lineItem);
            }

            HttpContext.Response.Clear();
            HttpContext.Response.AddHeader("Content-Type", "application/pdf");
            var html = base.RenderPartialViewToString("~/Views/Reports/InvoicingInvoice.cshtml", model);
            HttpContext.Response.Filter = new PdfFilter(HttpContext.Response.Filter, html);
            return View(model);
        }
	}
}