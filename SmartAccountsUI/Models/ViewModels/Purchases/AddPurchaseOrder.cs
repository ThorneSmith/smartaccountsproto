﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SmartAccountsUI.Models.ViewModels.Purchases
{
    public class AddPurchaseOrder
    {
        public AddPurchaseOrder()
        {
            PurchaseOrderLines = new List<AddPurchaseOrderLine>();
            Vendors = new HashSet<SelectListItem>();
            Items = new HashSet<SelectListItem>();
            UnitOfMeasurements = new HashSet<SelectListItem>();
            Date = DateTime.Now;
        }

        public DateTime Date { get; set; }
        public long VendorId { get; set; }

        public IList<AddPurchaseOrderLine> PurchaseOrderLines { get; set; }

        public ICollection<SelectListItem> Vendors { get; set; }
        public ICollection<SelectListItem> Items { get; set; }
        public ICollection<SelectListItem> UnitOfMeasurements { get; set; }

        #region Fields Add Line Item
        public long ItemId { get; set; }
        public long UnitOfMeasurementId { get; set; }
        public decimal Cost { get; set; }
        public decimal Quantity { get; set; }
        #endregion
    }

    public partial class AddPurchaseOrderLine
    {
        public long ItemId { get; set; }
        public string Description { get; set; }
        public long UnitOfMeasurementId { get; set; }
        public decimal Quantity { get; set; }
        public decimal? Cost { get; set; }
        public decimal TotalLineCost { get; set; }
    }
}