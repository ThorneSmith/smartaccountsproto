﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartAccountsUI.Models.ViewModels.MasterTable
{
    public class Customers
    {
        public Customers()
        {
            CustomersListLines = new HashSet<CustomersListLine>();
            CustomersEditLines = new HashSet<CustomersEditLine>();
        }

        public ICollection<CustomersListLine> CustomersListLines { get; set; }
        public ICollection<CustomersEditLine> CustomersEditLines { get; set; }
    }

    public class CustomersListLine
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public decimal Balance { get; set; }
    }

    public class CustomersEditLine
    {


        public long Id { get; set; }
        public string Name { get; set; }
        public long PrimaryContactId { get; set; }

        public long? InvoicingSaAccountId { get; set; }
        public long? InvoicingDiscountSaAccountId { get; set; }
        public long? AccountsReceivableSaAccountId { get; set; }
        public long? SalesSaAccountId { get; set; }
        public long? SalesDiscountSaAccountId { get; set; }
        public long? InvoicingPenaltySaAccountId { get; set; }
    }

    //public class Customer
    //{
    //    public Customer()
    //    {
    //        //CustomerItems = new HashSet<SelectListItem>();
    //        CustomerAllocations = new HashSet<CustomerAllocation>();
    //        CustomerInvoices = new HashSet<CustomerSalesInvoice>();
    //        ActualAllocations = new HashSet<Allocations>();
    //    }

    //    public long Id { get; set; }
    //    public string Name { get; set; }
    //    public decimal Balance { get; set; }

    //    public virtual ICollection<CustomerAllocation> CustomerAllocations { get; set; }
    //    public virtual ICollection<CustomerSalesInvoice> CustomerInvoices { get; set; }
    //    public ICollection<Allocations> ActualAllocations { get; set; }
    //    //public virtual ICollection<SelectListItem> CustomerItems { get; set; }
    //}

    //public class CustomerReceipt
    //{

    //}

    //public class CustomerSalesInvoice
    //{
    //    public long Id { get; set; }
    //    public DateTime Date { get; set; }
    //    public string InvoiceNo { get; set; }
    //    public decimal Amount { get; set; }
    //    public string Status { get; set; }
    //}

    //public class CustomerSalesOrder
    //{

    //}

    //public class CustomerAllocation
    //{
    //    public long Id { get; set; }
    //    public decimal AmountAllocated { get; set; }
    //    public decimal AvailableAmountToAllocate { get; set; }
    //}

    //public class Allocations
    //{
    //    public string InvoiceNo { get; set; }
    //    public string ReceiptNo { get; set; }
    //    public DateTime Date { get; set; }
    //    public decimal Amount { get; set; }
    //}

    //public class Allocate
    //{
    //    public Allocate()
    //    {
    //       // OpenInvoices = new HashSet<SelectListItem>();
    //    }

    //    public long ReceiptId { get; set; }
    //    public long InvoiceId { get; set; }
    //    public string InvoiceNo { get; set; }
    //    public decimal AmountToAllocate { get; set; }
    //    public decimal InvoiceAmount { get; set; }
    //    public decimal TotalAmountAvailableToAllocate { get; set; }
    //    public decimal LeftToAllocateFromReceipt { get; set; }

    //   // public ICollection<SelectListItem> OpenInvoices { get; set; }
    //}


    public class CustomerEdit
    {
        public CustomerEdit()
        {
        }

        public long Id { get; set; }
        [Required]
        public string Name { get; set; }
        public long PrimaryContactId { get; set; }

        public long? AccountsReceivableSaAccountId { get; set; }
        public long? InvoicingSaAccountId { get; set; }
        public long? InvoicingDiscountSaAccountId { get; set; }
        public long? InvoicingPenaltySaAccountId { get; set; }
    }

    public class CustomerAdd
    {
        public long Id { get; set; }
        [Required]
        public string Name { get; set; }
        public long PrimaryContactId { get; set; }

        public long? AccountsReceivableSaAccountId { get; set; }
        public long? InvoicingSaAccountId { get; set; }
        public long? InvoicingDiscountSaAccountId { get; set; }
        public long? InvoicingPenaltySaAccountId { get; set; }
    }
}