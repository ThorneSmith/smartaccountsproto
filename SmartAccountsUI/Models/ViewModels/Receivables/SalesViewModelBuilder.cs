﻿using SACore.Domain.Receivables;
using SAServices.Financial;
using SAServices.Inventory;
using SAServices.Receivables;
using System.Collections.Generic;
using System.Linq;

namespace SmartAccountsUI.Models.ViewModels.Receivables
{
    public partial class ReceivablesViewModelBuilder
    {
        private readonly IInventoryService _inventoryService;
        private readonly IFinancialService _financialService;
        private readonly IInvoicingService _receivablesService;

        public ReceivablesViewModelBuilder(IInventoryService inventoryService,
            IFinancialService financialService,
            IInvoicingService salesService)
        {
            _inventoryService = inventoryService;
            _financialService = financialService;
            _receivablesService = salesService;

            
        }

        public InvoicingDeliveryViewModel CreateInvoicingDeliveryViewModel()
        {
            return new InvoicingDeliveryViewModel();
        }

        public SalesDeliveries CreateSalesDeliveriesViewModel(ICollection<InvoicingDeliveryHeader> salesDeliveries)
        {
            var model = new SalesDeliveries();

            foreach (var sd in salesDeliveries)
            {
                model.SalesDeliveriesViewModel.Add(new InvoicingDeliveryViewModel()
                {
                    //CustomerId = sd.CustomerId.Value,
                    //PaymentTermId = sd.PaymentTermId.HasValue ? sd.PaymentTermId.Value : -1,
                    //Date = sd.Date,
                    //Amount = sd.InvoicingDeliveryLines.Sum(a => a.Price)
                });
            }

            return model;
        }

        public SalesOrders CreateSalesOrdersViewModel(ICollection<InvoicingOrderHeader> salesOrders)
        {
            var model = new SalesOrders();
            foreach (var order in salesOrders)
            {
                model.SalesOrdersViewModel.Add(new SalesOrderViewModel()
                {
                    //Id = order.Id,
                    //Customer = order.Customer.Name,
                    //PaymentTermId = order.PaymentTermId,
                    //Date = order.Date,
                    //Amount = order.SalesOrderLines.Sum(a => a.Amount)
                });
            }
            return model;
        }
    }
}