﻿using SACore.Domain.Receivables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartAccountsUI.Models.ViewModels.Receivables
{
    public class SalesReceipts
    {
        public SalesReceipts()
        {
            SalesReceiptListLines = new HashSet<SalesReceiptListLine>();
        }

        public virtual ICollection<SalesReceiptListLine> SalesReceiptListLines { get; set; }
    }

    public class SalesReceiptListLine
    {
        public string No { get; set; }
        public string InvoiceNo { get; set; }
        public string CustomerName { get; set; }
        public long CustomerId { get; set; }
        public DateTime Date { get; set; }        
        public decimal? Amount { get; set; }
        public decimal? AmountPaid { get; set; }
    }

    public class AddSalesReceipt
    {
        public AddSalesReceipt()
        {
            AddSalesReceiptLines = new List<AddSalesReceiptLine>();
            CustomerOutstandingSalesInvoices = new List<InvoicingInvoiceHeader>();
            Date = DateTime.Now;
        }

        [UIHint("Into Bank SaAccount")]
        public long? SaAccountToDebitId { get; set; }
        public long? SaAccountToCreditId { get; set; }
        public long? SalesInvoiceId { get; set; }
        public string SalesInvoiceNo { get; set; }
        public long? CustomerId { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime Date { get; set; }
        public decimal PaymentAmount { get; set; }

        public IList<AddSalesReceiptLine> AddSalesReceiptLines { get; set; }
        public IList<InvoicingInvoiceHeader> CustomerOutstandingSalesInvoices { get; set; }

        #region Fields to add new receipt
        public long? ItemId { get; set; }
        public long? MeasurementId { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Amount { get; set; }
        public decimal AmountToPay { get; set; }
        public string SaAccountCode { get; set; }
        #endregion
    }

    public class AddSalesReceiptLine
    {
        public string RowId { get; set; }
        public long? SalesInvoiceLineId { get; set; }
        public long? ItemId { get; set; }
        public long? SaAccountToCreditId { get; set; }
        public long? MeasurementId { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Amount { get; set; }
        public decimal AmountToPay { get; set; }
    }
}