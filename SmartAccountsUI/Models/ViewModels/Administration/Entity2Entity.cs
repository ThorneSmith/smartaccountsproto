﻿using System;
using System.Collections.Generic;
using SACore.Domain;

namespace SmartAccountsUI.Models.ViewModels.Financials
{
    public partial class Entity2Entity
    {
        public Entity2Entity()
        {
           
        }

        public ICollection<AddEntity2EntityLine> Entity2EntityLine { get; set; }

    }
    public class AddEntity2EntityLine
    {
        public Guid ParentGuid { get; set; }
        public Guid ChildGuid { get; set; }
        public int ParentRelationshipType { get; set; }

    }
}