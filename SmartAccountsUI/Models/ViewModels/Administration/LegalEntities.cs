﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SmartAccountsUI.Models.ViewModels.Administration
{
    public class LegalEntities
    {
        public LegalEntities()
        {
            LegalEntitiesList = new HashSet<LegalEntitiesListLine>();
        }

        public virtual ICollection<LegalEntitiesListLine> LegalEntitiesList
        {
            get { return GetLegalEntity(); }
            set { }
        }

        private ICollection<LegalEntitiesListLine> GetLegalEntity()
        {
            LegalEntitiesListLine lell = new LegalEntitiesListLine();
            lell.LegalEntityName = "Small Hoa";
            List<LegalEntitiesListLine> llell = new List<LegalEntitiesListLine>();
            llell.Add(lell);
            return llell;
        }
    }



    public class LegalEntitiesListLine
    {
        public Guid LegalEntityId { get; set; }
        public string LegalEntityName { get; set; }
        public Guid LegalEntityParentId { get; set; }
        public int ParentRelationshipType { get; set; }
    }

    //public class LegalEntity
    //{
    //    public LegalEntity()
    //    {
    //        LegalEntities = new HashSet<SelectListItem>();
    //    }

    //    public Guid LegalEntityId { get; set; }
    //    public string LegalEntityName { get; set; }
    //    public Guid LegalEntityParentId { get; set; }
    //    public int ParentRelationshipType { get; set; }

    //    public virtual ICollection<SelectListItem> LegalEntities { get; set; }
    //}

    public class AddLegalEntity
    {
        public AddLegalEntity()
        {
            LegalEntities = new HashSet<SelectListItem>();
        }
        public Guid LegalEntityId { get; set; }
        public string LegalEntityName { get; set; }
        public Guid? LegalEntityParentId { get; set; }
        public Guid? ParentRelationshipType { get; set; }

        public virtual ICollection<SelectListItem> LegalEntities { get; set; }
    }
}