﻿using System.Collections.Generic;
using System.Web.Mvc;
using SACore.Domain.Items;

namespace SmartAccountsUI.Models.ViewModels.Items
{
    public class EditItem
    {
        public EditItem()
        {
            UnitOfMeasurements = new HashSet<SelectListItem>();
            ItemCategories = new HashSet<SelectListItem>();
            SaAccounts = new HashSet<SelectListItem>();
            Taxes = new HashSet<SelectListItem>();
            Vendors = new HashSet<SelectListItem>();
            Inventories = new HashSet<SelectListItem>();
            ItemTaxGroups = new HashSet<SelectListItem>();
        }

        public long Id { get; set; }
        public long? ItemCategoryId { get; set; }
        public long? SmallestMeasurementId { get; set; }
        public long? InventorySaAccountId { get; set; }
        public long? ItemTaxGroupId { get; set; }
        public long? SellMeasurementId { get; set; }
        public long? PreferredVendorId { get; set; }
        public long? PurchaseMeasurementId { get; set; }
        public long? SellSaAccountId { get; set; }
        public long? InventoryAdjustmentSaAccountId { get; set; }
        public long? CostOfGoodsSoldSaAccountId { get; set; }
        public string No { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string PurchaseDescription { get; set; }
        public string SellDescription { get; set; }
        public decimal? Cost { get; set; }
        public decimal? Price { get; set; }

        public ICollection<SelectListItem> UnitOfMeasurements { get; set; }
        public ICollection<SelectListItem> ItemCategories { get; set; }
        public ICollection<SelectListItem> SaAccounts { get; set; }
        public ICollection<SelectListItem> Taxes { get; set; }
        public ICollection<SelectListItem> ItemTaxGroups { get; set; }
        public ICollection<SelectListItem> Vendors { get; set; }
        public ICollection<SelectListItem> Inventories { get; set; }

        public void PrepareEditItemViewModel(Item item)
        {
            Id = item.Id;
            ItemCategoryId = item.ItemCategoryId;
            SmallestMeasurementId = item.SmallestMeasurementId;
            InventorySaAccountId = item.InventorySaAccountId;
            ItemTaxGroupId = item.ItemTaxGroupId;
            SellMeasurementId = item.SellMeasurementId;
            PreferredVendorId = item.PreferredVendorId;
            PurchaseMeasurementId = item.PurchaseMeasurementId;
            SellSaAccountId = item.InvoicingSaAccountId;
            InventoryAdjustmentSaAccountId = item.InventoryAdjustmentSaAccountId;
            CostOfGoodsSoldSaAccountId = item.CostOfGoodsSoldSaAccountId;
            No = item.No;
            Code = item.Code;
            Description = item.Description;
            PurchaseDescription = item.PurchaseDescription;
            SellDescription = item.SellDescription;
            Cost = item.Cost;
            Price = item.Price;
        }
    }
}