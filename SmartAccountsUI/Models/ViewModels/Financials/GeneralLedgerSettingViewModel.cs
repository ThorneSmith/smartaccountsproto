﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartAccountsUI.Models.ViewModels.Financials
{
    public class GeneralLedgerSettingsViewModel
    {
        public long Id { get; set; }
        public long? CompanyId { get; set; }
        public long? PayableSaAccountId { get; set; }
        public long? PurchaseDiscountSaAccountId { get; set; }
        public long? GoodsReceiptNoteClearingSaAccountId { get; set; }
        public long? InvoicingDiscountSaAccountId { get; set; }
        public long? ShippingChargeSaAccountId { get; set; }
    }
}