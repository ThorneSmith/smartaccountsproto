﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using SACore.Domain;

namespace SmartAccountsUI.Models.ViewModels.Financials
{
    public class JournalEntries
    {
        public JournalEntries()
        {
            JournalEntriesListLines = new HashSet<JournalEntriesListLine>();
        }

        public DateTime? From { get; set; }
        public DateTime? To { get; set; }

        public ICollection<JournalEntriesListLine> JournalEntriesListLines { get; set; }
    }

    public class JournalEntriesListLine
    {
        public long SaAccountId { get; set; }
        public string SaAccountCode { get; set; }
        public string SaAccountName { get; set; }
        public string DrCr { get; set; }
        public decimal Amount { get; set; }
        public DateTime Date { get; set; }
    }

    public class AddJournalEntry
    {
        public AddJournalEntry()
        {
            SaAccounts = new HashSet<SelectListItem>();
            AddJournalEntryLines = new List<AddJournalEntryLine>();
            Date = DateTime.Now;
        }

        public DateTime Date { get; set; }
        public string ReferenceNo { get; set; }
        public string Memo { get; set; }

        public ICollection<SelectListItem> SaAccounts { get; set; }
        public IList<AddJournalEntryLine> AddJournalEntryLines { get; set; }

        #region Fields for New Journal Entry
        public long SaAccountId { get; set; }
        public TransactionTypes DrCr { get; set; }
        public decimal Amount { get; set; }
        public string MemoLine { get; set; }
        #endregion

        public string ErrorMsgLine { get; set; }
    }

    public class AddJournalEntryLine
    {
        public string RowId { get; set; }
        public long SaAccountId { get; set; }
        public string SaAccountName { get; set; }
        public TransactionTypes DrCr { get; set; }
        public decimal Amount { get; set; }
        public string Memo { get; set; }


    }
}