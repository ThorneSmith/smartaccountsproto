﻿using System.Collections.Generic;
using System.Linq;
using SACore.Domain.Receivables;
using SAServices.Financial;
using SAServices.Inventory;
using SAServices.Receivables;

namespace SmartAccountsUI.Models.ViewModels.Invoicing
{
    public partial class InvoicingViewModelBuilder
    {
        private readonly IInventoryService _inventoryService;
        private readonly IFinancialService _financialService;
        private readonly IInvoicingService _invoicingService;

        public InvoicingViewModelBuilder(IInventoryService inventoryService,
            IFinancialService financialService,
            IInvoicingService invoicingService)
        {
            _inventoryService = inventoryService;
            _financialService = financialService;
            _invoicingService = invoicingService;

            
        }

        public InvoicingDeliveryViewModel CreateInvoicingDeliveryViewModel()
        {
            return new InvoicingDeliveryViewModel();
        }

        public InvoicingDeliveries CreateInvoicingDeliveriesViewModel(ICollection<InvoicingDeliveryHeader> invoicingDeliveries)
        {
            var model = new InvoicingDeliveries();

            foreach (var sd in invoicingDeliveries)
            {
                model.InvoicingDeliveriesViewModel.Add(new InvoicingDeliveryViewModel()
                {
                    CustomerId = sd.CustomerId.Value,
                    PaymentTermId = sd.PaymentTermId.HasValue ? sd.PaymentTermId.Value : -1,
                    Date = sd.Date,
                    Amount = sd.InvoicingDeliveryLines.Sum(a => a.Price)
                });
            }

            return model;
        }

        public InvoicingOrders CreateInvoicingOrdersViewModel(ICollection<InvoicingOrderHeader> invoicingOrders)
        {
            var model = new InvoicingOrders();
            foreach (var order in invoicingOrders)
            {
                model.InvoicingOrdersViewModel.Add(new InvoicingOrderViewModel()
                {
                    Id = order.Id,
                    Customer = order.Customer.Name,
                    PaymentTermId = order.PaymentTermId,
                    Date = order.Date,
                    Amount = order.InvoicingOrderLines.Sum(a => a.Amount)
                });
            }
            return model;
        }
    }
}