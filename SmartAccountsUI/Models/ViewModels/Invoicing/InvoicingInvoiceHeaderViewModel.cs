﻿
using System;
using SAServices.Financial;
using SAServices.Inventory;

namespace SmartAccountsUI.Models.ViewModels.Invoicing
{
    public class InvoicingInvoiceHeaderViewModel
    {
        public InvoicingInvoiceHeaderViewModel(IInventoryService inventoryService, IFinancialService financialService)
        {
            InvoicingLine = new InvoicingLineItemsViewModel(inventoryService, financialService);
            Date = DateTime.Now;
        }

        public long? Id { get; set; }
        public long? CustomerId { get; set; }
        public long? PaymentTermId { get; set; }
        public string Reference { get; set; }
        public string No { get; set; }
        public DateTime Date { get; set; }
        public InvoicingLineItemsViewModel InvoicingLine { get; set; }

        public void SetServiceHelpers(IInventoryService inventoryService, IFinancialService financialService)
        {
            InvoicingLine.SetServiceHelpers(inventoryService, financialService);
        }
    }
}