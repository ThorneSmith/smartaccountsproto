﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SACore.Domain.Receivables;

namespace SmartAccountsUI.Models.ViewModels.Invoicing
{
    public class InvoicingReceipts
    {
        public InvoicingReceipts()
        {
            InvoicingReceiptListLines = new HashSet<InvoicingReceiptListLine>();
        }

        public virtual ICollection<InvoicingReceiptListLine> InvoicingReceiptListLines { get; set; }
    }

    public class InvoicingReceiptListLine
    {
        public string No { get; set; }
        public string InvoiceNo { get; set; }
        public string CustomerName { get; set; }
        public long CustomerId { get; set; }
        public DateTime Date { get; set; }        
        public decimal? Amount { get; set; }
        public decimal? AmountPaid { get; set; }
    }

    public class AddInvoicingReceipt
    {
        public AddInvoicingReceipt()
        {
            AddInvoicingReceiptLines = new List<AddInvoicingReceiptLine>();
            CustomerOutstandingInvoicingInvoices = new List<InvoicingInvoiceHeader>();
            Date = DateTime.Now;
        }

        [UIHint("Into Bank SaAccount")]
        public long? SaAccountToDebitId { get; set; }
        public long? SaAccountToCreditId { get; set; }
        public long? InvoicingInvoiceId { get; set; }
        public string InvoicingInvoiceNo { get; set; }
        public long? CustomerId { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime Date { get; set; }
        public decimal PaymentAmount { get; set; }

        public IList<AddInvoicingReceiptLine> AddInvoicingReceiptLines { get; set; }
        public IList<InvoicingInvoiceHeader> CustomerOutstandingInvoicingInvoices { get; set; }

        #region Fields to add new receipt
        public long? ItemId { get; set; }
        public long? MeasurementId { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Amount { get; set; }
        public decimal AmountToPay { get; set; }
        public string SaAccountCode { get; set; }
        #endregion
    }

    public class AddInvoicingReceiptLine
    {
        public string RowId { get; set; }
        public long? InvoicingInvoiceLineId { get; set; }
        public long? ItemId { get; set; }
        public long? SaAccountToCreditId { get; set; }
        public long? MeasurementId { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Amount { get; set; }
        public decimal AmountToPay { get; set; }
    }
}