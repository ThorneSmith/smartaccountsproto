﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartAccountsUI.Models.ViewModels.Invoicing
{
    public class CustomerViewModel
    {
        public CustomerViewModel()
        {
            CustomerAllocations = new HashSet<CustomerAllocation>();
            CustomerInvoices = new HashSet<CustomerInvoicingInvoice>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public decimal Balance { get; set; }

        public virtual ICollection<CustomerAllocation> CustomerAllocations { get; set; }
        public virtual ICollection<CustomerInvoicingInvoice> CustomerInvoices { get; set; }
    }
}