﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartAccountsUI.Models.ViewModels.Invoicing
{
    public partial class InvoicingOrders
    {
        public InvoicingOrders()
        {
            InvoicingOrdersViewModel = new HashSet<InvoicingOrderViewModel>();
        }
        public virtual ICollection<InvoicingOrderViewModel> InvoicingOrdersViewModel { get; set; }
    }

    public partial class InvoicingOrderViewModel
    {
        public InvoicingOrderViewModel()
        {
            Date = DateTime.Now;
        }

        public long Id { get; set; }
        public string Customer { get; set; }
        public DateTime Date { get; set; }
        public long? PaymentTermId { get; set; }
        public decimal Amount { get; set; }
    }
}