﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SmartAccounts.WebAPI.Models;

namespace SmartAccounts.WebAPI.Controllers
{
    public class GeneralLedgerHeaderController : ApiController
    {

        private SWAccountContext db = new SWAccountContext();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.db.Dispose();
            }

            base.Dispose(disposing);
        }


        private async Task<GeneralLedgerHeader> GetLedgerAccounts()
        {
            var glHeaders =  this.db.GeneralLedgerHeaders.ToList();

            return await this.db.GeneralLedgerHeaders.FindAsync(CancellationToken.None,null);
        }

        // GET: api/GeneralLedgerHeader
         [ResponseType(typeof(GeneralLedgerHeader))]
        public async Task<IHttpActionResult> Get()
        {
            var userId = User.Identity.Name;

           GeneralLedgerHeader generalLedgerHeaders = await this.GetLedgerAccounts();


           return this.Ok(generalLedgerHeaders);
        }

        // GET: api/GeneralLedgerHeader/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/GeneralLedgerHeader
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/GeneralLedgerHeader/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/GeneralLedgerHeader/5
        public void Delete(int id)
        {
        }
    }
}
