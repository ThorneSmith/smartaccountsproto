


using SmartAccounts.WebAPI.Models;
using SmartAccounts.Models;

namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public  class GeneralLedgerLine
    {
        public int Id { get; set; }
        public int GeneralLedgerHeaderId { get; set; }
        public int AccountId { get; set; }
        public int DrCr { get; set; }
        public decimal Amount { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual Account Account { get; set; }
        public virtual GeneralLedgerHeader GeneralLedgerHeader { get; set; }
    }
}
