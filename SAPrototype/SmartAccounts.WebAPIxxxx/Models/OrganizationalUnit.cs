//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SmartAccountsService
{
    using System;
    using System.Collections.Generic;
    
    public partial class OrganizationalUnit
    {
        public int Id { get; set; }
        public string OULabel { get; set; }
        public string OUDescription { get; set; }
        public Nullable<int> OUTypeId { get; set; }
        public Nullable<int> SubOUUnitOf { get; set; }
        public Nullable<int> OROwnerId { get; set; }
        public Nullable<int> ORAgentId { get; set; }
        public Nullable<System.Guid> ORLegalEntity { get; set; }
    }
}
