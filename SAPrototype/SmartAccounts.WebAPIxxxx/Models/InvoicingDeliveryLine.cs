

namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public partial class InvoicingDeliveryLine
    {
        public int Id { get; set; }
        public int InvoicingDeliveryHeaderId { get; set; }
        public Nullable<int> ItemId { get; set; }
        public Nullable<int> MeasurementId { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Discount { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual Item Item { get; set; }
        public virtual Measurement Measurement { get; set; }
        public virtual InvoicingDeliveryHeader InvoicingDeliveryHeader { get; set; }
    }
}
