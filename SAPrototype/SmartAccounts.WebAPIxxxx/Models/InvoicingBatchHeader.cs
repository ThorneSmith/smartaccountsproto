﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;


namespace SmartAccounts
{
    [Table("InvoicingBatchHeader")]
    public partial class InvoicingBatchHeader 
    {
        public int Id { get; set; }
        public Guid LegalEntityId { get; set; }
        public int InvoicingBatchHeaderNumber { get; set; }
        public DateTime InvoicingBatchHeaderDate { get; set; }

        public int CustomerId { get; set; }
        public int? GeneralLedgerHeaderId { get; set; }
        public int? InvoicingDeliveryHeaderId { get; set; }
        public string No { get; set; }
        public DateTime Date { get; set; }
        public decimal ShippingHandlingCharge{ get; set; }
        // public InvoicingInvoiceStatus Status { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual GeneralLedgerHeader GeneralLedgerHeader { get; set; }
        public virtual InvoicingDeliveryHeader InvoicingDeliveryHeader { get; set; }

        public virtual ICollection<InvoicingBatchLineItem> InvoicingBatchLineItems { get; set; }
        [NotMapped]
        public virtual ICollection<InvoicingReceiptHeader> InvoicingReceipts { get; set; }
        public virtual ICollection<CustomerAllocation> CustomerAllocations { get; set; }

        
    }
}
