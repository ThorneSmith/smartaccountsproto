using System;
using System.Collections.Generic;

namespace SmartAccounts.WebAPI.Models
{
    public class AccountClass
    {
       public AccountClass()
        {
            this.Accounts = new HashSet<Account>();
        }
    
        public int Id { get; set; }
        public Guid LegalEntityId { get; set; }
        public string Name { get; set; }
        public string NormalBalance { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual ICollection<Account> Accounts { get; set; }
    }
}
