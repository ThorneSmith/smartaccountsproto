
using SmartAccounts.WebAPI.Models;
using SmartAccounts.Models;

namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public partial class Customer
    {
    
        public int Id { get; set; }
        public string No { get; set; }
        public Guid LegalEntityId { get; set; }
        public Nullable<int> PrimaryContactId { get; set; }
        public Nullable<int> TaxGroupId { get; set; }
        public Nullable<int> AccountsReceivableAccountId { get; set; }
        public Nullable<int> InvoicingAccountId { get; set; }
        public Nullable<int> InvoicingDiscountAccountId { get; set; }
        public Nullable<int> PromptPaymentDiscountAccountId { get; set; }
        public Nullable<int> PaymentTermId { get; set; }
    
        public virtual Account Account { get; set; }
        public virtual Account Account1 { get; set; }
        public virtual Account Account2 { get; set; }
        public virtual Account Account3 { get; set; }
        public virtual Contact Contact { get; set; }
        public virtual Party Party { get; set; }
        public virtual PaymentTerm PaymentTerm { get; set; }
        public virtual TaxGroup TaxGroup { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }

        public virtual ICollection<CustomerAllocation> CustomerAllocations { get; set; }

        public virtual ICollection<InvoicingDeliveryHeader> InvoicingDeliveryHeaders { get; set; }
        public virtual ICollection<InvoicingInvoiceHeader> InvoicingInvoiceHeaders { get; set; }

        public virtual ICollection<InvoicingOrderHeader> InvoicingOrderHeaders { get; set; }
        public virtual ICollection<InvoicingQuoteHeader> InvoicingQuoteHeaders { get; set; }

        public virtual ICollection<InvoicingReceiptHeader> InvoicingReceiptHeaders { get; set; }
    }
}
