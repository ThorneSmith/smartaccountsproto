
namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public partial class Company
    {
    
        public int Id { get; set; }
        public Guid LegalEntityId { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public Guid ParentId { get; set; }
        public int CompanyType { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual ICollection<CompanySetting> CompanySettings { get; set; }
        public virtual ICollection<GeneralLedgerSetting> GeneralLedgerSettings { get; set; }
    }
}
