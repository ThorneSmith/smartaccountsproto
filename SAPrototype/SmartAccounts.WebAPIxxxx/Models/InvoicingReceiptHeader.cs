
using SmartAccounts.WebAPI.Models;
using SmartAccounts.Models;

namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public class InvoicingReceiptHeader
    {
    
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public Nullable<int> GeneralLedgerHeaderId { get; set; }
        public Nullable<int> AccountToDebitId { get; set; }
        public string No { get; set; }
        public System.DateTime Date { get; set; }
        public decimal Amount { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual Account Account { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual ICollection<CustomerAllocation> CustomerAllocations { get; set; }
        public virtual GeneralLedgerHeader GeneralLedgerHeader { get; set; }
        public virtual ICollection<InvoicingReceiptLine> InvoicingReceiptLines { get; set; }
    }
}
