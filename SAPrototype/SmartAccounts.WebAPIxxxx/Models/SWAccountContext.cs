﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartAccounts.WebAPI.Models
{
    using System.Data.Entity;

    public class SWAccountContext : DbContext
    {
        public SWAccountContext()
            : base("name=DefaultConnection")
        {
        }

        public DbSet<GeneralLedgerHeader> GeneralLedgerHeaders      { get; set; }
        public DbSet<GeneralLedgerLine> GeneralLedgerLines          { get; set; }
        public DbSet<GeneralLedgerSetting> GeneralLedgerSettings    { get; set; }

        public DbSet<Account> GLAccounts { get; set; }

    }
}