

using SmartAccounts.WebAPI.Models;
using SmartAccounts.Models;

namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public partial class ItemCategory
    {
   
        public int Id { get; set; }
        public int ItemType { get; set; }
        public Nullable<int> MeasurementId { get; set; }
        public Nullable<int> InvoicingAccountId { get; set; }
        public Nullable<int> InventoryAccountId { get; set; }
        public Nullable<int> CostOfGoodsSoldAccountId { get; set; }
        public Nullable<int> AdjustmentAccountId { get; set; }
        public Nullable<int> AssemblyAccountId { get; set; }
        public string Name { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual Account Account { get; set; }
        public virtual Account Account1 { get; set; }
        public virtual Account Account2 { get; set; }
        public virtual Account Account3 { get; set; }
        public virtual Account Account4 { get; set; }

        public virtual ICollection<Item> Items { get; set; }
        public virtual Measurement Measurement { get; set; }
    }
}
