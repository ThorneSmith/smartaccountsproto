namespace IdentitySample
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MenuDetail
    {
        [Key]
        [Column(Order = 0)]
        public int MDetail_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(20)]
        public string Menu_ChildID { get; set; }

        [Required]
        [StringLength(100)]
        public string MenuName { get; set; }

        [Required]
        [StringLength(200)]
        public string MenuDisplayTxt { get; set; }

        [Required]
        [StringLength(100)]
        public string MenuFileName { get; set; }

        [Required]
        [StringLength(500)]
        public string MenuURL { get; set; }

        [StringLength(1)]
        public string USE_YN { get; set; }

        [StringLength(50)]
        public string UserID { get; set; }

        public DateTime? CreatedDate { get; set; }
    }
}
