﻿using System;
using System.Configuration;
using System.Data.Entity;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using SACore.Data;
using SAData;
using SAServices.Administration;
using SAServices.Financial;
using SAServices.Inventory;
using SAServices.MasterTable;
using SAServices.Purchasing;
using SAServices.Receivables;


namespace SmartAccountsUI
{
    public class MvcApplication : HttpApplication
    {
        private IContainer container;
        private ContainerBuilder builder;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            DependecyResolver();
        }

        //This is a test line to check Source Control




        private void DependecyResolver()
        {
            builder = new ContainerBuilder();

            //controllers
            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            //generic repositories
            builder.RegisterGeneric(typeof(EfRepository<>)).As(typeof(IRepository<>)).InstancePerLifetimeScope();

            //dbcontext
            if (HttpContext.Current.Request.IsLocal)
            {
                var context = new ApplicationContext(ConfigurationManager.ConnectionStrings["ApplicationContext"].ConnectionString);
                var dbInitializer = new DbInitializer<ApplicationContext>();
                Database.SetInitializer<ApplicationContext>(dbInitializer);
                dbInitializer.InitializeDatabase(context);
                builder.Register<IDbContext>(c => context).SingleInstance();
            }
            else
            {
                Database.SetInitializer<ApplicationContext>(null);
                var context = new ApplicationContext("ApplicationContext");
                builder.Register<IDbContext>(c => context).SingleInstance();
            }

            //services
            builder.RegisterType<FinancialService>().As<IFinancialService>().InstancePerLifetimeScope();
            builder.RegisterType<MasterTableService>().As<IMasterTableService>().InstancePerLifetimeScope();
            builder.RegisterType<InventoryService>().As<IInventoryService>().InstancePerLifetimeScope();
            builder.RegisterType<InvoicingService>().As<IInvoicingService>().InstancePerLifetimeScope();
            builder.RegisterType<PurchasingService>().As<IPurchasingService>().InstancePerLifetimeScope();
            builder.RegisterType<AdministrationService>().As<IAdministrationService>().InstancePerLifetimeScope();
            //builder.RegisterType<UserManager<ApplicationUser>>().As<UserStore<ApplicationUser>>().InstancePerLifetimeScope();
            //builder.RegisterType<RoleManager<ApplicationRole>>().As<RoleStore<ApplicationRole>>().InstancePerLifetimeScope();
            
            container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}
