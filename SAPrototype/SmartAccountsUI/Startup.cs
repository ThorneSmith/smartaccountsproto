﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SmartAccountsUI.Startup))]
namespace SmartAccountsUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
