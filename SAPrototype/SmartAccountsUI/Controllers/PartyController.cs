﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication3;

namespace WebApplication3.Controllers
{
    public class PartyController : Controller
    {
        private SAEntities db = new SAEntities();

        // GET: /Party/
        public ActionResult Index()
        {
            var parties = db.Parties.Include(p => p.Contact).Include(p => p.Customer).Include(p => p.Vendor);
            return View(parties.ToList());
        }

        // GET: /Party/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Party party = db.Parties.Find(id);
            if (party == null)
            {
                return HttpNotFound();
            }
            return View(party);
        }

        // GET: /Party/Create
        public ActionResult Create()
        {
            ViewBag.Id = new SelectList(db.Contacts, "Id", "FirstName");
            ViewBag.Id = new SelectList(db.Customers, "Id", "No");
            ViewBag.Id = new SelectList(db.Vendors, "Id", "No");
            return View();
        }

        // POST: /Party/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,PartyType,Name,Email,Website,Phone,Fax,IsActive")] Party party)
        {
            if (ModelState.IsValid)
            {
                db.Parties.Add(party);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Id = new SelectList(db.Contacts, "Id", "FirstName", party.Id);
            ViewBag.Id = new SelectList(db.Customers, "Id", "No", party.Id);
            ViewBag.Id = new SelectList(db.Vendors, "Id", "No", party.Id);
            return View(party);
        }

        // GET: /Party/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Party party = db.Parties.Find(id);
            if (party == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id = new SelectList(db.Contacts, "Id", "FirstName", party.Id);
            ViewBag.Id = new SelectList(db.Customers, "Id", "No", party.Id);
            ViewBag.Id = new SelectList(db.Vendors, "Id", "No", party.Id);
            return View(party);
        }

        // POST: /Party/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,PartyType,Name,Email,Website,Phone,Fax,IsActive")] Party party)
        {
            if (ModelState.IsValid)
            {
                db.Entry(party).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Id = new SelectList(db.Contacts, "Id", "FirstName", party.Id);
            ViewBag.Id = new SelectList(db.Customers, "Id", "No", party.Id);
            ViewBag.Id = new SelectList(db.Vendors, "Id", "No", party.Id);
            return View(party);
        }

        // GET: /Party/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Party party = db.Parties.Find(id);
            if (party == null)
            {
                return HttpNotFound();
            }
            return View(party);
        }

        // POST: /Party/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Party party = db.Parties.Find(id);
            db.Parties.Remove(party);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
