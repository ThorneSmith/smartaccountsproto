﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SAServices.Financial;

namespace WebApplication3.Controllers
{
    public class AccountClassController : Controller
    {
        private readonly IFinancialService _financialService;

        public AccountClassController(IFinancialService financialService)
        {
            _financialService = financialService;
        }
        public ActionResult Index()
        {
            return View(_financialService.GetSaAccountClasses.ToList());
        }

        // GET: /AccountClass/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SaAccountClass saaccountclass = _financialService.SaAccountClasses.Find(id);
            if (saaccountclass == null)
            {
                return HttpNotFound();
            }
            return View(saaccountclass);
        }

        // GET: /AccountClass/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /AccountClass/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,LegalEntityId,Name,NormalBalance")] SaAccountClass saaccountclass)
        {
            if (ModelState.IsValid)
            {
                _financialService.SaAccountClasses.Add(saaccountclass);
                _financialService.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(saaccountclass);
        }

        // GET: /AccountClass/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SaAccountClass saaccountclass = _financialService.SaAccountClasses.Find(id);
            if (saaccountclass == null)
            {
                return HttpNotFound();
            }
            return View(saaccountclass);
        }

        // POST: /AccountClass/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,LegalEntityId,Name,NormalBalance")] SaAccountClass saaccountclass)
        {
            if (ModelState.IsValid)
            {
                _financialService.Entry(saaccountclass).State = EntityState.Modified;
                _financialService.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(saaccountclass);
        }

        // GET: /AccountClass/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SaAccountClass saaccountclass = _financialService.SaAccountClasses.Find(id);
            if (saaccountclass == null)
            {
                return HttpNotFound();
            }
            return View(saaccountclass);
        }

        // POST: /AccountClass/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            SaAccountClass saaccountclass = _financialService.SaAccountClasses.Find(id);
            _financialService.SaAccountClasses.Remove(saaccountclass);
            _financialService.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _financialService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
