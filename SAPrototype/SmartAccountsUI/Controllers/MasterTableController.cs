﻿using System.Net;
using System.Web.Mvc;
using SACore.Domain;
using SACore.Domain.MasterTables;
using SAServices.MasterTable;
using SAServices.Receivables;
using SmartAccountsUI.Models;
using SmartAccountsUI.Models.ViewModels;
using SmartAccountsUI.Models.ViewModels.MasterTable;

namespace SmartAccountsUI.Controllers
{
     [Authorize]
    public class MasterTableController : BaseController
     {
        private readonly IMasterTableService _masterTableService;
        private readonly IInvoicingService _invoicingService = null;

        public MasterTableController(IMasterTableService masterTableService)
        {
            _masterTableService = masterTableService;

        }
        public ActionResult CustomerIndex()
        {
            var customers = _masterTableService.GetCustomers();
            var model = new Customers();

            foreach (var customer in customers)
            {
                model.CustomersListLines.Add(new CustomersListLine()
                { 
                    Id = customer.Id,
                    Name = customer.Name,
                    Balance = customer.Balance
               });
            }


            return View(model);
        }

        public ActionResult BankIndex()
        {
            var banks = _masterTableService.GetBanks();
            var model = new Banks();

            foreach (var bank in banks)
            {
                model.BankListLines.Add(new BankListLine()
                {
                    Id = bank.Id,
                    Name = bank.Name,
                    Balance = bank.Balance
                });
            }


            return View(model);
        }

        public ActionResult AddressIndex()
        {
            var addresses = _masterTableService.GetAddresses();
            var model = new Addresses();

            foreach (var address in addresses)
            {
                model.AddressListLines.Add(new AddressListLine()
                {
                    Id = address.Id,
                    Address1 = address.Address1,
                    Address2 = address.Address2,
                    Address3 =  address.Address3,
                    City    =   address.City    ,
                    StateId = address.StateId,
                    Zip       = address.Zip    
                });
            }


            return View(model);
        }

        public ActionResult AddressEdit(long id = 0)
        {
            AddressEdit model = new AddressEdit();
            if (id != 0)
            {
                var address = _masterTableService.GetAddressById(id);

                model.Id = address.Id;
                model.Address1 = address.Address1;
                model.Address2 = address.Address2;
                model.Address3 = address.Address3;
                model.City = address.City;
                model.StateId = address.StateId;
                model.Zip = address.Zip;
            }
            return View(model);

        }


         //GET: /Customer/Details/5
        public ActionResult Details(long id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var customer = _masterTableService.GetCustomerById(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // GET: /Customer/Create
        public ActionResult BankEdit(long id = 0)
        {
            BankEdit model = new BankEdit();
            if (id != 0)
            {
                var bank = _masterTableService.GetBankById(id);
                model.Id = bank.Id;
                model.Name = bank.Name;
                model.BankName = bank.BankName;
                model.BankType = bank.BankType;
                model.AddressId = bank.AddressId;
                model.IsDefault = bank.IsDefault;
            }
            return View(model);

        }
        


        // GET: /Customer/Create
        public ActionResult CustomerEdit(long id = 0)
        {
            CustomerEdit model = new CustomerEdit();
            if (id != 0)
            {
                var customer = _masterTableService.GetCustomerById(id);
                model.Id = customer.Id;
                model.Name = customer.Name;
                model.PrimaryContactId = customer.PrimaryContactId.HasValue ? customer.PrimaryContactId.Value : -1;
                model.AccountsReceivableSaAccountId = customer.AccountsReceivableSaAccountId.HasValue ? customer.AccountsReceivableSaAccountId.Value : -1;
                model.InvoicingSaAccountId = customer.InvoicingSaAccountId.HasValue ? customer.InvoicingSaAccountId.Value : -1;
                model.InvoicingDiscountSaAccountId = customer.InvoicingDiscountSaAccountId.HasValue ? customer.InvoicingDiscountSaAccountId.Value : -1;
                model.InvoicingPenaltySaAccountId = customer.InvoicingPenaltySaAccountId.HasValue ? customer.InvoicingPenaltySaAccountId.Value : -1;
            }
            return View(model);

        }

         //GET: /Customer/Create
        public ActionResult CustomerAdd()
        {
            ViewBag.PrimaryContactId = new SelectList(_masterTableService.GetContacts(), "Id", "FirstName");
            ViewBag.Id = new SelectList(_masterTableService.GetParties(), "Id", "Name");
            ViewBag.PaymentTermId = new SelectList(_masterTableService.GetPaymentTerms(), "Id", "Description");
            ViewBag.InvoicingDiscountSaAccountId = new SelectList(_masterTableService.GetInvoicingPenaltySaAccounts(), "Id", "SaAccountCode");
            ViewBag.InvoicingSaAccountId = new SelectList(_masterTableService.GetInvoicingSaAccounts(), "Id", "SaAccountCode");
            ViewBag.InvoicingPenaltySaAccountId = new SelectList(_masterTableService.GetInvoicingPenaltySaAccounts(), "Id", "SaAccountCode");
            ViewBag.AccountsReceivableSaAccountId = new SelectList(_masterTableService.GetReceivableSaAccounts(), "Id", "SaAccountCode");
            ViewBag.TaxGroupId = new SelectList(_masterTableService.GetCustomerTaxGroups(), "Id", "Description");
            return View();
        }
        
        // GET: /Customer/Delete/5
        public ActionResult CustomerDelete(long id)
        {
            var customer = _masterTableService.GetCustomerById(id);
            if (customer == null)
            {
                return HttpNotFound();
            }

            customer.IsActive = false;

            _masterTableService.SaveCustomer(customer);
            return RedirectToAction("CustomerIndex");
        }

        public ActionResult VendorDelete(long id)
        {
            var vendor = _masterTableService.GetVendorById(id);
            if (vendor == null)
            {
                return HttpNotFound();
            }

            vendor.IsActive = false;

            _masterTableService.SaveVendor(vendor);
            return RedirectToAction("VendorIndex");
        }

        [HttpPost, ActionName("CustomerEdit")]
        [FormValueRequired("SaveCustomer")]
        public ActionResult Edit(CustomerEdit model)
        {

            Customer customer = null;
            if (model.Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (model.Id == 0)
            {

                customer = new Customer();

            }
            else
            {

                customer = _masterTableService.GetCustomerById(model.Id);
            }

            customer.Name = model.Name;
            customer.InvoicingDiscountSaAccountId = model.InvoicingDiscountSaAccountId;
            customer.InvoicingSaAccountId = model.InvoicingSaAccountId;
            customer.InvoicingPenaltySaAccountId = model.InvoicingPenaltySaAccountId;
            customer.AccountsReceivableSaAccountId = model.AccountsReceivableSaAccountId;
            customer.PrimaryContactId = model.PrimaryContactId;
            customer.IsActive = true;
            customer.PartyType = PartyTypes.Customer;


                if (model.Id != 0)
                _masterTableService.SaveCustomer(customer);
            else
                    _masterTableService.AddCustomer(customer);

            return RedirectToAction("CustomerIndex");
            
        }

        public ActionResult VendorIndex()
        {
            var vendors = _masterTableService.GetVendors();
            var model = new Vendors();
            if (vendors != null)
            {
                foreach (var vendor in vendors)
                {
                    model.VendorsList.Add(new VendorsListLine()
                    {
                        Id = vendor.Id,
                        Name = vendor.Name == null ? "Name" : vendor.Name,
                        Balance = vendor.GetBalance()
                    });
                }
            }
            return View(model);
        }

        public ActionResult AddOrEditVendor(long id = 0)
        {
            Vendor vendor = null;
            var model = new VendorEdit();
            model.Id = id;
            if (id != 0)
            {
                vendor = _masterTableService.GetVendorById(id);
                model.Name = vendor.Name;
                model.AccountsPayableSaAccountId = vendor.AccountsPayableSaAccountId;
                model.PurchaseSaAccountId = vendor.PurchaseSaAccountId;
                model.PurchaseDiscountSaAccountId = vendor.PurchaseDiscountSaAccountId;
               
            }

            return View(model);
        }

        [HttpPost, ActionName("AddOrEditVendor")]
        [FormValueRequired("SaveVendor")]
        public ActionResult AddOrEditVendor(Vendor model)
        {
            Vendor vendor = null;
            if (model.Id != 0)
            {
                vendor = _masterTableService.GetVendorById(model.Id);
            }
            else
            {
                vendor = new Vendor();
                //    vendor.CreatedBy = User.Identity.Name;
                //    vendor.CreatedOn = DateTime.Now;
                //
            }

            //vendor.ModifiedBy = User.Identity.Name;
            //vendor.ModifiedOn = DateTime.Now;
            vendor.Name = model.Name;
            vendor.AccountsPayableSaAccountId = model.AccountsPayableSaAccountId.Value == -1 ? null : model.AccountsPayableSaAccountId;
            vendor.PurchaseSaAccountId = model.PurchaseSaAccountId.Value == -1 ? null : model.PurchaseSaAccountId;
            vendor.PurchaseDiscountSaAccountId = model.PurchaseDiscountSaAccountId.Value == -1 ? null : model.PurchaseDiscountSaAccountId;
            vendor.IsActive = true;
            vendor.PartyType = PartyTypes.Vendor;

            if (model.Id != 0)
                _masterTableService.SaveVendor(vendor);
            else
                _masterTableService.AddVendor(vendor);

            return RedirectToAction("VendorIndex");
        }

        public ActionResult SaveVendor()
        {
            var model = new VendorAdd();
            model.SaAccounts = ModelViewHelper.SaAccounts(_masterTableService.GetSaAccounts());
            return View(model);
        }

        [HttpPost, ActionName("SaveVendor")]
        [FormValueRequired("SaveVendor")]
        public ActionResult SaveVendor(VendorAdd model)
        {
            var vendor = new Vendor()
            {
                Name = model.VendorName,
                AccountsPayableSaAccountId = model.AccountsPayableSaAccountId.Value == -1 ? null : model.AccountsPayableSaAccountId,
                PurchaseSaAccountId = model.PurchaseSaAccountId.Value == -1 ? null : model.PurchaseSaAccountId,
                PurchaseDiscountSaAccountId = model.PurchaseDiscountSaAccountId.Value == -1 ? null : model.PurchaseDiscountSaAccountId,
                IsActive = true
            };
            _masterTableService.SaveVendor(vendor);
            return RedirectToAction("VendorIndex");
        }
        public ActionResult SaveCustomerContact()
        {
            var model = new ContactViewModel();
            return View("Contact", model);
        }

        public ActionResult AddressAddDialog()
        {
            return PartialView();
        }
        [HttpPost]
        public ActionResult AddressAddDialog(AddressViewModel model)
        {
            var address = new Address();
            address.Address1 = model.Address1;
            address.Address2 = model.Address2;
            address.Address3 = model.Address3;
            address.City = model.City;
            address.StateId = model.StateId;
            address.Zip = model.Zip;
            address.IsActive = true;

            
            var saveMessage = "Address Saved";

            if (ModelState.IsValid)
            {
                if (model.Address1 != null && model.Zip != null)
                {
                    _masterTableService.AddAddress(address);

                    return this.DialogResult(saveMessage); // Close dialog via DialogResult call
                }
                else
                    ModelState.AddModelError("", string.Format("Address 1, City, State, Zip are required", ""));
            }
            return PartialView(model);
        }

        public ActionResult ContactAddDialog()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult ContactAddDialog(ContactViewModel model)
        {
            var contact = new Contact();
            contact.ContactType = ContactTypes.Customer;
            contact.FirstName = model.FirstName;
            contact.MiddleName = model.MiddleName;
            contact.LastName = model.LastName;
            contact.IsActive = true;

            contact.Name = model.FirstName + " " + model.LastName;
            contact.PartyType = PartyTypes.Contact;
            contact.LegalEntityId = _masterTableService.GetCurrentLegalEntity().LegalEntityId;
            //contact.Party.PartyType = PartyTypes.Contact;
            

            var saveMessage = "Contact " + contact.Name + " Saved";

            if (ModelState.IsValid)
            {
                if (model.FirstName != null && model.LastName != null)
                {
                    _masterTableService.SaveContact(contact);
                   
                    return this.DialogResult(saveMessage); // Close dialog via DialogResult call
                }
                else
                    ModelState.AddModelError("", string.Format("First and Last Names are Required", ""));
            }
            return PartialView(model);
        }


        public ActionResult Banks()
        {
            var model = new Banks();
            var banks = _masterTableService.GetBanks();
            foreach (var bank in banks)
            {
                model.BankListLines.Add(new BankListLine()
                {
                    Name = bank.Name,
                    BankName = bank.BankName,
                    SaAccountId = bank.SaAccountId,
                    Number = bank.Number,
                    BankType = bank.BankType,
                    AddressId = bank.AddressId,
                    IsActive = bank.IsActive,
                    IsDefault = bank.IsDefault
                });
            }
            return View(model);
        }
        [HttpPost, ActionName("BankEdit")]
        [FormValueRequired("SaveBank")]
        public ActionResult Edit(BankEdit model)
        {

            Bank bank = null;
            if (model.Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (model.Id == 0)
            {

                bank = new Bank();

            }
            else
            {

                bank = _masterTableService.GetBankById(model.Id);
            }

            bank.Name = model.Name;
            bank.BankName = model.BankName;
            bank.AddressId = model.AddressId;
            bank.BankType = model.BankType;
            bank.IsDefault = model.IsDefault;
            bank.IsActive = true;
            bank.LegalEntityId = _masterTableService.GetCurrentLegalEntity().LegalEntityId;
            bank.IsActive = true;

            if (model.Id != 0)
                _masterTableService.SaveBank(bank);
            else
                _masterTableService.AddBank(bank);

            return RedirectToAction("BankIndex");

        }
        public ActionResult Dialog1()
        {
            return PartialView();
        }

        [HttpPost, ActionName("AddressEdit")]
        [FormValueRequired("SaveAddress")]
        public ActionResult Edit(AddressEdit model)
        {

            Address address = null;
            if (model.Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (model.Id == 0)
            {

                address = new Address();

            }
            else
            {

                address = _masterTableService.GetAddressById(model.Id);
            }

            address.Address1 = model.Address1;
            address.Address2 = model.Address2;
            address.Address3 = model.Address3;
            address.City = model.City;
            address.StateId = model.StateId;
            address.Zip = model.Zip;


            if (model.Id != 0)
                _masterTableService.SaveAddress(address);
            else
                _masterTableService.AddAddress(address);

            return RedirectToAction("AddressIndex");

        }
        ActionResult ProcessDialog(DialogModel model, int answer, string message)
        {
            if (ModelState.IsValid)
            {
                if (model.Value == answer)
                    return this.DialogResult(message);  // Close dialog via DialogResult call
                else
                    ModelState.AddModelError("", string.Format("Invalid input value. The correct value is {0}", answer));
            }

            return PartialView(model);
        }

    }
}
