﻿//using AspNetExtendingIdentityRoles.Models;

using System.Collections.Generic;

using System.Data.Entity;

using System.Linq;

using System.Net;

using System.Web.Mvc;
using SmartAccountsUI.Models;

namespace SmartAccountsUI.Controllers
{

    public class RolesController : Controller
    {

        private ApplicationDbContext _db = new ApplicationDbContext();



        public ActionResult Index()
        {

            var rolesList = new List<RoleViewModel>();

            foreach (var role in _db.Roles)
            {

                var roleModel = new RoleViewModel(role);

                rolesList.Add(roleModel);

            }

            return View(rolesList);

        }





        [Authorize(Roles = "Admin")]

        public ActionResult Create(string message = "")
        {

            ViewBag.Message = message;

            return View();

        }





        [HttpPost]

        [Authorize(Roles = "Admin")]

        public ActionResult Create([Bind(Include =

            "RoleName,Description")]RoleViewModel model)
        {

            string message = "That role name has already been used";

            if (ModelState.IsValid)
            {

                var role = new ApplicationRole(model.RoleName);

                //var idManager = new IdentityManager();



                //if (idManager.RoleExists(model.RoleName))
                //{

                //    return View(message);

                //}

                //else
                //{

                //    idManager.CreateRole(model.RoleName);

                //    return RedirectToAction("Index", "SaAccount");

                //}

            }

            return View();

        }





        //[Authorize(Roles = "Admin")]

        //public ActionResult Edit(string id)
        //{

        //    // It's actually the Role.Name tucked into the id param:

        //    var role = _db.Roles.First(r => r.Name == id);

        //    var roleModel = new EditRoleViewModel(role);

        //    return System.Web.UI.WebControls.View(roleModel);

        //}





        //[HttpPost]

        //[Authorize(Roles = "Admin")]

        //public ActionResult Edit([Bind(Include =

        //    "RoleName,OriginalRoleName,Description")] EditRoleViewModel model)
        //{

        //    if (ModelState.IsValid)
        //    {

        //        var role = _db.Roles.First(r => r.Name == model.OriginalRoleName);

        //        role.Name = model.RoleName;

        //        //role.Description = model.Description;

        //        _db.Entry(role).State = EntityState.Modified;

        //        _db.SaveChanges();

        //        return RedirectToAction("Index");

        //    }

        //    return System.Web.UI.WebControls.View(model);

        //}





        [Authorize(Roles = "Admin")]

        public ActionResult Delete(string id)
        {

            if (id == null)
            {

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }

            var role = _db.Roles.First(r => r.Name == id);

            var model = new RoleViewModel(role);

            if (role == null)
            {

                return HttpNotFound();

            }

            return View(model);

        }





        [Authorize(Roles = "Admin")]

        [HttpPost, ActionName("Delete")]

        public ActionResult DeleteConfirmed(string id)
        {

            var role = _db.Roles.First(r => r.Name == id);

            //var idManager = new IdentityManager();

            //idManager.DeleteRole(role.Id);

            return RedirectToAction("Index");

        }

        public void RemoveFromRole(string userId, string roleName)
        {

            //_userManager.RemoveFromRole(userId, roleName);

        }

        public void DeleteRole(string roleId)
        {

            var roleUsers = _db.Users.Where(u => u.Roles.Any(r => r.RoleId == roleId));

            var role = _db.Roles.Find(roleId);



            foreach (var user in roleUsers)
            {

                this.RemoveFromRole(user.Id, role.Name);

            }

            _db.Roles.Remove(role);

            _db.SaveChanges();

        }
        public class RoleViewModel
        {

            public string RoleName { get; set; }

            public string Description { get; set; }



            public RoleViewModel() { }

            public RoleViewModel(ApplicationRole role)
            {

                this.RoleName = role.Name;

                //this.Description = role.Description;

            }

        }

        public class EditRoleViewModel
        {

            public string OriginalRoleName { get; set; }

            public string RoleName { get; set; }

            public string Description { get; set; }



            public EditRoleViewModel() { }

            public EditRoleViewModel(ApplicationRole role)
            {

                this.OriginalRoleName = role.Name;

                this.RoleName = role.Name;

                //this.Description = role.Description;

            }

        }


    }

}