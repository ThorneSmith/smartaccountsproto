﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication3;

namespace WebApplication3.Controllers
{
    public class CustomerTypeController : Controller
    {
        private SAEntities db = new SAEntities();

        // GET: /CustomerType/
        public ActionResult Index()
        {
            return View(db.CustomerTypes.ToList());
        }

        // GET: /CustomerType/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CustomerType customertype = db.CustomerTypes.Find(id);
            if (customertype == null)
            {
                return HttpNotFound();
            }
            return View(customertype);
        }

        // GET: /CustomerType/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /CustomerType/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Name")] CustomerType customertype)
        {
            if (ModelState.IsValid)
            {
                db.CustomerTypes.Add(customertype);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(customertype);
        }

        // GET: /CustomerType/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CustomerType customertype = db.CustomerTypes.Find(id);
            if (customertype == null)
            {
                return HttpNotFound();
            }
            return View(customertype);
        }

        // POST: /CustomerType/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name")] CustomerType customertype)
        {
            if (ModelState.IsValid)
            {
                db.Entry(customertype).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(customertype);
        }

        // GET: /CustomerType/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CustomerType customertype = db.CustomerTypes.Find(id);
            if (customertype == null)
            {
                return HttpNotFound();
            }
            return View(customertype);
        }

        // POST: /CustomerType/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            CustomerType customertype = db.CustomerTypes.Find(id);
            db.CustomerTypes.Remove(customertype);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
