﻿using SAServices.Financial;
using SAServices.Inventory;
using SAServices.Receivables;
using System.Web.Mvc;
using System.Linq;
using SACore.Domain.Receivables;
using System.Collections.Generic;
using System;
using SACore.Domain;

namespace SmartAccountsUI.Controllers
{
    [Authorize]
    public class ReceivablesController : BaseController
    {
        private SmartAccountsUI.Models.ViewModels.Receivables.ReceivablesViewModelBuilder _viewModelBuilder;

        private readonly IInventoryService _inventoryService;
        private readonly IFinancialService _financialService;
        private readonly IInvoicingService _receivablesService;

        public ReceivablesController(IInventoryService inventoryService,
            IFinancialService financialService,
            IInvoicingService salesService)
        {
            _inventoryService = inventoryService;
            _financialService = financialService;
            _receivablesService = salesService;

            _viewModelBuilder = new SmartAccountsUI.Models.ViewModels.Receivables.ReceivablesViewModelBuilder(inventoryService, financialService, salesService);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SalesOrders()
        {
            var salesOrders = _receivablesService.GetInvoicingOrders();
            var model = _viewModelBuilder.CreateSalesOrdersViewModel(salesOrders.ToList());
            return View(model);
        }

        public ActionResult SalesDeliveries()
        {
            var salesDeliveries = _receivablesService.GetInvoicingDeliveries();
            var model = _viewModelBuilder.CreateSalesDeliveriesViewModel(salesDeliveries.ToList());
            return View(model);
        }

        public ActionResult AddInvoicingDelivery(bool direct = false)
        {
            if (direct)
            {
                var model = _viewModelBuilder.CreateInvoicingDeliveryViewModel();
                return View(model);
            }
            return RedirectToAction("SalesDeliveries");
        }

        [HttpPost, ActionName("AddInvoicingDelivery")]
        [FormValueRequiredAttribute("SaveInvoicingDelivery")]
        public ActionResult SaveInvoicingDelivery(Models.ViewModels.Receivables.InvoicingDeliveryViewModel model)
        {
            var InvoicingDelivery = new InvoicingDeliveryHeader()
            {
                CustomerId = model.CustomerId,
                PaymentTermId = model.PaymentTermId,
                Date = model.Date,
                CreatedBy = User.Identity.Name,
                CreatedOn = DateTime.Now,
                ModifiedBy = User.Identity.Name,
                ModifiedOn = DateTime.Now,
            };
            foreach(var line in model.InvoicingDeliveryLines)
            {
                InvoicingDelivery.InvoicingDeliveryLines.Add(new InvoicingDeliveryLine()
                {
                    ItemId = line.ItemId,
                    MeasurementId = line.MeasurementId,
                    Quantity = line.Quantity,
                    Discount = line.Discount,
                    Price = line.Quantity * line.Price,
                    CreatedBy = User.Identity.Name,
                    CreatedOn = DateTime.Now,
                    ModifiedBy = User.Identity.Name,
                    ModifiedOn = DateTime.Now,
                });
            }
            _receivablesService.AddInvoicingDelivery(InvoicingDelivery, true);
            return RedirectToAction("SalesDeliveries");
        }

        [HttpPost, ActionName("AddInvoicingDelivery")]
        [FormValueRequiredAttribute("AddInvoicingDeliveryLineItem")]
        public ActionResult AddInvoicingDeliveryLineItem(Models.ViewModels.Receivables.InvoicingDeliveryViewModel model)
        {
            var item = _inventoryService.GetItemById(model.ItemId.Value);
            var line = new Models.ViewModels.Receivables.InvoicingDeliveryLineViewModel()
            {
                ItemId = model.ItemId,
                MeasurementId = model.MeasurementId,
                Quantity = model.Quantity,
                Price = item.Price.Value * model.Quantity,
                Discount  = model.Discount,
                LineTotalTaxAmount = item.ItemTaxAmountOutput * model.Quantity
            };

            model.InvoicingDeliveryLines.Add(line);
            return View(model);
        }

        [HttpPost, ActionName("AddInvoicingDelivery")]
        [FormValueRequiredAttribute("DeleteSaleDeliveryLineItem")]
        public ActionResult DeleteSaleDeliveryLineItem(Models.ViewModels.Receivables.InvoicingDeliveryViewModel model)
        {
            var request = HttpContext.Request;
            var deletedItem = request.Form["DeletedLineItem"];
            model.InvoicingDeliveryLines.Remove(model.InvoicingDeliveryLines.Where(i => i.ItemId == int.Parse(deletedItem.ToString())).FirstOrDefault());
            return View(model);
        }

        public ActionResult SalesInvoices()
        {
            var invoices = _receivablesService.GetInvoicingInvoices();
            var model = new SmartAccountsUI.Models.ViewModels.Receivables.SalesInvoices();
            foreach(var invoice in invoices)
            {
                model.SalesInvoiceListLines.Add(new Models.ViewModels.Receivables.SalesInvoiceListLine()
                {
                    Id = invoice.Id,
                    No = invoice.No,
                    Customer = invoice.Customer.Name,
                    Date = invoice.Date,
                    Amount = invoice.ComputeTotalAmount(),
                    IsFullPaid = invoice.IsFullPaid()
                });
            }
            return View(model);
        }

        public ActionResult AddSalesInvoice(bool direct = false)
        {
            var model = new SmartAccountsUI.Models.ViewModels.Receivables.AddSalesInvoice();
            model.Customers = Models.ModelViewHelper.Customers(_receivablesService.GetCustomers());
            model.Items = Models.ModelViewHelper.Items(_inventoryService.GetAllItems());
            model.Measurements = Models.ModelViewHelper.Measurements(_inventoryService.GetMeasurements());

            return View(model);
        }

        [HttpPost, ActionName("AddSalesInvoice")]
        [FormValueRequiredAttribute("SaveSalesInvoice")]
        public ActionResult SaveSalesInvoice(Models.ViewModels.Receivables.AddSalesInvoice model)
        {
            if (model.AddSalesInvoiceLines.Sum(i => i.Amount) == 0 || model.AddSalesInvoiceLines.Count < 1)
            {
                model.Customers = Models.ModelViewHelper.Customers(_receivablesService.GetCustomers());
                model.Items = Models.ModelViewHelper.Items(_inventoryService.GetAllItems());
                model.Measurements = Models.ModelViewHelper.Measurements(_inventoryService.GetMeasurements());
                ModelState.AddModelError("Amount", "No invoice line");
                return View(model);
            }
            var invoiceHeader = new InvoicingInvoiceHeader();
            var invoiceLines = new List<InvoicingInvoiceLine>();
            foreach (var item in model.AddSalesInvoiceLines)
            {
                var Item = _inventoryService.GetItemById(item.ItemId);
                var invoiceDetail = new InvoicingInvoiceLine();
                invoiceDetail.TaxId = Item.ItemTaxGroupId;
                invoiceDetail.ItemId = item.ItemId;
                invoiceDetail.MeasurementId = item.MeasurementId;
                invoiceDetail.Quantity = item.Quantity;
                invoiceDetail.Discount = item.Discount;
                invoiceDetail.Amount = Convert.ToDecimal(item.Quantity * Item.Price);
                invoiceDetail.CreatedBy = User.Identity.Name;
                invoiceDetail.CreatedOn = DateTime.Now;
                invoiceDetail.ModifiedBy = User.Identity.Name;
                invoiceDetail.ModifiedOn = DateTime.Now;
                invoiceLines.Add(invoiceDetail);
            }
            invoiceHeader.InvoicingInvoiceLines = invoiceLines;
            invoiceHeader.CustomerId = model.CustomerId;
            invoiceHeader.Date = model.Date;
            invoiceHeader.ShippingHandlingCharge = 4;// model.ShippingHandlingCharge;
            invoiceHeader.CreatedBy = User.Identity.Name;
            invoiceHeader.CreatedOn = DateTime.Now;
            invoiceHeader.ModifiedBy = User.Identity.Name;
            invoiceHeader.ModifiedOn = DateTime.Now;

            _receivablesService.AddInvoicingInvoice(invoiceHeader, model.SalesOrderId);
            return RedirectToAction("SalesInvoices");
        }

        [HttpPost, ActionName("AddSalesInvoice")]
        [FormValueRequiredAttribute("AddSalesInvoiceLine")]
        public ActionResult AddSalesInvoiceLine(Models.ViewModels.Receivables.AddSalesInvoice model)
        {
            model.Customers = Models.ModelViewHelper.Customers(_receivablesService.GetCustomers());
            model.Items = Models.ModelViewHelper.Items(_inventoryService.GetAllItems());
            model.Measurements = Models.ModelViewHelper.Measurements(_inventoryService.GetMeasurements());
            if (model.Quantity > 0)
            {
                var item = _inventoryService.GetItemById(model.ItemId);
                if (!item.Price.HasValue)
                {
                    ModelState.AddModelError("Amount", "Selling price is not set.");
                    return View(model);
                }
                Models.ViewModels.Receivables.AddSalesInvoiceLine itemModel = new Models.ViewModels.Receivables.AddSalesInvoiceLine()
                {
                    ItemId = model.ItemId,
                    MeasurementId = model.MeasurementId,
                    Quantity = model.Quantity,
                    Discount = model.Discount,
                    Amount = item.Price.Value * model.Quantity,
                    Price = item.Price.Value,
                };
                if (model.AddSalesInvoiceLines.FirstOrDefault(i => i.ItemId == model.ItemId) == null)
                    model.AddSalesInvoiceLines.Add(itemModel);
            }
            return View(model);
        }

        [HttpPost, ActionName("AddSalesInvoice")]
        [FormValueRequiredAttribute("DeleteInvoiceLineItem")]
        public ActionResult DeleteInvoiceLineItem(Models.ViewModels.Receivables.AddSalesInvoice model)
        {
            model.Customers = Models.ModelViewHelper.Customers(_receivablesService.GetCustomers());
            model.Items = Models.ModelViewHelper.Items(_inventoryService.GetAllItems());
            model.Measurements = Models.ModelViewHelper.Measurements(_inventoryService.GetMeasurements());

            var request = HttpContext.Request;
            var deletedItem = request.Form["DeletedLineItem"];
            model.AddSalesInvoiceLines.Remove(model.AddSalesInvoiceLines.Where(i => i.ItemId == int.Parse(deletedItem.ToString())).FirstOrDefault());

            return View(model);
        }

        public ActionResult AddSalesReceipt(int? salesInvoiceId = null)
        {
            var model = new Models.ViewModels.Receivables.AddSalesReceipt();
            if (salesInvoiceId.HasValue)
            {
                var salesInvoice = _receivablesService.GetInvoicingInvoiceById(salesInvoiceId.Value);
                model.SalesInvoiceId = salesInvoice.Id;
                model.SalesInvoiceNo = salesInvoice.No;
                model.InvoiceDate = salesInvoice.Date;
                model.Date = DateTime.Now;
                model.CustomerId = salesInvoice.CustomerId;

                foreach (var line in salesInvoice.InvoicingInvoiceLines)
                {
                    model.AddSalesReceiptLines.Add(new Models.ViewModels.Receivables.AddSalesReceiptLine()
                    {
                        SalesInvoiceLineId = line.Id,
                        ItemId = line.ItemId,
                        MeasurementId = line.MeasurementId,
                        Quantity = line.Quantity,
                        Discount = line.Discount,
                        Amount = line.Amount
                    });
                }
            }
            return View(model);
        }

        [HttpPost, ActionName("AddSalesReceipt")]
        [FormValueRequiredAttribute("SaveSalesReceipt")]
        public ActionResult SaveSalesReceipt(Models.ViewModels.Receivables.AddSalesReceipt model)
        {
            var receipt = new InvoicingReceiptHeader()
            {
                SaAccountToDebitId = model.SaAccountToDebitId,
                //SalesInvoiceHeaderId = model.SalesInvoiceId,
                CustomerId = model.CustomerId.Value,
                Date = model.Date,
                CreatedBy = User.Identity.Name,
                CreatedOn = DateTime.Now,
                ModifiedBy = User.Identity.Name,
                ModifiedOn = DateTime.Now,
            };
            foreach(var line in model.AddSalesReceiptLines)
            {
                if(line.AmountToPay > line.Amount)
                    return RedirectToAction("SalesInvoices");

                receipt.InvoicingReceiptLines.Add(new InvoicingReceiptLine()
                {
                    InvoicingInvoiceLineId = line.SalesInvoiceLineId,
                    ItemId = line.ItemId,
                    MeasurementId = line.MeasurementId,
                    Quantity = line.Quantity,
                    Discount = line.Discount,
                    Amount = line.Amount,
                    AmountPaid = line.AmountToPay,
                    CreatedBy = User.Identity.Name,
                    CreatedOn = DateTime.Now,
                    ModifiedBy = User.Identity.Name,
                    ModifiedOn = DateTime.Now,
                });
            }
            _receivablesService.AddInvoicingReceipt(receipt);
            return RedirectToAction("SalesInvoices");
        }

        public ActionResult Customers()
        {
            var customers = _receivablesService.GetCustomers();
            var model = new Models.ViewModels.Receivables.Customers();
            foreach(var customer in customers)
            {
                model.CustomerListLines.Add(new Models.ViewModels.Receivables.CustomerListLine()
                {
                    Id = customer.Id,
                    Name = customer.Name,
                    //FirstName = customer.FirstName,
                    //LastName = customer.LastName,
                    //Balance = customer.Balance
                });
            }
            return View(model);
        }

        public ActionResult Customer(long id = 0)
        {
            var model = new Models.ViewModels.CustomerViewModel();
            if (id == 0)
            {
                model.Id = id;
            }
            else
            {
                var customer = _receivablesService.GetCustomerById(id);
                var allocations = _receivablesService.GetCustomerReceiptsForAllocation(id);
                model.Id = customer.Id;
                model.Name = customer.Name;
                //model.Balance = customer.Balance;
                foreach (var receipt in allocations)
                {
                    model.CustomerAllocations.Add(new Models.ViewModels.Receivables.CustomerAllocation()
                    {
                        Id = receipt.Id,
                        AmountAllocated = receipt.InvoicingReceiptLines.Sum(a => a.AmountPaid),
                        AvailableAmountToAllocate = receipt.AvailableAmountToAllocate
                    });
                }
                //foreach (var invoice in customer.InvoicingInvoices)
                //{
                //    model.CustomerInvoices.Add(new Models.ViewModels.Receivables.CustomerSalesInvoice()
                //    {
                //        Id = invoice.Id,
                //        InvoiceNo = invoice.No,
                //        Date = invoice.Date,
                //        Amount = invoice.SalesInvoiceLines.Sum(a => a.Amount),
                //        Status = invoice.IsFullPaid() ? "Paid" : "Open"
                //    });
                //}            
            }
            return View(model);
        }

        public ActionResult AddCustomer()
        {
            var model = new Models.ViewModels.Receivables.AddCustomer();
            return View(model);
        }

        [HttpPost, ActionName("AddCustomer")]
        [FormValueRequiredAttribute("SaveCustomer")]
        public ActionResult AddCustomer(Models.ViewModels.Receivables.AddCustomer model)
        {
            var customer = new Customer()
            {
                Name = model.Name,
            };
            _receivablesService.AddCustomer(customer);
            return RedirectToAction("EditCustomer", new { id = customer.Id });
        }

        public ActionResult AddOrEditCustomer(int id = 0)
        {
            Models.ViewModels.Receivables.EditCustomer model = new Models.ViewModels.Receivables.EditCustomer();
            var saSaAccounts = _financialService.GetSaAccounts();
            if (id != 0)
            {
                var customer = _receivablesService.GetCustomerById(id);
                model.Id = customer.Id;
                model.Name = customer.Name;
                model.PrimaryContactId = customer.PrimaryContactId.HasValue ? customer.PrimaryContactId.Value : -1;
                model.AccountsReceivableSaAccountId = customer.SaAccountsReceivableSaAccountId.HasValue ? customer.SaAccountsReceivableSaAccountId : -1;
                model.SalesSaAccountId = customer.InvoicingSaAccountId.HasValue ? customer.InvoicingSaAccountId : -1;
                model.SalesDiscountSaAccountId = customer.InvoicingDiscountSaAccountId.HasValue ? customer.InvoicingDiscountSaAccountId : -1;
                model.PromptPaymentDiscountSaAccountId = customer.PromptPaymentDiscountSaAccountId.HasValue ? customer.PromptPaymentDiscountSaAccountId : -1;
            }
            return View(model);
        }

        [HttpPost, ActionName("AddOrEditCustomer")]
        [FormValueRequiredAttribute("SaveCustomer")]
        public ActionResult AddOrEditCustomer(Models.ViewModels.Receivables.EditCustomer model)
        {
            Customer customer = null;
            if (model.Id != 0)
            {
                customer = _receivablesService.GetCustomerById(model.Id);                
            }
            else
            {
                customer = new Customer();
   
            }


            customer.Name = model.Name;
            if (model.PrimaryContactId != -1) customer.PrimaryContactId = model.PrimaryContactId;
            customer.SaAccountsReceivableSaAccountId = model.AccountsReceivableSaAccountId.Value == -1 ? null : model.AccountsReceivableSaAccountId;
            customer.InvoicingSaAccountId = model.SalesSaAccountId.Value == -1 ? null : model.SalesSaAccountId;
            customer.InvoicingDiscountSaAccountId = model.SalesDiscountSaAccountId.Value == -1 ? null : model.SalesDiscountSaAccountId;
            customer.PromptPaymentDiscountSaAccountId = model.PromptPaymentDiscountSaAccountId.Value == -1 ? null : model.PromptPaymentDiscountSaAccountId;

            if (model.Id != 0)
                _receivablesService.UpdateCustomer(customer);
            else
                _receivablesService.AddCustomer(customer);

            return RedirectToAction("Customers");
        }

        public ActionResult CustomerDetail(int id)
        {
            var customer = _receivablesService.GetCustomerById(id);
            var allocations = _receivablesService.GetCustomerReceiptsForAllocation(id);
            var model = new Models.ViewModels.Receivables.CustomerDetail()
            {
                Id = customer.Id,
                Name = customer.Name,
                //Balance = customer.Balance
            };
            foreach(var receipt in allocations)
            {
                model.CustomerAllocations.Add(new Models.ViewModels.Receivables.CustomerAllocation()
                {
                    Id = receipt.Id,
                    AmountAllocated = receipt.InvoicingReceiptLines.Sum(a => a.AmountPaid),
                    AvailableAmountToAllocate = receipt.AvailableAmountToAllocate
                });
            }
            //foreach (var allocation in customer.CustomerAllocations)
            //{
            //    model.ActualAllocations.Add(new Models.ViewModels.Receivables.Allocations()
            //    {
            //        InvoiceNo = allocation.SalesInvoiceHeader.No,
            //        ReceiptNo = allocation.SalesReceiptHeader.No,
            //        Date = allocation.Date,
            //        Amount = allocation.Amount
            //    });
            //}
            //foreach(var invoice in customer.InvoicingInvoices)
            //{
            //    model.CustomerInvoices.Add(new Models.ViewModels.Receivables.CustomerSalesInvoice()
            //    {
            //        Id = invoice.Id,
            //        InvoiceNo = invoice.No,
            //        Date = invoice.Date,
            //        Amount = invoice.ComputeTotalAmount(),
            //        Status = invoice.Status.ToString()
            //     });
            //}
            return View(model);
        }

        public ActionResult Allocate(int id)
        {
            var receipt = _receivablesService.GetInvoicingReceiptById(id);
            var customer = _receivablesService.GetCustomerById(receipt.CustomerId);
            var allocations = _receivablesService.GetCustomerReceiptsForAllocation(customer.Id);
            var model = new Models.ViewModels.Receivables.Allocate();
            model.ReceiptId = id;
            model.TotalAmountAvailableToAllocate = allocations.Sum(a => a.AvailableAmountToAllocate);
            model.LeftToAllocateFromReceipt = receipt.AvailableAmountToAllocate;
            var openInvoices = _receivablesService.GetInvoicingInvoices().Where(inv => inv.IsFullPaid() == false);
            foreach(var invoice in openInvoices)
            {
                model.OpenInvoices.Add(new SelectListItem()
                {
                    Value = invoice.Id.ToString(),
                    Text = invoice.No + " - " + (invoice.ComputeTotalAmount() - invoice.InvoicingInvoiceLines.Sum(a => a.GetAmountPaid()))
                });
            }
            return PartialView(model);
        }

        [HttpPost, ActionName("Allocate")]
        [FormValueRequiredAttribute("SaveAllocation")]
        public ActionResult SaveAllocation(Models.ViewModels.Receivables.Allocate model)
        {
            var receipt = _receivablesService.GetInvoicingReceiptById(model.ReceiptId);
            var customer = _receivablesService.GetCustomerById(receipt.CustomerId);
            var invoice = _receivablesService.GetInvoicingInvoiceById(model.InvoiceId);
            var allocations = _receivablesService.GetCustomerReceiptsForAllocation(customer.Id);
            model.InvoiceId = model.InvoiceId;
            model.TotalAmountAvailableToAllocate = allocations.Sum(a => a.AvailableAmountToAllocate);
            model.LeftToAllocateFromReceipt = receipt.AvailableAmountToAllocate;
            if (invoice == null)
            {
                return View(model);
            }
            else
            {
                var invoiceTotalAmount = invoice.ComputeTotalAmount();
                if (model.AmountToAllocate > invoiceTotalAmount
                    || model.AmountToAllocate > receipt.AvailableAmountToAllocate
                    || invoice.Status == SACore.Domain.InvoicingInvoiceStatus.Closed)
                {
                    return View(model);
                }

                var allocation = new CustomerAllocation()
                {
                    CustomerId = customer.Id,
                    InvoicingReceiptHeaderId = receipt.Id,
                    InvoicingInvoiceHeaderId = invoice.Id,
                    Amount = model.AmountToAllocate,
                    Date = DateTime.Now
                };
                _receivablesService.SaveCustomerAllocation(allocation);
            }
            return RedirectToAction("CustomerDetail", new { id = customer.Id });
        }

        /// <summary>
        /// Add new receipt with no invoice
        /// </summary>
        /// <returns></returns>
        public ActionResult AddReceipt()
        {
            var model = new Models.ViewModels.Receivables.AddSalesReceipt();
            model.SaAccountToCreditId = _financialService.GetSaAccounts().Where(a => a.SaAccountName == "SaAccounts Receivable").FirstOrDefault().Id;
            return View(model);
        }

        [HttpPost, ActionName("AddReceipt")]
        [FormValueRequiredAttribute("SaveReceipt")]
        public ActionResult SaveReceipt(Models.ViewModels.Receivables.AddSalesReceipt model)
        {
            if (model.SaAccountToDebitId.Value == -1 || model.CustomerId.Value == -1)
                return View(model);

            var receipt = new InvoicingReceiptHeader()
            {
                SaAccountToDebitId = model.SaAccountToDebitId,
                CustomerId = model.CustomerId.Value,
                Date = model.Date,
                CreatedBy = User.Identity.Name,
                CreatedOn = DateTime.Now,
                ModifiedBy = User.Identity.Name,
                ModifiedOn = DateTime.Now,
                Amount = model.PaymentAmount
            };

            receipt.InvoicingReceiptLines.Add(new InvoicingReceiptLine()
            {
                SaAccountToCreditId = model.SaAccountToCreditId,
                Amount = model.Amount,
                AmountPaid = model.PaymentAmount,
                CreatedBy = User.Identity.Name,
                CreatedOn = DateTime.Now,
                ModifiedBy = User.Identity.Name,
                ModifiedOn = DateTime.Now,
            });

            _receivablesService.AddInvoicingReceiptNoInvoice(receipt);
            return RedirectToAction("Receipts");
        }

        [HttpPost, ActionName("AddReceipt")]
        [FormValueRequiredAttribute("AddReceiptItem")]
        public ActionResult AddReceiptItem(Models.ViewModels.Receivables.AddSalesReceipt model)
        {
            var rowId = Guid.NewGuid();
            if (model.ItemId.Value != -1 && model.Quantity > 0)
            {
                var item = _inventoryService.GetItemById(model.ItemId.Value);
                if (!item.Price.HasValue)
                {
                    ModelState.AddModelError("Amount", "Selling price is not set.");
                    return View(model);
                }
                Models.ViewModels.Receivables.AddSalesReceiptLine itemModel = new Models.ViewModels.Receivables.AddSalesReceiptLine()
                {
                    RowId = rowId.ToString(),
                    ItemId = model.ItemId.Value,
                    MeasurementId = model.MeasurementId.Value,
                    Quantity = model.Quantity,
                    Discount = model.Discount,
                    Amount = item.Price.Value * model.Quantity,
                    AmountToPay = model.AmountToPay
                };
                if (model.AddSalesReceiptLines.FirstOrDefault(i => i.ItemId == model.ItemId) == null)
                    model.AddSalesReceiptLines.Add(itemModel);
            }
            else if(!string.IsNullOrEmpty(model.SaAccountCode) && model.Amount != 0)
            {
                var saSaAccount = _financialService.GetSaAccounts().Where(a => a.SaAccountCode == model.SaAccountCode).FirstOrDefault();
                if(saSaAccount != null)
                {
                    Models.ViewModels.Receivables.AddSalesReceiptLine saSaAccountItemModel = new Models.ViewModels.Receivables.AddSalesReceiptLine()
                    {
                        RowId = rowId.ToString(),
                        SaAccountToCreditId = saSaAccount.Id,
                        Amount = model.AmountToPay,
                        AmountToPay = model.AmountToPay,
                    };
                    model.AddSalesReceiptLines.Add(saSaAccountItemModel);
                }                
            }
            return View(model);
        }

        [HttpPost, ActionName("AddReceipt")]
        [FormValueRequiredAttribute("DeleteReceiptLineItem")]
        public ActionResult DeleteReceiptLineItem(Models.ViewModels.Receivables.AddSalesReceipt model)
        {
            var request = HttpContext.Request;
            var deletedItem = request.Form["DeletedLineItem"];
            model.AddSalesReceiptLines.Remove(model.AddSalesReceiptLines.Where(i => i.ItemId.ToString() == deletedItem.ToString()).FirstOrDefault());
            return View(model);
        }

        public ActionResult Receipts()
        {
            var receipts = _receivablesService.GetInvoicingReceipts();
            var model = new Models.ViewModels.Receivables.SalesReceipts();
            foreach(var receipt in receipts)
            {
                model.SalesReceiptListLines.Add(new Models.ViewModels.Receivables.SalesReceiptListLine()
                {
                    No = receipt.No,
                    //InvoiceNo = receipt.SalesInvoiceHeader != null ? receipt.SalesInvoiceHeader.No : string.Empty,
                    CustomerId = receipt.CustomerId,
                    CustomerName = receipt.Customer.Name,
                    Date = receipt.Date,
                    Amount = receipt.InvoicingReceiptLines.Sum(r => r.Amount),
                    AmountPaid = receipt.InvoicingReceiptLines.Sum(r => r.AmountPaid)
                });
            }
            return View(model);
        }

        public ActionResult InvoicingDelivery(int id = 0)
        {
            var model = new SmartAccountsUI.Models.ViewModels.Receivables.SalesHeaderViewModel(_inventoryService, _financialService);
            model.DocumentType = SACore.Domain.DocumentTypes.InvoicingDelivery;

            if (id == 0)
            {
                return View(model);
            }
            else
            {
                var invoice = _receivablesService.GetInvoicingInvoiceById(id);
                model.Id = invoice.Id;
                model.CustomerId = invoice.CustomerId;
                model.Date = invoice.Date;
                model.No = invoice.No;
                foreach (var line in invoice.InvoicingInvoiceLines)
                {
                    var lineItem = new Models.ViewModels.Receivables.SalesLineItemViewModel(_financialService);
                    lineItem.Id = line.Id;
                    lineItem.ItemId = line.ItemId;
                    lineItem.ItemNo = line.Item.No;
                    lineItem.ItemDescription = line.Item.Description;
                    lineItem.Measurement = line.Measurement.Description;
                    lineItem.Quantity = line.Quantity;
                    lineItem.Discount = line.Discount;
                    lineItem.Price = line.Amount;
                    model.SalesLine.SalesLineItems.Add(lineItem);
                }
                return View(model);
            }
        }

        public ActionResult SalesInvoice(int id = 0)
        {
            var model = new SmartAccountsUI.Models.ViewModels.Receivables.SalesHeaderViewModel(_inventoryService, _financialService);
            model.DocumentType = SACore.Domain.DocumentTypes.InvoicingInvoice;

            if (id == 0)
            {   
                return View(model);
            }
            else
            {
                var invoice = _receivablesService.GetInvoicingInvoiceById(id);
                model.Id = invoice.Id;
                model.CustomerId = invoice.CustomerId;
                model.Date = invoice.Date;
                model.No = invoice.No;
                foreach (var line in invoice.InvoicingInvoiceLines)
                {
                    var lineItem = new Models.ViewModels.Receivables.SalesLineItemViewModel(_financialService);
                    lineItem.SetServiceHelpers(_financialService);
                    lineItem.Id = line.Id;
                    lineItem.ItemId = line.ItemId;
                    lineItem.ItemNo = line.Item.No;
                    lineItem.ItemDescription = line.Item.Description;
                    lineItem.Measurement = line.Measurement.Description;
                    lineItem.Quantity = line.Quantity;
                    lineItem.Discount = line.Discount;
                    lineItem.Price = line.Amount;
                    model.SalesLine.SalesLineItems.Add(lineItem);
                }
                return View(model);
            }
        }

        public ActionResult SalesOrder(int id = 0)
        {
            var model = new Models.ViewModels.Receivables.SalesHeaderViewModel(_inventoryService, _financialService);
            model.DocumentType = SACore.Domain.DocumentTypes.InvoicingOrder;

            if (id == 0)
            {
                return View(model);
            }
            else
            {
                var order = _receivablesService.GetInvoicingOrderById(id);
                model.Id = order.Id;
                model.CustomerId = order.CustomerId;
                model.PaymentTermId = order.PaymentTermId;
                model.Date = order.Date;
                model.Reference = order.ReferenceNo;
                model.No = order.No;

                foreach (var line in order.InvoicingOrderLines)
                {
                    var lineItem = new Models.ViewModels.Receivables.SalesLineItemViewModel(_financialService);
                    lineItem.Id = line.Id;
                    lineItem.ItemId = line.ItemId;
                    lineItem.ItemNo = line.Item.No;
                    lineItem.ItemDescription = line.Item.Description;
                    lineItem.Measurement = line.Measurement.Description;
                    lineItem.Quantity = line.Quantity;
                    lineItem.Discount = line.Discount;
                    lineItem.Price = line.Amount;
                    model.SalesLine.SalesLineItems.Add(lineItem);
                }
                return View(model);
            }
        }

        [HttpPost, ActionName("SalesOrder")]
        [FormValueRequiredAttribute("Save")]
        public ActionResult SaveOrder(Models.ViewModels.Receivables.SalesHeaderViewModel model)
        {
            InvoicingOrderHeader order = null;
            if (model.Id == 0)
            {
                order = new InvoicingOrderHeader();
                order.CreatedBy = User.Identity.Name;
                order.CreatedOn = DateTime.Now;
            }
            else
            {
                order = _receivablesService.GetInvoicingOrderById(model.Id);
            }

            order.ModifiedBy = User.Identity.Name;
            order.ModifiedOn = DateTime.Now;
            order.CustomerId = model.CustomerId.Value;
            order.PaymentTermId = model.PaymentTermId;
            order.Date = model.Date;

            foreach (var line in model.SalesLine.SalesLineItems)
            {
                InvoicingOrderLine lineItem = null;
                var item = _inventoryService.GetItemByNo(line.ItemNo);
                if (!line.Id.HasValue)
                {
                    lineItem = new InvoicingOrderLine();
                    lineItem.CreatedBy = User.Identity.Name;
                    lineItem.CreatedOn = DateTime.Now;
                    order.InvoicingOrderLines.Add(lineItem);
                }
                else
                {
                    lineItem = order.InvoicingOrderLines.Where(i => i.Id == line.Id).FirstOrDefault();
                }

                lineItem.ModifiedBy = User.Identity.Name;
                lineItem.ModifiedOn = DateTime.Now;
                lineItem.ItemId = line.ItemId;
                lineItem.MeasurementId = item.SellMeasurementId.Value;
                lineItem.Quantity = line.Quantity;
                lineItem.Discount = line.Discount;
                lineItem.Amount = line.Price;
            }

            if (model.Id == 0)
                _receivablesService.AddInvoicingOrder(order, true);
            else
                _receivablesService.UpdateInvoicingOrder(order);

            return RedirectToAction("SalesOrders");
        }

        [HttpPost, ActionName("InvoicingDelivery")]
        [FormValueRequiredAttribute("Save")]
        public ActionResult SaveDelivery(Models.ViewModels.Receivables.SalesHeaderViewModel model)
        {
            InvoicingDeliveryHeader delivery = null;
            if (model.Id == 0)
            {
                delivery = new InvoicingDeliveryHeader();
                delivery.CreatedBy = User.Identity.Name;
                delivery.CreatedOn = DateTime.Now;
            }
            else
            {
                delivery = _receivablesService.GetInvoicingDeliveryById(model.Id);
            }

            delivery.ModifiedBy = User.Identity.Name;
            delivery.ModifiedOn = DateTime.Now;
            delivery.CustomerId = model.CustomerId.Value;
            delivery.PaymentTermId = model.PaymentTermId;
            delivery.Date = model.Date;

            foreach (var line in model.SalesLine.SalesLineItems)
            {
                InvoicingDeliveryLine lineItem = null;
                var item = _inventoryService.GetItemByNo(line.ItemNo);
                if (!line.Id.HasValue)
                {
                    lineItem = new InvoicingDeliveryLine();
                    lineItem.CreatedBy = User.Identity.Name;
                    lineItem.CreatedOn = DateTime.Now;
                    delivery.InvoicingDeliveryLines.Add(lineItem);
                }
                else
                {
                    lineItem = delivery.InvoicingDeliveryLines.Where(i => i.Id == line.Id).FirstOrDefault();
                }

                lineItem.ModifiedBy = User.Identity.Name;
                lineItem.ModifiedOn = DateTime.Now;
                lineItem.ItemId = line.ItemId;
                lineItem.MeasurementId = item.SellMeasurementId.Value;
                lineItem.Quantity = line.Quantity;
                lineItem.Discount = line.Discount;
                lineItem.Price = line.Price;
            }

            if (model.Id == 0)
            {
                _receivablesService.AddInvoicingDelivery(delivery, true);
            }
            else
            {
                
            }

            return RedirectToAction("SalesDeliveries");
        }


        [HttpPost, ActionName("SalesInvoice")]
        [FormValueRequiredAttribute("Save")]
        public ActionResult SaveInvoice(Models.ViewModels.Receivables.SalesHeaderViewModel model)
        {
            InvoicingInvoiceHeader invoice = null;
            if (model.Id == 0)
            {
                invoice = new InvoicingInvoiceHeader();
                invoice.CreatedBy = User.Identity.Name;
                invoice.CreatedOn = DateTime.Now;
            }
            else
            {
                invoice = _receivablesService.GetInvoicingInvoiceById(model.Id);
            }

            invoice.ModifiedBy = User.Identity.Name;
            invoice.ModifiedOn = DateTime.Now;
            invoice.CustomerId = model.CustomerId.Value;
            invoice.Date = model.Date;
            invoice.ShippingHandlingCharge = model.ShippingHandlingCharges;
            
            foreach (var line in model.SalesLine.SalesLineItems)
            {
                InvoicingInvoiceLine lineItem = null;
                var item = _inventoryService.GetItemByNo(line.ItemNo);
                if (!line.Id.HasValue)
                {
                    lineItem = new InvoicingInvoiceLine();
                    lineItem.CreatedBy = User.Identity.Name;
                    lineItem.CreatedOn = DateTime.Now;
                    invoice.InvoicingInvoiceLines.Add(lineItem);
                }
                else
                {
                    lineItem = invoice.InvoicingInvoiceLines.Where(i => i.Id == line.Id).FirstOrDefault();
                }

                lineItem.ModifiedBy = User.Identity.Name;
                lineItem.ModifiedOn = DateTime.Now;
                lineItem.ItemId = line.ItemId;
                lineItem.MeasurementId = item.SellMeasurementId.Value;
                lineItem.Quantity = line.Quantity;
                lineItem.Discount = line.Discount;
                lineItem.Amount = line.Price;
            }

            if (model.Id == 0)
            {
                _receivablesService.AddInvoicingInvoice(invoice, null);
            }
            else
            {
                _receivablesService.UpdateInvoicingInvoice(invoice);
            }

            return RedirectToAction("SalesInvoices");
        }

        [HttpPost, ActionName("SalesOrder")]
        [FormValueRequiredAttribute("AddLineItem")]
        public ActionResult AddLineItemOrder(Models.ViewModels.Receivables.SalesHeaderViewModel model)
        {
            AddLineItem(model);
            return ReturnView(model);
        }

        [HttpPost, ActionName("SalesInvoice")]
        [FormValueRequiredAttribute("AddLineItem")]
        public ActionResult AddLineItemInvoice(Models.ViewModels.Receivables.SalesHeaderViewModel model)
        {
            AddLineItem(model);
            return ReturnView(model);
        }

        [HttpPost, ActionName("InvoicingDelivery")]
        [FormValueRequiredAttribute("AddLineItem")]
        public ActionResult AddLineItemDelivery(Models.ViewModels.Receivables.SalesHeaderViewModel model)
        {
            AddLineItem(model);
            return ReturnView(model);
        }

        public ActionResult ReturnView(Models.ViewModels.Receivables.SalesHeaderViewModel model)
        {
            string actionName = string.Empty;
            switch (model.DocumentType)
            {
                case SACore.Domain.DocumentTypes.InvoicingOrder:
                    actionName = "SalesOrder";
                    break;
                case SACore.Domain.DocumentTypes.InvoicingInvoice:
                    actionName = "SalesInvoice";
                    break;
                case SACore.Domain.DocumentTypes.InvoicingDelivery:
                    actionName = "InvoicingDelivery";
                    break;
            }

            return View(actionName, model);
        }

        private void AddLineItem(Models.ViewModels.Receivables.SalesHeaderViewModel model)
        {
            var item = _inventoryService.GetItemByNo(model.SalesLine.ItemNo);
            var newLine = new Models.ViewModels.Receivables.SalesLineItemViewModel(_financialService);
            newLine.ItemId = item.Id;
            newLine.ItemNo = item.No;
            newLine.ItemDescription = item.Description;
            newLine.Measurement = item.SellMeasurement.Description;
            newLine.Quantity = model.SalesLine.Quantity;
            newLine.Price = model.SalesLine.Price;
            newLine.Discount = model.SalesLine.Discount;            
            model.SalesLine.SalesLineItems.Add(newLine);

            foreach (var line in model.SalesLine.SalesLineItems)
                line.SetServiceHelpers(_financialService);
        }

        [HttpPost, ActionName("SalesOrder")]
        [FormValueRequiredAttribute("DeleteLineItem")]
        public ActionResult DeleteLineItemOrder(Models.ViewModels.Receivables.SalesHeaderViewModel model)
        {
            DeleteLineItem(model);
            return ReturnView(model);
        }

        [HttpPost, ActionName("SalesInvoice")]
        [FormValueRequiredAttribute("DeleteLineItem")]
        public ActionResult DeleteLineItemInvoice(Models.ViewModels.Receivables.SalesHeaderViewModel model)
        {
            DeleteLineItem(model);
            return ReturnView(model);
        }

        [HttpPost, ActionName("InvoicingDelivery")]
        [FormValueRequiredAttribute("DeleteLineItem")]
        public ActionResult DeleteLineItemDelivery(Models.ViewModels.Receivables.SalesHeaderViewModel model)
        {
            DeleteLineItem(model);
            return ReturnView(model);
        }

        private void DeleteLineItem(Models.ViewModels.Receivables.SalesHeaderViewModel model)
        {
            model.SetServiceHelpers(_inventoryService, _financialService);
            var request = HttpContext.Request;
            var deletedItem = request.Form["DeletedLineItem"];
            if (!string.IsNullOrEmpty(deletedItem))
            {
                model.SalesLine.SalesLineItems.RemoveAt(int.Parse(deletedItem));
            }
        }

        public ActionResult AddCustomerContact()
        {
            var model = new SmartAccountsUI.Models.ViewModels.ContactViewModel();
            return View("Contact", model);
        }

        [HttpPost]
        public ActionResult AddCustomerContact(SmartAccountsUI.Models.ViewModels.ContactViewModel model)
        {
            var contact = new SACore.Domain.Contact();
            contact.ContactType = SACore.Domain.ContactTypes.Customer;
            contact.FirstName = model.FirstName;
            contact.MiddleName = model.MiddleName;
            contact.LastName = model.LastName;
            contact.IsActive = true;
            _receivablesService.SaveContact(contact);
            if(contact.Id > 0)
                return Json(new { Status = "success" });
            else
                return Json(new { Status = "falied" });
        }
    }
}
