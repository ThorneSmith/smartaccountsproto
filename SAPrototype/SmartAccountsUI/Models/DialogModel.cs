﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SmartAccountsUI.Models
{
    public class DialogModel
    {
        [Required]
        public int Value { get; set; }
    }
}