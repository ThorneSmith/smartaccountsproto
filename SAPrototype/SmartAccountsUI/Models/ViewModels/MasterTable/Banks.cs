﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using SACore.Domain;
using SACore.Domain.Financials;

namespace SmartAccountsUI.Models.ViewModels.MasterTable
{
    public partial class Banks
    {
        public Banks()
        {
            BankListLines = new HashSet<BankListLine>();
            BankEditLines = new HashSet<BankEditLine>();
        }

        public ICollection<BankListLine> BankListLines { get; set; }
        public ICollection<BankEditLine> BankEditLines { get; set; }
    }

    public partial class BankEditLine
    {
        public long Id { get; set; }
        public BankTypes BankType { get; set; }
        public string Name { get; set; }
        public long? SaAccountId { get; set; }
        public string BankName { get; set; }
        public string Number { get; set; }
        public long AddressId { get; set; }
        public bool IsDefault { get; set; }
        public bool IsActive { get; set; }
    }

    
    public partial class BankListLine
    {
        public long Id { get; set; }
        public BankTypes BankType { get; set; }
        public string Name { get; set; }
        public long? SaAccountId { get; set; }
        public string BankName { get; set; }
        public string Number { get; set; }
        public long AddressId { get; set; }
        public bool IsDefault { get; set; }
        public bool IsActive { get; set; }
        public decimal Balance { get; set; }
    }

    public partial class BankEdit
    {
        public BankEdit()
        {
        }

        public long         Id                      { get; set; }
        public Guid         LegalEntityId                   { get; set; }
        public BankTypes    BankType                    { get; set; }
        public string       Name                    { get; set; }
        public long?        SaAccountId             { get; set; }
        public string       BankName                { get; set; }
        public string       Number                  { get; set; }
        public long AddressId { get; set; }
        public bool         IsDefault               { get; set; }
        public bool         IsActive                { get; set; }

        public virtual SaAccount SaAccount { get; set; }

    }

    public partial class BankAdd
    {
        public BankAdd()
        {
        }

        public long Id { get; set; }
        public Guid LegalEntityId { get; set; }
        public BankTypes BankType { get; set; }
        public string Name { get; set; }
        public long? SaAccountId { get; set; }
        public string BankName { get; set; }
        public string Number { get; set; }
        public long AddressId { get; set; }
        public bool IsDefault { get; set; }
        public bool IsActive { get; set; }

        public virtual SaAccount SaAccount { get; set; }

    }
}