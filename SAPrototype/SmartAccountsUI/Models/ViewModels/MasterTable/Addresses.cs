﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using SACore.Domain;
using SACore.Domain.MasterTables;

namespace SmartAccountsUI.Models.ViewModels.MasterTable
{
    public partial class Addresses
    {
        public Addresses()
        {
            AddressListLines = new HashSet<AddressListLine>();
            AddressEditLines = new HashSet<AddressEditLine>();
        }

        public ICollection<AddressListLine> AddressListLines { get; set; }
        public ICollection<AddressEditLine> AddressEditLines { get; set; }
    }

    public partial class AddressEditLine
    {
        public long Id { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City      { get; set; }
        public long StateId     { get; set; }
        public string Zip       { get; set; }
    }

    
    public partial class AddressListLine
    {
        public long Id { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public long StateId { get; set; }
        public string Zip { get; set; }

    }

    public partial class AddressEdit
    {
        public AddressEdit()
        {
        }

        public long   Id { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public long StateId { get; set; }
        public string Zip { get; set; }

    }

    public partial class AddressAdd
    {
        public AddressAdd()
        {
        }

        public long Id { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public long StateId { get; set; }
        public string Zip { get; set; }

    }
}