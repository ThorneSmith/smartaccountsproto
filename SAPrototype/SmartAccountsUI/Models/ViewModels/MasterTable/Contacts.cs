﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using SACore.Domain;
using SACore.Domain.Financials;

namespace SmartAccountsUI.Models.ViewModels.MasterTable
{
    public partial class Contacts
    {
        public Contacts()
        {
            ContactListLines = new HashSet<ContactListLine>();
            ContactEditLines = new HashSet<ContactEditLine>();
        }

        public ICollection<ContactListLine> ContactListLines { get; set; }
        public ICollection<ContactEditLine> ContactEditLines { get; set; }
    }

    public partial class ContactEditLine
    {
        public long Id { get; set; }
        public ContactTypes ContactType { get; set; }
        public long? PartyId { get; set; }
        public Guid LegalEntityId { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string MiddleName { get; set; }
    }

    
    public partial class ContactListLine
    {
        public long Id { get; set; }
        public ContactTypes ContactType { get; set; }
        public long? PartyId { get; set; }
        public Guid LegalEntityId { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string MiddleName { get; set; }
    }

    public partial class ContactEdit
    {
        public ContactEdit()
        {
        }

        public long         Id                      { get; set; }
        public ContactTypes ContactType { get; set; }
        public long? PartyId { get; set; }
        public Guid LegalEntityId { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string MiddleName { get; set; }

    }

    public partial class ContactAdd
    {
        public ContactAdd()
        {
        }

        public long Id { get; set; }
        public ContactTypes ContactType { get; set; }
        public long? PartyId { get; set; }
        public Guid LegalEntityId { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string MiddleName { get; set; }

    }
}