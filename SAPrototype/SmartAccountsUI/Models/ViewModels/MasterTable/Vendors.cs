﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SmartAccountsUI.Models.ViewModels.MasterTable
{
    public class Vendors
    {
        public Vendors()
        {
            VendorsList = new HashSet<VendorsListLine>();
        }

        public virtual ICollection<VendorsListLine> VendorsList { get; set; }
    }

    public class VendorsListLine
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public decimal Balance { get; set; }
    }

    public class VendorEdit
    {
        public VendorEdit()
        {
            VendorItems = new HashSet<SelectListItem>();
        }

        public long? Id { get; set; }
        [Required]
        public string Name { get; set; }
        public long? TaxGroupId { get; set; }
        public long? AccountsPayableSaAccountId { get; set; }
        public long? PurchaseSaAccountId { get; set; }
        public long? PurchaseDiscountSaAccountId { get; set; }

        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string MiddleName { get; set; }
        public virtual string Email { get; set; }
        public virtual string Website { get; set; }
        public virtual string Phone { get; set; }
        public virtual string Fax { get; set; }

        public virtual ICollection<SelectListItem> VendorItems { get; set; }
    }

    public class VendorAdd
    {
        public VendorAdd()
        {
            SaAccounts = new HashSet<SelectListItem>();
        }
        public string VendorName { get; set; }
        public int? TaxGroupId { get; set; }
        public int? AccountsPayableSaAccountId { get; set; }
        public int? PurchaseSaAccountId { get; set; }
        public int? PurchaseDiscountSaAccountId { get; set; }

        public virtual ICollection<SelectListItem> SaAccounts { get; set; }
    }
}