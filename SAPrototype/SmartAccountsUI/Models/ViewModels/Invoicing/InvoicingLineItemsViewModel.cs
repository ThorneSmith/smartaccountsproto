﻿using System;
using System.Collections.Generic;
using System.Linq;
using SAServices.Financial;
using SAServices.Inventory;

namespace SmartAccountsUI.Models.ViewModels.Invoicing
{
    public class InvoicingLineItemsViewModel
    {
        private IInventoryService _inventoryService;
        private IFinancialService _financialService;

        public InvoicingLineItemsViewModel()
        {
            InvoicingLineItems = new List<InvoicingLineItemViewModel>();
            InvoicingLineItemsTaxes = new List<InvoicingLineItemTaxViewModel>();
            Quantity = 1;
        }

        public InvoicingLineItemsViewModel(IInventoryService inventoryService, IFinancialService financialService)
            : this()
        {
            _inventoryService = inventoryService;
            _financialService = financialService;
        }

        public decimal? ShippingCharges { get; set; }
        public decimal SubTotal { get { return ComputeSubTotal(); } }
        public decimal AmountTotal { get { return ComputeAmountTotal(); } }
        public decimal AmountTax { get { return ComputeAmountTax(); } }

        public IList<InvoicingLineItemViewModel> InvoicingLineItems { get; set; }
        public IList<InvoicingLineItemTaxViewModel> InvoicingLineItemsTaxes { get; set; }

        #region Fields for New Line Item
        public string ItemId { get; set; }
        public string ItemNo { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Discount { get; set; }
        #endregion

        private decimal ComputeSubTotal()
        {
            decimal subTotal = 0;
            foreach (var line in InvoicingLineItems)
            {
                subTotal += line.Total;
            }
            return subTotal;
        }

        private decimal ComputeAmountTotal()
        {
            decimal amountTotal = 0;
            foreach (var line in InvoicingLineItems)
            {
                amountTotal += line.Total;
            }
            return amountTotal;
        }

        public decimal ComputeAmountTax()
        {
            decimal amountTax = 0;
            amountTax = InvoicingLineItems.Sum(t => t.TaxAmount);
            return amountTax;
        }

        public void SetServiceHelpers(IInventoryService inventoryService, IFinancialService financialService)
        {
            _inventoryService = inventoryService;
            foreach(var line in InvoicingLineItems)
                line.SetServiceHelpers(financialService);
        }
    }

    public class InvoicingLineItemViewModel
    {
        private IFinancialService _financialService;
        
        public InvoicingLineItemViewModel()
        { }

        public InvoicingLineItemViewModel(IFinancialService financialService)
            :this()
        {
            _financialService = financialService;
        }

        public long? Id { get; set; }
        public long ItemId { get; set; }
        public string ItemNo { get; set; }
        public string ItemDescription { get; set; }
        public string Measurement { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Discount { get; set; }
        public decimal Total { get { return ComputeLineTotal(); } }
        public decimal TaxAmount { get { return ComputeLineTaxAmount(); } }

        private decimal ComputeLineTotal()
        {
            return (Quantity * Price) - ComputeLineDiscount();
        }

        public decimal ComputeLineDiscount()
        {
            decimal discountAmount = 0;
            if (Discount > 0)
                discountAmount = (Quantity * Price) * (Discount / 100);
            return discountAmount;
        }

        public decimal ComputeLineTaxAmount()
        {
            decimal lineTaxAmount = 0;
            if (_financialService != null)
            {
                var taxes = _financialService.ComputeOutputTax(ItemId, Quantity, Total, Discount);
                lineTaxAmount = Math.Round(taxes.Sum(t => t.Value), 2);
            }
            return lineTaxAmount;
        }

        public void SetServiceHelpers(IFinancialService financialService)
        {
            _financialService = financialService;
        }
    }

    public class InvoicingLineItemTaxViewModel
    {
        public decimal TaxId { get; set; }
        public decimal TaxRate { get; set; }
        public decimal TaxName { get; set; }
        public decimal Amount { get; set; }
    }
}