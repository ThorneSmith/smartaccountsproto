﻿using System;
using SACore.Domain;
using SAServices.Financial;
using SAServices.Inventory;

namespace SmartAccountsUI.Models.ViewModels.Invoicing
{
    public class InvoicingHeaderViewModel
    {
        IInventoryService _inventoryService;
        IFinancialService _financialService;

        public InvoicingHeaderViewModel()
        {
            InvoicingLine = new InvoicingLineItemsViewModel(_inventoryService, _financialService);
            Date = DateTime.Now;
        }

        public InvoicingHeaderViewModel(IInventoryService inventoryService, IFinancialService financialService)
            :this()
        {
            _inventoryService = inventoryService;
            _financialService = financialService;
        }

        public DocumentTypes DocumentType { get; set; }
        public long Id { get; set; }
        public long? CustomerId { get; set; }
        public long? PaymentTermId { get; set; }
        public string Reference { get; set; }
        public string No { get; set; }
        public DateTime Date { get; set; }
        public decimal ShippingHandlingCharges { get; set; }
        public InvoicingLineItemsViewModel InvoicingLine { get; set; }

        public void SetServiceHelpers(IInventoryService inventoryService, IFinancialService financialService)
        {
            InvoicingLine.SetServiceHelpers(inventoryService, financialService);
        }
    }
}