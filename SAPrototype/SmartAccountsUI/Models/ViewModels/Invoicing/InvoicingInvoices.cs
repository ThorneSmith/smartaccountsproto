﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartAccountsUI.Models.ViewModels.Invoicing
{
    public class InvoicingInvoices
    {
        public InvoicingInvoices()
        {
            InvoicingInvoiceListLines = new HashSet<InvoicingInvoiceListLine>();
        }

        public virtual ICollection<InvoicingInvoiceListLine> InvoicingInvoiceListLines { get; set; }
    }

    public class InvoicingInvoiceListLine
    {
        public long Id { get; set; }
        public string No { get; set; }
        public string Customer { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }
        public bool IsFullPaid { get; set; }
    }

    public class AddInvoicingInvoice
    {
        public AddInvoicingInvoice()
        {
            Date = DateTime.Now;
            AddInvoicingInvoiceLines = new List<AddInvoicingInvoiceLine>();
            
            //remove after testing
            Discount = 2;
            ShippingHandlingCharge = 4;
        }

        public long CustomerId { get; set; }
        public DateTime Date { get; set; }
        public long? InvoicingOrderId { get; set; }
        public decimal ShippingHandlingCharge { get; set; }

        public IList<AddInvoicingInvoiceLine> AddInvoicingInvoiceLines { get; set; }

        public IEnumerable<SelectListItem> Customers { get; set; }
        public IEnumerable<SelectListItem> Items { get; set; }
        public IEnumerable<SelectListItem> Measurements { get; set; }

        #region Fields for new invoice item
        public long ItemId { get; set; }
        public long MeasurementId { get; set; }
        public decimal Quantity { get; set; }
        public decimal Amount { get; set; }
        public decimal Discount { get; set; }
        public decimal Price { get; set; }
        #endregion
    }

    public class AddInvoicingInvoiceLine
    {
        public long ItemId { get; set; }
        public long MeasurementId { get; set; }
        public decimal Quantity { get; set; }
        public decimal Amount { get; set; }
        public decimal Discount { get; set; }
        public decimal Price { get; set; }
    }
}