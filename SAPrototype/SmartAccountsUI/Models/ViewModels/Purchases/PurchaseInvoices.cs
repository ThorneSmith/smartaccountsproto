﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartAccountsUI.Models.ViewModels.Purchases
{
    public class PurchaseInvoices
    {
        public PurchaseInvoices()
        {
            PurchaseInvoiceListLines = new HashSet<PurchaseInvoiceListLine>();
        }

        public ICollection<PurchaseInvoiceListLine> PurchaseInvoiceListLines { get; set; }
    }

    public class PurchaseInvoiceListLine
    {
        public long Id { get; set; }
        public string No { get; set; }
        public DateTime Date { get; set; }
        public string Vendor { get; set; }
        public decimal Amount { get; set; }
        public string Status { get; set; }
        public bool IsPaid { get; set; }
    }

    public class MakePayment
    {
        public MakePayment()
        {
        }
        public long InvoiceId { get; set; }
        public long SaAccountId { get; set; }
        public string InvoiceNo { get; set; }
        public string Vendor { get; set; }
        public decimal Amount { get; set; }
        public decimal AmountToPay { get; set; }
    }
}