﻿using System;
using SACore.Domain;

namespace SmartAccountsUI.Models.ViewModels.Purchases
{
    public class PurchaseHeaderViewModel
    {
        public PurchaseHeaderViewModel()
        {
            PurchaseLine = new PurchaseLineItemsViewModel();
            Date = DateTime.Now;
        }

        public long? Id { get; set; }
        public long? VendorId { get; set; }
        public DateTime Date { get; set; }
        public string ReferenceNo { get; set; }
        public string VendorInvoiceNo { get; set; }
        public DocumentTypes DocumentType { get; set; }
        public PurchaseLineItemsViewModel PurchaseLine { get; set; }
        public bool IsDirect { get; set; }
    }
}