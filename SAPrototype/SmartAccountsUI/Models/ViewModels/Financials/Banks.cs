﻿using System.Collections.Generic;
using SACore.Domain;

namespace SmartAccountsUI.Models.ViewModels.Financials
{
    public partial class Banks
    {
        public Banks()
        {
            BankList = new HashSet<BankListLine>();
            BankEdit = new HashSet<BankEditLine>();
        }

        public ICollection<BankListLine> BankList { get; set; }
        public ICollection<BankEditLine> BankEdit { get; set; }
    }

    public partial class BankEditLine
    {
        public long Id { get; set; }
        public BankTypes Type { get; set; }
        public string Name { get; set; }
        public long? SaAccountId { get; set; }
        public string BankName { get; set; }
        public string Number { get; set; }
        public string Address { get; set; }
        public bool IsDefault { get; set; }
        public bool IsActive { get; set; }
    }




    public partial class BankListLine
    {
        public BankTypes Type { get; set; }
        public string Name { get; set; }
        public long? SaAccountId { get; set; }
        public string BankName { get; set; }
        public string Number { get; set; }
        public string Address { get; set; }
        public bool IsDefault { get; set; }
        public bool IsActive { get; set; }
    }
}