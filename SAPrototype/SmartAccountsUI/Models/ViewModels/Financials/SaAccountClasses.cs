﻿using System;
using System.Collections.Generic;
using SACore.Domain.Financials;

namespace SmartAccountsUI.Models.ViewModels.Financials
{
    public partial class SaAccountClasses
    {
        public SaAccountClasses()
        {
            SaAccountClassListLines = new HashSet<SaAccountClassListLine>();
            SaAccountClassEditLines = new HashSet<SaAccountClassEditLine>();
        }

        public ICollection<SaAccountClassListLine> SaAccountClassListLines { get; set; }
        public ICollection<SaAccountClassEditLine> SaAccountClassEditLines { get; set; }
    }

    public partial class SaAccountClassEditLine
    {
        public long Id { get; set; }
        public Guid LegalEntityId { get; set; }
        public string Name { get; set; }
        public string BeginningRangeForType { get; set; }
        public string EndRangeForType { get; set; }
        public string NormalBalance { get; set; }
    }

    
    public partial class SaAccountClassListLine
    {
        public long Id { get; set; }
        public Guid LegalEntityId { get; set; }
        public string Name { get; set; }
        public string BeginningRangeForType { get; set; }
        public string EndRangeForType { get; set; }
        public string NormalBalance { get; set; }
    }

    public partial class SaAccountClassEdit
    {
        public SaAccountClassEdit()
        {
        }

        public long         Id                      { get; set; }
        public Guid LegalEntityId { get; set; }
        public string Name { get; set; }
        public string BeginningRangeForType { get; set; }
        public string EndRangeForType { get; set; }
        public string NormalBalance { get; set; }

        public virtual SaAccount SaAccount { get; set; }

    }

    public partial class SaAccountClassAdd
    {
        public SaAccountClassAdd()
        {
        }

        public long Id { get; set; }
        public Guid LegalEntityId { get; set; }
        public string Name { get; set; }
        public string BeginningRangeForType { get; set; }
        public string EndRangeForType { get; set; }
        public string NormalBalance { get; set; }

        public virtual SaAccount SaAccount { get; set; }

    }
}