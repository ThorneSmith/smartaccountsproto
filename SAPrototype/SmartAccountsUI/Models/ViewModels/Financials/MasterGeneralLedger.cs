﻿using System;
using System.Collections.Generic;


namespace SmartAccountsUI.Models.ViewModels.Financials
{
    public class MasterGeneralLedger
    {
        public MasterGeneralLedger()
        {
            MasterGeneralLedgerListLines = new HashSet<MasterGeneralLedgerListLine>();
        }

        public ICollection<MasterGeneralLedgerListLine> MasterGeneralLedgerListLines { get; set; }
    }

    public class MasterGeneralLedgerListLine
    {
        public long TransactionNo { get; set; }
        public string SaAccountCode { get; set; }
        public string SaAccountName { get; set; }
        public DateTime Date { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
    }
}