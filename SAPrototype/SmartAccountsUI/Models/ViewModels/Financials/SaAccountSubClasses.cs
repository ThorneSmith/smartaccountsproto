﻿using System;
using System.Collections.Generic;
using SACore.Domain.Financials;

namespace SmartAccountsUI.Models.ViewModels.Financials
{
    public partial class SaAccountSubClasses
    {
        public SaAccountSubClasses()
        {
            SaAccountSubClassListLines = new HashSet<SaAccountSubClassListLine>();
            SaAccountSubClassEditLines = new HashSet<SaAccountSubClassEditLine>();
        }

        public ICollection<SaAccountSubClassListLine> SaAccountSubClassListLines { get; set; }
        public ICollection<SaAccountSubClassEditLine> SaAccountSubClassEditLines { get; set; }
    }

    public partial class SaAccountSubClassEditLine
    {
        public long Id { get; set; }
        public Guid LegalEntityId { get; set; }
        public string Name { get; set; }
        public string BeginningRangeForType { get; set; }
        public string EndRangeForType { get; set; }

        public string NormalBalance { get; set; }
    }


    public partial class SaAccountSubClassListLine
    {
        public long Id { get; set; }
        public Guid LegalEntityId { get; set; }
        public string Name { get; set; }
        public string BeginningRangeForType { get; set; }
        public string EndRangeForType { get; set; }

        public string NormalBalance { get; set; }
    }

    public partial class SaAccountSubClassEdit
    {
        public SaAccountSubClassEdit()
        {
        }

        public long         Id                      { get; set; }
        public Guid LegalEntityId { get; set; }
        public string Name { get; set; }
        public string BeginningRangeForType { get; set; }
        public string EndRangeForType { get; set; }

        public string NormalBalance { get; set; }

 

    }

    public partial class SaAccountSubClassAdd
    {
        public SaAccountSubClassAdd()
        {
        }

        public long Id { get; set; }
        public Guid LegalEntityId { get; set; }
        public string Name { get; set; }
        public string BeginningRangeForType { get; set; }
        public string EndRangeForType { get; set; }

        public string NormalBalance { get; set; }

        

    }
}