﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SACore.Domain;
using SACore.Domain.Financials;


namespace SmartAccountsUI.Models.ViewModels.Financials
{
    public class SaAccounts
    {
        public SaAccounts()
        {
            SaAccountsListLines = new HashSet<SaAccountsListLine>();
            SaAccountEditLines = new HashSet<SaAccountEditLine>();
        }

        public ICollection<SaAccountsListLine> SaAccountsListLines { get; set; }
        public ICollection<SaAccountEditLine> SaAccountEditLines { get; set; }
    }

    public class SaAccountsListLine
    {
        public long Id { get; set; }
        public string SaAccountCode { get; set; }
        public string SaAccountName { get; set; }
        public decimal Balance { get; set; }
    }

    public class SaAccountEditLine
    {
        public long Id { get; set; }
        public string SaAccountCode { get; set; }
        public string SaAccountName { get; set; }
        public decimal Balance { get; set; }
    }

    public class SaAccountEdit
    {
        public SaAccountEdit()
        {
        }

        public long             Id                  { get; set; }
        [Required]
        public string           SaAccountCode       { get; set; }
        public string           SaAccountName       { get; set; }
        public long             SaAccountClassId    { get; set; }
        public long?            SaAccountSubClassId { get; set; }
        public long?            ParentSaAccountId   { get; set; }
        public SaAccountTypes   SaAccountType       { get; set; }
        public string           Description         { get; set; }

        //public SaAccount SaAccountClass { get; set; }
        //public long? InvoicingSaAccountId { get; set; }
        //public long? InvoicingDiscountSaAccountId { get; set; }
        //public long? InvoicingPenaltySaAccountId { get; set; }
    }
}