﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartAccountsUI.Models.ViewModels
{
    public class AddressViewModel
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public long StateId { get; set; }
        public string Zip { get; set; }
    }
}