﻿
using SAServices.Inventory;
using System;
using SAServices.Financial;
namespace SmartAccountsUI.Models.ViewModels.Receivables
{
    public class SalesInvoiceHeaderViewModel
    {
        public SalesInvoiceHeaderViewModel(IInventoryService inventoryService, IFinancialService financialService)
        {
            SalesLine = new SalesLineItemsViewModel(inventoryService, financialService);
            Date = DateTime.Now;
        }

        public int? Id { get; set; }
        public int? CustomerId { get; set; }
        public int? PaymentTermId { get; set; }
        public string Reference { get; set; }
        public string No { get; set; }
        public DateTime Date { get; set; }
        public SalesLineItemsViewModel SalesLine { get; set; }

        public void SetServiceHelpers(IInventoryService inventoryService, IFinancialService financialService)
        {
            SalesLine.SetServiceHelpers(inventoryService, financialService);
        }
    }
}