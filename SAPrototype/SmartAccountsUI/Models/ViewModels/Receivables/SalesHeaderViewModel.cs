﻿using SACore.Domain;
using SAServices.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SAServices.Financial;

namespace SmartAccountsUI.Models.ViewModels.Receivables
{
    public class SalesHeaderViewModel
    {
        IInventoryService _inventoryService;
        IFinancialService _financialService;

        public SalesHeaderViewModel()
        {
            SalesLine = new SalesLineItemsViewModel(_inventoryService, _financialService);
            Date = DateTime.Now;
        }

        public SalesHeaderViewModel(IInventoryService inventoryService, IFinancialService financialService)
            :this()
        {
            _inventoryService = inventoryService;
            _financialService = financialService;
        }

        public DocumentTypes DocumentType { get; set; }
        public long Id { get; set; }
        public long? CustomerId { get; set; }
        public long? PaymentTermId { get; set; }
        public string Reference { get; set; }
        public string No { get; set; }
        public DateTime Date { get; set; }
        public decimal ShippingHandlingCharges { get; set; }
        public SalesLineItemsViewModel SalesLine { get; set; }

        public void SetServiceHelpers(IInventoryService inventoryService, IFinancialService financialService)
        {
            SalesLine.SetServiceHelpers(inventoryService, financialService);
        }
    }
}