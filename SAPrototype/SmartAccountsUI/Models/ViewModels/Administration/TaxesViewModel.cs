﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using SACore.Domain;

namespace SmartAccountsUI.Models.ViewModels.Administration
{
    public class TaxesViewModel
    {
        public TaxesViewModel()
        {
            Taxes = new List<TaxViewModel>();
        }

        public IList<TaxViewModel> Taxes { get; set; }
    }

    public class TaxViewModel
    {
        public long? InvoicingSaAccountId { get; set; }
        public long? PurchasingSaAccountId { get; set; }
        [Required]
        [StringLength(50)]
        public string TaxName { get; set; }
        [Required]
        [StringLength(16)]
        public string TaxCode { get; set; }
        public decimal Rate { get; set; }
        public bool IsActive { get; set; }
    }

    public class ItemTaxGroupViewModel
    {
        public ItemTaxGroupViewModel()
        {
            ItemTaxGroupTaxes = new List<ItemTaxGroupTaxViewModel>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsFullyExempt { get; set; }

        public IList<ItemTaxGroupTaxViewModel> ItemTaxGroupTaxes { get; set; }
    }

    public class ItemTaxGroupTaxViewModel
    {
        public long TaxId { get; set; }
        public string TaxName { get; set; }
        public decimal Rate { get; set; }
        public bool IsExempt { get; set; }
    }
    public class AddItemTaxGroup
    {
        public AddItemTaxGroup()
        {
            SaAccounts = new HashSet<SelectListItem>();
            AddItemTaxGroupLines = new List<AddItemTaxGroupLine>();
            Date = DateTime.Now;
        }

        public DateTime Date { get; set; }
        public string ReferenceNo { get; set; }
        public string Memo { get; set; }

        public ICollection<SelectListItem> SaAccounts { get; set; }
        public IList<AddItemTaxGroupLine> AddItemTaxGroupLines { get; set; }

        #region Fields for New Item Tax Group
        public long SaAccountId { get; set; }
        public TransactionTypes DrCr { get; set; }
        public decimal Amount { get; set; }
        public string MemoLine { get; set; }
        #endregion
    }
    public class AddItemTaxGroupLine
    {
        public string RowId { get; set; }
        public long SaAccountId { get; set; }
        public string SaAccountName { get; set; }
        public TransactionTypes DrCr { get; set; }
        public decimal Amount { get; set; }
        public string Memo { get; set; }
    }
}