﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using SACore.Domain;
using SACore.Domain.Financials;
using SACore.Domain.Items;
using SACore.Domain.MasterTables;
using SACore.Domain.Payables;
using SACore.Domain.Receivables;
using SAServices.Administration;
using SAServices.Financial;
using SAServices.Inventory;
using SAServices.MasterTable;
using SAServices.Purchasing;
using SAServices.Receivables;

namespace SmartAccountsUI.Models
{
    public sealed class ModelViewHelper
    {
        private static IInvoicingService _invoicingService = DependencyResolver.Current.GetService<IInvoicingService>();
        private static IPurchasingService _purchasingService = DependencyResolver.Current.GetService<IPurchasingService>();
        private static IFinancialService _financialService = DependencyResolver.Current.GetService<IFinancialService>();
        private static IMasterTableService _masterTableService = DependencyResolver.Current.GetService<IMasterTableService>();
        private static IInventoryService _inventoryService = DependencyResolver.Current.GetService<IInventoryService>();
        private static IAdministrationService _administrationService = DependencyResolver.Current.GetService<IAdministrationService>();




        public static ICollection<SelectListItem> States()
        {
            var selections = new HashSet<SelectListItem>();
            foreach (var state in _masterTableService.GetStates())
                selections.Add(new SelectListItem() { Text = state.Name, Value = state.Id.ToString() });
            return selections;
        }
        public static string GetStateById(long id)
        {
            var selections = new HashSet<SelectListItem>();
            return _masterTableService.GetStateById(id);
        }
       
       
        //public static ICollection<SelectListItem> BHdAccounts(IEnumerable<SaAccount> saSaAccounts)
        //{
        //    var selections = new HashSet<SelectListItem>();
        //    selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
        //    foreach (var saSaAccount in _financialService.GetSubAccountsByType(9))
        //        selections.Add(new SelectListItem() { Text = saSaAccount.SaAccountName, Value = saSaAccount.Id.ToString() });
        //    return selections;
        //}

        //public static ICollection<SelectListItem> BloAccounts(IEnumerable<SaAccount> saSaAccounts)
        //{
        //    var selections = new HashSet<SelectListItem>();
        //    selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
        //    foreach (var saSaAccount in _financialService.GetSubAccountsByType(10))
        //        selections.Add(new SelectListItem() { Text = saSaAccount.SaAccountName, Value = saSaAccount.Id.ToString() });
        //    return selections;
        //}
        //public static ICollection<SelectListItem> BOcAccounts(IEnumerable<SaAccount> saSaAccounts)
        //{
        //    var selections = new HashSet<SelectListItem>();
        //    selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
        //    foreach (var saSaAccount in _financialService.GetSubAccountsByType(11))
        //        selections.Add(new SelectListItem() { Text = saSaAccount.SaAccountName, Value = saSaAccount.Id.ToString() });
        //    return selections;
        //}

        public static ICollection<SelectListItem> ArAccounts(IEnumerable<SaAccount> saSaAccounts)
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var saSaAccount in _financialService.GetSubAccountsByType(2))
                selections.Add(new SelectListItem() { Text = saSaAccount.SaAccountName, Value = saSaAccount.Id.ToString() });
            return selections;
        }

        public static ICollection<SelectListItem> InAccounts(IEnumerable<SaAccount> saSaAccounts)
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var saSaAccount in _financialService.GetSubAccountsByType(3))
                selections.Add(new SelectListItem() { Text = saSaAccount.SaAccountName, Value = saSaAccount.Id.ToString() });
            return selections;
        }
        public static ICollection<SelectListItem> IdAccounts(IEnumerable<SaAccount> saSaAccounts)
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var saSaAccount in _financialService.GetSubAccountsByType(5))
                selections.Add(new SelectListItem() { Text = saSaAccount.SaAccountName, Value = saSaAccount.Id.ToString() });
            return selections;
        }
        public static ICollection<SelectListItem> PaAccounts(IEnumerable<SaAccount> saSaAccounts)
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var saSaAccount in _financialService.GetSubAccountsByType(4))
                selections.Add(new SelectListItem() { Text = saSaAccount.SaAccountName, Value = saSaAccount.Id.ToString() });
            return selections;
        }

        public static ICollection<SelectListItem> ApAccounts(IEnumerable<SaAccount> saSaAccounts)
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var saSaAccount in _financialService.GetSubAccountsByType(6))
                selections.Add(new SelectListItem() { Text = saSaAccount.SaAccountName, Value = saSaAccount.Id.ToString() });
            return selections;
        }

        public static ICollection<SelectListItem> PAccounts(IEnumerable<SaAccount> saSaAccounts)
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var saSaAccount in _financialService.GetSubAccountsByType(7))
                selections.Add(new SelectListItem() { Text = saSaAccount.SaAccountName, Value = saSaAccount.Id.ToString() });
            return selections;
        }

        public static ICollection<SelectListItem> PdAccounts(IEnumerable<SaAccount> saSaAccounts)
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var saSaAccount in _financialService.GetSubAccountsByType(8))
                selections.Add(new SelectListItem() { Text = saSaAccount.SaAccountName, Value = saSaAccount.Id.ToString() });
            return selections;
        }

        public static ICollection<SelectListItem> SaAccountClasses(IEnumerable<SaAccountClass> saSaAccountClass)
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var saAccountClasses in _financialService.GetSaAccountClasses())
                selections.Add(new SelectListItem() { Text = saAccountClasses.Name, Value = saAccountClasses.Id.ToString() });
            return selections;
        }

        public static ICollection<SelectListItem> SaAccountSubClasses(IEnumerable<SaAccountSubClass> saAccountSubClass)
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var saAccountSubClasses in _financialService.GetSaAccountSubClasses())
                selections.Add(new SelectListItem() { Text = saAccountSubClasses.Name, Value = saAccountSubClasses.Id.ToString() });
            return selections;
        }

        public static ICollection<SelectListItem> SaAccountSubClasses()
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var saAccountSubClasses in _financialService.GetSaAccountSubClasses())
                selections.Add(new SelectListItem() { Text = saAccountSubClasses.Name, Value = saAccountSubClasses.Id.ToString() });
            return selections;
        }
        
        public static ICollection<SelectListItem> SaAccountClasses()
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var saAccountClasses in _financialService.GetSaAccountClasses())
                selections.Add(new SelectListItem() { Text = saAccountClasses.Name, Value = saAccountClasses.Id.ToString() });
            return selections;
        }

        public static ICollection<SelectListItem> SaAccountTypes()
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var saAccountClasses in _financialService.GetSaAccountTypes())
                selections.Add(new SelectListItem() { Text = saAccountClasses.Description, Value = saAccountClasses.Id.ToString() });
            return selections;
        }

        public static ICollection<SelectListItem> SaAccounts(IEnumerable<SaAccount> saSaAccounts)
        {             
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var saSaAccount in _financialService.GetSaAccounts())
                selections.Add(new SelectListItem() { Text = saSaAccount.SaAccountName, Value = saSaAccount.Id.ToString() });
            return selections;
        }

        public static ICollection<SelectListItem> SaAccounts()
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var saSaAccount in _financialService.GetSaAccounts())
                selections.Add(new SelectListItem() { Text = saSaAccount.SaAccountName, Value = saSaAccount.Id.ToString() });
            return selections;
        }

        /// <summary>
        /// [Deprecated] Change to Items()
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public static ICollection<SelectListItem> Items(IEnumerable<Item> items)
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var item in _inventoryService.GetAllItems())
                selections.Add(new SelectListItem() { Text = item.Description, Value = item.Id.ToString() });
            return selections;
        }

        public static ICollection<SelectListItem> Items()
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var item in _inventoryService.GetAllItems())
                selections.Add(new SelectListItem() { Text = item.Description, Value = item.No });
            return selections;
        }

        /// <summary>
        /// [Deprecated] Change to Measurements()
        /// </summary>
        /// <param name="measurements"></param>
        /// <returns></returns>
        public static ICollection<SelectListItem> Measurements(IEnumerable<Measurement> measurements)
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var measurement in _inventoryService.GetMeasurements())
                selections.Add(new SelectListItem() { Text = measurement.Code, Value = measurement.Id.ToString() });
            return selections;
        }

        public static ICollection<SelectListItem> Measurements()
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var measurement in _inventoryService.GetMeasurements())
                selections.Add(new SelectListItem() { Text = measurement.Code, Value = measurement.Id.ToString() });
            return selections;
        }

        /// <summary>
        /// [Deprecated] Change to ItemCategories()
        /// </summary>
        /// <param name="itemCategories"></param>
        /// <returns></returns>
        public static ICollection<SelectListItem> ItemCategories(IEnumerable<ItemCategory> itemCategories)
        {
            var selections = new HashSet<SelectListItem>();
            foreach (var itemCategory in _inventoryService.GetItemCategories())
                selections.Add(new SelectListItem() { Text = itemCategory.Name, Value = itemCategory.Id.ToString() });
            return selections;
        }

        public static ICollection<SelectListItem> ItemCategories()
        {
            var selections = new HashSet<SelectListItem>();
            foreach (var itemCategory in _inventoryService.GetItemCategories())
                selections.Add(new SelectListItem() { Text = itemCategory.Name, Value = itemCategory.Id.ToString() });
            return selections;
        }

        /// <summary>
        /// [Deprecated] Change to Vendors()
        /// </summary>
        /// <param name="vendors"></param>
        /// <returns></returns>
        public static ICollection<SelectListItem> Vendors(IEnumerable<Vendor> vendors)
        {
            var selections = new HashSet<SelectListItem>();
            foreach (var vendor in _purchasingService.GetVendors())
                selections.Add(new SelectListItem() { Text = vendor.Name, Value = vendor.Id.ToString() });
            return selections;
        }

        public static ICollection<SelectListItem> Vendors()
        {
            var selections = new HashSet<SelectListItem>();
            foreach (var vendor in _purchasingService.GetVendors())
                selections.Add(new SelectListItem() { Text = vendor.Name, Value = vendor.Id.ToString() });
            return selections;
        }

        public static ICollection<SelectListItem> LegalEntities()
        {
            var selections = new HashSet<SelectListItem>();
            foreach (var legalEntity in _administrationService.GetAllLegalEntities(false))
                selections.Add(new SelectListItem() { Text = legalEntity.LegalEntityName, Value = legalEntity.Id.ToString() });
            return selections;
        }

        /// <summary>
        /// [Deprecated] Change to Customers()
        /// </summary>
        /// <param name="customers"></param>
        /// <returns></returns>
        public static ICollection<SelectListItem> Customers(IEnumerable<Customer> customers)
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var customer in _invoicingService.GetCustomers())
                selections.Add(new SelectListItem() { Text = customer.Name, Value = customer.Id.ToString() });
            return selections;
        }

        public static ICollection<SelectListItem> Customers()
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var customer in _invoicingService.GetCustomers())
                selections.Add(new SelectListItem() { Text = customer.Name, Value = customer.Id.ToString() });
            return selections;
        }

        public static ICollection<SelectListItem> CustomerContacts()
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var contact in _masterTableService.GetContactsByType(PartyTypes.Contact))
                selections.Add(new SelectListItem() { Text = contact.FirstName + " " + contact.LastName, Value = contact.Id.ToString() });
            return selections;
        }

        /// <summary>
        /// [Deprecated] Change to Taxes()
        /// </summary>
        /// <param name="taxes"></param>
        /// <returns></returns>
        public static ICollection<SelectListItem> Taxes(IEnumerable<Tax> taxes)
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var tax in _financialService.GetTaxes())
                selections.Add(new SelectListItem() { Text = tax.TaxCode, Value = tax.Id.ToString() });
            return selections;
        }

        public static ICollection<SelectListItem> Taxes()
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var tax in _financialService.GetTaxes())
                selections.Add(new SelectListItem() { Text = tax.TaxCode, Value = tax.Id.ToString() });
            return selections;
        }

        /// <summary>
        /// [Deprecated] Change to ItemTaxGroups()
        /// </summary>
        /// <param name="itemTaxGroups"></param>
        /// <returns></returns>
        public static ICollection<SelectListItem> ItemTaxGroups(IEnumerable<ItemTaxGroup> itemTaxGroups)
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var tax in _financialService.GetItemTaxGroups())
                selections.Add(new SelectListItem() { Text = tax.Name, Value = tax.Id.ToString() });
            return selections;
        }

        public static ICollection<SelectListItem> ItemTaxGroups()
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var tax in _financialService.GetItemTaxGroups())
                selections.Add(new SelectListItem() { Text = tax.Name, Value = tax.Id.ToString() });
            return selections;
        }

        /// <summary>
        /// [Deprecated] Change to TaxGroups()
        /// </summary>
        /// <param name="taxGroups"></param>
        /// <returns></returns>
        public static ICollection<SelectListItem> TaxGroups(IEnumerable<ItemTaxGroup> taxGroups)
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var tax in _financialService.GetItemTaxGroups())
                selections.Add(new SelectListItem() { Text = tax.Name, Value = tax.Id.ToString() });
            return selections;
        }

        public static ICollection<SelectListItem> TaxGroups()
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var tax in _financialService.GetItemTaxGroups())
                selections.Add(new SelectListItem() { Text = tax.Name, Value = tax.Id.ToString() });
            return selections;
        }

        public static ICollection<SelectListItem> TransactionTypes()
        {
            var selections = new HashSet<SelectListItem>();
            return selections;
        }

        public static ICollection<SelectListItem> PaymentTerms()
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var paymentTerm in _invoicingService.GetPaymentTerms())
                selections.Add(new SelectListItem() { Text = paymentTerm.Description, Value = paymentTerm.Id.ToString() });
            return selections;
        }

        public static ICollection<SelectListItem> Banks()
        {
            var selections = new HashSet<SelectListItem>();
            //selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var bank in _masterTableService.GetBanks())
                selections.Add(new SelectListItem() { Text = bank.Name, Value = bank.SaAccountId.ToString() });
            return selections;
        }

        public static ICollection<SelectListItem> BankTypes()
        {
            var selections = new HashSet<SelectListItem>();
            //selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var bankType in _masterTableService.GetBankTypes())
                selections.Add(new SelectListItem() { Text = bankType.Description, Value = bankType.Id.ToString() });
            return selections;
        }



        
        public static ICollection<SelectListItem> Addresses()
        {
            var selections = new HashSet<SelectListItem>();
            selections.Add(new SelectListItem() { Text = string.Empty, Value = "-1", Selected = true });
            foreach (var address in _masterTableService.GetAddresses())
                selections.Add(new SelectListItem() { Text = address.GetFullAddress(), Value = address.Id.ToString() });
            return selections;
        }
    }
}