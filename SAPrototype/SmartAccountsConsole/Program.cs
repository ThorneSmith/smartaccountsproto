﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

using System.Threading.Tasks;
using SmartAccounts.Models;

namespace AccountingConsole
{
    //class Program
    //{
    //    static void Main()
    //    {
    //        RunAsync().Wait();
    //    }

    //    static async Task RunAsync()
    //    {
    //        using (var client = new HttpClient())
    //        {
    //            client.BaseAddress = new Uri("http://localhost:2177/");
    //            client.DefaultRequestHeaders.Accept.Clear();
    //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

    //            HttpResponseMessage response = await client.GetAsync("api/products/1");
    //            if (response.IsSuccessStatusCode)
    //            {
    //                Product product = await response.Content.ReadAsAsync<Product>();
    //                Console.WriteLine("{0}\t${1}\t{2}", product.Name, product.Price, product.Category);
    //            }
    //            Console.Read();
    //        }
    //    }
    //}

    class Program
    {
        static void Main()
        {
            RunAsync().Wait();
        }

        static async Task RunAsync()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:2177/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // HTTP GET
                HttpResponseMessage response = await client.GetAsync("api/products");
                if (response.IsSuccessStatusCode)
                {
                    Product[] products = await response.Content.ReadAsAsync<Product[]>();

                    for (int i = 0; i < products.Length; i++)
                    {
                        Console.WriteLine("{0}\t${1}", products[i].Name, products[i].Price);
                    }
                }
                Console.Read();
                // HTTP POST
                var gizmo = new Product() { Name = "Gizmo", Price = 100 };
                response = await client.PostAsJsonAsync("api/products", gizmo);
                
                
                if (response.IsSuccessStatusCode)
                {
                    Uri gizmoUrl = response.Headers.Location;

                    // HTTP PUT
                    gizmo.Price = 80;   // Update price
                    response = await client.PutAsJsonAsync(gizmoUrl, gizmo);

                    // HTTP DELETE
                    response = await client.DeleteAsync(gizmoUrl);
                }
            }
        }
    }
}
