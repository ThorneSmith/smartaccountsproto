﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SmartAccounts.WebAPI.Models;

namespace SmartAccounts.WebAPI.Controllers
{
    public class GLAccountsController : ApiController
    {

        private SmartAccountsEntities db = new SmartAccountsEntities();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.db.Dispose();
            }

            base.Dispose(disposing);
        }

        private async Task<Account> GetGLAccounts()
        {
            var glAccounts = this.db.GLAccounts.ToList();

            return await this.db.GLAccounts.FindAsync(CancellationToken.None, null);
        }

        // GET: api/GLAccounts
        [ResponseType(typeof(List<Account>))]
        public async Task<IHttpActionResult> Get()
        {

            var glAccounts = await GetGLAccounts();

            return this.Ok(glAccounts);
        }

        // GET: api/GLAccounts/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/GLAccounts
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/GLAccounts/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/GLAccounts/5
        public void Delete(int id)
        {
        }
    }
}
