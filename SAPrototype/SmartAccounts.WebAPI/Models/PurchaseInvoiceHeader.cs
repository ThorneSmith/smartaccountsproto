

namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public partial class PurchaseInvoiceHeader
    {
    
        public long Id { get; set; }
        public Nullable<int> VendorId { get; set; }
        public Nullable<int> GeneralLedgerHeaderId { get; set; }
        public System.DateTime Date { get; set; }
        public string No { get; set; }
        public string VendorInvoiceNo { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual GeneralLedgerHeader GeneralLedgerHeader { get; set; }
        public virtual Vendor Vendor { get; set; }

        public virtual ICollection<PurchaseInvoiceLine> PurchaseInvoiceLines { get; set; }
        public virtual ICollection<PurchaseOrderHeader> PurchaseOrderHeaders { get; set; }

        public virtual ICollection<VendorPayment> VendorPayments { get; set; }
    }
}
