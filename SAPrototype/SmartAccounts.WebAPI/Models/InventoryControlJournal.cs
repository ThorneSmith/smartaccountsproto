

namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public partial class InventoryControlJournal
    {


        public long Id { get; set; }
        public long ItemId { get; set; }
        public Guid LegalEntityId { get; set; }
        public long MeasurementId { get; set; }
        public long DocumentType { get; set; }
        public Nullable<decimal> INQty { get; set; }
        public Nullable<decimal> OUTQty { get; set; }
        public System.DateTime Date { get; set; }
        public Nullable<decimal> TotalCost { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
        public bool IsReverse { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }

        public virtual Item Item { get; set; }
        public virtual Measurement Measurement { get; set; }
        public virtual ICollection<PurchaseInvoiceLine> PurchaseInvoiceLines { get; set; }
        public virtual ICollection<PurchaseReceiptLine> PurchaseReceiptLines { get; set; }
        public virtual ICollection<InvoicingInvoiceLine> InvoicingInvoiceLines { get; set; }
    }
}
