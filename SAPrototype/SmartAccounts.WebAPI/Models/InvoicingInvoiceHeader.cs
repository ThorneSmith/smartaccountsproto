
namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public partial class InvoicingInvoiceHeader
    {
   
        public long Id { get; set; }
        public int CustomerId { get; set; }
        public Nullable<int> GeneralLedgerHeaderId { get; set; }
        public Nullable<int> InvoicingDeliveryHeaderId { get; set; }
        public string No { get; set; }
        public System.DateTime Date { get; set; }
        public decimal ShippingHandlingCharge { get; set; }
        public int Status { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual Customer Customer { get; set; }
        public virtual ICollection<CustomerAllocation> CustomerAllocations { get; set; }
        public virtual GeneralLedgerHeader GeneralLedgerHeader { get; set; }
        public virtual InvoicingDeliveryHeader InvoicingDeliveryHeader { get; set; }

        public virtual ICollection<InvoicingInvoiceLine> InvoicingInvoiceLines { get; set; }
    }
}
