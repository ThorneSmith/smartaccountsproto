
namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;

    public class GeneralLedgerHeader
    {
    
        public long Id { get; set; }

        public Guid LegalEntityId { get; set; }

        public System.DateTime Date { get; set; }
        public int DocumentType { get; set; }
        public string Description { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    

        public virtual ICollection<GeneralLedgerLine> GeneralLedgerLines { get; set; }
        public virtual ICollection<JournalEntryHeader> JournalEntryHeaders { get; set; }
        public virtual ICollection<PurchaseInvoiceHeader> PurchaseInvoiceHeaders { get; set; }
        public virtual ICollection<PurchaseReceiptHeader> PurchaseReceiptHeaders { get; set; }
        public virtual ICollection<InvoicingDeliveryHeader> InvoicingDeliveryHeaders { get; set; }
        public virtual ICollection<InvoicingInvoiceHeader> InvoicingInvoiceHeaders { get; set; }
        public virtual ICollection<InvoicingReceiptHeader> InvoicingReceiptHeaders { get; set; }
        public virtual ICollection<VendorPayment> VendorPayments { get; set; }
    }
}
