
using SmartAccounts.WebAPI.Models;
using SmartAccounts.Models;

namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tax
    {
    
        public long Id { get; set; }
        public Nullable<int> InvoicingAccountId { get; set; }
        public Nullable<int> PurchasingAccountId { get; set; }
        public string TaxName { get; set; }
        public string TaxCode { get; set; }
        public decimal Rate { get; set; }
        public bool IsActive { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual Account Account { get; set; }
        public virtual Account Account1 { get; set; }

        public virtual ICollection<ItemTaxGroupTax> ItemTaxGroupTaxes { get; set; }
        public virtual ICollection<PurchaseInvoiceLine> PurchaseInvoiceLines { get; set; }
        public virtual ICollection<PurchaseReceiptLine> PurchaseReceiptLines { get; set; }
        public virtual ICollection<InvoicingInvoiceLine> InvoicingInvoiceLines { get; set; }
        public virtual ICollection<TaxGroupTax> TaxGroupTaxes { get; set; }
    }
}
