
namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public partial class InvoicingQuoteLine
    {
        public long Id { get; set; }
        public int InvoicingQuoteHeaderId { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual InvoicingQuoteHeader InvoicingQuoteHeader { get; set; }
    }
}
