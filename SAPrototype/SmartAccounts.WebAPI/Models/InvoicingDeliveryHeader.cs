
namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public partial class InvoicingDeliveryHeader
    {
    
        public long Id { get; set; }
        public Nullable<int> PaymentTermId { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public Nullable<int> GeneralLedgerHeaderId { get; set; }
        public Nullable<int> InvoicingOrderHeaderId { get; set; }
        public string No { get; set; }
        public System.DateTime Date { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual Customer Customer { get; set; }
        public virtual GeneralLedgerHeader GeneralLedgerHeader { get; set; }
        public virtual PaymentTerm PaymentTerm { get; set; }
        public virtual InvoicingOrderHeader InvoicingOrderHeader { get; set; }

        public virtual ICollection<InvoicingDeliveryLine> InvoicingDeliveryLines { get; set; }
        public virtual ICollection<InvoicingInvoiceHeader> InvoicingInvoiceHeaders { get; set; }
    }
}
