﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;


namespace SmartAccounts
{
    [Table("InvoicingBatchLineItem")]
    public partial class InvoicingBatchLineItem 
    {

        public long InvoicingBatchHeaderId { get; set; }
        public long ItemId { get; set; }
        public long MeasurementId { get; set; }
        public long? InventoryControlJournalId { get; set; }
        public long? TaxId { get; set; }
        public decimal Quantity { get; set; }
        public decimal Discount { get; set; }
        public decimal Amount { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual InvoicingBatchHeader InvoicingBatchHeader { get; set; }
        public virtual Item Item { get; set; }
        public virtual Measurement Measurement { get; set; }
        public virtual InventoryControlJournal InventoryControlJournal { get; set; }
        public virtual Tax Tax { get; set; }

        public virtual ICollection<InvoicingReceiptLine> InvoicingReceiptLines { get; set; }



    }
}
