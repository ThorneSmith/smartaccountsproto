
using SmartAccounts.WebAPI.Models;
using SmartAccounts.Models;

namespace SmartAccounts.WebAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public class Bank
    {
        public long Id { get; set; }
        public Guid LegalEntityId { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
        public Nullable<int> AccountId { get; set; }
        public string BankName { get; set; }
        public string Number { get; set; }
        public string Address { get; set; }
        public bool IsDefault { get; set; }
        public bool IsActive { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual Account Account { get; set; }
    }
}
