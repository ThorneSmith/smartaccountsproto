

namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public partial class InvoicingInvoiceLine
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public InvoicingInvoiceLine()
        {
            this.InvoicingReceiptLines = new HashSet<InvoicingReceiptLine>();
        }
    
        public long Id { get; set; }
        public int InvoicingInvoiceHeaderId { get; set; }
        public int ItemId { get; set; }
        public int MeasurementId { get; set; }
        public Nullable<int> InventoryControlJournalId { get; set; }
        public Nullable<int> TaxId { get; set; }
        public decimal Quantity { get; set; }
        public decimal Discount { get; set; }
        public decimal Amount { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual InventoryControlJournal InventoryControlJournal { get; set; }
        public virtual InvoicingInvoiceHeader InvoicingInvoiceHeader { get; set; }
        public virtual Item Item { get; set; }
        public virtual Measurement Measurement { get; set; }
        public virtual Tax Tax { get; set; }

        public virtual ICollection<InvoicingReceiptLine> InvoicingReceiptLines { get; set; }

    }
}
