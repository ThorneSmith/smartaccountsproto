

namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public partial class PurchaseOrderHeader
    {
    
        public long Id { get; set; }
        public int VendorId { get; set; }
        public Nullable<int> PurchaseInvoiceHeaderId { get; set; }
        public string No { get; set; }
        public System.DateTime Date { get; set; }
        public string Description { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual PurchaseInvoiceHeader PurchaseInvoiceHeader { get; set; }
        public virtual Vendor Vendor { get; set; }

        public virtual ICollection<PurchaseOrderLine> PurchaseOrderLines { get; set; }
        public virtual ICollection<PurchaseReceiptHeader> PurchaseReceiptHeaders { get; set; }
    }
}
