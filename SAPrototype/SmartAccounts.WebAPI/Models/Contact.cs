
namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public partial class Contact
    {
    
        public long Id { get; set; }
        public Guid LegalEntityId { get; set; }
        public int ContactType { get; set; }
        public Nullable<int> PartyId { get; set; }


        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
    
        public virtual Party Party { get; set; }
        public virtual Party Party1 { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<Vendor> Vendors { get; set; }
    }
}
