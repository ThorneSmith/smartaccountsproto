
using SmartAccounts.WebAPI.Models;
using SmartAccounts.Models;

namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public partial class Item
    {
    
        public long Id { get; set; }
        public Guid LegalEntityId { get; set; }
        public Nullable<int> ItemCategoryId { get; set; }
        public Nullable<int> SmallestMeasurementId { get; set; }
        public Nullable<int> SellMeasurementId { get; set; }
        public Nullable<int> PurchaseMeasurementId { get; set; }
        public Nullable<int> PreferredVendorId { get; set; }
        public Nullable<int> ItemTaxGroupId { get; set; }
        public Nullable<int> InvoicingAccountId { get; set; }
        public Nullable<int> InventoryAccountId { get; set; }
        public Nullable<int> CostOfGoodsSoldAccountId { get; set; }
        public Nullable<int> InventoryAdjustmentAccountId { get; set; }
        public string No { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string PurchaseDescription { get; set; }
        public string SellDescription { get; set; }
        public Nullable<decimal> Cost { get; set; }
        public Nullable<decimal> Price { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual Account Account { get; set; }
        public virtual Account Account1 { get; set; }
        public virtual Account Account2 { get; set; }
        public virtual Account Account3 { get; set; }
        public virtual ICollection<InventoryControlJournal> InventoryControlJournals { get; set; }
        public virtual ItemCategory ItemCategory { get; set; }
        public virtual ItemTaxGroup ItemTaxGroup { get; set; }
        public virtual Measurement Measurement { get; set; }
        public virtual Measurement Measurement1 { get; set; }
        public virtual Measurement Measurement2 { get; set; }
        public virtual Vendor Vendor { get; set; }
        public virtual ICollection<PurchaseInvoiceLine> PurchaseInvoiceLines { get; set; }
        public virtual ICollection<PurchaseOrderLine> PurchaseOrderLines { get; set; }

        public virtual ICollection<PurchaseReceiptLine> PurchaseReceiptLines { get; set; }
        public virtual ICollection<InvoicingDeliveryLine> InvoicingDeliveryLines { get; set; }
        public virtual ICollection<InvoicingInvoiceLine> InvoicingInvoiceLines { get; set; }
        public virtual ICollection<InvoicingOrderLine> InvoicingOrderLines { get; set; }
    }
}
