
namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public partial class ItemTaxGroup
    {

        public long Id { get; set; }
        public Guid LegalEntityId { get; set; }
        public string Name { get; set; }
        public bool IsFullyExempt { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }

        public virtual ICollection<Item> Items { get; set; }

        public virtual ICollection<ItemTaxGroupTax> ItemTaxGroupTaxes { get; set; }
    }
}
