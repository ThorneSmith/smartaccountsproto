

namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public partial class Party
    {
    
        public long Id { get; set; }
        public Guid LegalEntityId { get; set; }
        public int PartyType { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public bool IsActive { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual Contact Contact { get; set; }

        public virtual ICollection<Contact> Contacts { get; set; }
        public virtual Customer Customer { get; set; }

        public virtual ICollection<JournalEntryHeader> JournalEntryHeaders { get; set; }
        public virtual Vendor Vendor { get; set; }
    }
}
