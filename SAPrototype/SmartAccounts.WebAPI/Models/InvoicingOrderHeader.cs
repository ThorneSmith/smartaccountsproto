

namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public partial class InvoicingOrderHeader
    {
    
        public long Id { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public Nullable<int> PaymentTermId { get; set; }
        public string No { get; set; }
        public string ReferenceNo { get; set; }
        public System.DateTime Date { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual Customer Customer { get; set; }
        public virtual PaymentTerm PaymentTerm { get; set; }

        public virtual ICollection<InvoicingDeliveryHeader> InvoicingDeliveryHeaders { get; set; }
        public virtual ICollection<InvoicingOrderLine> InvoicingOrderLines { get; set; }
    }
}
