
namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public partial class PurchaseReceiptHeader
    {

    
        public long Id { get; set; }
        public int VendorId { get; set; }
        public int PurchaseOrderHeaderId { get; set; }
        public Nullable<int> GeneralLedgerHeaderId { get; set; }
        public System.DateTime Date { get; set; }
        public string No { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual GeneralLedgerHeader GeneralLedgerHeader { get; set; }
        public virtual PurchaseOrderHeader PurchaseOrderHeader { get; set; }
        public virtual Vendor Vendor { get; set; }
        public virtual ICollection<PurchaseReceiptLine> PurchaseReceiptLines { get; set; }
    }
}
