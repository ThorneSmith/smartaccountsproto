

namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public  class JournalEntryHeader
    {
    
        public long Id { get; set; }
        public Guid LegalEntityId { get; set; }
        public Nullable<int> GeneralLedgerHeaderId { get; set; }
        public Nullable<int> PartyId { get; set; }
        public Nullable<int> VoucherType { get; set; }
        public System.DateTime Date { get; set; }
        public string Memo { get; set; }
        public string ReferenceNo { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual GeneralLedgerHeader GeneralLedgerHeader { get; set; }
        public virtual Party Party { get; set; }
        public virtual ICollection<JournalEntryLine> JournalEntryLines { get; set; }
    }
}
