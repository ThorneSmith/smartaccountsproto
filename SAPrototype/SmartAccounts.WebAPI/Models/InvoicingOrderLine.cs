
namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public partial class InvoicingOrderLine
    {
        public long Id { get; set; }
        public int InvoicingOrderHeaderId { get; set; }
        public int ItemId { get; set; }
        public int MeasurementId { get; set; }
        public decimal Quantity { get; set; }
        public decimal Discount { get; set; }
        public decimal Amount { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual Item Item { get; set; }
        public virtual Measurement Measurement { get; set; }
        public virtual InvoicingOrderHeader InvoicingOrderHeader { get; set; }
    }
}
