
namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public partial class ItemTaxGroupTax
    {
        public long Id { get; set; }
        public int TaxId { get; set; }
        public int ItemTaxGroupId { get; set; }
        public bool IsExempt { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual ItemTaxGroup ItemTaxGroup { get; set; }
        public virtual Tax Tax { get; set; }
    }
}
