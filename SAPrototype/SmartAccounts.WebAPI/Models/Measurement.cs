
namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public partial class Measurement
    {
   
        public long Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    

        public virtual ICollection<InventoryControlJournal> InventoryControlJournals { get; set; }

        public virtual ICollection<Item> Items { get; set; }
        public virtual ICollection<Item> Items1 { get; set; }
        public virtual ICollection<Item> Items2 { get; set; }

        public virtual ICollection<ItemCategory> ItemCategories { get; set; }

        public virtual ICollection<PurchaseInvoiceLine> PurchaseInvoiceLines { get; set; }
        public virtual ICollection<PurchaseOrderLine> PurchaseOrderLines { get; set; }
        public virtual ICollection<PurchaseReceiptLine> PurchaseReceiptLines { get; set; }

        public virtual ICollection<InvoicingDeliveryLine> InvoicingDeliveryLines { get; set; }
        public virtual ICollection<InvoicingInvoiceLine> InvoicingInvoiceLines { get; set; }
        public virtual ICollection<InvoicingOrderLine> InvoicingOrderLines { get; set; }
    }
}
