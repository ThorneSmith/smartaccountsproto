
namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public class CompanySetting
    {
        public long Id { get; set; }
        public int CompanyId { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual Company Company { get; set; }
    }
}
