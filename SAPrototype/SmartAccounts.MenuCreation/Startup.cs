﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SmartAccounts.MenuCreation.Startup))]
namespace SmartAccounts.MenuCreation
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
