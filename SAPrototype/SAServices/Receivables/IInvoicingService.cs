﻿using System.Collections.Generic;
using SACore.Domain;
using SACore.Domain.MasterTables;
using SACore.Domain.Receivables;

namespace SAServices.Receivables
{
    public partial interface IInvoicingService
    {
        void AddInvoicingOrder(InvoicingOrderHeader invoicingOrder, bool toSave);
        void UpdateInvoicingOrder(InvoicingOrderHeader invoicingOrder);
        void AddInvoicingInvoice(InvoicingInvoiceHeader invoicingInvoice, long? invoicingOrderId);
        void AddInvoicingReceipt(InvoicingReceiptHeader invoicingReceipt);
        void AddInvoicingReceiptNoInvoice(InvoicingReceiptHeader invoicingReceipt);
        void AddInvoicingDelivery(InvoicingDeliveryHeader invoicingDelivery, bool toSave);
        IEnumerable<InvoicingInvoiceHeader> GetInvoicingInvoices();
        InvoicingInvoiceHeader GetInvoicingInvoiceById(long id);
        InvoicingInvoiceHeader GetInvoicingInvoiceByNo(string no);
        void UpdateInvoicingInvoice(InvoicingInvoiceHeader invoicingInvoice);
        IEnumerable<InvoicingReceiptHeader> GetInvoicingReceipts();
        InvoicingReceiptHeader GetInvoicingReceiptById(long id);
        void UpdateInvoicingReceipt(InvoicingReceiptHeader invoicingReceipt);
        IEnumerable<Customer> GetCustomers();
        Customer GetCustomerById(long id);
        void SaveCustomer(Customer customer);
        ICollection<InvoicingReceiptHeader> GetCustomerReceiptsForAllocation(long customerId);
        void SaveCustomerAllocation(CustomerAllocation allocation);
        IEnumerable<PaymentTerm> GetPaymentTerms();
        IEnumerable<InvoicingDeliveryHeader> GetInvoicingDeliveries();
        IEnumerable<InvoicingOrderHeader> GetInvoicingOrders();
        InvoicingOrderHeader GetInvoicingOrderById(long id);
        InvoicingDeliveryHeader GetInvoicingDeliveryById(long id);
        IEnumerable<Contact> GetContacts();
        long SaveContact(Contact contact);
        ICollection<InvoicingInvoiceHeader> GetInvoicingInvoicesByCustomerId(long customerId, InvoicingInvoiceStatus status);
        ICollection<CustomerAllocation> GetCustomerAllocations(long customerId);
    }
}
