﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using SACore.Domain.MasterTables;

namespace SAServices.Administration
{
    public interface ICustomPrincipalService : IPrincipal
    {
        int Id { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        LegalEntity LegalEntity { get; set; }
    }
}
