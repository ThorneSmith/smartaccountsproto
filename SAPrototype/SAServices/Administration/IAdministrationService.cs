﻿using System;
using System.Collections.Generic;
using SACore.Domain;
using SACore.Domain.Financials;
using SACore.Domain.MasterTables;

namespace SAServices.Administration
{
    public interface IAdministrationService
    {
        ICollection<Tax> GetAllTaxes(bool includeInActive);
        ICollection<ItemTaxGroup> GetItemTaxGroups(bool includeInActive);
        ICollection<TaxGroup> GetTaxGroups(bool includeInActive);
        ICollection<LegalEntity> GetAllLegalEntities(bool includeInActive);
        LegalEntity GetLegalEntityById(Guid Id, bool includeInActive);

        void AddNewTax(Tax tax);
        void AddNewLegalEntity(LegalEntity legalEntity);
        ItemTaxGroup AddNewItemTaxGroup(ItemTaxGroup itemTaxGroup);
        void UpdateLegalEntity(LegalEntity legalEntity);
        void UpdateTax(Tax tax);
        void DeleteTax(long id);
    }
}
