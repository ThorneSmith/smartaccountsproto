﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using SACore.Domain.MasterTables;

namespace SAServices.Administration
{
    public class CustomPrincipalService : ICustomPrincipalService
    {
        public IIdentity Identity { get; private set; }
        public bool IsInRole(string role) { return false; }

        public CustomPrincipalService(string email)
        {
            this.Identity = new GenericIdentity(email);
        }

        //public CustomPrincipalService(Guid LegalEntityId)
        //{
        //    this.Identity = new GenericIdentity(LegalEntityId);
        //}
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public LegalEntity LegalEntity { get; set; }
    }
}
