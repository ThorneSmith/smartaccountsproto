﻿using System;
using System.Collections.Generic;
using SACore.Domain;
using SACore.Domain.Financials;
using SACore.Domain.MasterTables;

namespace SAServices.Financial
{
    public partial interface IFinancialService
    {
        ErrorHandler.Errors ValidateGeneralLedgerEntry(GeneralLedgerHeader glEntry);
        void SaveGeneralLedgerEntry(GeneralLedgerHeader entry);
        GeneralLedgerLine CreateGeneralLedgerLine(TransactionTypes DrCr, long saSaAccountId, decimal amount);
        GeneralLedgerHeader CreateGeneralLedgerHeader(DocumentTypes documentType, DateTime Date, string description);
        IEnumerable<SaAccount> GetSaAccounts();
        IEnumerable<SaAccountType> GetSaAccountTypes();
        SaAccount GetSaAccountById(long p);
        IEnumerable<SaAccount> GetSubAccountsByType(int p);
        void UpdateSaAccount(SaAccount saAccount);

        IEnumerable<SaAccountClass> GetSaAccountClasses();
        IEnumerable<SaAccountSubClass> GetSaAccountSubClasses();
        SaAccountClass GetSaAccountClassById(long p);
        SaAccountSubClass GetSaAccountSubClassById(long p);

        IEnumerable<JournalEntryHeader> GetJournalEntries();
        ErrorHandler.Errors AddJournalEntry(JournalEntryHeader journalEntry);
        ICollection<TrialBalance> TrialBalance(DateTime? from = null, DateTime? to = null);
        ICollection<BalanceSheet> BalanceSheet(DateTime? from = null, DateTime? to = null);
        ICollection<IncomeStatement> IncomeStatement(DateTime? from = null, DateTime? to = null);
        ICollection<MasterGeneralLedger> MasterGeneralLedger(DateTime? from = null, DateTime? to = null);
        FiscalYear CurrentFiscalYear();
        LegalEntity GetCurrentLegalEntity();
        IEnumerable<Tax> GetTaxes();
        IEnumerable<ItemTaxGroup> GetItemTaxGroups();
        IEnumerable<TaxGroup> GetTaxGroups();

        List<KeyValuePair<long, decimal>> ComputeInputTax(long itemId, decimal quantity, decimal amount);
        List<KeyValuePair<long, decimal>> ComputeOutputTax(long itemId, decimal quantity, decimal amount, decimal discount);
        GeneralLedgerSettings GetGeneralLedgerSettings(long? companyId = null);
        void UpdateGeneralLedgerSettings(GeneralLedgerSettings setting);

        void AddSaAccount(SaAccount saAccount);

        string CreateSaAccountCode(long? parentSaAccountId, SaAccountTypes saAccountTypes, long? saAccountSubClassId, long saAccountClass);
    }
}
