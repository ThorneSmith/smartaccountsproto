﻿using System.Collections.Generic;
using SACore.Domain;
using SACore.Domain.Items;

namespace SAServices.Inventory
{
    public partial interface IInventoryService
    {
        InventoryControlJournal CreateInventoryControlJournal(long itemId,
            long measurementId,
            DocumentTypes documentType,
            decimal? inQty,
            decimal? outQty,
            decimal? totalCost,
            decimal? totalAmount);

        void AddItem(Item item);
        void UpdateItem(Item item);
        void DeleteItem(long itemId);
        Item GetItemById(long id);
        Item GetItemByNo(string itemNo);
        IEnumerable<Item> GetAllItems();
        IEnumerable<Measurement> GetMeasurements();
        Measurement GetMeasurementById(long id);
        IEnumerable<ItemCategory> GetItemCategories();
        IEnumerable<InventoryControlJournal> GetInventoryControlJournals();
    }
}
