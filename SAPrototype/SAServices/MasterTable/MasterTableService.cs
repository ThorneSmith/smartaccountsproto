﻿using System;
using System.Collections.Generic;
using System.Linq;
using SACore.Data;
using SACore.Domain;
using SACore.Domain.Financials;
using SACore.Domain.Items;
using SAServices.Inventory;
using System.Data.Entity;
using SACore.Domain.MasterTables;


namespace SAServices.MasterTable
{
    public partial class MasterTableService : BaseService, IMasterTableService
    {
        public Guid CurrentLegalEntity = new Guid("1FFDA348-B02D-40DC-82CC-6C7293DA34CC");

        private readonly IRepository<SaAccount> _saAccountRepo;
        private readonly IRepository<Tax> _taxRepo;
        private readonly IRepository<Customer> _customerRepo;
        private readonly IRepository<Contact> _contactRepo;
        private readonly IRepository<Party> _partyRepo;
        private readonly IRepository<Vendor> _vendorRepo;
        private readonly IRepository<Address> _addressRepo;
        private readonly IRepository<State> _stateRepo;
        private readonly IRepository<FiscalYear> _fiscalYearRepo;
        private readonly IRepository<LegalEntity> _legalEntityRepo;
        private readonly IRepository<TaxGroup> _taxGroupRepo;
        private readonly IRepository<ItemTaxGroup> _itemTaxGroupRepo;
        private readonly IRepository<PaymentTerm> _paymentTermRepo;
        private readonly IRepository<Bank> _bankRepo;
        private readonly IRepository<Item> _itemRepo;
        private readonly IRepository<GeneralLedgerSettings> _glSettingRepo;

        public MasterTableService(
            IRepository<SaAccount> saSaAccountRepo,
            IRepository<Customer> customerRepo,
            IRepository<Contact> contactRepo,
            IRepository<Party> partyRepo,
            IRepository<Tax> taxRepo,
            IRepository<Vendor> vendorRepo,
            IRepository<Address> addressRepo,
            IRepository<State> stateRepo,
            IRepository<FiscalYear> fiscalYearRepo,
            IRepository<LegalEntity> legalEntityRepo,
            IRepository<TaxGroup> taxGroupRepo,
            IRepository<ItemTaxGroup> itemTaxGroupRepo,
            IRepository<PaymentTerm> paymentTermRepo,
            IRepository<Bank> bankRepo,
            IRepository<Item> itemRepo,
            IRepository<GeneralLedgerSettings> glSettingRepo
            )
            :base(null, null, null, paymentTermRepo, bankRepo)
        {
            _saAccountRepo = saSaAccountRepo;
            _taxRepo = taxRepo;
            _customerRepo = customerRepo;
            _contactRepo = contactRepo;
            _partyRepo = partyRepo;
            _vendorRepo = vendorRepo;
            _addressRepo = addressRepo;
            _stateRepo = stateRepo;
            _fiscalYearRepo = fiscalYearRepo;
            _legalEntityRepo = legalEntityRepo;
            _taxGroupRepo = taxGroupRepo;
            _itemTaxGroupRepo = itemTaxGroupRepo;
            _paymentTermRepo = paymentTermRepo;
            _bankRepo = bankRepo;
            _itemRepo = itemRepo;
            _glSettingRepo = glSettingRepo;
        }

        public IEnumerable<State> GetStates()
        {
            var query = from b in _stateRepo.Table
                        select b;
            return query;
        }


        public string GetStateById(long id)
        {
            var query = from b in _stateRepo.Table
                        where b.Id == id
                        select b;
            return query.FirstOrDefault().Name;
        }



        public Bank GetBankById(long id)
        {
           return _bankRepo.GetById(id);
        }

        public IEnumerable<Bank> GetBanks()
        {
            var query = from b in _bankRepo.Table
                        select b;
            return query;
        }

        public IEnumerable<BankType> GetBankTypes()
        {
            int i = 0;
            List<BankType> lbT = new List<BankType>();
            var values = EnumUtil.GetValues<BankTypes>();
            foreach (var t in values)
            {
                BankType bT = new BankType();
                bT.Id = i++;
                bT.Description = t.ToString();
                lbT.Add(bT);
            }
            return lbT;
        }

        public void AddBank(Bank bank)
        {
            _bankRepo.Insert(bank);
        }

        public void SaveBank(Bank bank)
        {
            _bankRepo.Update(bank);
        }

        public IEnumerable<Vendor> GetVendors()
        {
            var query = from f in _vendorRepo.Table
                        where f.IsActive == true
                        select f;
            return query.AsEnumerable();
        }

        public Vendor GetVendorById(long id)
        {
            return _vendorRepo.GetById(id);
        }

        public bool DeleteVendorById(Vendor vendor)
        {
            vendor.IsActive = false;
            _vendorRepo.Update(vendor);

            return true;
        }
        public void AddVendor(Vendor vendor)
        {
            _vendorRepo.Insert(vendor);
        }

        public void SaveVendor(Vendor vendor)
        {
            _vendorRepo.Update(vendor);
        }



        public IEnumerable<SaAccount> GetSaAccounts()
        {
            var query = from u in _saAccountRepo.Table
            orderby u.SaAccountName
            join c in _saAccountRepo.Table
                on u.ParentSaAccountId equals c.Id
            where c.SaAccountName != null
            select u;
        
            var query1 = from f in query

                         orderby f.SaAccountName
                         select f
                        ;
            return query1.AsEnumerable();
        }



        public Customer GetCustomerById(long Id)
        {
            
            IQueryable<Customer> custQuery = _customerRepo.Table.AsQueryable();

            //var customer = custQuery.Include(c => c.Contacts).Include(c => c.Party).Include(c => c.InvoicingDiscountSaAccount).Include(c => c.InvoicingPenaltySaAccount).Include(c => c.AccountsReceivableSaAccount).Include(c => c.InvoicingSaAccount).Include(c => c.PaymentTerm).Include(c => c.TaxGroup).FirstOrDefault(c => c.Id == Id);
              var customer = custQuery.Include(c => c.Contacts).Include(c => c.PaymentTerm).Include(c => c.TaxGroup).FirstOrDefault(c => c.Id == Id);

            customer.InvoicingSaAccount = null;



            return customer;
        }
        
        public IEnumerable<Customer> GetCustomers()
        {
            
            IQueryable<Customer> custQuery = _customerRepo.Table.AsQueryable();

            var customers = custQuery.Include(c => c.Contacts).Include(c => c.InvoicingDiscountSaAccount).Include(c => c.InvoicingPenaltySaAccount).Include(c => c.AccountsReceivableSaAccount).Include(c => c.InvoicingSaAccount).Include(c => c.PaymentTerm).Include(c => c.TaxGroup).Where(c => c.IsActive  == true);

            return customers;
        }

        public bool DeleteCustomerById(Customer customer)
        {
            customer.IsActive = false;
            _customerRepo.Update(customer);

            return true;
        }
        public void SaveCustomer(Customer customer)
        {
            customer.LegalEntityId = GetCurrentLegalEntity().LegalEntityId;
            _customerRepo.Update(customer);
        }

        public void AddCustomer(Customer customer)
        {
            _customerRepo.Insert(customer);
        }

        

        


        public IEnumerable<Tax> GetTaxes()
        {
            var query = from f in _taxRepo.Table
                        select f;
            return query.AsEnumerable();
        }

        public IEnumerable<ItemTaxGroup> GetItemTaxGroups()
        {
            var query = from f in _itemTaxGroupRepo.Table
                        select f;
            return query;
        }

        public IEnumerable<TaxGroup> GetCustomerTaxGroups()
        {
            var query = from f in _taxGroupRepo.Table
                        select f;
            return query;
        }



        public LegalEntity GetCurrentLegalEntity()
        {
            var query = from f in _legalEntityRepo.Table
                        where f.LegalEntityId == new Guid("1ffda348-b02d-40dc-82cc-6c7293da34cc")
                        select f;
            return query.FirstOrDefault();
        }


        public GeneralLedgerSettings GetGeneralLedgerSettings(long? companyId = null)
        {
            GeneralLedgerSettings glSetting = null;
            if (companyId == null)
                glSetting = _glSettingRepo.Table.FirstOrDefault();
            else
                glSetting = _glSettingRepo.Table.Where(s => s.CompanyId == companyId).FirstOrDefault();

            return glSetting;
        }

        public void UpdateGeneralLedgerSettings(GeneralLedgerSettings setting)
        {
            _glSettingRepo.Update(setting);
        }


        public static class EnumUtil
        {
            public static IEnumerable<T> GetValues<T>()
            {
                return Enum.GetValues(typeof(T)).Cast<T>();
            }
        }









        public IEnumerable<Party> GetParties()
        {
            var query = from f in _partyRepo.Table
                        select f;
            return query;
        }

        public new IEnumerable<PaymentTerm> GetPaymentTerms()
        {
            var query = from f in _paymentTermRepo.Table
                        select f;
            return query;
        }

        public IEnumerable<SaAccount> GetInvoicingDiscountSaAccounts()
        {
            var query = from f in _saAccountRepo.Table
                        where f.SaAccountSubClassId == 5
                        select f;
            return query;
        }

        public IEnumerable<SaAccount> GetInvoicingSaAccounts()
        {
            var query = from f in _saAccountRepo.Table
                        where f.SaAccountSubClassId == 3
                        select f;
            return query;
        }

        public IEnumerable<SaAccount> GetInvoicingPenaltySaAccounts()
        {
            var query = from f in _saAccountRepo.Table
                        where f.SaAccountSubClassId == 4
                        select f;
            return query;
        }

        public IEnumerable<SaAccount> GetReceivableSaAccounts()
        {
            var query = from f in _saAccountRepo.Table
                        where f.SaAccountSubClassId == 2
                        select f;
            return query;
        }




        public IEnumerable<Contact> GetContacts()
        {

            IQueryable<Contact> custQuery = _contactRepo.Table.AsQueryable();

            var contacts = custQuery.Include(c => c.Contacts);

            return contacts;
        }
        public IEnumerable<Contact> GetContactsByType(PartyTypes p)
        {
            var query = from c in _contactRepo.Table
                where c.PartyType == p 
                        select c;
            return query;

        }
        public Contact  GetContactById(long p)
        {
            var query = from c in _contactRepo.Table
                        where c.Id == p
                        select c;
            return query.FirstOrDefault();

        }
        public long SaveContact(Contact contact)
        {
            _contactRepo.Insert(contact);
            return contact.Id;
        }

        public bool DeleteContactById(Contact contact)
        {
            contact.IsActive = false;
            _contactRepo.Update(contact);

            return true;
        }

        public IEnumerable<Address> GetAddresses()
        {
            var query = from b in _addressRepo.Table
                        select b;
            return query;
        }

        public Address GetAddressById(long p)
        {
            var query = from c in _addressRepo.Table
                        where c.Id == p
                        select c;
            return query.FirstOrDefault();

        }

        public long SaveAddress(Address address)
        {
            _addressRepo.Update(address);
            return address.Id;

        }
        public void AddAddress(Address address)
        {
            _addressRepo.Insert(address);
        }

        public bool DeleteAddressById(Address address)
        {
            address.IsActive = false;
            _addressRepo.Update(address);

            return true;
        }

    }
}
