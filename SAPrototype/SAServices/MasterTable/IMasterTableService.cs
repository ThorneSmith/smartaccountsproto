﻿using System;
using System.Collections.Generic;
using SACore.Domain;
using SACore.Domain.Financials;
using SACore.Domain.MasterTables;


namespace SAServices.MasterTable
{
    public partial interface IMasterTableService
    {
        IEnumerable<SaAccount> GetSaAccounts();
        
        
        // Customers 
        IEnumerable<Customer> GetCustomers();
        Customer GetCustomerById(long id);
        void SaveCustomer(Customer customer);
        void AddCustomer(Customer customer);
        bool DeleteCustomerById(Customer customer);
       
       

        // Tax Groups
        IEnumerable<TaxGroup> GetCustomerTaxGroups();


        // Contacts
        IEnumerable<Contact> GetContacts();
        IEnumerable<Contact> GetContactsByType(PartyTypes p);
        Contact GetContactById(long p);
        long SaveContact(Contact contact);
        bool DeleteContactById(Contact contact);

        // Parties
        IEnumerable<Party> GetParties();

        // Payments
        IEnumerable<PaymentTerm> GetPaymentTerms();

        // Invoicing
        IEnumerable<SaAccount> GetInvoicingDiscountSaAccounts();
        IEnumerable<SaAccount> GetInvoicingSaAccounts();
        IEnumerable<SaAccount> GetInvoicingPenaltySaAccounts();
        IEnumerable<SaAccount> GetReceivableSaAccounts();

        //LegalEntities
        LegalEntity GetCurrentLegalEntity();

    
        // Vendors
        IEnumerable<Vendor> GetVendors();
        Vendor GetVendorById(long id);
        void AddVendor(Vendor vendor);
        void SaveVendor(Vendor vendor);
        bool DeleteVendorById(Vendor vendor);

       
        // Taxes
        IEnumerable<Tax> GetTaxes();
        IEnumerable<ItemTaxGroup> GetItemTaxGroups();

        // GeneralLedger
        GeneralLedgerSettings GetGeneralLedgerSettings(long? companyId = null);


        // Addresses
        IEnumerable<Address> GetAddresses();
        Address GetAddressById(long id);
        long SaveAddress(Address contact);
        void AddAddress(Address address);
        bool DeleteAddressById(Address contact);
        IEnumerable<State> GetStates();
        string GetStateById(long id);

        // Banks
        IEnumerable<Bank> GetBanks();
        IEnumerable<BankType> GetBankTypes();
        Bank GetBankById(long id);
        void SaveBank(Bank bank);
        void AddBank(Bank bank);


    }
}
