
using SmartAccounts.WebAPI.Models;
using SmartAccounts.Models;

namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public class Vendor
    {
    
        public int Id { get; set; }
        public string No { get; set; }
        public Guid LegalEntityId { get; set; }
        public Nullable<int> AccountsPayableAccountId { get; set; }
        public Nullable<int> PurchaseAccountId { get; set; }
        public Nullable<int> PurchaseDiscountAccountId { get; set; }
        public Nullable<int> PrimaryContactId { get; set; }
        public Nullable<int> PaymentTermId { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
    
        public virtual Account Account { get; set; }
        public virtual Account Account1 { get; set; }
        public virtual Account Account2 { get; set; }

        public virtual Contact Contact { get; set; }
        public virtual ICollection<Item> Items { get; set; }
        public virtual Party Party { get; set; }
        public virtual PaymentTerm PaymentTerm { get; set; }

        public virtual ICollection<PurchaseInvoiceHeader> PurchaseInvoiceHeaders { get; set; }
        public virtual ICollection<PurchaseOrderHeader> PurchaseOrderHeaders { get; set; }
        public virtual ICollection<PurchaseReceiptHeader> PurchaseReceiptHeaders { get; set; }

        public virtual ICollection<VendorPayment> VendorPayments { get; set; }
    }
}
