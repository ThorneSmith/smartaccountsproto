﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartAccounts.Models
{
    public class Product
    {
        public int Id { get; set; }
        public Guid LegalEntityId { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public decimal Price { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
                                    
    }
}