

namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public partial class TaxGroup
    {

    
        public int Id { get; set; }
        public string Description { get; set; }
        public bool TaxAppliedToShipping { get; set; }
        public bool IsActive { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }

        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<TaxGroupTax> TaxGroupTaxes { get; set; }
    }
}
