

using SmartAccounts.WebAPI.Models;
using SmartAccounts.Models;

namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public  class GeneralLedgerSetting
    {
        public int Id { get; set; }
        public Guid LegalEntityId { get; set; }
        public Nullable<int> CompanyId { get; set; }
        public Nullable<int> PayableAccountId { get; set; }
        public Nullable<int> PurchaseDiscountAccountId { get; set; }
        public Nullable<int> GoodsReceiptNoteClearingAccountId { get; set; }
        public Nullable<int> InvoicingDiscountAccountId { get; set; }
        public Nullable<int> ShippingChargeAccountId { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual Account Account { get; set; }
        public virtual Account Account1 { get; set; }
        public virtual Account Account2 { get; set; }
        public virtual Account Account3 { get; set; }
        public virtual Account Account4 { get; set; }
        public virtual Company Company { get; set; }
    }
}
