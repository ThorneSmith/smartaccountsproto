
namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public partial class TaxGroupTax
    {
        public int Id { get; set; }
        public int TaxId { get; set; }
        public int TaxGroupId { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual Tax Tax { get; set; }
        public virtual TaxGroup TaxGroup { get; set; }
    }
}
