
using SmartAccounts.WebAPI.Models;
using SmartAccounts.Models;

namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public class InvoicingReceiptLine
    {
        public int Id { get; set; }
        public int InvoicingReceiptHeaderId { get; set; }
        public Nullable<int> InvoicingInvoiceLineId { get; set; }
        public Nullable<int> ItemId { get; set; }
        public Nullable<int> AccountToCreditId { get; set; }
        public Nullable<int> MeasurementId { get; set; }
        public Nullable<decimal> Quantity { get; set; }
        public Nullable<decimal> Discount { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public decimal AmountPaid { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual Account Account { get; set; }
        public virtual InvoicingInvoiceLine InvoicingInvoiceLine { get; set; }
        public virtual InvoicingReceiptHeader InvoicingReceiptHeader { get; set; }
    }
}
