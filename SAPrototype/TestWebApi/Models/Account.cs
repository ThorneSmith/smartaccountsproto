using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SmartAccounts;

namespace SmartAccounts.WebAPI.Models
{
    public class Account
    {


        public long Id { get; set; }
        public Guid LegalEntityId { get; set; }
        public int AccountClassId { get; set; }
        public Nullable<int> ParentAccountId { get; set; }
        public int AccountType { get; set; }
        public string AccountCode { get; set; }
        public string AccountName { get; set; }
        public string Description { get; set; }
        public bool IsCash { get; set; }

        //public ICollection<Account> Account1 { get; set; }
        //public Account Account2 { get; set; }
        //public AccountClass AccountClass { get; set; }
        //public ICollection<Bank> Banks { get; set; }

        //public ICollection<Customer> Customers { get; set; }
        //public ICollection<Customer> Customers1 { get; set; }
        //public ICollection<Customer> Customers2 { get; set; }
        //public ICollection<Customer> Customers3 { get; set; }

        //public ICollection<GeneralLedgerLine> GeneralLedgerLines { get; set; }
        //public ICollection<GeneralLedgerSetting> GeneralLedgerSettings { get; set; }
        //public ICollection<GeneralLedgerSetting> GeneralLedgerSettings1 { get; set; }
        //public ICollection<GeneralLedgerSetting> GeneralLedgerSettings2 { get; set; }
        //public ICollection<GeneralLedgerSetting> GeneralLedgerSettings3 { get; set; }
        //public ICollection<GeneralLedgerSetting> GeneralLedgerSettings4 { get; set; }

        //public ICollection<Item> Items { get; set; }
        //public ICollection<Item> Items1 { get; set; }
        //public ICollection<Item> Items2 { get; set; }
        //public ICollection<Item> Items3 { get; set; }
        //public ICollection<ItemCategory> ItemCategories { get; set; }
        //public ICollection<ItemCategory> ItemCategories1 { get; set; }
        //public ICollection<ItemCategory> ItemCategories2 { get; set; }
        //public ICollection<ItemCategory> ItemCategories3 { get; set; }
        //public ICollection<ItemCategory> ItemCategories4 { get; set; }
        //public ICollection<JournalEntryLine> JournalEntryLines { get; set; }
        
        //public ICollection<InvoicingReceiptHeader> InvoicingReceiptHeaders { get; set; }
        //public virtual ICollection<InvoicingReceiptLine> InvoicingReceiptLines { get; set; }

        //public virtual ICollection<Tax> Taxes { get; set; }
        //public virtual ICollection<Tax> Taxes1 { get; set; }

        //public virtual ICollection<Vendor> Vendors { get; set; }
        //public virtual ICollection<Vendor> Vendors1 { get; set; }
        //public virtual ICollection<Vendor> Vendors2 { get; set; }
    }
}
