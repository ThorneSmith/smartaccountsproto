

namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public partial class PaymentTerm
    {
   
        public int Id { get; set; }
        public string Description { get; set; }
        public int PaymentType { get; set; }
        public Nullable<int> DueAfterDays { get; set; }
        public bool IsActive { get; set; }
        
        
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    

        public virtual ICollection<Customer> Customers { get; set; }

        public virtual ICollection<InvoicingDeliveryHeader> InvoicingDeliveryHeaders { get; set; }
        public virtual ICollection<InvoicingOrderHeader> InvoicingOrderHeaders { get; set; }

        public virtual ICollection<Vendor> Vendors { get; set; }
    }
}
