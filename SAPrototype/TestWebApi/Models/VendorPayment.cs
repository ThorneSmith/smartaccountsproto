
namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public class VendorPayment
    {
        public int Id { get; set; }
        public int VendorId { get; set; }
        public Nullable<int> PurchaseInvoiceHeaderId { get; set; }
        public Nullable<int> GeneralLedgerHeaderId { get; set; }
        public string No { get; set; }
        public System.DateTime Date { get; set; }
        public decimal Amount { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual GeneralLedgerHeader GeneralLedgerHeader { get; set; }
        public virtual PurchaseInvoiceHeader PurchaseInvoiceHeader { get; set; }
        public virtual Vendor Vendor { get; set; }
    }
}
