

namespace SmartAccounts
{
    using System;
    using System.Collections.Generic;
    
    public partial class InvoicingQuoteHeader
    {
    
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public System.DateTime Date { get; set; }

        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
    
        public virtual Customer Customer { get; set; }
        public virtual ICollection<InvoicingQuoteLine> InvoicingQuoteLines { get; set; }
    }
}
