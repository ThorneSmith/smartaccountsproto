using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SACore.Domain
{
    [Table("MenuMaster")]
    public partial class MenuMaster
    {
        [Key]
        [Column(Order = 0)]
        public int Menu_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(30)]
        public string Menu_RootID { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(30)]
        public string Menu_ChildID { get; set; }

        [StringLength(50)]
        public string UserID { get; set; }

        public DateTime? CreatedDate { get; set; }

        //public string CreatedBy { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public string ModifiedBy { get; set; }
        //public DateTime ModifiedOn { get; set; }
    }
}
