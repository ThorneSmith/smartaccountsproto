﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using SACore.Domain.Items;

namespace SACore.Domain.Receivables
{
    [Table("InvoicingOrderLine")]
    public partial class InvoicingOrderLine : BaseEntity
    {
        public long InvoicingOrderHeaderId { get; set; }
        public long ItemId { get; set; }
        public long MeasurementId { get; set; }
        public decimal Quantity { get; set; }
        public decimal Discount { get; set; }
        public decimal Amount { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual InvoicingOrderHeader InvoicingOrderHeader { get; set; }
        public virtual Item Item { get; set; }
        public virtual Measurement Measurement { get; set; }
    }
}
