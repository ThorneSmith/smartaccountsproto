﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using SACore.Domain.Items;
using SACore.Domain.MasterTables;

namespace SACore.Domain.Receivables
{
    [Table("InvoicingInvoiceLine")]
    public partial class InvoicingInvoiceLine : BaseEntity
    {
        public InvoicingInvoiceLine()
        {
            //InvoicingBatchLineItem = new HashSet<InvoicingBatchLineItem>();
        }
        public long InvoicingInvoiceHeaderId { get; set; }
        public long ItemId { get; set; }
        public long MeasurementId { get; set; }
        public long? InventoryControlJournalId { get; set; }
        public long? TaxId { get; set; }
        public decimal Quantity { get; set; }
        public decimal Discount { get; set; }
        public decimal Amount { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual InvoicingInvoiceHeader InvoicingInvoiceHeader { get; set; }
        public virtual Item Item { get; set; }
        public virtual Measurement Measurement { get; set; }
        public virtual InventoryControlJournal InventoryControlJournal { get; set; }
        public virtual Tax Tax { get; set; }

        public virtual ICollection<InvoicingReceiptLine> InvoicingReceiptLines { get; set; }


        public decimal ComputeLineTaxAmount()
        {
            decimal taxAmount = 0;
            
            return taxAmount;
        }

        public decimal GetAmountPaid()
        {
            return InvoicingReceiptLines.Sum(a => a.AmountPaid);
        }

        public bool IsPaid()
        {
            return Amount == InvoicingReceiptLines.Sum(a => a.AmountPaid);
        }
    }
}
