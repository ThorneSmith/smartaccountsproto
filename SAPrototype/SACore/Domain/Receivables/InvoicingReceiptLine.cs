﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using SACore.Domain.Financials;

namespace SACore.Domain.Receivables
{
    [Table("InvoicingReceiptLine")]
    public partial class InvoicingReceiptLine : BaseEntity
    {
        public InvoicingReceiptLine()
        {            
        }

        public long InvoicingReceiptHeaderId { get; set; }
        public long? InvoicingInvoiceLineId { get; set; }
        public long? ItemId { get; set; }
        public long? SaAccountToCreditId { get; set; }
        public long? MeasurementId { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Amount { get; set; }
        public decimal AmountPaid { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual InvoicingReceiptHeader InvoicingReceiptHeader { get; set; }
        public virtual InvoicingInvoiceLine InvoicingInvoiceLine { get; set; }
        public virtual SaAccount SaAccountToCredit { get; set; }
    }
}
