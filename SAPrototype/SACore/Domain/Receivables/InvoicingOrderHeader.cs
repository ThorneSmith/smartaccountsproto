﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using SACore.Domain.MasterTables;

namespace SACore.Domain.Receivables
{
    [Table("InvoicingOrderHeader")]
    public partial class InvoicingOrderHeader : BaseEntity
    {
        public InvoicingOrderHeader()
        {
            InvoicingOrderLines = new HashSet<InvoicingOrderLine>();
        }

        public long? CustomerId { get; set; }
        public long? PaymentTermId { get; set; }
        public string No { get; set; }
        public string ReferenceNo { get; set; }
        public DateTime Date { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual PaymentTerm PaymentTerm { get; set; }

        public virtual ICollection<InvoicingOrderLine> InvoicingOrderLines { get; set; }
    }
}
