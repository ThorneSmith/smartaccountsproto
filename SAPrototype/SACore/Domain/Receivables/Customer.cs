﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using SACore.Domain.Financials;

namespace SACore.Domain.Receivables
{
    [Table("Customer")]
    public partial class Customer : Party
    {
        public Customer()
        {
            InvoicingInvoices = new HashSet<InvoicingInvoiceHeader>();
            InvoicingReceipts = new HashSet<InvoicingReceiptHeader>();
            InvoicingOrders = new HashSet<InvoicingOrderHeader>();
            CustomerAllocations = new HashSet<CustomerAllocation>();
            //Contacts = new HashSet<Contact>();
        }

        public string No { get; set; }
        public Guid LegalEntityId { get; set; }
        public long? PrimaryContactId { get; set; }
        public long? TaxGroupId { get; set; }
        public long? AccountsReceivableAccountId { get; set; }
        public long? InvoicingAccountId { get; set; }
        public long? InvoicingDiscountAccountId { get; set; }
        public long? PromptPaymentDiscountAccountId { get; set; }
        public long? PaymentTermId { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual TaxGroup TaxGroup { get; set; }
        public virtual Account AccountsReceivableAccount { get; set; }
        public virtual Account InvoicingAccount { get; set; }
        public virtual Account InvoicingDiscountAccount { get; set; }
        public virtual Account PromptPaymentDiscountAccount { get; set; }
        public virtual Contact PrimaryContact { get; set; }
        public virtual PaymentTerm PaymentTerm { get; set; }

        public virtual ICollection<InvoicingInvoiceHeader> InvoicingInvoices { get; set; }
        public virtual ICollection<InvoicingReceiptHeader> InvoicingReceipts { get; set; }
        public virtual ICollection<InvoicingOrderHeader> InvoicingOrders { get; set; }
        public virtual ICollection<CustomerAllocation> CustomerAllocations { get; set; }
        //public virtual ICollection<Contact> Contacts { get; set; }

        [NotMapped]
        public decimal Balance { get { return GetBalance(); } }

        private decimal GetBalance()
        {
            decimal balance = 0;
            decimal totalInvoiceAmount = 0;
            decimal totalReceiptAmount = 0;
            decimal totalAllocation = 0;

            foreach (var header in InvoicingInvoices)
            {
                totalInvoiceAmount += header.ComputeTotalAmount();
                totalAllocation += header.CustomerAllocations.Sum(a => a.Amount);

                foreach (var receipt in header.InvoicingReceipts)
                    foreach(var receiptLine in receipt.InvoicingReceiptLines)
                        totalReceiptAmount += receiptLine.AmountPaid;
            }

            balance = (totalInvoiceAmount - totalReceiptAmount) - totalAllocation;

            return balance;
        }
    }
}
