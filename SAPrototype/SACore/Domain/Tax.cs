﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SACore.Domain.Financials;

namespace SACore.Domain
{
    [Table("Tax")]
    public partial class Tax : BaseEntity
    {
        public Tax()
        {
            TaxGroupTaxes = new HashSet<TaxGroupTax>();
            ItemTaxGroupTaxes = new HashSet<ItemTaxGroupTax>();
        }

        public long? InvoicingSaAccountId { get; set; }
        public long? PurchasingSaAccountId { get; set; }
        [Required]
        [StringLength(50)]
        public string TaxName { get; set; }
        [Required]
        [StringLength(16)]
        public string TaxCode { get; set; }
        public decimal Rate { get; set; }
        public bool IsActive { get; set; }

        //public string CreatedBy { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public string ModifiedBy { get; set; }
        //public DateTime ModifiedOn { get; set; }

        public virtual SaAccount InvoicingSaAccount { get; set; }
        public virtual SaAccount PurchasingSaAccount { get; set; }

        public virtual ICollection<TaxGroupTax> TaxGroupTaxes { get; set; }
        public virtual ICollection<ItemTaxGroupTax> ItemTaxGroupTaxes { get; set; }
    }
}
