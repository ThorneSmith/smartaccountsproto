﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SACore.Domain
{
    [Table("CustomerRollupPeriodHeader")]
    public partial class CustomerRollupPeriodHeader : BaseEntity
    {
        public CustomerRollupPeriodHeader()
        {
        }

        public long CustomerRollupPeriodHeaderCustomerId { get; set; }
        public long CustomerRollupPeriodHeaderTypeId { get; set; }
        public long ParentRelationshipType { get; set; }

    }
}