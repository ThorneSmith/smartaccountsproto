﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;


namespace SACore.Domain
{
    [Table("Entity2Entity")]
    public partial class Entity2Entity : BaseEntity
    {
        public Entity2Entity()
        {
        }
        public Guid ParentGuid { get; set; }
        public Guid ChildGuid { get; set; }
        public int ParentRelationshipType { get; set; }

       
        }
    }
