﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using SACore.Domain.Financials;

namespace SACore.Domain.Payables
{
    [Table("Vendor")]
    public partial class Vendor : Party
    {
        public Vendor()
        {
            PurchaseOrders = new HashSet<PurchaseOrderHeader>();
            PurchaseReceipts = new HashSet<PurchaseReceiptHeader>();
            PurchaseInvoices = new HashSet<PurchaseInvoiceHeader>();
            VendorPayments = new HashSet<VendorPayment>();
            //Contacts = new HashSet<Contact>();
        }

        public string No { get; set; }
        public Guid LegalEntityId { get; set; }
        public long? SaAccountsPayableSaAccountId { get; set; }
        public long? PurchaseSaAccountId { get; set; }
        public long? PurchaseDiscountSaAccountId { get; set; }
        public long? PrimaryContactId { get; set; }
        public long? PaymentTermId { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual SaAccount SaAccountsPayableSaAccount { get; set; }
        public virtual SaAccount PurchaseSaAccount { get; set; }
        public virtual SaAccount PurchaseDiscountSaAccount { get; set; }
        public virtual Contact PrimaryContact { get; set; }
        public virtual PaymentTerm PaymentTerm { get; set; }

        public virtual ICollection<PurchaseOrderHeader> PurchaseOrders { get; set; }
        public virtual ICollection<PurchaseReceiptHeader> PurchaseReceipts { get; set; }
        public virtual ICollection<PurchaseInvoiceHeader> PurchaseInvoices { get; set; }
        public virtual ICollection<VendorPayment> VendorPayments { get; set; }
        //public virtual ICollection<Contact> Contacts { get; set; }

        public decimal GetBalance()
        {
            decimal balance = 0;
            decimal totalInvoiceAmount = 0;
            decimal totalInvoicePayment = 0;

            foreach (var invoice in PurchaseInvoices)
                totalInvoiceAmount += invoice.PurchaseInvoiceLines.Sum(a => a.Amount);

            foreach (var payment in VendorPayments)
                totalInvoicePayment += payment.Amount;

            balance = totalInvoiceAmount - totalInvoicePayment;
            return balance;
        }
    }
}
