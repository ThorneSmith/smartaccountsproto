﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using SACore.Domain.MasterTables;

namespace SACore.Domain.Receivables
{
    [Table("CustomerAllocation")]
    public partial class CustomerAllocation : BaseEntity
    {
        public CustomerAllocation()
        {
        }

        public long CustomerId { get; set; }
        public long InvoicingInvoiceHeaderId { get; set; }
        public long InvoicingReceiptHeaderId { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual InvoicingInvoiceHeader InvoicingInvoiceHeader { get; set; }
        public virtual InvoicingReceiptHeader InvoicingReceiptHeader { get; set; }
    }
}
