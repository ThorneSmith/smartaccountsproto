﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;


namespace SACore.Domain
{
    [Table("LegalEntity")]
    public partial class LegalEntity : BaseEntity
    {
        public LegalEntity()
        {
        }
        public Guid LegalEntityId { get; set; }
        public string LegalEntityName { get; set; }
        public Guid? LegalEntityParentId { get; set; }
        public int? ParentRelationshipType { get; set; }
        public int LegalEntityType { get; set; }
        public bool InActive { get; set; }
        
      

        public decimal GetBalances()
        {
            decimal balance = 0;
            //decimal totalInvoiceAmount = 0;
            //decimal totalInvoicePayment = 0;

            //foreach (var invoice in PurchaseInvoices)
            //    totalInvoiceAmount += invoice.PurchaseInvoiceLines.Sum(a => a.Amount);

            //foreach (var payment in VendorPayments)
            //    totalInvoicePayment += payment.Amount;

            //balance = totalInvoiceAmount - totalInvoicePayment;
            return balance;
        }
    }
}