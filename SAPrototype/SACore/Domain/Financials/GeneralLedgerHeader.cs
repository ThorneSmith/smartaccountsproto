﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using SACore.Domain.Payables;

namespace SACore.Domain.Financials
{
    [Table("GeneralLedgerHeader")]
    public partial class GeneralLedgerHeader : BaseEntity
    {
        public GeneralLedgerHeader()
        {
            GeneralLedgerLines = new HashSet<GeneralLedgerLine>();
            PurchaseOrderReceipts = new HashSet<PurchaseReceiptHeader>();
        }

        public Guid LegalEntityId { get; set; }

        public virtual DateTime Date { get; set; }
        public virtual DocumentTypes DocumentType { get; set; }
        public virtual string Description { get; set; }

        public virtual ICollection<GeneralLedgerLine> GeneralLedgerLines { get; set; }
        public virtual ICollection<PurchaseReceiptHeader> PurchaseOrderReceipts { get; set; }

    }
}
