﻿using System;

namespace SACore.Domain.Financials
{
    public partial class Bank : BaseEntity
    {
        public Bank()
        {
        }

        public Guid LegalEntityId { get; set; }
        public BankTypes Type { get; set; }
        public string Name { get; set; }
        public long? SaAccountId { get; set; }
        public string BankName { get; set; }
        public string Number { get; set; }
        public string Address { get; set; }
        public bool IsDefault { get; set; }
        public bool IsActive { get; set; }

        public virtual SaAccount SaAccount { get; set; }
    }
}
