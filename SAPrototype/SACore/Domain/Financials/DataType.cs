
using System;
using System.ComponentModel.DataAnnotations;

namespace SACore.Domain.Financials
{
    public partial class DataType : BaseEntity
    {
        //[Key]
        //public long id { get; set; }

        [StringLength(150)]
        public string Type { get; set; }

        public int? TypeValue { get; set; }

        [StringLength(250)]
        public string Description { get; set; }

        public Guid? LegalEntityId { get; set; }

        public long? TypeId { get; set; }

        public virtual DataTypeType DataTypeType { get; set; }
    }
}
