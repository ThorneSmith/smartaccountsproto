﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SACore.Domain.Financials
{
    [Table("GeneralLedgerLine")]
    public partial class GeneralLedgerLine : BaseEntity
    {
        public GeneralLedgerLine()
        {
        }

        public long GeneralLedgerHeaderId { get; set; }
        public long SaAccountId { get; set; }
        public TransactionTypes DrCr { get; set; }
        public decimal Amount { get; set; }

        public virtual SaAccount SaAccount { get; set; }
        public virtual GeneralLedgerHeader GeneralLedgerHeader { get; set; }
    }
}
