﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SACore.Domain.Financials
{
    [Table("SaAccountClass")]
    public class SaAccountClass : BaseEntity
    {
        public SaAccountClass()
        {
            SaAccounts = new HashSet<SaAccount>();
        }
        public Guid LegalEntityId { get; set; }
        public string Name { get; set; }
        public string BeginningRangeForType { get; set; }
        public string EndRangeForType { get; set; }
        public string NormalBalance { get; set; }

        public virtual ICollection<SaAccount> SaAccounts { get; set; }
    }
}
