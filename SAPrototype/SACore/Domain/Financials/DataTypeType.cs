using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SACore.Domain.Financials
{

    public partial class DataTypeType : BaseEntity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DataTypeType()
        {
            DataTypes = new HashSet<DataType>();
        }

        public long Id { get; set; }

        [StringLength(50)]
        public string DataType { get; set; }

        [StringLength(210)]
        public string DataTypeDescription { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DataType> DataTypes { get; set; }
    }
}
