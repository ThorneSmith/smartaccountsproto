﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SACore.Domain.Financials
{
    [Table("JournalEntryLine")]
    public partial class JournalEntryLine : BaseEntity
    {
        public long JournalEntryHeaderId { get; set; }
        public long SaAccountId { get; set; }
        public TransactionTypes DrCr { get; set; }
        public decimal Amount { get; set; }
        public string Memo { get; set; }

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] RowVersion { get; set; }

        public virtual JournalEntryHeader JournalEntryHeader { get; set; }
        public virtual SaAccount SaAccount { get; set; }
    }
}
