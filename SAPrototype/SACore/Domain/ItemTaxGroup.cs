﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using SACore.Domain.MasterTables;

namespace SACore.Domain
{
    [Table("ItemTaxGroup")]
    public partial class ItemTaxGroup : BaseEntity
    {
        public ItemTaxGroup()
        {
            ItemTaxGroupTax = new HashSet<ItemTaxGroupTax>();
        }

        public string Name { get; set; }
        public bool IsFullyExempt { get; set; }

        //public string CreatedBy { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public string ModifiedBy { get; set; }
        //public DateTime ModifiedOn { get; set; }

        public virtual ICollection<ItemTaxGroupTax> ItemTaxGroupTax { get; set; }
    }

    [Table("ItemTaxGroupTax")]
    public partial class ItemTaxGroupTax : BaseEntity
    {
        public long TaxId { get; set; }
        public long ItemTaxGroupId { get; set; }
        public bool IsExempt { get; set; }

        //public string CreatedBy { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public string ModifiedBy { get; set; }
        //public DateTime ModifiedOn { get; set; }

        public virtual Tax Tax { get; set; }
        public virtual ItemTaxGroup ItemTaxGroup { get; set; }
    }
}
