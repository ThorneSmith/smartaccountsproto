﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace SACore.Domain
{
    [Table("CustomerSaAccountRollUp")]
    public partial class CustomerSaAccountRollUp : BaseEntity
    {
        public CustomerSaAccountRollUp()
        {
        }

        public Guid SourceCustomerId { get; set; }
        public Guid TargetCustomerParentId { get; set; }
        public long SourceSaAccountId { get; set; }
        public long TargetSaAccountId { get; set; }

    }
}