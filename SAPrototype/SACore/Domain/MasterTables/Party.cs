﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SACore.Domain.MasterTables
{
    [Table("Party")]
    public partial class Party : BaseEntity
    {
        public Party()
        {
            Contacts = new HashSet<Contact>();
        }

        public virtual PartyTypes PartyType { get; set; }
        public virtual string Name { get; set; }
        public virtual string Email { get; set; }
        public virtual string Website { get; set; }
        public virtual string Phone { get; set; }
        public virtual string Fax { get; set; }
        public virtual bool IsActive { get; set; }

        public virtual ICollection<Contact> Contacts { get; set; }
    }
}
