﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using SACore.Domain.Financials;

namespace SACore.Domain.MasterTables
{
    public partial class Bank : BaseEntity
    {
        public Bank()
        {
        }

        public Guid LegalEntityId { get; set; }
        public BankTypes BankType { get; set; }
        public string Name { get; set; }
        public long? SaAccountId { get; set; }
        public string BankName { get; set; }
        public string Number { get; set; }
        public long AddressId { get; set; }
        public bool IsDefault { get; set; }
        public bool IsActive { get; set; }

        public virtual SaAccount SaAccount { get; set; }
        [NotMapped]
        public decimal Balance { get { return GetBalance(); } }

        private decimal GetBalance()
        {
            decimal balance = 0;
            decimal totalInvoiceAmount = 0;
            decimal totalReceiptAmount = 0;
            decimal totalAllocation = 0;

            //foreach (var header in InvoicingInvoices)
            //{
            //    totalInvoiceAmount += header.ComputeTotalAmount();
            //    totalAllocation += header.CustomerAllocations.Sum(a => a.Amount);

            //    foreach (var receipt in header.InvoicingReceipts)
            //        foreach (var receiptLine in receipt.InvoicingReceiptLines)
            //            totalReceiptAmount += receiptLine.AmountPaid;
            //}

            balance = (totalInvoiceAmount - totalReceiptAmount) - totalAllocation;

            return balance;
        }
    }
}
