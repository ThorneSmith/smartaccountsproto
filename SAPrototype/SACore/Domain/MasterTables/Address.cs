﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SACore.Domain.MasterTables
{
    [Table("Address")]
    public partial class Address : BaseEntity
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public long StateId { get; set; }
        public string Zip { get; set; }
        public bool IsActive { get; set; }

        public string GetStateById(long p)
        {
            return "";
        }
        
        public string GetFullAddress()
        {
            return Address1 + ", " + City + ", " + GetStateById(Id) + ", " + Zip;
        }
    }
}
