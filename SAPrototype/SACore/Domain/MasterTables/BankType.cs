﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SACore.Domain.MasterTables
{
    [Table("BankType")]
    public partial class BankType : BaseEntity
    {
        public BankType()
        {
        }
        [Key]
        public long Id { get; set; }
        [Required]
        public Guid LegalEntityId { get; set; }
        public string Description { get; set; }
        
    }
}
