﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SACore.Domain.MasterTables
{
    [Table("State")]
    public partial class State : BaseEntity
    {
        public string Name { get; set; }
        public string Abbreviation { get; set; }
        public string Country { get; set; }

    }
}
