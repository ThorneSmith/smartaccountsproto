﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using SACore.Domain.Financials;
using SACore.Domain.Receivables;

namespace SACore.Domain.MasterTables
{
    [Table("Customer")]
    public partial class Customer : Party
    {
        public Customer()
        {
            //LegalEntitys = new HashSet<LegalEntity>();
            //InvoicingInvoices = new HashSet<InvoicingInvoiceHeader>();
            //InvoicingReceipts = new HashSet<InvoicingReceiptHeader>();
            //InvoicingOrders = new HashSet<InvoicingOrderHeader>();
            //CustomerAllocations = new HashSet<CustomerAllocation>();
            //Contacts = new HashSet<Contact>();
        }

        public Customer(string no, int? primaryContactId, int? taxGroupId, int? AccountsReceivableSaAccountId, int? invoicingSaAccountId, int? invoicingDiscountSaAccountId, int? invoicingPenaltySaAccountId, int? paymentTermId, int? backendTypeId)
        {
            No = no;
            //LegalEntity = LegalEntity;
            PrimaryContactId = primaryContactId;
            TaxGroupId = taxGroupId;
            AccountsReceivableSaAccountId = AccountsReceivableSaAccountId;
            InvoicingSaAccountId = invoicingSaAccountId;
            InvoicingDiscountSaAccountId = invoicingDiscountSaAccountId;
            InvoicingPenaltySaAccountId = invoicingPenaltySaAccountId;
            PaymentTermId = paymentTermId;
            BackendTypeId = backendTypeId;
        }

       
        public Guid LegalEntityId { get; set; }

        public string No { get; set; }
        public long? PrimaryContactId { get; set; }
        public long? TaxGroupId { get; set; }
        public long? AccountsReceivableSaAccountId { get; set; }
        public long? InvoicingSaAccountId { get; set; }
        public long? InvoicingDiscountSaAccountId { get; set; }
        public long? InvoicingPenaltySaAccountId { get; set; }
        public long? PaymentTermId { get; set; }
        public long? BackendTypeId { get; set; }



        public virtual Party Party { get; set; }
        public virtual LegalEntity LegalEntity { get; set; }
        public virtual TaxGroup TaxGroup { get; set; }
        public virtual SaAccount AccountsReceivableSaAccount { get; set; }
        public virtual SaAccount InvoicingSaAccount { get; set; }
        public virtual SaAccount InvoicingDiscountSaAccount { get; set; }
        public virtual SaAccount InvoicingPenaltySaAccount { get; set; }
        public virtual Contact PrimaryContact { get; set; }
        public virtual PaymentTerm PaymentTerm { get; set; }
        

        public virtual ICollection<LegalEntity> LegalEntitys { get; set; }
        public virtual ICollection<InvoicingInvoiceHeader> InvoicingInvoices { get; set; }
        public virtual ICollection<InvoicingReceiptHeader> InvoicingReceipts { get; set; }
        public virtual ICollection<InvoicingOrderHeader> InvoicingOrders { get; set; }
        public virtual ICollection<CustomerAllocation> CustomerAllocations { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }

        [NotMapped]
        public decimal Balance { get { return GetBalance(); } }

        private decimal GetBalance()
        {
            decimal balance = 0;
            decimal totalInvoiceAmount = 0;
            decimal totalReceiptAmount = 0;
            decimal totalAllocation = 0;

            foreach (var header in InvoicingInvoices)
            {
                totalInvoiceAmount += header.ComputeTotalAmount();
                totalAllocation += header.CustomerAllocations.Sum(a => a.Amount);

                foreach (var receipt in header.InvoicingReceipts)
                    foreach (var receiptLine in receipt.InvoicingReceiptLines)
                        totalReceiptAmount += receiptLine.AmountPaid;
            }

            balance = (totalInvoiceAmount - totalReceiptAmount) - totalAllocation;

            return balance;
        }
    }
}
