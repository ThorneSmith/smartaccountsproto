﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SACore.Domain.MasterTables
{
    [Table("CompanySetting")]
    public partial class CompanySetting : BaseEntity
    {
        public long CompanyId { get; set; }

        public Company Company { get; set; }

    }
}
