﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SACore.Domain.MasterTables
{
    [Table("Contact")]
    public partial class Contact : Party
    {
        public ContactTypes ContactType { get; set; }
        public long? PartyId { get; set; }

        public Guid LegalEntityId { get; set; }

        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string MiddleName { get; set; }

        public virtual Party Party { get; set; }
    }
}
