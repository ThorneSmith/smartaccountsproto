﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SACore.Domain
{
    [Table("Company")]
    public partial class Company : BaseEntity
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
        public Guid ParentId { get; set; }
        public int CompanyType { get; set; }

    }
}
