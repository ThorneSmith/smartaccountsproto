namespace IdentitySample
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public virtual DbSet<MenuDetail> MenuDetails { get; set; }
        public virtual DbSet<MenuMaster> MenuMasters { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MenuDetail>()
                .Property(e => e.Menu_ChildID)
                .IsUnicode(false);

            modelBuilder.Entity<MenuDetail>()
                .Property(e => e.MenuName)
                .IsUnicode(false);

            modelBuilder.Entity<MenuDetail>()
                .Property(e => e.MenuDisplayTxt)
                .IsUnicode(false);

            modelBuilder.Entity<MenuDetail>()
                .Property(e => e.MenuFileName)
                .IsUnicode(false);

            modelBuilder.Entity<MenuDetail>()
                .Property(e => e.MenuURL)
                .IsUnicode(false);

            modelBuilder.Entity<MenuDetail>()
                .Property(e => e.USE_YN)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<MenuDetail>()
                .Property(e => e.UserID)
                .IsUnicode(false);

            modelBuilder.Entity<MenuMaster>()
                .Property(e => e.Menu_RootID)
                .IsUnicode(false);

            modelBuilder.Entity<MenuMaster>()
                .Property(e => e.Menu_ChildID)
                .IsUnicode(false);

            modelBuilder.Entity<MenuMaster>()
                .Property(e => e.UserID)
                .IsUnicode(false);
        }
    }
}
