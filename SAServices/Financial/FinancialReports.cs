﻿using System;

namespace SAServices.Financial
{
    public class TrialBalance
    {
        public string SaAccountCode { get; set; }
        public string SaAccountName { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
    }

    public class BalanceSheet
    {
        public long SaAccountClassId { get; set; }
        public string SaAccountCode { get; set; }
        public string SaAccountName { get; set; }
        public decimal Amount { get; set; }
    }

    public class IncomeStatement
    {
        public bool IsExpense { get; set; }
        public string SaAccountCode { get; set; }
        public string SaAccountName { get; set; }
        public decimal Amount { get; set; }
    }

    public partial class MasterGeneralLedger
    {
        public long Id { get; set; }
        public long SaAccountId { get; set; }
        public long CurrencyId { get; set; }
        public string DocumentType { get; set; }
        public long TransactionNo { get; set; }
        public string SaAccountCode { get; set; }
        public string SaAccountName { get; set; }
        public DateTime Date { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
    }
}
