﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using SACore.Data;
using SACore.Domain;
using SACore.Domain.Financials;
using SACore.Domain.Items;
using SACore.Domain.MasterTables;
using SAServices.Inventory;

namespace SAServices.Financial
{
    public partial class FinancialService : BaseService, IFinancialService
    {
        public Guid currentLegalEntity = new Guid("1FFDA348-B02D-40DC-82CC-6C7293DA34CC");

        private readonly IInventoryService _inventoryService;

        private readonly IRepository<GeneralLedgerHeader> _generalLedgerRepository;
        private readonly IRepository<SaAccount> _saAccountRepo;
        private readonly IRepository<SaAccountClass> _saAccountClassRepo;
        private readonly IRepository<SaAccountType> _saAccountTypeRepo;
        private readonly IRepository<SaAccountSubClass> _saAccountSubClassRepo;
        private readonly IRepository<Tax> _taxRepo;
        private readonly IRepository<JournalEntryHeader> _journalEntryRepo;
        private readonly IRepository<GeneralLedgerLine> _generalLedgerLineRepository;
        private readonly IRepository<FiscalYear> _fiscalYearRepo;
        private readonly IRepository<LegalEntity> _legalEntityRepo;
        private readonly IRepository<TaxGroup> _taxGroupRepo;
        private readonly IRepository<ItemTaxGroup> _itemTaxGroupRepo;
        private readonly IRepository<PaymentTerm> _paymentTermRepo;
        private readonly IRepository<Bank> _bankRepo;
        private readonly IRepository<Item> _itemRepo;
        private readonly IRepository<GeneralLedgerSettings> _glSettingRepo;

        public FinancialService(IInventoryService inventoryService, 
            IRepository<GeneralLedgerHeader> generalLedgerRepository,
            IRepository<GeneralLedgerLine> generalLedgerLineRepository,
            IRepository<SaAccount> saAccountRepo,
            IRepository<SaAccountClass> saAccountClassRepo,
            IRepository<SaAccountSubClass> saAccountSubClassRepo,
            IRepository<SaAccountType> saAccountTypeRepo,
            IRepository<Tax> taxRepo,
            IRepository<JournalEntryHeader> journalEntryRepo,
            IRepository<FiscalYear> fiscalYearRepo,
            IRepository<LegalEntity> legalEntityRepo,
            IRepository<TaxGroup> taxGroupRepo,
            IRepository<ItemTaxGroup> itemTaxGroupRepo,
            IRepository<PaymentTerm> paymentTermRepo,
            IRepository<Bank> bankRepo,
            IRepository<Item> itemRepo,
            IRepository<GeneralLedgerSettings> glSettingRepo
            )
            :base(null, null,null, paymentTermRepo, bankRepo)
        {
            _inventoryService = inventoryService;
            _generalLedgerRepository = generalLedgerRepository;
            _saAccountRepo = saAccountRepo;
            _saAccountClassRepo = saAccountClassRepo;
            _saAccountTypeRepo = saAccountTypeRepo;
            _saAccountSubClassRepo = saAccountSubClassRepo;
            _taxRepo = taxRepo;
            _journalEntryRepo = journalEntryRepo;
            _generalLedgerLineRepository = generalLedgerLineRepository;
            _fiscalYearRepo = fiscalYearRepo;
            _legalEntityRepo = legalEntityRepo;
            _taxGroupRepo = taxGroupRepo;
            _itemTaxGroupRepo = itemTaxGroupRepo;
            _paymentTermRepo = paymentTermRepo;
            _bankRepo = bankRepo;
            _itemRepo = itemRepo;
            _glSettingRepo = glSettingRepo;
        }

        public FiscalYear CurrentFiscalYear()
        {
            var query = (from fy in _fiscalYearRepo.Table
                         where fy.IsActive == true && fy.LegalEntityId == currentLegalEntity
                        select fy).FirstOrDefault();

            return query;
        }

        public LegalEntity GetCurrentLegalEntity()
        {
            var query = (from cle in _legalEntityRepo.Table
                         where cle.LegalEntityId == currentLegalEntity
                         select cle).FirstOrDefault();

            return query;
        }

        public GeneralLedgerHeader CreateGeneralLedgerHeader(DocumentTypes documentType, DateTime date, string description)
        {
            var entry = new GeneralLedgerHeader()
            {
                DocumentType = documentType,
                Date = date,
                Description = description,

            };
            return entry;
        }

        public GeneralLedgerLine CreateGeneralLedgerLine(TransactionTypes DrCr, long saSaAccountId, decimal amount)
        {
            var line = new GeneralLedgerLine()
            {
                DrCr = DrCr,
                SaAccountId = saSaAccountId,
                Amount = amount,

            };
            return line; 
        }

        public string CreateSaAccountCode(long? parentSaAccountId, SaAccountTypes saAccountTypes, long? saAccountSubClassId, long saAccountClassId)
        {
            var newCode = "";

            if (parentSaAccountId != null)
            {
                
            }


            return newCode;
        }


        public IEnumerable<SaAccount> GetSaAccounts()
        {

            var query = from u in _saAccountRepo.Table
            orderby u.SaAccountName
            join c in _saAccountRepo.Table
                on u.ParentSaAccountId equals c.Id
            where c.SaAccountName != null
            select u;
        
            var query1 = from f in query

                         orderby f.SaAccountName
                         select f
                        ;
            return query1.AsEnumerable();
        }

       
        public SaAccount GetSaAccountById(long p)
        {

            var query = from a in _saAccountRepo.Table
            
            where a.Id == p
            select a;

            return query.FirstOrDefault();
        }


        public IEnumerable<SaAccount> GetSubAccountsByType(int p)
            {
                var query = from f in _saAccountRepo.Table
                            where f.SaAccountSubClassId == p
                            select f;
                return query.AsEnumerable();

            }

        public IEnumerable<SaAccountType> GetSaAccountTypes()
        {
            int i = 0;
            List<SaAccountType> lSaT = new  List<SaAccountType>();
            var values = EnumUtil.GetValues<SaAccountTypes>();
            foreach (var t in values)
            {
                SaAccountType sAT = new SaAccountType();
                sAT.Id = i++;
                sAT.Description = t.ToString();
                lSaT.Add(sAT);
            }
            return lSaT;
        }

        public void AddSaAccount(SaAccount saAccount)
        {
            saAccount.LegalEntityId = GetCurrentLegalEntity().LegalEntityId;
            _saAccountRepo.Insert(saAccount);
        }


        public void UpdateSaAccount(SaAccount saAccount)
        {
            saAccount.LegalEntityId = GetCurrentLegalEntity().LegalEntityId;
            _saAccountRepo.Update(saAccount);
        }

        public static class EnumUtil
        {
            public static IEnumerable<T> GetValues<T>()
            {
                return Enum.GetValues(typeof(T)).Cast<T>();
            }
        }

        public IEnumerable<SaAccountClass> GetSaAccountClasses()
        {

            var query = from f in _saAccountClassRepo.Table
                        select f;
            return query.AsEnumerable();

        }

        public IEnumerable<SaAccountSubClass> GetSaAccountSubClasses()
        {
            var query = from f in _saAccountSubClassRepo.Table
                        select f;
            return query.AsEnumerable();

        }
        
        public IEnumerable<Tax> GetTaxes()
        {
            var query = from f in _taxRepo.Table
                        select f;
            return query.AsEnumerable();
        }

        public IEnumerable<ItemTaxGroup> GetItemTaxGroups()
        {
            var query = from f in _itemTaxGroupRepo.Table
                        select f;
            return query;
        }

        public IEnumerable<TaxGroup> GetTaxGroups()
        {
            var query = from f in _taxGroupRepo.Table
                        select f;
            return query;
        }

        public void SaveGeneralLedgerEntry(GeneralLedgerHeader entry)
        {
            if (ValidateGeneralLedgerEntry(entry).NoErrors)
            {
                try
                {
                    _generalLedgerRepository.Insert(entry);
                }
                catch { }
            }
        }

        public LegalEntity GetCurLegalEntity()
        {
            var cle = GetCurrentLegalEntity();
            return cle;
        }

        public ErrorHandler.Errors ValidateGeneralLedgerEntry(GeneralLedgerHeader glEntry)
        {
            ErrorHandler.Errors eh = new ErrorHandler.Errors();
            eh.NoErrors = true;


            if (!FinancialHelper.DrCrEqualityValidated(glEntry))
            {
                eh.NoErrors = false;
                eh.ErrorMsg = "Debit/Credit are not equal.";
            }


            if (!FinancialHelper.NoLineAmountIsEqualToZero(glEntry))
            {
                eh.NoErrors = false;
                eh.ErrorMsg = "One or more line(s) amount is zero or you have no offsetting debits for your credits.";
            }


            var fiscalYear = CurrentFiscalYear();
            if (fiscalYear == null || !FinancialHelper.InRange(glEntry.Date, fiscalYear.StartDate, fiscalYear.EndDate))
            {
                eh.NoErrors = false;
                eh.ErrorMsg = "Date is out of range. Must within financial year.";
            }


            //var duplicateSaAccounts = glEntry.GeneralLedgerLines.GroupBy(gl => gl.SaAccountId).Where(gl => gl.Count() > 1);
            //if (duplicateSaAccounts.Count() > 0)
            //    throw new InvalidOperationException("Duplicate saSaAccount id in a collection.");

            foreach (var saSaAccount in glEntry.GeneralLedgerLines)
                if (!_saAccountRepo.GetById(saSaAccount.SaAccountId).HasChildSaAccounts())
                {
                    eh.NoErrors = false;
                    eh.ErrorMsg = "SaAccount is not valid for posting.";
                }



            return eh;
                }

        public IEnumerable<JournalEntryHeader> GetJournalEntries()
        {
            var query = from je in _journalEntryRepo.Table
                        select je;
            return query.ToList();
        }

        public ErrorHandler.Errors AddJournalEntry(JournalEntryHeader journalEntry)
        {

            var glEntry = new GeneralLedgerHeader()
            {
                Date = DateTime.Now,
                DocumentType = SACore.Domain.DocumentTypes.JournalEntry,
                Description = journalEntry.Memo,

            };

            foreach (var je in journalEntry.JournalEntryLines)
            {
                glEntry.GeneralLedgerLines.Add(new GeneralLedgerLine()
                {
                    SaAccountId = je.SaAccountId,
                    DrCr = je.DrCr,
                    Amount = je.Amount,

                });
            }

            ErrorHandler.Errors eh = new ErrorHandler.Errors();
            eh = ValidateGeneralLedgerEntry(glEntry);
            if (eh.NoErrors)
            {
                journalEntry.GeneralLedgerHeader = glEntry;
                _journalEntryRepo.Insert(journalEntry);
                eh.NoErrors = true;
            }
            return eh;
        }

        public ICollection<TrialBalance> TrialBalance(DateTime? from = default(DateTime?), DateTime? to = default(DateTime?))
        {            
            var allDr = (from dr in _generalLedgerLineRepository.Table.AsEnumerable()
                         where dr.DrCr == TransactionTypes.Dr
                         //&& IsDateBetweenFinancialYearStartDateAndEndDate(dr.GLHeader.Date)
                         group dr by new { dr.SaAccount.SaAccountCode, dr.SaAccount.SaAccountName, dr.Amount } into tb
                         select new
                         {
                             SaAccountCode = tb.Key.SaAccountCode,
                             SaAccountName = tb.Key.SaAccountName,
                             Debit = tb.Sum(d => d.Amount),
                         });

            var allCr = (from cr in _generalLedgerLineRepository.Table.AsEnumerable()
                         where cr.DrCr == TransactionTypes.Cr
                         //&& IsDateBetweenFinancialYearStartDateAndEndDate(cr.GLHeader.Date)
                         group cr by new { cr.SaAccount.SaAccountCode, cr.SaAccount.SaAccountName, cr.Amount } into tb
                         select new
                         {
                             SaAccountCode = tb.Key.SaAccountCode,
                             SaAccountName = tb.Key.SaAccountName,
                             Credit = tb.Sum(c => c.Amount),
                         });

            var allDrcr = (from x in allDr
                           select new TrialBalance
                           {
                               SaAccountCode = x.SaAccountCode,
                               SaAccountName = x.SaAccountName,
                               Debit = x.Debit,
                               Credit = (decimal)0,
                           }
                          ).Concat(from y in allCr
                                   select new TrialBalance
                                   {
                                       SaAccountCode = y.SaAccountCode,
                                       SaAccountName = y.SaAccountName,
                                       Debit = (decimal)0,
                                       Credit = y.Credit,
                                   });

            var sortedList = allDrcr
                .OrderBy(tb => tb.SaAccountCode)
                .ToList()
                .Reverse<TrialBalance>();

            var saSaAccounts = sortedList.ToList().GroupBy(a => a.SaAccountCode)
                .Select(tb => new TrialBalance
                {
                    SaAccountCode = tb.First().SaAccountCode,
                    SaAccountName = tb.First().SaAccountName,
                    Credit = tb.Sum(x => x.Credit),
                    Debit = tb.Sum(y => y.Debit)
                }).ToList();
            return saSaAccounts;
        }

        public ICollection<BalanceSheet> BalanceSheet(DateTime? from = default(DateTime?), DateTime? to = default(DateTime?))
        {
            var assets = from a in _saAccountRepo.Table
                         where a.SaAccountClassId == 1 && a.ParentSaAccountId != null
                         select a;
            var liabilities = from a in _saAccountRepo.Table
                              where a.SaAccountClassId == 2 && a.ParentSaAccountId != null
                              select a;
            var equities = from a in _saAccountRepo.Table
                           where a.SaAccountClassId == 3 && a.ParentSaAccountId != null
                           select a;

            var balanceSheet = new HashSet<BalanceSheet>();
            foreach (var asset in assets)
            {
                balanceSheet.Add(new BalanceSheet()
                {
                    SaAccountClassId = asset.SaAccountClassId,
                    SaAccountCode = asset.SaAccountCode,
                    SaAccountName = asset.SaAccountName,
                    Amount = asset.Balance
                });
            }
            foreach (var liability in liabilities)
            {
                balanceSheet.Add(new BalanceSheet()
                {
                    SaAccountClassId = liability.SaAccountClassId,
                    SaAccountCode = liability.SaAccountCode,
                    SaAccountName = liability.SaAccountName,
                    Amount = liability.Balance
                });
            }
            foreach (var equity in equities)
            {
                balanceSheet.Add(new BalanceSheet()
                {
                    SaAccountClassId = equity.SaAccountClassId,
                    SaAccountCode = equity.SaAccountCode,
                    SaAccountName = equity.SaAccountName,
                    Amount = equity.Balance
                });
            }
            return balanceSheet;
        }

        public ICollection<IncomeStatement> IncomeStatement(DateTime? from = default(DateTime?), DateTime? to = default(DateTime?))
        {
            var revenues = from r in _saAccountRepo.Table
                           where r.SaAccountClassId == 4 && r.ParentSaAccountId != null
                           select r;

            var expenses = from e in _saAccountRepo.Table
                           where e.SaAccountClassId == 5 && e.ParentSaAccountId != null
                           select e;

            var revenues_expenses = new HashSet<IncomeStatement>();
            foreach (var revenue in revenues)
            {
                revenues_expenses.Add(new IncomeStatement()
                {
                    SaAccountCode = revenue.SaAccountCode,
                    SaAccountName = revenue.SaAccountName,
                    Amount = revenue.Balance,
                    IsExpense = false
                });
            }
            foreach (var expense in expenses)
            {
                revenues_expenses.Add(new IncomeStatement()
                {
                    SaAccountCode = expense.SaAccountCode,
                    SaAccountName = expense.SaAccountName,
                    Amount = expense.Balance,
                    IsExpense = true
                });
            }
            return revenues_expenses;
        }

        public ICollection<MasterGeneralLedger> MasterGeneralLedger(DateTime? from = default(DateTime?), DateTime? to = default(DateTime?))
        {
            var allDr = (from dr in _generalLedgerLineRepository.Table.AsEnumerable()
                         where dr.DrCr == TransactionTypes.Dr
                         //&& GeneralLedgerHelper.IsBetween(dr.GLHeader.Date, (DateTime)fromDate, (DateTime)toDate) == true
                         select new MasterGeneralLedger
                         {
                             Id = dr.Id,
                             TransactionNo = dr.GeneralLedgerHeader.Id,
                             SaAccountId = dr.SaAccountId,
                             SaAccountCode = dr.SaAccount.SaAccountCode,
                             SaAccountName = dr.SaAccount.SaAccountName,
                             Date = dr.GeneralLedgerHeader.Date,
                             Debit = dr.Amount,
                             Credit = (decimal)0,
                             //CurrencyId = dr.CurrencyId,
                             DocumentType = Enum.GetName(typeof(DocumentTypes), dr.GeneralLedgerHeader.DocumentType)
                         });

            var allCr = (from cr in _generalLedgerLineRepository.Table.AsEnumerable()
                         where cr.DrCr == TransactionTypes.Cr
                         //&& GeneralLedgerHelper.IsBetween(cr.GLHeader.Date, (DateTime)fromDate, (DateTime)toDate) == true
                         select new MasterGeneralLedger
                         {
                             Id = cr.Id,
                             TransactionNo = cr.GeneralLedgerHeader.Id,
                             SaAccountId = cr.SaAccountId,
                             SaAccountCode = cr.SaAccount.SaAccountCode,
                             SaAccountName = cr.SaAccount.SaAccountName,
                             Date = cr.GeneralLedgerHeader.Date,
                             Debit = (decimal)0,
                             Credit = cr.Amount,
                             //CurrencyId = cr.CurrencyId,
                             DocumentType = Enum.GetName(typeof(DocumentTypes), cr.GeneralLedgerHeader.DocumentType)
                         });

            var allDrcr = (from x in allDr
                           select new MasterGeneralLedger
                           {
                               Id = x.Id,
                               TransactionNo = x.TransactionNo,
                               SaAccountId = x.SaAccountId,
                               SaAccountCode = x.SaAccountCode,
                               SaAccountName = x.SaAccountName,
                               Date = x.Date,
                               Debit = x.Debit,
                               Credit = (decimal)0,
                               CurrencyId = x.CurrencyId,
                               DocumentType = x.DocumentType
                           }
                          ).Concat(from y in allCr
                                   select new MasterGeneralLedger
                                   {
                                       Id = y.Id,
                                       TransactionNo = y.TransactionNo,
                                       SaAccountId = y.SaAccountId,
                                       SaAccountCode = y.SaAccountCode,
                                       SaAccountName = y.SaAccountName,
                                       Date = y.Date,
                                       Debit = (decimal)0,
                                       Credit = y.Credit,
                                       CurrencyId = y.CurrencyId,
                                       DocumentType = y.DocumentType
                                   });

            var sortedList = allDrcr.OrderBy(gl => gl.Id).ToList().Reverse<MasterGeneralLedger>();
            return sortedList.ToList();
        }



        /// <summary>
        /// Input VAT is the value added tax added to the price when you purchase goods or services liable to VAT. If the buyer is registered in the VAT Register, the buyer can deduct the amount of VAT paid from his/her settlement with the tax authorities. 
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="quantity"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public List<KeyValuePair<long, decimal>> ComputeInputTax(long itemId, decimal quantity, decimal amount)
        {
            var taxes = new List<KeyValuePair<long, decimal>>();
            decimal taxAmount = 0;
            var item = _inventoryService.GetItemById(itemId);
            var lineAmount = quantity * amount;

            if (item.ItemTaxGroup != null)
            {
                foreach (var tax in item.ItemTaxGroup.ItemTaxGroupTax)
                {
                    if (!tax.IsExempt)
                    {
                        var lineTaxAmount = (tax.Tax.Rate / 100) * lineAmount;
                        taxAmount += lineTaxAmount;
                        taxes.Add(new KeyValuePair<long, decimal>(tax.Id, lineTaxAmount));
                    }
                }
            }
            return taxes;
        }

        /// <summary>
        /// Output VAT is the value added tax you calculate and charge on your own invoicing of goods and services
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="quantity"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public List<KeyValuePair<long, decimal>> ComputeOutputTax(long itemId, decimal quantity, decimal amount, decimal discount)
        {
            decimal taxAmount = 0, amountXquantity = 0, discountAmount = 0, subTotalAmount = 0;
            var item = _itemRepo.GetById(itemId);
            var taxes = new List<KeyValuePair<long, decimal>>();
            amountXquantity = amount * quantity;

            if(discount > 0)
                discountAmount = (discount / 100) * amountXquantity;

            subTotalAmount = amountXquantity - discountAmount;

            if (item.ItemTaxGroup != null)
            {
                foreach (var tax in item.ItemTaxGroup.ItemTaxGroupTax)
                {
                    if (!tax.IsExempt)
                    {
                        taxAmount = subTotalAmount - (subTotalAmount / (1 + (tax.Tax.Rate / 100)));
                        taxes.Add(new KeyValuePair<long, decimal>(tax.Id, taxAmount));
                    }
                }
            }
            return taxes;
        }

        public GeneralLedgerSettings GetGeneralLedgerSettings(long? companyId = null)
        {
            GeneralLedgerSettings glSetting = null;
            if (companyId == null)
                glSetting = _glSettingRepo.Table.FirstOrDefault();
            else
                glSetting = _glSettingRepo.Table.Where(s => s.CompanyId == companyId).FirstOrDefault();

            return glSetting;
        }

        public void UpdateGeneralLedgerSettings(GeneralLedgerSettings setting)
        {
            _glSettingRepo.Update(setting);
        }
    }
}
