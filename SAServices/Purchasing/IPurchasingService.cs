﻿using System;
using System.Collections.Generic;
using SACore.Domain.MasterTables;
using SACore.Domain.Payables;

namespace SAServices.Purchasing
{
    public partial interface IPurchasingService
    {
        void AddPurchaseInvoice(PurchaseInvoiceHeader purchaseIvoice, long? purchaseOrderId);
        void AddPurchaseOrder(PurchaseOrderHeader purchaseOrder, bool toSave);
        void AddPurchaseOrderReceipt(PurchaseReceiptHeader purchaseOrderReceipt);

        IEnumerable<PurchaseOrderHeader> GetPurchaseOrders();
        PurchaseOrderHeader GetPurchaseOrderById(long id);
        PurchaseReceiptHeader GetPurchaseReceiptById(long id);

        IEnumerable<PurchaseInvoiceHeader> GetPurchaseInvoices();
        PurchaseInvoiceHeader GetPurchaseInvoiceById(long id);
        void SavePayment(long invoiceId, long vendorId, long saSaAccountId, decimal amount, DateTime date);


        Vendor GetVendorById(long id);
        IEnumerable<Vendor> GetVendors();

    }
}
