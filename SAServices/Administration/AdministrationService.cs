﻿using System;
using System.Collections.Generic;
using System.Linq;
using SACore.Data;
using SACore.Domain;
using SACore.Domain.Financials;
using SACore.Domain.MasterTables;

namespace SAServices.Administration
{
    public class AdministrationService : BaseService, IAdministrationService
    {
        private readonly IRepository<FiscalYear> _fiscalYearRepo;
        private readonly IRepository<TaxGroup> _taxGroupRepo;
        private readonly IRepository<ItemTaxGroup> _itemTaxGroupRepo;
        private readonly IRepository<PaymentTerm> _paymentTermRepo;
        private readonly IRepository<LegalEntity> _legalEntityRepo;
        private readonly IRepository<Bank> _bankRepo;
        private readonly IRepository<GeneralLedgerSettings> _genetalLedgerSetting;
        private readonly IRepository<Tax> _taxRepo;

        public AdministrationService(IRepository<FiscalYear> fiscalYearRepo,
            IRepository<TaxGroup> taxGroupRepo,
            IRepository<ItemTaxGroup> itemTaxGroupRepo,
            IRepository<PaymentTerm> paymentTermRepo,
            IRepository<LegalEntity> legalEntityRepo,
            IRepository<Bank> bankRepo,
            IRepository<Tax> taxRepo,
            IRepository<GeneralLedgerSettings> generalLedgerSetting)
            : base(null, null, generalLedgerSetting, paymentTermRepo, bankRepo)
        {
            _fiscalYearRepo = fiscalYearRepo;
            _taxGroupRepo = taxGroupRepo;
            _itemTaxGroupRepo = itemTaxGroupRepo;
            _paymentTermRepo = paymentTermRepo;
            _legalEntityRepo = legalEntityRepo;
            _bankRepo = bankRepo;
            _genetalLedgerSetting = generalLedgerSetting;
            _taxRepo = taxRepo;
        }

        public ICollection<Tax> GetAllTaxes(bool includeInActive)
        {
            var query = from f in _taxRepo.Table
                        select f;
            return query.ToList();
        }

        public ICollection<LegalEntity> GetAllLegalEntities(bool includeInActive)
        {
            var query = from f in _legalEntityRepo.Table
                        select f;
            return query.ToList();
        }

        public LegalEntity GetLegalEntityById(Guid legalEntityId, bool includeInActive)
        {
            var query = from f in _legalEntityRepo.Table 
                             where f.LegalEntityId == legalEntityId
                                select f
                        ;
            return (LegalEntity) query;
        }

        public void AddNewTax(Tax tax)
        {
            _taxRepo.Insert(tax);
        }

        public ItemTaxGroup AddNewItemTaxGroup(ItemTaxGroup itemTaxGroup)
        {
            _itemTaxGroupRepo.Insert(itemTaxGroup);
            return itemTaxGroup;
            
        }


        public void AddNewLegalEntity(LegalEntity legalEntity)
        {
            _legalEntityRepo.Insert(legalEntity);
        }
        public void UpdateLegalEntity(LegalEntity legalEntity)
        {
            _legalEntityRepo.Update(legalEntity);
        }

        public void UpdateTax(Tax tax)
        {
            _taxRepo.Update(tax);
        }

        public void DeleteTax(long id)
        {
            throw new System.NotImplementedException();
        }

        public ICollection<ItemTaxGroup> GetItemTaxGroups(bool includeInActive)
        {
            var query = from f in _itemTaxGroupRepo.Table
                        select f;
            return query.ToList();
        }

        public ICollection<TaxGroup> GetTaxGroups(bool includeInActive)
        {
            var query = from f in _taxGroupRepo.Table
                        select f;
            return query.ToList();
        }
    }
}
