﻿using System;
using System.Collections.Generic;
using System.Linq;
using SACore.Data;
using SACore.Domain;
using SACore.Domain.Financials;
using SACore.Domain.Items;
using SACore.Domain.MasterTables;
using SACore.Domain.Receivables;
using SAServices.Financial;
using SAServices.Inventory;

namespace SAServices.Receivables
{
    public partial class InvoicingService : BaseService, IInvoicingService
    {
        private readonly IFinancialService _financialService;
        private readonly IInventoryService _inventoryService;

        private readonly IRepository<InvoicingOrderHeader> _invoicingOrderRepo;
        private readonly IRepository<InvoicingInvoiceHeader> _invoicingInvoiceRepo;
        private readonly IRepository<InvoicingReceiptHeader> _invoicingReceiptRepo;
        private readonly IRepository<Customer> _customerRepo;
        private readonly IRepository<SaAccount> _saAccountRepo;
        private readonly IRepository<Item> _itemRepo;
        private readonly IRepository<Measurement> _measurementRepo;
        private readonly IRepository<SequenceNumber> _sequenceNumberRepo;
        private readonly IRepository<PaymentTerm> _paymentTermRepo;
        private readonly IRepository<InvoicingDeliveryHeader> _invoicingDeliveryRepo;
        private readonly IRepository<Bank> _bankRepo;
        private readonly IRepository<GeneralLedgerSettings> _genetalLedgerSetting;
        private readonly IRepository<Contact> _contactRepo;

        public InvoicingService(IFinancialService financialService,
            IInventoryService inventoryService,
            IRepository<InvoicingOrderHeader> invoicingOrderRepo,
            IRepository<InvoicingInvoiceHeader> invoicingInvoiceRepo,
            IRepository<InvoicingReceiptHeader> invoicingReceiptRepo,
            IRepository<Customer> customerRepo,
            IRepository<SaAccount> saSaAccountRepo,
            IRepository<Item> itemRepo,
            IRepository<Measurement> measurementRepo,
            IRepository<SequenceNumber> sequenceNumberRepo,
            IRepository<PaymentTerm> paymentTermRepo,
            IRepository<InvoicingDeliveryHeader> invoicingDeliveryRepo,
            IRepository<Bank> bankRepo,
            IRepository<GeneralLedgerSettings> generalLedgerSettings,
            IRepository<Contact> contactRepo)
            : base(sequenceNumberRepo, customerRepo, generalLedgerSettings, paymentTermRepo, bankRepo)
        {
            _financialService = financialService;
            _inventoryService = inventoryService;
            _invoicingOrderRepo = invoicingOrderRepo;
            _invoicingInvoiceRepo = invoicingInvoiceRepo;
            _invoicingReceiptRepo = invoicingReceiptRepo;
            _customerRepo = customerRepo;
            _saAccountRepo = saSaAccountRepo;
            _itemRepo = itemRepo;
            _measurementRepo = measurementRepo;
            _sequenceNumberRepo = sequenceNumberRepo;
            _paymentTermRepo = paymentTermRepo;
            _invoicingDeliveryRepo = invoicingDeliveryRepo;
            _bankRepo = bankRepo;
            _genetalLedgerSetting = generalLedgerSettings;
            _contactRepo = contactRepo;
        }

        public void AddInvoicingOrder(InvoicingOrderHeader invoicingOrder, bool toSave)
        {
            if (string.IsNullOrEmpty(invoicingOrder.No))
                invoicingOrder.No = GetNextNumber(SequenceNumberTypes.InvoicingOrder).ToString();
            if(toSave)
                _invoicingOrderRepo.Insert(invoicingOrder);
        }

        public void UpdateInvoicingOrder(InvoicingOrderHeader invoicingOrder)
        {
            var persisted = _invoicingOrderRepo.GetById(invoicingOrder.Id);
            foreach (var persistedLine in persisted.InvoicingOrderLines)
            {
                bool found = false;
                foreach (var currentLine in invoicingOrder.InvoicingOrderLines)
                {
                    if (persistedLine.Id == currentLine.Id)
                    {
                        found = true;
                        break;
                    }
                }

                if (found)
                    continue;
                else
                {
                   
                }
            }
            _invoicingOrderRepo.Update(invoicingOrder);
        }

        public void AddInvoicingInvoice(InvoicingInvoiceHeader invoicingInvoice, long? invoicingDeliveryId)
        {   
            decimal taxAmount = 0, totalAmount = 0, totalDiscount = 0;

            var taxes = new List<KeyValuePair<long, decimal>>();
            var invoicing = new List<KeyValuePair<long, decimal>>();
            
            foreach (var lineItem in invoicingInvoice.InvoicingInvoiceLines)
            {
                var item = _itemRepo.GetById(lineItem.ItemId);

                var lineDiscountAmount = (lineItem.Discount / 100) * lineItem.Amount;
                totalDiscount += lineDiscountAmount;

                var lineAmount = lineItem.Amount - lineDiscountAmount;
                
                totalAmount += lineAmount;
                
                var lineTaxes = _financialService.ComputeOutputTax(item.Id, lineItem.Quantity, lineItem.Amount, lineItem.Discount);
                
                foreach (var t in lineTaxes)
                    taxes.Add(t);
                
                invoicing.Add(new KeyValuePair<long, decimal>(item.InvoicingSaAccountId.Value, lineAmount));

                if (item.ItemCategory.ItemType == ItemTypes.Purchased)
                {
                    lineItem.InventoryControlJournal = _inventoryService.CreateInventoryControlJournal(lineItem.ItemId,
                        lineItem.MeasurementId,
                        DocumentTypes.InvoicingInvoice,
                        null,
                        lineItem.Quantity,
                        lineItem.Quantity * item.Cost,
                        lineItem.Quantity * item.Price);
                }
            }

            var glHeader = _financialService.CreateGeneralLedgerHeader(DocumentTypes.InvoicingInvoice, invoicingInvoice.Date, string.Empty);
            var customer = _customerRepo.GetById(invoicingInvoice.CustomerId);
            totalAmount += invoicingInvoice.ShippingHandlingCharge;
            var debitCustomerAR = _financialService.CreateGeneralLedgerLine(TransactionTypes.Dr, customer.AccountsReceivableSaAccount.Id, Math.Round(totalAmount, 2, MidpointRounding.ToEven));
            glHeader.GeneralLedgerLines.Add(debitCustomerAR);

            var groupedInvoicingSaAccount = from s in invoicing
                                      group s by s.Key into grouped
                                      select new
                                      {
                                          Key = grouped.Key,
                                          Value = grouped.Sum(s => s.Value)
                                      };

            foreach (var invoicingSaAccount in groupedInvoicingSaAccount)
            {
                var invoicingAmount = (invoicingSaAccount.Value + totalDiscount) - taxAmount;
                var creditInvoicingSaAccount = _financialService.CreateGeneralLedgerLine(TransactionTypes.Cr, invoicingSaAccount.Key, Math.Round(invoicingAmount, 2, MidpointRounding.ToEven));
                glHeader.GeneralLedgerLines.Add(creditInvoicingSaAccount);
            }

            if (taxAmount > 0)
            {
                var groupedTaxes = from t in taxes
                                   group t by t.Key into grouped
                                   select new
                                   {
                                       Key = grouped.Key,
                                       Value = grouped.Sum(t => t.Value)
                                   };

                foreach (var tax in groupedTaxes)
                {
                    var tx = _financialService.GetTaxes().Where(t => t.Id == tax.Key).FirstOrDefault();
                    var creditInvoicingTaxSaAccount = _financialService.CreateGeneralLedgerLine(TransactionTypes.Cr, tx.InvoicingSaAccountId.Value, Math.Round(tax.Value, 2, MidpointRounding.ToEven));
                    glHeader.GeneralLedgerLines.Add(creditInvoicingTaxSaAccount);
                }
            }

            if (totalDiscount > 0)
            {
                var invoicingDiscountSaAccount = base.GetGeneralLedgerSettings().InvoicingDiscountSaAccount;
                var creditInvoicingDiscountSaAccount = _financialService.CreateGeneralLedgerLine(TransactionTypes.Dr, invoicingDiscountSaAccount.Id, Math.Round(totalDiscount, 2, MidpointRounding.ToEven));
                glHeader.GeneralLedgerLines.Add(creditInvoicingDiscountSaAccount);
            }

            if (invoicingInvoice.ShippingHandlingCharge > 0)
            {
                var shippingHandlingSaAccount = base.GetGeneralLedgerSettings().ShippingChargeSaAccount;
                var creditShippingHandlingSaAccount = _financialService.CreateGeneralLedgerLine(TransactionTypes.Cr, shippingHandlingSaAccount.Id, Math.Round(invoicingInvoice.ShippingHandlingCharge, 2, MidpointRounding.ToEven));
                glHeader.GeneralLedgerLines.Add(creditShippingHandlingSaAccount);
            }

            if (_financialService.ValidateGeneralLedgerEntry(glHeader).NoErrors)
            {
                invoicingInvoice.GeneralLedgerHeader = glHeader;

                invoicingInvoice.No = GetNextNumber(SequenceNumberTypes.InvoicingInvoice).ToString();

                if (!invoicingDeliveryId.HasValue)
                {
                    var invoicingDelivery = new InvoicingDeliveryHeader()
                    {
                        CustomerId = invoicingInvoice.CustomerId,
                        Date = invoicingInvoice.Date,
                        CreatedBy = invoicingInvoice.CreatedBy,
                        CreatedOn = DateTime.Now,
                        ModifiedBy = invoicingInvoice.ModifiedBy,
                        ModifiedOn = DateTime.Now,
                    };
                    foreach(var line in invoicingInvoice.InvoicingInvoiceLines)
                    {
                        var item = _itemRepo.GetById(line.ItemId);
                        invoicingDelivery.InvoicingDeliveryLines.Add(new InvoicingDeliveryLine()
                        {
                            ItemId = line.ItemId,
                            MeasurementId = line.MeasurementId,
                            Quantity = line.Quantity,
                            Discount = line.Discount,
                            Price = item.Cost.Value,
                            CreatedBy = invoicingInvoice.CreatedBy,
                            CreatedOn = DateTime.Now,
                            ModifiedBy = invoicingInvoice.ModifiedBy,
                            ModifiedOn = DateTime.Now,
                        });
                    }
                    AddInvoicingDelivery(invoicingDelivery, false);
                    invoicingInvoice.InvoicingDeliveryHeader = invoicingDelivery;
                }
                _invoicingInvoiceRepo.Insert(invoicingInvoice);
            }
        }

        public void AddInvoicingReceipt(InvoicingReceiptHeader invoicingReceipt)
        {
            var customer = _customerRepo.GetById(invoicingReceipt.CustomerId);
            var glHeader = _financialService.CreateGeneralLedgerHeader(DocumentTypes.InvoicingReceipt, invoicingReceipt.Date, string.Empty);
            var debit = _financialService.CreateGeneralLedgerLine(TransactionTypes.Dr, invoicingReceipt.SaAccountToDebitId.Value, invoicingReceipt.InvoicingReceiptLines.Sum(i => i.AmountPaid));
            var credit = _financialService.CreateGeneralLedgerLine(TransactionTypes.Cr, customer.AccountsReceivableSaAccountId.Value, invoicingReceipt.InvoicingReceiptLines.Sum(i => i.AmountPaid));
            glHeader.GeneralLedgerLines.Add(debit);
            glHeader.GeneralLedgerLines.Add(credit);

            if (_financialService.ValidateGeneralLedgerEntry(glHeader).NoErrors)
            {
                invoicingReceipt.GeneralLedgerHeader = glHeader;

                invoicingReceipt.No = GetNextNumber(SequenceNumberTypes.InvoicingReceipt).ToString();
                _invoicingReceiptRepo.Insert(invoicingReceipt);
            }
        }

        public void AddInvoicingReceiptNoInvoice(InvoicingReceiptHeader invoicingReceipt)
        {
            var customer = _customerRepo.GetById(invoicingReceipt.CustomerId);
            var glHeader = _financialService.CreateGeneralLedgerHeader(DocumentTypes.InvoicingReceipt, invoicingReceipt.Date, string.Empty);
            var debit = _financialService.CreateGeneralLedgerLine(TransactionTypes.Dr, invoicingReceipt.SaAccountToDebitId.Value, invoicingReceipt.Amount);
            glHeader.GeneralLedgerLines.Add(debit);

            foreach (var line in invoicingReceipt.InvoicingReceiptLines)
            {
                var credit = _financialService.CreateGeneralLedgerLine(TransactionTypes.Cr, line.SaAccountToCreditId.Value, line.AmountPaid);
                glHeader.GeneralLedgerLines.Add(credit);
            }

            if (_financialService.ValidateGeneralLedgerEntry(glHeader).NoErrors)
            {
                invoicingReceipt.GeneralLedgerHeader = glHeader;

                invoicingReceipt.No = GetNextNumber(SequenceNumberTypes.InvoicingReceipt).ToString();
                _invoicingReceiptRepo.Insert(invoicingReceipt);
            }
        }

        public IEnumerable<InvoicingInvoiceHeader> GetInvoicingInvoices()
        {
            var query = from invoice in _invoicingInvoiceRepo.Table
                        select invoice;
            return query.ToList();
        }

        public InvoicingInvoiceHeader GetInvoicingInvoiceById(long id)
        {
            return _invoicingInvoiceRepo.GetById(id);
        }

        public InvoicingInvoiceHeader GetInvoicingInvoiceByNo(string no)
        {
            var query = from invoice in _invoicingInvoiceRepo.Table
                        where invoice.No == no
                        select invoice;
            return query.FirstOrDefault();
        }

        public void UpdateInvoicingInvoice(InvoicingInvoiceHeader invoicingInvoice)
        {
            _invoicingInvoiceRepo.Update(invoicingInvoice);
        }

        public IEnumerable<InvoicingReceiptHeader> GetInvoicingReceipts()
        {
            var query = from receipt in _invoicingReceiptRepo.Table
                        select receipt;
            return query.ToList();
        }

        public InvoicingReceiptHeader GetInvoicingReceiptById(long id)
        {
            return _invoicingReceiptRepo.GetById(id);
        }

        public void UpdateInvoicingReceipt(InvoicingReceiptHeader invoicingReceipt)
        {
            _invoicingReceiptRepo.Update(invoicingReceipt);
        }

        public IEnumerable<Customer> GetCustomers()
        {
            var query = from customer in _customerRepo.Table
                        select customer;
            return query.ToList();
        }

        public Customer GetCustomerById(long id)
        {
            return _customerRepo.GetById(id);
        }

        public void UpdateCustomer(Customer customer)
        {
            _customerRepo.Update(customer);
        }

        public ICollection<InvoicingReceiptHeader> GetCustomerReceiptsForAllocation(long customerId)
        {
            var customerReceipts = _invoicingReceiptRepo.Table.Where(r => r.CustomerId == customerId);
            var customerReceiptsWithNoInvoice = new HashSet<InvoicingReceiptHeader>();
            foreach (var receipt in customerReceipts)
            {
                //if (receipt.InvoicingInvoiceHeaderId == null)
                //    customerReceiptsWithNoInvoice.Add(receipt);
                customerReceiptsWithNoInvoice.Add(receipt);
            }
            return customerReceiptsWithNoInvoice;
        }

        public void SaveCustomerAllocation(CustomerAllocation allocation)
        {
            //Revenue recognition. Debit the customer advances (liability) saSaAccount and credit the revenue saSaAccount.
            var invoice = _invoicingInvoiceRepo.GetById(allocation.InvoicingInvoiceHeaderId);
            var receipt = _invoicingReceiptRepo.GetById(allocation.InvoicingReceiptHeaderId);

            var glHeader = _financialService.CreateGeneralLedgerHeader(SACore.Domain.DocumentTypes.CustomerAllocation, allocation.Date, string.Empty);

            foreach (var line in receipt.InvoicingReceiptLines)
            {
                var debit = _financialService.CreateGeneralLedgerLine(SACore.Domain.TransactionTypes.Dr, line.SaAccountToCreditId.Value, allocation.Amount);
                glHeader.GeneralLedgerLines.Add(debit);
            }

            SaAccount saSaAccountToCredit = invoice.Customer.AccountsReceivableSaAccount;
            var credit = _financialService.CreateGeneralLedgerLine(SACore.Domain.TransactionTypes.Cr, saSaAccountToCredit.Id, allocation.Amount);
            glHeader.GeneralLedgerLines.Add(credit);

            if (_financialService.ValidateGeneralLedgerEntry(glHeader).NoErrors)
            {
                invoice.GeneralLedgerHeader = glHeader;
                invoice.CustomerAllocations.Add(allocation);
                _invoicingInvoiceRepo.Update(invoice);
            }
        }

        public void AddCustomer(Customer customer)
        {
            _customerRepo.Insert(customer);
        }

        public new IEnumerable<PaymentTerm> GetPaymentTerms()
        {
            return base.GetPaymentTerms();
        }

        public IEnumerable<InvoicingDeliveryHeader> GetInvoicingDeliveries()
        {
            var query = from f in _invoicingDeliveryRepo.Table
                        select f;
            return query;
        }

        public void AddInvoicingDelivery(InvoicingDeliveryHeader invoicingDelivery, bool toSave)
        {
            var glHeader = _financialService.CreateGeneralLedgerHeader(DocumentTypes.InvoicingDelivery, invoicingDelivery.Date, string.Empty);
            // Debit = COGS, Credit = Inventory
            var debitSaAccounts = new List<KeyValuePair<long, decimal>>();
            var creditSaAccounts = new List<KeyValuePair<long, decimal>>();
            foreach (var line in invoicingDelivery.InvoicingDeliveryLines)
            {
                var item = _inventoryService.GetItemById(line.ItemId.Value);
                debitSaAccounts.Add(new KeyValuePair<long, decimal>(item.CostOfGoodsSoldSaAccountId.Value, item.Cost.Value * line.Quantity));
                creditSaAccounts.Add(new KeyValuePair<long, decimal>(item.InventorySaAccountId.Value, item.Cost.Value * line.Quantity));
            }
            var groupedDebitSaAccounts = (from kvp in debitSaAccounts
                                        group kvp by kvp.Key into g
                                        select new KeyValuePair<long, decimal>(g.Key, g.Sum(e => e.Value)));
            var groupedCreditSaAccounts = (from kvp in creditSaAccounts
                                         group kvp by kvp.Key into g
                                         select new KeyValuePair<long, decimal>(g.Key, g.Sum(e => e.Value)));
            foreach (var saSaAccount in groupedDebitSaAccounts)
            {
                glHeader.GeneralLedgerLines.Add(_financialService.CreateGeneralLedgerLine(TransactionTypes.Dr, saSaAccount.Key, saSaAccount.Value));
            }
            foreach (var saSaAccount in groupedCreditSaAccounts)
            {
                glHeader.GeneralLedgerLines.Add(_financialService.CreateGeneralLedgerLine(TransactionTypes.Cr, saSaAccount.Key, saSaAccount.Value));
            }

            if (_financialService.ValidateGeneralLedgerEntry(glHeader).NoErrors)
            {
                invoicingDelivery.GeneralLedgerHeader = glHeader;

                invoicingDelivery.No = GetNextNumber(SequenceNumberTypes.InvoicingDelivery).ToString();

                if(!invoicingDelivery.InvoicingOrderHeaderId.HasValue)
                {
                    var invoicingOrder = new InvoicingOrderHeader()
                    {
                        CustomerId = invoicingDelivery.CustomerId,
                        PaymentTermId = invoicingDelivery.PaymentTermId,
                        Date = invoicingDelivery.Date,
                        No = GetNextNumber(SequenceNumberTypes.InvoicingOrder).ToString(),
                        CreatedBy = invoicingDelivery.CreatedBy,
                        CreatedOn = DateTime.Now,
                        ModifiedBy = invoicingDelivery.ModifiedBy,
                        ModifiedOn = DateTime.Now,
                    };

                    foreach(var line in invoicingDelivery.InvoicingDeliveryLines)
                    {
                        var item = _inventoryService.GetItemById(line.ItemId.Value);
                        invoicingOrder.InvoicingOrderLines.Add(new InvoicingOrderLine()
                        {
                            ItemId = item.Id,
                            MeasurementId = line.MeasurementId.Value,
                            Quantity = line.Quantity,
                            Amount = item.Price.Value,
                            CreatedBy = invoicingDelivery.CreatedBy,
                            CreatedOn = DateTime.Now,
                            ModifiedBy = invoicingDelivery.ModifiedBy,
                            ModifiedOn = DateTime.Now,
                        });
                    }
                    AddInvoicingOrder(invoicingOrder, false);
                    invoicingDelivery.InvoicingOrderHeader = invoicingOrder;
                }

                if (toSave)
                    _invoicingDeliveryRepo.Insert(invoicingDelivery);
            }
        }

        public IEnumerable<InvoicingOrderHeader> GetInvoicingOrders()
        {
            var query = from f in _invoicingOrderRepo.Table
                        select f;
            return query;
        }

        public InvoicingOrderHeader GetInvoicingOrderById(long id)
        {
            return _invoicingOrderRepo.GetById(id);
        }

        public InvoicingDeliveryHeader GetInvoicingDeliveryById(long id)
        {
            return _invoicingDeliveryRepo.GetById(id);
        }

        public IEnumerable<Contact> GetContacts()
        {
            var query = from f in _contactRepo.Table
                        select f;
            return query;
        }

        public long SaveContact(Contact contact)
        {
            _contactRepo.Insert(contact);
            return contact.Id;
        }

        public ICollection<InvoicingInvoiceHeader> GetInvoicingInvoicesByCustomerId(long customerId, InvoicingInvoiceStatus status)
        {
            var query = from invoice in _invoicingInvoiceRepo.Table
                        where invoice.CustomerId == customerId && invoice.Status == status
                        select invoice;
            return query.ToList();
        }

        public ICollection<CustomerAllocation> GetCustomerAllocations(long customerId)
        {
            return null;
        }
    }
}
