﻿using System;
using System.Collections.Generic;
using SACore.Domain;
using SACore.Domain.Financials;
using SACore.Domain.MasterTables;


namespace SAServices.MasterTable
{
    public partial interface IMasterTableService
    {
        IEnumerable<SaAccount> GetSaAccounts();

        IEnumerable<Customer> GetCustomers();
        IEnumerable<Contact> GetContacts();
        IEnumerable<Party> GetParties();
        IEnumerable<PaymentTerm> GetPaymentTerms();
        IEnumerable<SaAccount> GetInvoicingDiscountSaAccounts();
        IEnumerable<SaAccount> GetInvoicingSaAccounts();
        IEnumerable<SaAccount> GetInvoicingPenaltySaAccounts();
        IEnumerable<SaAccount> GetReceivableSaAccounts();
        LegalEntity GetCurrentLegalEntity();
        IEnumerable<Bank> GetBanks();

        IEnumerable<TaxGroup> GetCustomerTaxGroups();
        IEnumerable<Vendor> GetVendors();
        Vendor GetVendorById(long id);
        void AddVendor(Vendor vendor);
        void UpdateVendor(Vendor vendor);
        Customer GetCustomerById(long id);
        bool DeleteCustomerById(Customer customer);
        void UpdateCustomer(Customer customer);
        void AddCustomer(Customer customer);
        IEnumerable<Contact> GetContactsByType(PartyTypes p);
        long SaveContact(Contact contact);

        IEnumerable<Tax> GetTaxes();
        IEnumerable<ItemTaxGroup> GetItemTaxGroups();

     
        GeneralLedgerSettings GetGeneralLedgerSettings(long? companyId = null);


        Contact GetContactById(long p);
    }
}
